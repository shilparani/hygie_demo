"""
WSGI config for hygie project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

import os
import sys

# add the hellodjango project path into the sys.path
#sys.path.append('/var/www/html/hygie')

# add the virtualenv site-packages path to the sys.path
#sys.path.append('/root/.pyenv/versions/3.6.5/envs/venv_hygie_django_dev/lib/python3.6/site-packages')

#Enable this for PRD set_up
sys.path.append('/var/www/html/hygie')
sys.path.append('/var/www/html/hygie/hygie')


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'hygie.settings')

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
