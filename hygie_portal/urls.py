from django.urls import path, re_path
from django.views.generic.base import RedirectView
from django.views.generic import TemplateView
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth import views as auth_views
from hygie_portal.forms import LoginForm
from . import views

app_name = "hygie_portal"

urlpatterns = [
    #path(r'login/', auth_views.LoginView.as_view(authentication_form=LoginForm), name='login'),
    #path("", LoginView.as_view(template_name='public/login.html'), name='login'),
    path('login/', views.login_request, name='login'),
    path('', RedirectView.as_view(url='/login/')),
    
    path('profile/', views.profile, name='profile'),
    
    #path('home/', TemplateView.as_view(template_name='users/home_public.html'), name='home_public'),
    
    path('ticket_search/', views.ticket_search, name='ticket_search'),
    
    path('home/', views.home, name='home'),
    path('logout/', views.logout_request, name='logout'),
    
    path('change_view/<str:view_type>/', views.change_view , name='change_view'),
    
    path('members/', views.members, name='members'),
    path('holidays/', views.holidays, name='holidays'),
    
    path('dashboard/', views.dashboard, name='dashboard'),
    path('get_team_trends/', views.get_team_trends, name='get_team_trends'),
    path('classic_dashboard_load_group/', views.classic_dashboard_load_group, name='classic_dashboard_load_group'),
    path('classic_dashboard_state_save/', views.classic_dashboard_state_save, name='classic_dashboard_state_save'),
    
    path('custom_dashboard/', views.custom_dashboard, name='custom_dashboard'),
    path('save_settings/', views.save_settings, name='save_settings'),
    
    path('tickets/open/team/<str:team>/', views.team , name='team'),
    path('tickets_notes_json/', views.tickets_notes_json, name='tickets_notes_json'),
    
    
    path('security/<str:security_type>/', views.security , name='security'),
    path('security_details/<str:security_type>/', views.security_details , name='security_details'),
    #path('security_search/<str:security_type>/', views.security_search , name='security_search'),
    
    
    path('patch_followup', views.patch_followup, name='patch_followup'),
    path('patch_management', views.patch_management, name='patch_management'),
    path('patch_save', views.patch_save, name='patch_save'),
    path('patch_delete', views.patch_delete, name='patch_delete'),
    #path('patch_search/<str:security_type>/', views.patch_search , name='patch_search'),
    #path('patch_edit/<str:security_type>/', views.patch_edit , name='patch_edit'),
    #path('patch_customsearch/<str:security_type>/', views.patch_customsearch , name='patch_customsearch'),
    path('patch_detail_search/<str:security_type>/', views.patch_detail_search , name='patch_detail_search'),
    path('patch_device_info/<str:security_type>/', views.patch_device_info , name='patch_device_info'),
    path('tickets_notes_json', views.tickets_notes_json, name='tickets_notes_json'),
    
    path('discrepancy/<str:discrepancy_type>/', views.discrepancy , name='discrepancy'),
    path('except_discrepancy/<str:discrepancy_type>/', views.except_discrepancy , name='except_discrepancy'),
    path('discrepancy/ticketstatus', views.ticket_status, name='ticket_status'),
    
    path('ostype/<str:server_type>/', views.ostype , name='ostype'),
    
    path('cmdb/', views.cmdb, name='cmdb'),
    path('cmdb_json/', views.cmdb_json, name='cmdb_json'),    
    path('oneretail_json/', views.oneretail_json, name='oneretail_json'),  
    path('exclusion_json/', views.exclusion_json, name='exclusion_json'),
    path('oneretail_exclusion_json/', views.oneretail_exclusion_json, name='oneretail_exclusion_json'),
    path('oneretail_delete_json/', views.oneretail_delete_json, name='oneretail_delete_json'),
    path('delete_json/', views.delete_json, name='delete_json'),
    path('device_information/', views.device_information, name='device_information'),
    path('oneretail_device_information/', views.oneretail_device_information, name='oneretail_device_information'),
    
    path('quality/', views.quality, name='quality'),
    path('save_rca/', views.save_rca, name='save_rca'),
    path('edit_rcareport_quality/', views.edit_rcareport_quality, name='edit_rcareport_quality'),
    path('delete_rca/', views.delete_rca, name='delete_rca'),
    
    path('reporting_esurvey/', views.reporting_esurvey, name='reporting_esurvey'),
    path('get_esurvey_details/', views.get_esurvey_details, name='get_esurvey_details'),
    
    path('closed_tickets/', views.closed_tickets, name='closed_tickets'),
    path('reporting/topten/', views.reporting_topten, name='reporting_topten'),
    path('save_topten/', views.save_topten, name='save_topten'),
    
    path('reporting/tsc_calendar/', views.reporting_tsccalendar, name='reporting_tsccalendar'),
    path('reporting/nest_calendar/', views.reporting_nestcalendar, name='reporting_nestcalendar'),
    
    path('reporting/ticket_stats/', views.reporting_ticket_stats, name='reporting_ticket_stats'),
    path('reporting/<str:ticket_analysis_type>/', views.reporting_ticket_analysis , name='reporting_ticket_analysis'),
    
    path('reporting/integration_view/<str:view_mode>/', views.reporting_integration_view , name='reporting_integration_view'),

    path('hypervision_oneretail/', views.hypervision_oneretail, name='hypervision_oneretail'),
    
    path('control_plan_global/', views.control_plan_global, name='control_plan_global'),
    path('control_plan_ko/', views.control_plan_ko, name='control_plan_ko'),
    path('control_plan_domain/', views.control_plan_domain, name='control_plan_domain'),
    path('control_plan_businessflag/', views.control_plan_businessflag, name='control_plan_businessflag'),
    
    path('admintools/control_plan_management/', views.control_plan_management, name='control_plan_management'),
    
    path('admintools/query_management/', views.query_management, name='query_management'),
    path('admintools/query_edit/', views.query_edit, name='query_edit'),
    path('admintools/query_save/', views.query_save, name='query_save'),
    path('admintools/query_delete/', views.query_delete, name='query_delete'),
    
    
    path('admintools/content_management/', views.content_management, name='content_management'),
    path('admintools/content_save/', views.content_save, name='content_save'),
    path('admintools/content_edit/', views.content_edit, name='content_edit'),
    path('admintools/content_delete/', views.content_delete, name='content_delete'),
]