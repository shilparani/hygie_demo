# -*- coding: utf-8 -*-
"""Helper utilities and decorators."""

from datetime import timedelta
from dateutil import tz
from math import floor
from collections import defaultdict,OrderedDict
from hygie_portal.models import Rca
from django.conf import settings
import datetime
import pytz
import operator
import json
import ast
import os

def get_all_rca():
    all_rca_details = Rca.objects.all()
    
    raw_rca_list = [r.__dict__ for r in all_rca_details]
    
    now = datetime.datetime.now()
    
    # Calculating Graph Data
    insla_count, soosla_count, oosla_count = 0,0,0
    inslaDurationCalculation = 3
    dayToday = datetime.datetime.today().weekday()
    if 1 < dayToday < 5: 
        inslaDurationCalculation = 5
    if dayToday == 5:
        inslaDurationCalculation = 4

    group_data = {}
    
    rca_list = {}
    rca_sla_dic = []
    rca_group_dic = []
    
    for rcaitem in raw_rca_list:
        del rcaitem['_state']
        rcaitem['lastupdated'] = rcaitem['lastupdated'].strftime('%Y-%m-%d %H:%M:%S') # to remove timezone from datetime "tzinfo=<UTC>"

        decoded_rcaitem = json.loads(rcaitem['content'])
        
        decoded_rcaitem['in_sla'] = datetime.datetime.strptime(decoded_rcaitem['dueDate'], '%Y-%m-%d %H:%M') > now
        
        rcaitem['content'] = json.dumps(decoded_rcaitem)
        
        rca_list[rcaitem['rcaid']] = rcaitem
        
        # only use open rca data for charts
        if rcaitem['status'] != "Sent":
            # By SLA data
            if (datetime.datetime.strptime(decoded_rcaitem['dueDate'], '%Y-%m-%d %H:%M') < now):
                oosla_count += 1
            elif (datetime.datetime.strptime(decoded_rcaitem['startDate'], '%Y-%m-%d %H:%M') + datetime.timedelta(days=inslaDurationCalculation)) > now:
                insla_count += 1
            else:
                soosla_count += 1
     
            # By Group Data
            if decoded_rcaitem['group'].lower() not in group_data:
                group_data[decoded_rcaitem['group'].lower()] = 0
            group_data[decoded_rcaitem['group'].lower()] += 1
        
    for group in group_data:
        rca_group_dic.append({"label":group.upper(), "y":group_data[group], "name":group.upper()})

    rca_sla_dic.append({"label":"OUT OF SLA (" + str(oosla_count) + " RCA)", "y":oosla_count, "color": settings.COLOR_SCHEME['OSLA'], "name": "OUT OF SLA"})
    rca_sla_dic.append({"label":"IN SLA (" + str(insla_count) + " RCA)", "y":insla_count, "color": settings.COLOR_SCHEME['SLA'], "name": "SLA"})
    rca_sla_dic.append({"label":"SOON OUT OF SLA (" + str(soosla_count) + " RCA)", "y":soosla_count, "color": settings.COLOR_SCHEME['SOOSLA'], "name" : "SOON OUT OF SLA"})

    
    return {'rca_list' : rca_list, 'rca_sla_chart' : rca_sla_dic, 'rca_group_chart' : rca_group_dic}

'''
def load_rca():
    print "============== load rca"
    project_upload_path = current_app.config.get('UPLOAD_FOLDER')
    quality_source_json = os.path.join(project_upload_path, current_app.config.get('RCA'))
    closed_quality_source_json = os.path.join(project_upload_path, current_app.config.get('RCA_CLOSED'))
    rca_report_source_json = os.path.join(project_upload_path, current_app.config.get('RCA_REPORT'))
    
    quality_list_dict = {}
    
    with open(rca_report_source_json,"r") as rca_report_source_file:
        rca_report_source_list = json.load(rca_report_source_file)
    
    print "========== rca report"
    print rca_report_source_list
    
    with open(quality_source_json,"r") as quality_source_file:
        quality_source_list = json.load(quality_source_file)
    open_quality_list = [item for item in quality_source_list.values()]
    
    with open(closed_quality_source_json,"r") as closed_quality_source_file:
        closed_quality_source_list = json.load(closed_quality_source_file)
    closed_quality_list = [item for item in closed_quality_source_list.values()]
    
    quality_list = open_quality_list + closed_quality_list
    
    print quality_list
    default_keys_rca = ['rcaId','week','zone','outageTitle','rcaStatus','startDate','endDate','dueDate','incidentStartDate','incidentEndDate','incidentDuration','rcaOwner','problemId','im','group','comments']
    default_keys_report = ['ticketNumber','rcaId','problemId','startDate','endDate','preparedBy','contributor','problemDescription','businessImpact','rootCause','problemDetection','serviceRestoration','preventiveAction','actions']
    default_keys_action = ['action', 'owner', 'targetDate', 'completed']
    
    for q in quality_list:
        temp = dict(zip(default_keys_rca, q))
        
        report = dict(zip(default_keys_report, rca_report_source_list[temp['rcaId']])) if temp['rcaId'] in rca_report_source_list else {}
        report['actions'] = [dict(zip(default_keys_action, act)) for act in report['actions']] if 'actions' in report else []
        
        if 'startDate' in report:
            report['startDate'] = temp['incidentStartDate']
        if 'endDate' in report:
            report['endDate'] = temp['incidentEndDate']
        
        id = temp['rcaId'].split('_')
        
        updated_id = temp['rcaId'] + '_01' if len(id) == 2 else id[0] + '_' + id[1] + '_0' + id[2]
        
        temp['rcaId'] = updated_id
        
        quality_list_dict[temp['rcaId']] = {
            'rcaid' : temp['rcaId'],
            'content' : json.dumps(temp),
            'report' : json.dumps(report),
            'status' : temp['rcaStatus'],
            'lastupdated' : datetime.datetime.now()
        }
        
        #ok = save_rca(quality_list_dict[temp['rcaId']])
        ok = save_rca(temp['rcaId'], json.dumps(temp), json.dumps(report), temp['rcaStatus'])
        if ok:
            print 'saved id :: ', temp['rcaId']
        else:
            print 'save failed id :: ', temp['rcaId']
    #print quality_list_dict
    
    
    
    return quality_list_dict
'''
def save_rca(rca_id, rca_content, rca_report, rca_status):
    print("rca_id :: %s"%rca_id)
    print("rca_content :: %s"%rca_content)
    print("rca_report :: %s"%rca_report)
    print("rca_status :: %s"%rca_status)
    
    # check if rca id exists from rca
    rca_details = Rca.objects.filter(rcaid = rca_id).first()
    print(rca_details)
    if rca_details:
        '''updated_settings = {}
        updated_settings['rcaid'] = rca_details.rcaid
        updated_settings['content'] = rca_content
        updated_settings['report'] = rca_report
        updated_settings['status'] = rca_status
        Rca.update_rca(rca_details, updated_settings)'''
        
        Rca.objects.filter(rcaid = rca_details.rcaid).update(content = rca_content, report = rca_report, status = rca_status, lastupdated = datetime.datetime.now())
        
    else:
        #create new settings
        print("new rca details")
        #rcaid=rcaid, content=content, report=report, status=status, lastupdated=lastupdated
        new_rca = Rca(rcaid = rca_id, content = rca_content, report = rca_report, status = rca_status, lastupdated = datetime.datetime.now())
        new_rca.save()
        #Rca.add_rca(new_rca)
    
    return True    

def delete_rca(rca_id):
    
    # check if rca id exists from rca
    rca_details = Rca.objects.filter(rcaid = rca_id).first()
    print(rca_details)
    if rca_details:
        Rca.delete(rca_details)
    
    return True

def format_report(report):
    formatted_report = {
        'actions' : []
    }
    actions = {}
    for k,v in report.iteritems():
        if k == 'actions': continue
        if 'action' in k:
            print(k)
            id = k.split('-')
            if id[1] not in actions:
                actions[id[1]] = {}
                
            if id[2] == 'actionContent':
                actions[id[1]]['action'] = v
            elif id[2] == 'actionOwner':
                actions[id[1]]['owner'] = v
            elif id[2] == 'actionTargetCheckbox':
                actions[id[1]]['completed'] = 1
            elif id[2] == 'actionTargetDate':
                actions[id[1]]['targetDate'] = v
            else:
                continue
        else:
            formatted_report[k] = v
    
    
    formatted_report['actions'] = [v for k,v in actions.items()]
        
    print(formatted_report)
    return formatted_report
    