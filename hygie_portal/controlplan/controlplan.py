# -*- coding: utf-8 -*-
"""Helper utilities and decorators."""

from datetime import timedelta
from dateutil import tz
from math import floor
from collections import defaultdict,OrderedDict
from hygie.settings import logger
import datetime
import pytz
import operator
import json
import ast
#import pyodbc
import MySQLdb


CONTROL_LIST = [
    {
        'category' : 'Processes Compliance',
        'domain' : 'Account Management',
        'control_id' : 'ACC0001',
        'business_flag' : '',
        'control_description' : {
            'name' : 'ACC-Illegitimate access to domain accounts',
            'description' : 'List and count of new added members into Active Directory for "Domain_Admins" group with identification of the accountable of these additions.',
            'team_accountable' : 'L4_Windows'
        },
        'control_processing' : {
            'frequency' : 'Weekly'
        },
        'control_data' : {},
        'data_management' : {},
        'control_valuation' : {},
        'comments' : '',
        'last_updated' : '',
        'table_name' : 'Domain_Admins',
        'function_name' : 'ad_validation',
        'template_function' : 'populateAdminValidationTemplate' 
    },
    {
        'category' : 'Processes Compliance',
        'domain' : 'Account Management',
        'control_id' : 'ACC0002',
        'business_flag' : '',
        'control_description' : {
            'name' : 'ACC-Illegitimate access to autobot server accounts',
            'description' : 'List and count of new added members into Active Directory for "Appl_Autobot_Srv" group with identification of the accountable of these additions.',
            'team_accountable' : 'L4_Windows'
        },
        'control_processing' : {
            'frequency' : 'Weekly'
        },
        'control_data' : {},
        'data_management' : {},
        'control_valuation' : {},
        'comments' : '',
        'last_updated' : '',
        'table_name' : 'Autobot_Srv',
        'function_name' : 'ad_validation',
        'template_function' : 'populateAdminValidationTemplate' 
    },
    {
        'category' : 'Processes Compliance',
        'domain' : 'Account Management',
        'control_id' : 'ACC0003',
        'business_flag' : '',
        'control_description' : {
            'name' : 'ACC-Illegitimate access to Server Admins accounts',
            'description' : 'List and count of new added members into Active Directory for "Server_Admins" group with identification of the accountable of these additions.',
            'team_accountable' : 'L4_Windows'
        },
        'control_processing' : {
            'frequency' : 'Weekly'
        },
        'control_data' : {},
        'data_management' : {},
        'control_valuation' : {},
        'comments' : '',
        'last_updated' : '',
        'table_name' : 'Server_Admins',
        'function_name' : 'ad_validation',
        'template_function' : 'populateAdminValidationTemplate' 
    },
    {
        'category' : 'Processes Compliance',
        'domain' : 'Account Management',
        'control_id' : 'ACC0004',
        'business_flag' : '',
        'control_description' : {
            'name' : 'ACC-Illegitimate access to O365 User Management accounts',
            'description' : 'List and count of new added members into Active Directory for "Appl_o365_Users_Management_-_Full_Access" group with identification of the accountable of these additions.',
            'team_accountable' : 'L4_Windows'
        },
        'control_processing' : {
            'frequency' : 'Weekly'
        },
        'control_data' : {},
        'data_management' : {},
        'control_valuation' : {},
        'comments' : '',
        'last_updated' : '',
        'table_name' : 'Appl_o365_UM_-_Full_Access',
        'function_name' : 'ad_validation',
        'template_function' : 'populateAdminValidationTemplate' 
    },
    {
        'category' : 'Processes Compliance',
        'domain' : 'Account Management',
        'control_id' : 'ACC0005',
        'business_flag' : '',
        'control_description' : {
            'name' : 'ACC-Illegitimate access to TSC Portal Security Admin accounts',
            'description' : 'List and count of new added members into Active Directory for "Appl_TSC_Portl_Secu_Adm" group with identification of the accountable of these additions.',
            'team_accountable' : 'L4_Windows'
        },
        'control_processing' : {
            'frequency' : 'Weekly'
        },
        'control_data' : {},
        'data_management' : {},
        'control_valuation' : {},
        'comments' : '',
        'last_updated' : '',
        'table_name' : 'Appl_TSC_Portl_Secu_Adm',
        'function_name' : 'ad_validation',
        'template_function' : 'populateAdminValidationTemplate'
    },
    {
        'category' : 'IT Safeness',
        'domain' : 'Server',
        'control_id' : 'SEC0007',
        'business_flag' : '',
        'control_description' : {
            'name' : 'SRV-SSL Certificate Validity',
            'description' : 'List of SSL certificates expiring < 30 days',
            'team_accountable' : 'L4 Server'
        },
        'control_processing' : {
            'frequency' : 'Weekly'
        },
        'control_data' : {},
        'data_management' : {},
        'control_valuation' : {},
        'comments' : '',
        'last_updated' : '',
        'table_name' : '',
        'function_name' : 'cert_validation',
        'template_function' : 'populateCertValidationTemplate'
    }
]

def get_control_data(mode):
    
    control_data = {}
    
    for control in CONTROL_LIST:
        if control['function_name'] == 'ad_validation':
            data = ad_validation(control)
        elif control['function_name'] == 'cert_validation':
            data = cert_validation(control)
            
        flag_list = control['business_flag'].split('|')
        data['flag_list'] = flag_list
        
        #format for global mode
        if mode == "global":
            if control['category'] not in control_data:
                control_data[control['category']] = {
                    'domainList' : {},
                    'name' : control['category'],
                    'total_count' : 0,
                    'ok_count' : 0
                }
                
            if control['domain'] not in control_data[control['category']]['domainList']:
                control_data[control['category']]['domainList'][control['domain']] = {
                    'controlList' : {},
                    'name' : control['domain'],
                    'total_count' : 0,
                    'ok_count' : 0,
                    'category' : control['category']
                }
                
            #populate data for the control
            control_data[control['category']]['total_count'] += 1
            control_data[control['category']]['domainList'][control['domain']]['total_count'] += 1
            if data['status'] == 'OK':
                control_data[control['category']]['ok_count'] += 1
                control_data[control['category']]['domainList'][control['domain']]['ok_count'] += 1
            
                
            control_data[control['category']]['domainList'][control['domain']]['controlList'][control['control_id']] = data
        
        #format for ko mode
        if mode == "ko" and data['status'] == "KO":
            control_data[control['control_id']] = data
            
        #format for domain mode
        if mode == "domain":
            if control['domain'] not in control_data:
                control_data[control['domain']] = {
                    'controlList' : {},
                    'name' : control['domain'],
                    'total_count' : 0,
                    'ok_count' : 0,
                    'category' : control['category']
                }
            
            #populate data for the control
            control_data[control['domain']]['total_count'] += 1
            if data['status'] == 'OK':
                control_data[control['domain']]['ok_count'] += 1
            
            control_data[control['domain']]['controlList'][control['control_id']] = data
        
        #TODO: format for business flag mode
        if mode == "business_flag":
            
            for flag in flag_list:
                flag = flag if flag else 'unknown'
                if flag not in control_data:
                    control_data[flag] = {
                        'controlList' : {},
                        'name' : flag,
                        'total_count' : 0,
                        'ok_count' : 0
                    }
                    
                control_data[flag]['total_count'] += 1
                if data['status'] == 'OK':
                    control_data[flag]['ok_count'] += 1
                
                data['current_business_flag'] = flag
                
                control_data[flag]['controlList'][control['control_id']] = data
    
    #import pdb;pdb.set_trace()    
    return control_data

def ad_validation(control):
    
    result = {}
    
    ad_group = control['table_name']
    
    conn_str = """
    select acc.CN as CN, la.creation_date, la.[action] from (
    select CN from [01_AD_SQLOPS_D1-{0}] where creation_date >= (select max(creation_date) from [01_AD_SQLOPS_D1-{0}]) and CN NOT IN (select va.CN from [01_AD_SQLOPS_D1-{0}_Validated] as va)
    UNION
    select va.CN from [01_AD_SQLOPS_D1-{0}_Validated] as va where CN NOT IN (select ca.CN from [01_AD_SQLOPS_D1-{0}] as ca where creation_date >= (select max(creation_date) from [01_AD_SQLOPS_D1-{0}]))
    ) as acc
    left join [01_AD_SQLOPS_D1-{0}_Logs] as la on la.CN = acc.CN 
    """.format(ad_group)
    
    validated_account_conn_str = "select CN, validation_date from `01_AD_SQLOPS_D1-{0}_Validated`;".format(ad_group)
    #validated_account_conn_str = "select CN from [01_AD_SQLOPS_D1-{0}_Validated]".format(ad_group)
    
    current_account_conn_str = "select CN from `01_AD_SQLOPS_D1-{0}` where creation_date = (select max(creation_date) from `01_AD_SQLOPS_D1-{0}`);".format(ad_group)
    
    logs_conn_str = "select * from `01_AD_SQLOPS_D1-{0}_Logs` order by creation_date desc  limit 5;".format(ad_group)
    
    db_conn = _get_connection()
    
    # check DB connection, if none return
    if db_conn is None:
        return result
    
    
    cursor = db_conn.cursor()
    logger.info('========================= VALIDATED ACC CONN STR :: %s'%validated_account_conn_str)
    cursor.execute(validated_account_conn_str)
    validated_account_data = cursor.fetchall()
    
    cursor.execute(current_account_conn_str)
    current_account_data = cursor.fetchall()
    
    cursor.execute(logs_conn_str)
    logs_data = cursor.fetchall()
    
    _close_connection(db_conn) #close DB connection
    
    log_table_keys = ['creation_date', 'cn', 'action']
    
    current_account_data_list = [r[0] for r in current_account_data]
    
    validated_account_data_list = [r[0] for r in validated_account_data]
    
    validated_account_date_list = [r[1] for r in validated_account_data]
    
    log_data_list = [dict(zip(log_table_keys, r)) for r in logs_data]

    for data in log_data_list:
        data['creation_date'] = str(data['creation_date'])
       
    #validated_account_max_date = max([datetime.datetime.strptime(date, '%Y-%m-%d').date() for date in validated_account_date_list])
    validated_account_max_date = max(str(date) for date in validated_account_date_list)
    
    ko_count = len(set(validated_account_data_list) - set(current_account_data_list)) + len(set(current_account_data_list) - set(validated_account_data_list))
    
    
    result['category'] = control['category']
    result['control_id'] = control['control_id']
    result['domain'] = control['domain']
    result['name'] = control['control_description']['name']
    result['status'] = 'KO' if ko_count > 0 else 'OK'
    result['statusColor'] = 'red' if ko_count > 0 else 'green'
    result['validatedList'] = validated_account_data_list
    result['currentList'] = current_account_data_list
    result['logList'] = log_data_list
    #result['lastUpdated'] =  validated_account_max_date.strftime("%Y-%m-%d")
    result['lastUpdated'] =  validated_account_max_date
    result['business_flag'] = control['business_flag']
    result['frequency'] = control['control_processing']['frequency']
    result['description'] = control['control_description']['description']
    result['template_function'] = control['template_function']
    
    return result

def cert_validation(control):
    
    result = {}
    
    ad_group = control['table_name']
    
    conn_str = """
    select * from `02_CERTS_SQLOPS_Cert_verification` as cv
    inner join `02_CERTS_SQLOPS_Cert_urls` as c on cv.url = c.url
    order by certexpirationin desc
    """.format(ad_group)
    
    db_conn = _get_connection()
    
    # check DB connection, if none return
    if db_conn is None:
        return result
    
    cursor = db_conn.cursor()
    cursor.execute(conn_str)
    cert_data = cursor.fetchall()
    
    _close_connection(db_conn) #close DB connection
    
    default_keys = ['check_date', 'url', 'cert_name', 'expiration_date', 'expiration_in_days', 'cert_serial_number', 'cert_effective_date', 'cert_issuer', 'status']
    
    cert_list = [dict(zip(default_keys, r)) for r in cert_data]

    #validated_account_max_date = max([datetime.datetime.strptime(cert['check_date'], '%Y-%m-%d').date() for cert in cert_list])
    validated_account_max_date = max(str(cert['check_date']) for cert in cert_list) if cert_list else ''
    
    
    unknownList, lt100List, lt250List, gt250List = [], [], [], []
    
    for cert in cert_list:
        if cert['status'] != "OK":
            unknownList.append(cert)
            continue
        
        if int(cert['expiration_in_days']) < 100:
            lt100List.append(cert)
        
        elif int(cert['expiration_in_days']) < 250:
            lt250List.append(cert)
            
        else:
            gt250List.append(cert)
    
    result['category'] = control['category']
    result['control_id'] = control['control_id']
    result['domain'] = control['domain']
    result['name'] = control['control_description']['name']
    result['status'] = 'KO' if len(unknownList) > 0 or len(lt100List) > 0 else 'OK'
    result['statusColor'] = 'red' if len(unknownList) > 0 or len(lt100List) > 0 else 'green'
    result['unknownList'] = unknownList
    result['lt100List'] = lt100List
    result['lt250List'] = lt250List
    result['gt250List'] = gt250List
    result['lastUpdated'] =  validated_account_max_date #.strftime("%Y-%m-%d")
    result['business_flag'] = control['business_flag']
    result['frequency'] = control['control_processing']['frequency']
    result['description'] = control['control_description']['description']
    result['template_function'] = control['template_function']
    
    return result


def _get_connection():
    try:
        #conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format('NLVCP1786P','1529','MSOPER1PRD','SQL_OPER_PRD','Ln7u9Mm!'))
        conn = MySQLdb.connect(user='root', passwd='Neurones@2018', host='10.236.0.3', db='hygie_controlplan', charset='utf8')
        return conn
    except MySQLdb.Error as err:
        print('Error connecting to landesk DB:', err)
        return None

def _close_connection(conn):
    conn.close()
    