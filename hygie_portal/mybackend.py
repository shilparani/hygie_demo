#from hygie_portal.models import CustomUser
from django.conf import settings
from hygie.settings import logger
#import ldap
import sys
from django.contrib.auth.backends import ModelBackend

from django.contrib.auth import get_user_model  #equivalent to from hygie_portal.models import CustomUser

CustomUser = get_user_model()

def get_bind_user(user):
    """ This method returns the bind user string for the user"""
    user_dn = settings.AD_DN
    login_attr = '(%s=%s)' % (settings.AD_LOGIN_ATTR, user)
    attr_search = settings.AD_ATTR_SEARCH
    
    ad_server = settings.AD_URL
    logger.info('+=+=+=AD Server : %s' % ad_server)
    
    conn = ldap.initialize(settings.AD_URL)
    conn.set_option(ldap.OPT_REFERRALS,0)
    conn.set_option(ldap.OPT_PROTOCOL_VERSION, settings.LDAP_PROTOCOL_VERSION)

    try:
        conn.bind(settings.AD_USER, settings.AD_PASSWORD)
        conn.result()
    except:
        exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
        logger.warning('User:%s bind error' % user)
        return []


    try:
        result = conn.search_s(user_dn,
                               ldap.SCOPE_SUBTREE,
                               login_attr, attr_search)
    except:
        exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
        clogger.warning('User:%s is invalid' % user)
        return []
    
    logger.info('+=+=+=USER DN : %s' % user_dn)

    # Check Groups
    try:
        cn_user = result[0][1]['cn'][0]
    except:
        logger.error('User:%s is invalid' % user)
        return []

    logger.info('+=+=+= USER CN : %s' % cn_user)

    is_tsc_user, is_tsc_manager, is_fo_user, is_emea_user, is_itl4_user, is_security_user, is_qual_user, is_topten_adm = '','','','','','','',''
    is_ams_user, is_proxius_user, is_proxigba_user, is_proxiblt_user, is_proxifdt_user, is_proximng_user, is_network_user = '','','','','','',''

    cn_user = cn_user.decode()
    # check if tsc user, if found proceed, if not return []
    try:
        # this returns the groups!
        user_group = conn.search_s(user_dn, ldap.SCOPE_SUBTREE, settings.ORD_GRP_DN, ['cn',])
        #logger.info('user_group:%s'%user_group)
        or_group = [mg[0] for mg in user_group]

        if any(cn_user in o for o in or_group):
            is_tsc_user = True
            logger.info('TSC user:%s' % user)
            result[0][1]['role'] = 'tsc_user'
    except:
        exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
        # Exit the script and print an error telling what happened.
        logger.info("%s: Not found in TSC User Group" % user)

    # Check if SERVICE_DESK user
    try:
        # this returns the groups!
        fo_user_group = conn.search_s(user_dn, ldap.SCOPE_SUBTREE, settings.FO_GRP_DN, ['cn',])
        fo_group = [fg[0] for fg in fo_user_group]

        if any(cn_user in service_desk for service_desk in fo_group):
            is_fo_user = True
            logger.info('Front Office user:%s' % user)
            try:
                result[0][1]['role'] = result[0][1]['role'] + '|' +'fo_user'
            except KeyError:
                result[0][1]['role'] = 'fo_user'

    except:
        exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
        # Exit the script and print an error telling what happened.
        logger.info("%s: Not found in SERVICE_DESK User Group" % user)
        logger.error(exceptionValue)

    # Check if EMEA  user
    try:
        # this returns the groups!
        emea_user_group = conn.search_s(user_dn, ldap.SCOPE_SUBTREE, settings.EMEA_GRP_DN, ['cn',])
        #current_app.logger.info("EMEA USER Group:%s" % emea_user_group)
        emea_group = [eug[0] for eug in emea_user_group]

        if any(cn_user in emea for emea in emea_group):
            is_emea_user = True
            logger.info('EMEA user:%s' % user)
            try:
                result[0][1]['role'] = result[0][1]['role'] + '|' +'proxi_emea'
            except KeyError:
                result[0][1]['role'] = 'proxi_emea'

    except:
        exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
        # Exit the script and print an error telling what happened.
        logger.info("%s: Not found in EMEA User Group" % user)
        logger.error(exceptionValue)

    # Check if Proximity Americas user
    try:
        # this returns the groups!
        proxius_user_group = conn.search_s(user_dn, ldap.SCOPE_SUBTREE, settings.PROXIUS_GRP_DN, ['cn',])
        #logger.info("ProxiUS USER Group:%s" % proxius_user_group)
        
        proxius_group = [pug[0] for pug in proxius_user_group]
        del proxius_group[-1] # to remove None in the user list

        if any(cn_user in proxius for proxius in proxius_group):
            is_proxius_user = True
            logger.info('PROXI US user:%s' % user)
            try:
                result[0][1]['role'] = result[0][1]['role'] + '|' +'proxi_us'
            except KeyError:
                result[0][1]['role'] = 'proxi_us'

    except:
        exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
        # Exit the script and print an error telling what happened.
        logger.info("%s: Not found in PROXI US User Group" % user)
        logger.error(exceptionValue)

    # Check if Proximity Global Asie user
    try:
        # this returns the groups!
        proxigba_user_group = conn.search_s(user_dn, ldap.SCOPE_SUBTREE, settings.PROXIGBA_GRP_DN, ['cn',])
        proxigba_group = [pug[0] for pug in proxigba_user_group]
        del proxigba_group[-1] # to remove None in the user list

        if any(cn_user in proxigba for proxigba in proxigba_group):
            is_proxigba_user = True
            logger.info('PROXI Global Asie user:%s' % user)
            try:
                result[0][1]['role'] = result[0][1]['role'] + '|' +'proxi_gba'
            except KeyError:
                result[0][1]['role'] = 'proxi_gba'

    except:
        exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
        # Exit the script and print an error telling what happened.
        logger.info("%s: Not found in PROXI Global Asie User Group" % user)
        logger.error(exceptionValue)

    # Check if Proximity Global Asie user
    try:
        # this returns the groups!
        proxiblt_user_group = conn.search_s(user_dn, ldap.SCOPE_SUBTREE, settings.PROXIBLT_GRP_DN, ['cn',])
        proxiblt_group = [pug[0] for pug in proxiblt_user_group]
        del proxiblt_group[-1] # to remove None in the user list

        if any(cn_user in proxiblt for proxiblt in proxiblt_group):
            is_proxiblt_user = True
            logger.info('PROXI Berluti user:%s' % user)
            try:
                result[0][1]['role'] = result[0][1]['role'] + '|' +'proxi_blt'
            except KeyError:
                result[0][1]['role'] = 'proxi_blt'

    except:
        exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
        # Exit the script and print an error telling what happened.
        logger.info("%s: Not found in PROXI Berluti User Group" % user)
        logger.error(exceptionValue)

    # Check if Proximity Fondation user
    try:
        # this returns the groups!
        proxifdt_user_group = conn.search_s(user_dn, ldap.SCOPE_SUBTREE, settings.PROXIFDT_GRP_DN, ['cn',])
        proxifdt_group = [pug[0] for pug in proxifdt_user_group]
        del proxifdt_group[-1] # to remove None in the user list

        if any(cn_user in proxifdt for proxifdt in proxifdt_group):
            is_proxifdt_user = True
            logger.info('PROXI Fondation user:%s' % user)
            try:
                result[0][1]['role'] = result[0][1]['role'] + '|' +'proxi_fdt'
            except KeyError:
                result[0][1]['role'] = 'proxi_fdt'

    except:
        exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
        # Exit the script and print an error telling what happened.
        logger.info("%s: Not found in PROXI Fondation User Group" % user)
        logger.error(exceptionValue)

    # Check if Proximity Manager user
    try:
        # this returns the groups!
        proximng_user_group = conn.search_s(user_dn, ldap.SCOPE_SUBTREE, settings.PROXIMNG_GRP_DN, ['cn',])
        proximng_group = [pug[0] for pug in proximng_user_group]

        if any(cn_user in proximng for proximng in proximng_group):
            is_proximng_user = True
            logger.info('PROXI Manager user:%s' % user)
            try:
                result[0][1]['role'] = result[0][1]['role'] + '|' +'proxi_mng'
            except KeyError:
                result[0][1]['role'] = 'proxi_mng'

    except:
        exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
        # Exit the script and print an error telling what happened.
        logger.info("%s: Not found in PROXI Manager User Group" % user)
        logger.error(exceptionValue)

    # Check if ITL4 user
    try:
        # this returns the groups!
        itl4_user_group = conn.search_s(user_dn, ldap.SCOPE_SUBTREE, settings.ITL4_GRP_DN, ['cn',])
        #current_app.logger.info("ITL4 USER Group:%s" % itl4_user_group)
        itl4_group = [eug[0] for eug in itl4_user_group]

        if any(cn_user in itl4 for itl4 in itl4_group):
            is_itl4_user = True
            logger.info('ITL4 user:%s' % user)
            try:
                result[0][1]['role'] = result[0][1]['role'] + '|' +'itl4_user'
            except KeyError:
                result[0][1]['role'] = 'itl4_user'

    except:
        exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
        # Exit the script and print an error telling what happened.
        logger.info("%s: Not found in ITL4 User Group" % user)
        logger.error(exceptionValue)

    # Check if AMS user
    try:
        # this returns the groups!
        ams_user_group = conn.search_s(user_dn, ldap.SCOPE_SUBTREE, settings.AMS_GRP_DN, ['cn',])
        #current_app.logger.info("AMS USER Group:%s" % ams_user_group)
        ams_group = [eug[0] for eug in ams_user_group]

        if any(cn_user in ams for ams in ams_group):
            is_ams_user = True
            logger.info('AMS user:%s' % user)
            try:
                result[0][1]['role'] = result[0][1]['role'] + '|' +'ams_user'
            except KeyError:
                result[0][1]['role'] = 'ams_user'

    except:
        exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
        # Exit the script and print an error telling what happened.
        logger.info("%s: Not found in AMS User Group" % user)
        logger.error(exceptionValue)

    # Check if Security user
    try:
        # this returns the groups!
        security_user_group = conn.search_s(user_dn, ldap.SCOPE_SUBTREE, settings.SECU_GRP_DN, ['cn',])
        security_group = [secug[0] for secug in security_user_group]

        if any(cn_user in secu for secu in security_group):
            is_security_user = True
            logger.info('Security user:%s' % user)
            try:
                result[0][1]['role'] = result[0][1]['role'] + '|' +'security_user'
            except KeyError:
                result[0][1]['role'] = 'security_user'

    except:
        exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
        # Exit the script and print an error telling what happened.
        logger.info("%s: Not found in Security User Group" % user)
        logger.error(exceptionValue)

    # check if manager
    try:
        # this returns the groups!
        tsc_manager_group = conn.search_s(user_dn, ldap.SCOPE_SUBTREE, settings.MGR_GRP_DN, ['cn',])
        mg_group = [mg[0] for mg in tsc_manager_group]

        if any(cn_user in m for m in mg_group):
            is_tsc_manager = True
            logger.info('%s is a Manager' % cn_user )
            try:
                result[0][1]['role'] = result[0][1]['role'] + '|' +'tsc_manager'
            except KeyError:
                result[0][1]['role'] = 'tsc_manager'

    except:
        exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
        # Exit the script and print an error telling what happened.
        logger.info("Not found in TSC Manager Group")
        logger.error(exceptionValue)
    
    
    # Check if Quality user
    try:
        # this returns the groups!
        qual_user_group = conn.search_s(user_dn, ldap.SCOPE_SUBTREE, settings.QUAL_GRP_DN, ['cn',])
        qual_group = [fg[0] for fg in qual_user_group]
        if any(cn_user in qual for qual in qual_group):
            is_qual_user = True
            logger.info('Quality user:%s' % user)
            try:
                result[0][1]['role'] = result[0][1]['role'] + '|' +'qual_user'
            except KeyError:
                result[0][1]['role'] = 'qual_user'

    except:
        exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
        # Exit the script and print an error telling what happened.
        logger.info("%s: Not found in qual User Group" % user)
        logger.error(exceptionValue)

    # Check if Top 10 Admin
    try:
        # this returns the groups!
        topten_adm_user_group = conn.search_s(user_dn, ldap.SCOPE_SUBTREE, settings.TOP_TEN_ADM_GRP_DN, ['cn',])
        topten_adm_group = [fg[0] for fg in topten_adm_user_group]
        if any(cn_user in topten for topten in topten_adm_group):
            is_topten_adm = True
            logger.info('Topten admin user:%s' % user)
            try:
                result[0][1]['role'] = result[0][1]['role'] + '|' +'topten_adm'
            except KeyError:
                result[0][1]['role'] = 'topten_adm'

    except:
        exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
        # Exit the script and print an error telling what happened.
        logger.info("%s: Not found in Topten Adm Group" % user)
        logger.error(exceptionValue)
        
    # Check if Network 
    try:
        # this returns the groups!
        network_user_group = conn.search_s(user_dn, ldap.SCOPE_SUBTREE, settings.NETWORK_GRP_DN, ['cn',])
        network_group = [net[0] for net in network_user_group]
        if any(cn_user in network for network in network_group):
            is_network_user = True
            logger.info('Network user:%s' % user)
            try:
                result[0][1]['role'] = result[0][1]['role'] + '|' +'network_user'
            except KeyError:
                result[0][1]['role'] = 'network_user'

    except:
        exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
        # Exit the script and print an error telling what happened.
        logger.info("%s: Not found in Network Group" % user)
        logger.error(exceptionValue)
    
    if not is_tsc_user and not is_tsc_manager and not is_fo_user and not is_emea_user and not is_itl4_user and not is_security_user and not is_proxius_user and not is_proxigba_user and not is_proxiblt_user and not is_proxifdt_user and not is_proximng_user and not is_qual_user and not is_topten_adm and not is_ams_user and not is_network_user: 
        result[0][1]['role'] = ''

   # if not is_tsc_user and not is_tsc_manager and not is_fo_user and not is_emea_user and not is_itl4_user and not is_security_user and is_proxius_user and is_proxigba_user and is_proxiblt_user and is_proxifdt_user and is_proximng_user:
   #     return []    
    
    # Return the user's entry from AD, which includes
    # their 'distinguished name'
    # we use this to authenticate the credentials the
    # user has entered in the form
    return result[0][1]

class MyBackEnd(object):
    """
    This is the custom backend to authenticate the user in the DB.
    if this authentication fails then django default authentication  will get called
    """

    def authenticate(username, password, request=None):
        if username == 'admin' and password == 'admin':
            info = {'name':'admin', 'email':'admin@neurones-it.com', 'phone':'12345678', 'role':'tsc_user|itl4_user|security_user|tsc_manager|qual_user|topten_adm'} #|network_user
            return info
        else:
            return None
    
        '''bind_attr = settings.AD_BIND_ATTR
        user_dn = settings.AD_DN
        login_attr = '(%s=%s)' % (settings.AD_LOGIN_ATTR, username)

        data = get_bind_user(username)

        if len(data) == 0:
            return None

        # Information we want to return from the directory
        # for each user, season to taste.

        info = {}

        # dummy email if not found
        if 'mail' not in data:
            logger.warning('No email found in AD, adding dummy email')
 
        info['name'] = data['cn'][0] if 'cn' in data else None
        info['email'] = data['mail'][0] if 'mail' in data else 'nomail@louisvuitton.com' 
        try:
            info['phone'] = data['telephoneNumber'][0]
        except KeyError:
            info['phone'] = 'Not Available'

        try:
            info['role'] = data['role']
        except KeyError:
            info['role'] = False

        conn = ldap.initialize(settings.AD_URL)
        conn.set_option(ldap.OPT_REFERRALS,0)
        conn.set_option(ldap.OPT_PROTOCOL_VERSION, settings.LDAP_PROTOCOL_VERSION)

        try:
            # Now we have the "bind attribute" (LDAP username) for our user
            # we try and connect to see if LDAP will authenticate
            conn.bind_s(data[bind_attr][0].decode(), password)
            conn.search(user_dn, ldap.SCOPE_SUBTREE, login_attr, None)
            conn.result()
            return info
        except (ldap.INVALID_CREDENTIALS, ldap.OPERATIONS_ERROR):
            logger.info('Invalid credentials for :%s' % username)
            return None'''
            
    def get_user(self, user_id):
        try:
            return CustomUser.objects.get(pk=user_id)
        except CustomUser.DoesNotExist:
            return None
 
 
'''
# code for get_user
    def get_user(self, user_id):
        try:
            user = UserModel.objects.get(sys_id=user_id)
            if user.is_active:
                return user
            return None
        except UserModel.DoesNotExist:
            logging.getLogger("error_logger").error("user with %(user_id)d not found")
            return None
'''            