from django.shortcuts import render, redirect
from django.contrib import messages
from django.conf import settings #settings file in project
from django.http import HttpResponse
from hygie_portal.forms import LoginForm, CustomUserCreationForm
from hygie_portal.mybackend import MyBackEnd
from django.contrib.auth import login, logout
from django.contrib.auth import get_user_model  #equivalent to from hygie_portal.models import CustomUser
from hygie.settings import logger
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.template import Context, loader, RequestContext
from django.contrib.admin.widgets import AdminDateWidget
from django.core.files.storage import FileSystemStorage
from wtforms import Form, SubmitField, SelectField, DateField, DateTimeField, StringField, IntegerField, TextAreaField, SelectMultipleField, BooleanField, widgets, validators, FormField, FieldList
from django import forms
from django.http import JsonResponse
from django.core import serializers
from django.utils.safestring import SafeString
from django.utils.http import is_safe_url
from werkzeug.utils import secure_filename
from icalendar import Calendar
from hygie_portal.utils import make_tree
from hygie_portal.models import Patchcampaigns, DiscrepancyWorkstation, DiscrepancyServer, Holidays
from hygie_portal.tickets import open_tickets as ot
from hygie_portal.tickets import common as cmt
from hygie_portal.tickets import team_trends as tt
from hygie_portal.tickets import closed_tickets as ct
from hygie_portal.dashboard import classicdashboard as classicdash
from hygie_portal.dashboard import customdashboard as cd
from hygie_portal.securities import security as secus
from hygie_portal.discrepancy import ticketstatus as ticketstatus
from hygie_portal.cmdb import cmdb_report as cmdbrpt
from hygie_portal.quality import rca as rca
from hygie_portal.reporting import esurvey as esurvey
from hygie_portal.reporting import topten as topten
from hygie_portal.reporting import changecalendar as changecalendar
from hygie_portal.reporting import ticketstats as tstats
from hygie_portal.reporting import ticket_analysis as tanalysis
from hygie_portal.reporting import integration_view as intview
from hygie_portal.querymanager import querymanager as qm
from hygie_portal.contentmanager import contentmanager as cm
from hygie_portal.controlplan import controlplan as cp
from collections import OrderedDict
import calendar, re
import datetime
import MySQLdb
import os
import sys
import ast
import json
import collections

CustomUser = get_user_model() #<class 'hygie_portal.models.CustomUser'

class MultiCheckboxField(SelectMultipleField):
    widget = widgets.ListWidget(prefix_label=False)
    option_widget = widgets.CheckboxInput()
    
class GeneralForm(forms.Form):
    #start_date = DateField('Start at', widget=DatePickerWidget())
    #end_date = DateField('End at', widget=DatePickerWidget())
    ticket_cat= StringField(u'Ticket Category')
    ticket_priority= StringField(u'Ticket Priority')
    start_date = StringField(u'Start Date')
    end_date = StringField(u'End Date')
    open_check_tickets = MultiCheckboxField(u'Open Check Tickets')
    close_check_tickets = MultiCheckboxField(u'Close Check Tickets')
    bounce_check = MultiCheckboxField(u'Bouncing Tickets')
    open_search_date = StringField(u'Open Search Date')
    close_search_date = StringField(u'Close Search Date')
    groups_by_opengroup=SelectMultipleField(u'Choose Open Groups')
    groups_by_closedgroup=SelectMultipleField(u'Choose Closed Groups')
    groups_by_assignedgroup= SelectMultipleField(u'Choose Assigned Groups')
    groups_by_applications= SelectMultipleField(u'Choose Applications')
    sla_selection = StringField(u'All SLA')
    selected_month_year= StringField(u'Start Year')
    
    search_date = StringField(u'Search Date')
    ticket_stats_group = SelectMultipleField(u'Choose Groups')
    groups_by_sourcegroup=SelectMultipleField(u'Choose Source Groups')
    groups_by_targetgroup=SelectMultipleField(u'Choose Target Groups')
    #start_month=MultiCheckboxField(u'Label')
    submit = SubmitField('Submit')
    
class PatchForm(forms.Form):
    filterName = StringField(u'Filter Name', validators=[validators.InputRequired()])
    patchReleaseDate = DateField(u'Patch Release Date', widget=AdminDateWidget(),format='%m/%d/%Y', validators=[validators.InputRequired()])
    patchList = TextAreaField(u'Patch List',validators=[validators.InputRequired()])
    targetList = TextAreaField(u'Target List') 
    
class RCAForm(forms.Form):
    rcaId = StringField(u'RCA ID', validators=[validators.DataRequired()])
    week = StringField(u'Week', validators=[validators.InputRequired()])
    zone = SelectField(u'Zone',validators=[validators.InputRequired()], choices=[('Worldwide', 'Worldwide'), ('America', 'America'), ('Europe', 'Europe'), ('Asia', 'Asia')])
    outageTitle = TextAreaField(u'Outage Title',validators=[validators.InputRequired()])
    rcaStatus = SelectField(u'RCA Status', validators=[validators.InputRequired()], choices=[('Initiated', 'Initiated'), ('Waiting for owner', 'Waiting for owner'), ('Waiting for validation', 'Waiting for validation'), ('Sent', 'Sent')])
    startDate = DateTimeField(u'RCA Start Date', format='%Y-%m-%d %H:%M', validators=[validators.InputRequired()])
    endDate = DateTimeField(u'RCA End Date', format='%Y-%m-%d %H:%M', validators=[validators.Optional()])
    dueDate = DateTimeField(u'RCA Due Date', format='%Y-%m-%d %H:%M', validators=[validators.InputRequired()])
    incidentStartDate = DateTimeField(u'Incident Start Date',format='%Y-%m-%d %H:%M', validators=[validators.InputRequired()])
    incidentEndDate = DateTimeField(u'Incident End Date', format='%Y-%m-%d %H:%M', validators=[validators.InputRequired()])
    incidentDuration = StringField(u'Incident Duration', validators=[validators.InputRequired()])
    rcaOwner = StringField(u'RCA Owner',validators=[validators.InputRequired()])
    problemId = StringField(u'Problem Id', default="N/A", validators=[validators.InputRequired()])
    im = SelectField(u'IM',validators=[validators.InputRequired()], choices=[('FMM', 'FMM'),('FNE','FNE')])
    group = SelectField(u'Group',validators=[validators.InputRequired()], choices=[('AS400', 'AS400'), ('BI', 'BI'), ('BS', 'BS'), ('DB', 'DB'),('End User Computing', 'End User Computing'), ('External', 'External'), ('Middleware', 'Middleware'), ('Network', 'Network'), ('Unix', 'Unix'), ('WebApp', 'WebApp'), ('Windows', 'Windows')])
    comments = TextAreaField(u'Comments',validators=[validators.Optional()])
    submit = SubmitField('Submit')    
    
class ActionForm(Form):
    #actionNum = IntegerField(u'Action #',validators=[validators.InputRequired()])
    actionContent = TextAreaField(u'Action',validators=[validators.InputRequired()])
    actionOwner = StringField(u'Owner',validators=[validators.InputRequired()])
    actionTargetDate = DateField(u'Target Date', format='%Y-%m-%d' ,validators=[validators.InputRequired()])
    actionTargetCheckbox = BooleanField(u'Done', false_values='false', validators=[validators.DataRequired()])

class RCAReportForm(Form):
    ticketNumber = StringField(u'Ticket Number', validators=[validators.InputRequired()])
    rcaId = StringField(u'RCA ID', validators=[validators.InputRequired()])
    problemId = StringField(u'Problem ID', validators=[validators.InputRequired()])
    reportStartDate = DateTimeField(u'Start Date', validators=[validators.InputRequired()])
    reportEndDate = DateTimeField(u'End Date', validators=[validators.InputRequired()])
    preparedBy = StringField(u'Prepared By',validators=[validators.InputRequired()])
    contributors = StringField(u'Contributor(s)',validators=[validators.InputRequired()])
    description = TextAreaField(u'1 Problem Abstract/Event Description',validators=[validators.InputRequired()])
    impact = TextAreaField(u'2 Business Impact',validators=[validators.InputRequired()])
    rootcause= TextAreaField(u'3 Root Causes',validators=[validators.InputRequired()])
    detection = TextAreaField(u'4 Detection of the problem', validators=[validators.InputRequired()])
    restore = TextAreaField(u'5 Service restoration', validators=[validators.InputRequired()])
    preventiveAction = TextAreaField(u'6 Preventive Action', validators=[validators.InputRequired()])
    actions = FieldList(FormField(ActionForm))
    #actionNum = IntegerField(u'Action #',validators=[validators.InputRequired()])
    #actionContent = StringField(u'Action',validators=[validators.InputRequired()])
    #actionOwner = StringField(u'Owner',validators=[validators.InputRequired()])
    #actionTargetDate = DateTimeField(u'Target Date',validators=[validators.InputRequired()])

class ProblemForm(Form):
    zone = SelectField(u'Zone', validators=[validators.InputRequired()], choices=[('Worldwide', 'Worldwide'), ('America', 'America'), ('Europe', 'Europe'), ('Asia', 'Asia')])
    problemNumber = IntegerField(u'Problem Number', validators=[validators.InputRequired()])
    title = StringField(u'Title',validators=[validators.InputRequired()])
    openDate = DateField(u'Open Date', validators=[validators.InputRequired()])
    status = SelectField(u'Status', validators=[validators.InputRequired()], choices=[('Opened', 'Opened'), ('Resolved', 'Resolved')])
    businessImpact = TextAreaField(u'Business Impact',validators=[validators.InputRequired()])
    rootCause = TextAreaField(u'Root Cause', validators=[validators.InputRequired()])
    pbOwner = StringField(u'Pb Owner', validators=[validators.InputRequired()])
    nextTargetDate = DateTimeField(u'Next Target Date', validators=[validators.InputRequired()])
    lastUpdateDate = DateTimeField(u'Last Update Date', validators=[validators.InputRequired()])
    group = SelectField(u'Group',validators=[validators.InputRequired()], choices=[('Windows', 'Windows'), ('AS400', 'AS400'), ('Network', 'Network'), ('DB', 'DB'), ('BI', 'BI'), ('Unix', 'Unix'), ('WebApp', 'WebApp')])
    lastUpdate = TextAreaField(u'Last Update',validators=[validators.InputRequired()])    

    
def encode(text):
    """
    For printing unicode characters to the console.
    """
    text_encode = text.encode('utf-8')
    return text_encode.decode()    
    
def change_view(request,view_type=None):
    """
    Change the view to sla/priority and assign to 
    session 'view' and stay on same page
    """
    request.session['view'] = view_type
    return redirect(request.META.get('HTTP_REFERER', '/')) #redirects to the current url
    
def logout_request(request):
    logout(request)
    messages.info(request, 'Logged out successfully!')
    return redirect('hygie_portal:login')    

def login_request(request):
    if request.user.is_authenticated:
        return redirect('hygie_portal:home')
        
    if request.method == 'POST':
        form = LoginForm(data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            local_tz = request.GET.get('local_tz', None)
        
        user_info = MyBackEnd.authenticate(encode(username), encode(password), request=request)
        
        if not user_info:
            messages.error(request,'Invalid username or password. Please try again.')
            return redirect('hygie_portal:login')
            
        logger.info('Role:%s' %  user_info['role'])
        
        is_admin = True if 'tsc_manager' in user_info['role'] else False
        
        # if not AD/LDAP problem, get user from DB.
        try:
            user_profile = CustomUser.objects.get(username=username)
        except:
            user_profile = ''

        if not user_profile:
            if 'name' in user_info:
                try:
                    fname = user_info['name'].split()[0]
                except IndexError:
                    fname = ''
                try:
                    sname = user_info['name'].split()[1]
                except IndexError:
                    sname = ''

            else:
                fname, sname = '', ''  
            user_profile = CustomUser(username=username, email=user_info['email'], first_name=fname, last_name=sname, role=user_info['role'])
            user_profile.save()
            
        try:
            logger.info('Updating User:%s' % username)
            user_profile.set_is_admin(is_admin)
            user_profile.set_role(user_info['role'])
            user_profile.save()
        except Exception as e:
            logger.error('Exception updating user****' % e)
            logger.error('Error updating User:%s' % username)
           
        login(request, user_profile)
        #login(request, user_profile, backend='hygie_portal.mybackend.MyBackEnd')
        #request.session.set_expiry(300)
        logger.info('You are now logged in as : %s' % username)
        logger.info('Login OK for %s' % username)
        
        context= {'form': form}
        
        url = request.POST.get('next')
        if url and is_safe_url(url, request.get_host()):
            return redirect(url)
        else: 
            return redirect('hygie_portal:home')
            #return HttpResponseRedirect('hygie_portal:home')
            #return render(request, 'users/home_public.html',{'form':form})
            #return render(request, 'users/home_public.html', context=RequestContext(request)) #TypeError: context must be a dict rather than RequestContext.
            # redirect to home and check there for @login_required

    form = LoginForm()
    #messages.info(request, 'Please login!')     
    if form.errors:
        messages.error(form.errors, 'danger')
        flash(form.errors, 'danger')

    # default to login page if not authenticated or no form submit.
    return render(request, 'public/login.html',{'form': form})
 
@csrf_protect  
def home(request):  
    #import pdb;pdb.set_trace()  
    if request.user.is_authenticated:
        content_list = cm.get_content_details_by_module('home_public')
        return render(request, 'users/home_public.html', {'content_list' : content_list}) #redirect to home_public.html
    else:
        #next_redirect_url = request.GET.get('next', None)
        return redirect('hygie_portal:login')  
    
def _get_users(user=None):
    """ This method returns the list of all the users"""
    if not user:
        return CustomUser.objects.all()

    else:
        import pdb; pdb.set_trace()
        return CustomUser.objects.filter(username=user)
 
@login_required() 
def members(request):
    """List members."""
    members = _get_users()
    return render(request, 'users/members.html' , {'members' : members})

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in settings.ALLOWED_EXTENSIONS    
    
@login_required() 
@csrf_protect     
def holidays(request):
    """
    Upload holiday ics files. Data from: https://www.mozilla.org/en-US/projects/calendar/holidays/
    """
    form = request.GET

    # load holidays db
    holidays_data = Holidays.objects.all()

    filename = None
    region = 'fra'
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request._files.dict().keys():
            flash('No file part')
            return redirect(request.get_full_path())
        file = request.FILES['file'] #request._files.dict()['file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.name == '':
            flash('No selected file')
            return redirect(request.get_full_path())
        if file and allowed_file(file.name):

            # create upload dir if not exists
            project_upload_path = settings.UPLOAD_FOLDER
            if not os.path.exists(project_upload_path):
                os.makedirs(project_upload_path)

            # save ics first
            filename = secure_filename(file.name)
            fs = FileSystemStorage(location=project_upload_path)
            fs.save(file.name, file)
            #file.save(os.path.join(project_upload_path, filename))

            # udpate/insert data from ics
            cal = Calendar.from_ical(open(os.path.join(project_upload_path, filename),'rb').read())
            for e in cal.walk('vevent'):
                summary = e.decoded('summary').decode()
                uid = e.decoded('uid').decode()
                start_date = e[ 'dtstart'].dt
                end_date = e['dtend'].dt

                holidays = Holidays.objects.filter(uid=uid, start_date=start_date, end_date=end_date, region=region).first()
                if not holidays:
                    holidays = Holidays(uid, summary, start_date, end_date, region)
                    holidays.save()
                    #db.session.add(holidays)
                    #db.session.commit()

            return redirect('hygie_portal:holidays') 
            #return redirect(url_for('admin.holidays',filename=filename))

    project_upload_path = settings.UPLOAD_FOLDER                                       
    dir_tree = make_tree(project_upload_path)

    return render(request, 'users/holidays.html', {'form' : form, 'dir_tree' : dir_tree, 'holidays_data' : holidays_data})   

@login_required
def profile(request):
    """Profile page."""
    local_tz = request.GET.get('local_tz', None)
    #tsc_team = None if tsc_team == 'all' else tsc_teama

    criticality_all = settings.TICKET_CRITICALITY['ALL']

#    _tickets = ot.get_backlogs_data(None, local_tz, None, criticality_all, 'maniha')
    _tickets = ot.get_backlogs_data(request, None, local_tz, None, criticality_all, request.user.username)
    tickets = _tickets['isla']+_tickets['oosla']+_tickets['soosla']
 
    return render(request, 'users/profile.html' , {'user' : request.user, 'tickets' : tickets})
    
def team(request, team=None, ticket_type=None):
    """ Team/Open Tickets """

    #import pdb;pdb.set_trace()
    if "[" in team:
        team = eval(team)      

    if 'view' not in request.session:
        request.session['view'] = 'sla'

    local_tz = request.GET.get('local_tz', None)
    #import pdb;pdb.set_trace()

    criticality_all = settings.TICKET_CRITICALITY['ALL']
    criticality_high = settings.TICKET_CRITICALITY['HIGH']


    combine_teams = settings.TEAMS
    logger.info([x for x in combine_teams ])

    #keys = ['all_tsc', 'all_itl4', 'all_sd', 'all_proximity_ds', 'all_emea', 'all_america_ds', 'all_global_asie_ds', 'all_berluti_ds', 'all_fondation_ds' , 'all_ams', 'all_ams_cgi', 'all_ams_valtech']
    keys = settings.ALL_TEAMS_MAPPING.keys()
    
    for key, value in combine_teams.items():
        if isinstance(team, list):
            for sub_team in team:
                for v in value:
                    if sub_team in v['team']:
                        team_name= v['name']
        elif team in keys:
             team_name = 'All'

    team_backlogs_list = {}
    team_backlogs_count = []

    if request.session['view'] == 'sla':
        #team_backlogs_list = ot.get_backlogs_data(request, team, local_tz, ticket_type, criticality_all)
        team_backlogs_list = ot.get_backlogs_data(request, team, local_tz, ticket_type, criticality_all, None, 'itsm')
        #logger.info("team_backlogs_list_itsm:*******************%s***************"%team_backlogs_list_itsm)
        team_backlogs_list_temp = dict()
        for key,value in team_backlogs_list.items():
             if key == 'GRP':
                team_backlogs_list_temp[key] = value if team_backlogs_list.get(key,[]) == '' else team_backlogs_list.get(key,[])
             else:
                team_backlogs_list_temp[key] = value + team_backlogs_list.get(key,[])
        #team_backlogs_list = team_backlogs_list_temp
        logger.info("team_backlogs_list:*******************%s***************"%team_backlogs_list)
        #team_backlogs_count = ot.get_backlogs_sla_summary_groups(request, team, ticket_type)
        team_backlogs_count = ot.get_backlogs_sla_summary_groups(request, team, ticket_type, 'itsm')
        logger.info("team_backlogs_count:*******************%s***************"%(team_backlogs_count))
        #logger.info("team_backlogs_count_itsm:*******************%s***************"%team_backlogs_count_itsm)
        #team_backlogs_count = cmt._merge_deis_and_itsm_data(team_backlogs_count, team_backlogs_count_itsm)
        #logger.info("team_backlogs_count:*******************%s"%(team_backlogs_count))
    else:
        #team_backlogs_count = ot.get_backlogs_prio_summary_groups(request, team, ticket_type)
        team_backlogs_count = ot.get_backlogs_prio_summary_groups(request, team, ticket_type, None, 'itsm')
        logger.info("*******************%s***************"%team_backlogs_count)
        #logger.info("*******************%s***************"%team_backlogs_count_itsm)
        logger.info("*******************%s***************"%team_name)
        '''if team_name=="All":
            team_backlogs_count = cmt._merge_deis_and_itsm_data(team_backlogs_count, team_backlogs_count_itsm)
            for tbc,tbci in zip(team_backlogs_count,team_backlogs_count_itsm):
                for tbc_e, tbci_e in zip(tbc.values()[0], tbci.values()[0]):
                    tbc_e['y'] = tbc_e['y'] + tbci_e['y']
        else:
		    #team_backlogs_count = cmt._merge_deis_and_itsm_team_count_data(team_backlogs_count,team_backlogs_count_itsm)
            team_backlogs_count = cmt._merge_deis_and_itsm_data(team_backlogs_count,team_backlogs_count_itsm)
        logger.info("*******************%s***************"%team_backlogs_count)
        team_backlogs_deis = {
          'HIGH' : ot.get_backlogs_prio_data(request, team, local_tz, ticket_type, settings.TICKET_CRITICALITY['HIGH']),
          'MEDIUM' :
             ot.get_backlogs_prio_data(request, team, local_tz, ticket_type, settings.TICKET_CRITICALITY['MEDIUM']),
          'LOW' :
             ot.get_backlogs_prio_data(request, team, local_tz, ticket_type, settings.TICKET_CRITICALITY['LOW']),
          'PLANNED' :
             ot.get_backlogs_prio_data(request, team, local_tz, ticket_type, settings.TICKET_CRITICALITY['PLANNED'])
        }'''

        team_backlogs_itsm = {
          'HIGH' : ot.get_backlogs_prio_data(request, team, local_tz, ticket_type, settings.TICKET_CRITICALITY['HIGH'],None,'itsm'),
          'MEDIUM' :
             ot.get_backlogs_prio_data(request, team, local_tz, ticket_type, settings.TICKET_CRITICALITY['MEDIUM'],None,'itsm'),
          'LOW' :
             ot.get_backlogs_prio_data(request, team, local_tz, ticket_type, settings.TICKET_CRITICALITY['LOW'],None,'itsm'),
          'PLANNED' :
             ot.get_backlogs_prio_data(request, team, local_tz, ticket_type, settings.TICKET_CRITICALITY['PLANNED'],None,'itsm')
        }
        
        #collect the GRP from all tickets
        high_grp  = team_backlogs_itsm['HIGH']['GRP']
        medium_grp  = team_backlogs_itsm['MEDIUM']['GRP'] 
        low_grp  = team_backlogs_itsm['LOW']['GRP']
        planned_grp  = team_backlogs_itsm['PLANNED']['GRP']
            
        #Merge deis and itsm data
        team_backlogs_list = dict()
        team_backlogs_list['HIGH'] = team_backlogs_itsm['HIGH']['backlogs_data']
        team_backlogs_list['MEDIUM'] = team_backlogs_itsm['MEDIUM']['backlogs_data']
        team_backlogs_list['LOW'] = team_backlogs_itsm['LOW']['backlogs_data']
        team_backlogs_list['PLANNED'] = team_backlogs_itsm['PLANNED']['backlogs_data']
        
        #Take only one GRP value from all the tickets
        l = [high_grp,medium_grp,low_grp,planned_grp]
        grp_temp  = [s for s in l if s]
        team_backlogs_list['GRP'] = grp_temp[0] if grp_temp else ''
        
    total_ticket = sum(v['y'] for v in team_backlogs_count['all']) if team_backlogs_count else 0

    logger.info("total_ticket:%s"%total_ticket)
    #crit_tickets=ot.get_backlogs_data(request, team,local_tz, ticket_type, criticality_high)
    crit_tickets_itsm=ot.get_backlogs_data(request, team,local_tz, ticket_type, criticality_high, None, 'itsm')
    high_crit_tickets = crit_tickets_itsm['isla']+crit_tickets_itsm['oosla']+crit_tickets_itsm['soosla']

    return render(request, 'users/team.html', {'team' : team, 'team_name' : team_name, 'team_backlogs_count' : team_backlogs_count, 'team_backlogs_list' : team_backlogs_list, 'total_ticket' : total_ticket, 'high_crit_tickets' : high_crit_tickets}) 
    

#@login_required
def security(request, security_type=None):
    form = GeneralForm(request.GET)
    project_upload_path = settings.UPLOAD_FOLDER
    if security_type in ["Workstation","Server"]:
        last_hour_file = os.path.join(project_upload_path, settings.SEC_LAST_HOUR_DATA_V2)
        with open(last_hour_file,"r") as last_hour_file_in:
            last_hour_data = last_hour_file_in.readlines()
            
        security_microsoft_patch_wks_list = ast.literal_eval(last_hour_data[0]) if security_type=="Workstation" else ast.literal_eval(last_hour_data[8])
        security_microsoft_patch_srv_list_nonprd =  ast.literal_eval(last_hour_data[16])
        security_top5_microsoft_patches_wks_list = ast.literal_eval(last_hour_data[1]) if security_type=="Workstation" else ast.literal_eval(last_hour_data[9])
        security_top5_microsoft_patches_srv_list_nonprd = ast.literal_eval(last_hour_data[17])

        return render(request, 'users/security.html', {'form' : form, 'security_microsoft_patch_wks_list' : security_microsoft_patch_wks_list, 'security_top5_microsoft_patches_wks_list' : security_top5_microsoft_patches_wks_list, 'security_type' : security_type, 'security_microsoft_patch_srv_list_nonprd' : security_microsoft_patch_srv_list_nonprd, 'security_top5_microsoft_patches_srv_list_nonprd' : security_top5_microsoft_patches_srv_list_nonprd})
        
    if security_type in ["Kaspersky_WKS","Kaspersky_SRV"]:
        last_hour_file = os.path.join(project_upload_path, settings.SEC_LAST_HOUR_KASPERSKY_DATA)
        
        with open(last_hour_file,"r") as last_hour_file_in:
            last_hour_data = last_hour_file_in.readlines()
        kaspersky_count_list = ast.literal_eval(last_hour_data[0]) if security_type=="Kaspersky_WKS" else ast.literal_eval(last_hour_data[5])

        return render(request, 'users/kaspersky.html', {'form' : form, 'security_microsoft_patch_wks_list' : kaspersky_count_list,'security_type' : security_type})

def security_details(request, security_type=None):
    region = None
    if request.method == 'POST':
        request_data = json.loads(request.body)
        region= request_data['region']
    security_details = None
    security_details_summary = None 
    project_upload_path = settings.UPLOAD_FOLDER

    #last hour data, trace data from history file for microsoft patch
    if security_type in ["Workstation","Server"]:
        last_hour_file = os.path.join(project_upload_path, settings.SEC_LAST_HOUR_DATA_V2)
        with open(last_hour_file,"r") as last_hour_file_in:
            last_hour_data = last_hour_file_in.readlines()
        if region=="Americas":
            security_details = eval(last_hour_data[2]) if security_type=="Workstation" else eval(last_hour_data[10])
            security_details_summary = eval(last_hour_data[5]) if security_type=="Workstation" else eval(last_hour_data[13])
        elif region=="EMEA":
            security_details = eval(last_hour_data[3]) if security_type=="Workstation" else eval(last_hour_data[11])
            security_details_summary = eval(last_hour_data[6]) if security_type=="Workstation" else eval(last_hour_data[14])
        elif region=="Asia":
            security_details = eval(last_hour_data[4]) if security_type=="Workstation" else eval(last_hour_data[12])
            security_details_summary = eval(last_hour_data[7]) if security_type=="Workstation" else eval(last_hour_data[15])
        elif region=="Americas_Nonprd":
            security_details = eval(last_hour_data[18])
            security_details_summary = eval(last_hour_data[21])
        elif region=="EMEA_Nonprd":
            security_details = eval(last_hour_data[19])
            security_details_summary = eval(last_hour_data[22])
        elif region=="Asia_Nonprd":
            security_details = eval(last_hour_data[20])
            security_details_summary = eval(last_hour_data[23])
        elif region=="Worldwide_Nonprd":
            security_details = eval(last_hour_data[18]) + eval(last_hour_data[19]) + eval(last_hour_data[20])
            security_details_summary = eval(last_hour_data[21]) + eval(last_hour_data[22]) + eval(last_hour_data[23])
        else:
            if security_type=="Workstation":
                security_details = eval(last_hour_data[2]) + eval(last_hour_data[3]) + eval(last_hour_data[4])
                security_details_summary = eval(last_hour_data[5]) + eval(last_hour_data[6]) + eval(last_hour_data[7])
            elif security_type=="Server":
                security_details = eval(last_hour_data[10]) + eval(last_hour_data[11]) + eval(last_hour_data[12])
                security_details_summary = eval(last_hour_data[13]) + eval(last_hour_data[14]) + eval(last_hour_data[15])

    #last hour data, trace data from history file for kaspersky
    if security_type in ["Kaspersky_WKS","Kaspersky_SRV"]:
        last_hour_file = os.path.join(project_upload_path, settings.SEC_LAST_HOUR_KASPERSKY_DATA)
        with open(last_hour_file,"r") as last_hour_file_in:
            last_hour_data = last_hour_file_in.readlines()
        if region=="Americas":
            security_details = eval(last_hour_data[1]) if security_type=="Kaspersky_WKS" else eval(last_hour_data[6])
        elif region=="EMEA":
            security_details = eval(last_hour_data[2]) if security_type=="Kaspersky_WKS" else eval(last_hour_data[7])
        elif region=="Asia":
            security_details = eval(last_hour_data[3]) if security_type=="Kaspersky_WKS" else eval(last_hour_data[8])
        else:
            if security_type=="Kaspersky_WKS":
                security_details = eval(last_hour_data[1]) + eval(last_hour_data[2]) + eval(last_hour_data[3])
            elif security_type=="Kaspersky_SRV":
                security_details = eval(last_hour_data[6]) + eval(last_hour_data[7]) + eval(last_hour_data[8])

    #import pdb;pdb.set_trace()
    return JsonResponse({'security_details' : security_details, 'security_details_summary' : security_details_summary})
 
#@login_required
'''def patch_followup(request):
    project_upload_path = settings.UPLOAD_FOLDER
    campaign_list = [row.campaign_name for row in Patchcampaigns.objects.all().order_by('campaign_name')]
        
    if request.method == 'GET':
        return render(request, 'users/patch_followup.html', {'campaign_list' : campaign_list})
    
    if request.method == 'POST':
        #import pdb; pdb.set_trace()
        #campaign_name = request.json['campaign_name']
        request_data = json.loads(request.body)
        campaign_name = request_data['campaign_name']
        target_list = ''
     
        patch_list = Patchcampaigns.objects.filter(campaign_name = campaign_name).first()
        wks_patch_list = patch_list.wks_patch_list 
        srv_patch_list = patch_list.srv_patch_list
        wks_patch_list = ast.literal_eval(wks_patch_list)
        srv_patch_list = ast.literal_eval(srv_patch_list)

        wks_patch_search_static_result = secus._get_patch_search_static_result('Workstation',wks_patch_list.keys(),target_list)
        srv_patch_search_static_result = secus._get_patch_search_static_result('Server',srv_patch_list.keys(),target_list)

        wks_column_name = ["Zone","Ongoing","Completed","Total"] 
        srv_column_name = ["Domain","Ongoing","Completed","Total"]
        
        #wks_patch_search_static_result_ordered_dict = [OrderedDict(zip(wks_column_name, subl)) for subl in wks_patch_search_static_result]
        #srv_patch_search_static_result_ordered_dict = [OrderedDict(zip(srv_column_name, subl)) for subl in srv_patch_search_static_result]
        wks_patch_search_static_result_ordered_dict, srv_patch_search_static_result_ordered_dict = {}, {}
        for subl in wks_patch_search_static_result:
            wks_patch_search_static_result_ordered_dict[subl[0]] = OrderedDict(zip(wks_column_name, subl))
        for subl in srv_patch_search_static_result:
            srv_patch_search_static_result_ordered_dict[subl[0]] = OrderedDict(zip(srv_column_name, subl))
        
        wks_patch_list_str = ",".join(wks_patch_list)
        srv_patch_list_str = ",".join(srv_patch_list)
        target_list_str = ",".join(target_list)

        #import pdb; pdb.set_trace() 
        return JsonResponse({"wks_patch_search_static_result":wks_patch_search_static_result_ordered_dict,"srv_patch_search_static_result":srv_patch_search_static_result_ordered_dict,"wks_patch_list":wks_patch_list_str,"srv_patch_list":srv_patch_list_str,"target_list":target_list_str})'''
        
def patch_followup(request):
    project_upload_path = settings.UPLOAD_FOLDER
    campaign_list = [row.campaign_name for row in Patchcampaigns.objects.all().order_by('campaign_name')]
        
    if request.method == 'GET':
        return render(request, 'users/patch_followup.html', {'campaign_list' : campaign_list})
    
    if request.method == 'POST':
        request_data = json.loads(request.body)
        campaign_name = request_data['campaign_name']
        target_list = ''
     
        db_conn = MySQLdb.connect(user='root', passwd='Neurones@2018', host='10.236.0.3', db='Hygie_django', charset='utf8')
        cursor = db_conn.cursor()
        
        #patch_list = PatchCampaign.query.filter_by(campaign_name = request.json['campaign_name']).first()
        patch_list = "Select srv_patch_list,wks_patch_list from patchcampaigns where campaign_name = '{0}';".format(campaign_name)
        cursor.execute(patch_list)
        data = cursor.fetchall()
        db_conn.close()
        
        wks_patch_list = data[0][1] 
        srv_patch_list = data[0][0]
        wks_patch_list = ast.literal_eval(wks_patch_list)
        srv_patch_list = ast.literal_eval(srv_patch_list)

        #wks_patch_search_static_result = secus._get_patch_search_static_result('Workstation',wks_patch_list.keys(),target_list)
        #srv_patch_search_static_result = secus._get_patch_search_static_result('Server',srv_patch_list.keys(),target_list)

        wks_patch_search_static_result = [[u'AM', 3, 2, 5], [u'AS', 4, 3, 7], [u'EU', 3, 7, 10], [u'Total', 10, 12, 22]]
        srv_patch_search_static_result = [['non-PRD', 0, 5, 5], ['PRD', 0, 3, 3], [u'Total', 0, 8, 8]]
        
        wks_column_name = ["Zone","Ongoing","Completed","Total"] 
        srv_column_name = ["Domain","Ongoing","Completed","Total"]
        
        #wks_patch_search_static_result_ordered_dict = [OrderedDict(zip(wks_column_name, subl)) for subl in wks_patch_search_static_result]
        #srv_patch_search_static_result_ordered_dict = [OrderedDict(zip(srv_column_name, subl)) for subl in srv_patch_search_static_result]
        wks_patch_search_static_result_ordered_dict, srv_patch_search_static_result_ordered_dict = {}, {}
        for subl in wks_patch_search_static_result:
            wks_patch_search_static_result_ordered_dict[subl[0]] = OrderedDict(zip(wks_column_name, subl))
        for subl in srv_patch_search_static_result:
            srv_patch_search_static_result_ordered_dict[subl[0]] = OrderedDict(zip(srv_column_name, subl))
        
        wks_patch_list_str = ",".join(wks_patch_list)
        srv_patch_list_str = ",".join(srv_patch_list)
        target_list_str = ",".join(target_list)

        return JsonResponse({"wks_patch_search_static_result":wks_patch_search_static_result_ordered_dict,"srv_patch_search_static_result":srv_patch_search_static_result_ordered_dict,"wks_patch_list":wks_patch_list_str,"srv_patch_list":srv_patch_list_str,"target_list":target_list_str})        

#@login_required
def patch_management(request):
    project_upload_path = settings.UPLOAD_FOLDER
    campaign_list = [row.campaign_name for row in Patchcampaigns.objects.all().order_by('campaign_name')]
    
    if request.method == 'GET':
        return render(request, 'users/patch_management.html', {'campaign_list' : campaign_list})
    
    request_data = json.loads(request.body)
    campaign_name = request_data['campaign_name']

    patch_campaign_data = Patchcampaigns.objects.filter(campaign_name = campaign_name).first().__dict__
    del patch_campaign_data['_state']
    
    return JsonResponse({"data": patch_campaign_data})  

#@login_required
def patch_save(request):
    project_upload_path = settings.UPLOAD_FOLDER

    request_data = json.loads(request.body)      
    updated_data = request_data['updated_data']
    campaign_name = updated_data['campaignName']
    #search_filter_list[campaign_name] = updated_data
    
    #check for existing entry
    patch_campaign_data = Patchcampaigns.objects.filter(campaign_name = campaign_name).first()
    if patch_campaign_data:
        # update row
        patch_campaign_data.campaign_name = updated_data['campaignName']
        patch_campaign_data.campaign_startdate = updated_data['campaignStartDate']
        patch_campaign_data.campaign_target_enddate = updated_data['campaignTargetEndDate']
        patch_campaign_data.patch_release_date = updated_data['patchReleaseDate']
        patch_campaign_data.srv_patch_list = json.dumps(updated_data['srvPatchList'])
        patch_campaign_data.wks_patch_list = json.dumps(updated_data['wksPatchList'])
    else:
        # add new row
        patch_campaign_data = Patchcampaigns(campaign_name = updated_data['campaignName'], campaign_startdate = updated_data['campaignStartDate'], campaign_target_enddate = updated_data['campaignTargetEndDate'], patch_release_date = updated_data['patchReleaseDate'], srv_patch_list = json.dumps(updated_data['srvPatchList']), wks_patch_list = json.dumps(updated_data['wksPatchList']))
        #db.session.add(new_campaign)
        
    patch_campaign_data.save()
    
    campaign_list = [row.campaign_name for row in Patchcampaigns.objects.all()]
        
    return JsonResponse({"status":"OK", "campaign_list" : campaign_list})

#@login_required
def patch_delete(request):
    request_data = json.loads(request.body)
    campaign_name = request_data['campaign_name']
    
    patch_campaign_data = Patchcampaigns.objects.filter(campaign_name = campaign_name).first()
    if patch_campaign_data:
        # delete row
        patch_campaign_data.delete()
    
    campaign_list = [row.campaign_name for row in Patchcampaigns.objects.all()]
    
    return JsonResponse({"status":"OK", "campaign_list" : campaign_list})


#@login_required
def patch_detail_search(request, security_type=None):
    if request.method == 'GET':
        zone = request.GET.get('Zone')
        logic = request.GET.get('Logic')
        filterName = request.GET.get('filterName')
        patchList = request.GET.get('patchList')
        targetList = request.GET.get('targetList')

        patch_detail_search_result = [['HYNAUSUOH0007F7', 'G-HYUSUOHSAL01', 'Workstation', 'XXXX', 'Dell Inc.', 'OptiPlex 7050', '2XMKXK2', 3500, 'Intel(R) Core(TM) i5-7500T CPU @ 2.70GHz', 'Microsoft Windows 7 Enterprise Edition, 32-bit', '010.035.231.043', datetime.datetime(2019, 1, 14, 13, 19, 32), datetime.datetime(2019, 1, 18, 9, 16, 42, 177000), datetime.datetime(2019, 1, 18, 9, 16, 42, 177000), None, datetime.datetime(2019, 1, 18, 4, 41, 9), datetime.datetime(2019, 1, 18, 7, 25, 55), None, datetime.datetime(2018, 2, 6, 17, 22, 38, 327000)], ['HYNAUSUSX0003F7', 'G-HYUSUSXSAL01', 'Workstation', 'XXXX', 'Dell Inc.', 'OptiPlex 7050', '2XMPXK2', 3500, 'Intel(R) Core(TM) i5-7500T CPU @ 2.70GHz', 'Microsoft Windows 7 Enterprise Edition, 32-bit', '010.035.210.072', datetime.datetime(2019, 1, 13, 19, 3, 16), datetime.datetime(2019, 1, 17, 23, 49, 7, 710000), datetime.datetime(2019, 1, 18, 5, 25, 29, 523000), None, datetime.datetime(2019, 1, 17, 0, 16, 52), datetime.datetime(2019, 1, 17, 23, 31, 56), None, datetime.datetime(2018, 2, 10, 0, 18, 22)], ['HYNACAUHY0007F7', 'STORE_UHY', 'Workstation', 'XXXX', 'Dell Inc.', 'OptiPlex 7050', '33KNXK2', 3500, 'Intel(R) Core(TM) i5-7500T CPU @ 2.70GHz', 'Microsoft Windows 7 Enterprise Edition, 32-bit', '010.035.219.042', datetime.datetime(2019, 1, 17, 16, 43, 5), datetime.datetime(2019, 1, 17, 10, 51, 37, 397000), datetime.datetime(2019, 1, 18, 3, 25, 46, 17000), None, datetime.datetime(2019, 1, 17, 16, 54, 36), datetime.datetime(2019, 1, 18, 9, 1, 8), None, datetime.datetime(2018, 2, 19, 17, 57, 24, 707000)], ['HYNAUSU3R0182C7', 'HYUSU3RPCK', 'Workstation', 'XXXX', 'Dell Inc.', 'OptiPlex 3240 AIO', 'HX82B02', 3502, 'Intel(R) Core(TM) i5-6500 CPU @ 3.20GHz', 'Microsoft Windows 7 Enterprise Edition, 32-bit', '010.035.052.175', datetime.datetime(2019, 1, 18, 7, 36, 57), datetime.datetime(2019, 1, 18, 8, 24, 17, 847000), datetime.datetime(2019, 1, 18, 8, 20, 13, 543000), None, datetime.datetime(2019, 1, 18, 8, 8, 44), datetime.datetime(2019, 1, 18, 9, 0, 55), None, datetime.datetime(2018, 11, 30, 9, 20, 43, 247000)], ['HYNAUSFCA1001T8', 'HYUSFCAPROD01', 'Workstation', 'XXXX', 'Dell Inc.', 'OptiPlex 3030 AIO', 'F0JH942', 4013, 'Intel(R) Celeron(R) CPU G1840 @ 2.80GHz', 'Microsoft Windows 8.1 Enterprise Edition, 64-bit', '010.035.050.056', datetime.datetime(2018, 12, 23, 11, 31, 27), datetime.datetime(2018, 12, 23, 11, 50, 20, 410000), datetime.datetime(2018, 12, 23, 11, 50, 20, 410000), None, datetime.datetime(2018, 12, 23, 11, 49, 43), datetime.datetime(2018, 12, 23, 11, 59, 45), None, datetime.datetime(2018, 12, 17, 18, 1, 47, 733000)]]
        
    return render(request, 'users/patch_search_details.html', {'security_type' : security_type, 'patch_detail_search_result' : patch_detail_search_result, 'filterName' : filterName}) 

#@login_required
def patch_device_info(request, security_type=None):
    if request.method == 'GET':
       device_name = request.GET.get('DeviceName')
       filterName = request.GET.get('filterName')
       patch_device_info_result = secus._get_patch_device_info_result(security_type,device_name,filterName)
       #current_app.logger.info(patch_device_info_result)
    return render(request, 'users/patch_device_info.html', {'security_type' : security_type, 'patch_device_info_result' : patch_device_info_result, 'filterName' : filterName, 'device_name' : device_name})
  

#@login_required
def discrepancy(request, discrepancy_type=None):
    project_upload_path = settings.UPLOAD_FOLDER
    discrepancy_device_file = os.path.join(project_upload_path, settings.DIS_WKS_LASTUSER_DISCREPANCY) if discrepancy_type=="Workstation" else os.path.join(project_upload_path, settings.DIS_SRV_DISCREPANCY)
    ticket_data = []
    
    db_conn = MySQLdb.connect(user='root', passwd='Neurones@2018', host='10.236.0.3', db='Hygie_django', charset='utf8')
    
    cursor = db_conn.cursor()
    
    connstr = "Select workstation,ticketitsm, CONCAT('https://louisvuitton.landesk.com/LouisVuitton.WebAccess/wd/object/open.rails?class_name=IncidentManagement.Incident&key=',guiditsm) as guiditsm from events_discrepancy where status = '0';"
   
    cursor.execute(connstr)
    
    for t in cursor.fetchall():
        t = [x if x is not None else '' for x in t]
        ticket_data.append(t)              
    discrepancy_device_list_temp = list()
    with open(discrepancy_device_file,'r') as ddf:
        for line in ddf:
            discrepancy_device_list_temp.append(line.strip().split(',')) 
    discrepancy_type = discrepancy_type
    exceptions_discrepancy_device = DiscrepancyWorkstation.objects.all() if discrepancy_type=="Workstation" else DiscrepancyServer.objects.all()
    exceptions_discrepancy_yellow_list = []
    exceptions_useless_discrepancy_blue_list = []

    ###duplicate element of discrepancy db table in both section Discrepancy (data resource) and Discrepancy local db
    duplication_discre_exception = [edd for ddl in discrepancy_device_list_temp for edd in exceptions_discrepancy_device if edd.devicename in ddl]
    logger.info("duplication_discre_exception:%s"%exceptions_discrepancy_device)
    logger.info("duplication_discre_exception:%s"%duplication_discre_exception)

    ###get the element which are in Discrepancy local db, but not in system data resource
    exceptions_useless_discrepancy_allkook_list = set(exceptions_discrepancy_device) - set(duplication_discre_exception)
    ldap_list_file = os.path.join(project_upload_path, settings.DIS_WKS_LDAP_LIST) if discrepancy_type=="Workstation" else os.path.join(project_upload_path, settings.DIS_SRV_LDAP_LIST)
    with open(ldap_list_file) as ldaplf:
        ldap_device_list = [line.strip() for line in ldaplf]
    ld_list_file = os.path.join(project_upload_path, settings.DIS_WKS_LD_LIST) if discrepancy_type=="Workstation" else os.path.join(project_upload_path, settings.DIS_SRV_LD_LIST)
    with open(ld_list_file) as ldlf:
        ld_device_list = [line.strip() for line in ldlf]
    kaspersky_list_file = os.path.join(project_upload_path, settings.DIS_WKS_KAS_LIST) if discrepancy_type=="Workstation" else os.path.join(project_upload_path, settings.DIS_SRV_KAS_LIST)
    with open(kaspersky_list_file) as kaslf:
        kaspersky_device_list = [line.strip() for line in kaslf]
    deis_list_file = os.path.join(project_upload_path, settings.DIS_WKS_DEIS_LIST) if discrepancy_type=="Workstation" else os.path.join(project_upload_path, settings.DIS_SRV_DEIS_LIST)
    with open(deis_list_file) as deislf:
        deis_device_list = [line.strip() for line in deislf]
    exceptions_useless_discrepancy_allok_list = [ eudal for eudal in exceptions_useless_discrepancy_allkook_list if eudal.devicename in set(ldap_device_list+kaspersky_device_list+ld_device_list+deis_device_list)]
    exceptions_useless_discrepancy_allko_list = [ eudal for eudal in exceptions_useless_discrepancy_allkook_list if eudal.devicename not in set(ldap_device_list+kaspersky_device_list+ld_device_list+deis_device_list)]

    ###duplicate element of section Discrepancy (data resource) show in both section Discrepancy (data resource) and Discrepancy local db 
    duplication_discrepancy_list = [e for e in discrepancy_device_list_temp for edl in exceptions_discrepancy_device if edl.devicename in e]


    ###exception list from db which are validated and showed in section Exception of template
    for edd in duplication_discre_exception:
        #edd.adindicator,edd.ldindicator,edd.kasindicator = tuple([e for e in duplication_discrepancy_list if edd.devicename in e][0][2:-1])
        if discrepancy_type=="Workstation":
            for t in ticket_data:
                if t[0] == edd.devicename:
                    edd.ticket = "I" + t[1]
                    edd.link = t[2]
                    edd.devicenameticket = t[0]                         
            edd.adindicator,edd.ldindicator,edd.kasindicator,edd.deisindicator = tuple([e for e in duplication_discrepancy_list if edd.devicename in e][0][2:-1])
        else:
            edd.adindicator,edd.ldindicator,edd.kasindicator,edd.deisindicator = tuple([e for e in duplication_discrepancy_list if edd.devicename in e][0][2:])
        edd.save()
        #import pdb; pdb.set_trace()
        #db.session.commit()
        validate_ad = True if (edd.adindicator =="False" and edd.adcheck) else True if edd.adindicator =="True" else False
        validate_ld = True if (edd.ldindicator =="False" and edd.ldcheck) else True if edd.ldindicator =="True" else False
        validate_kas = True if (edd.kasindicator =="False" and edd.kascheck) else True if edd.kasindicator =="True" else False
        validate_deis = True if (edd.deisindicator =="False" and edd.deischeck) else True if edd.deisindicator =="True" else True if discrepancy_type=="Workstation" else False
        logger.info("edd:%s"%edd)
        logger.info("validate_deis:%s"%validate_deis)
        if validate_ad==validate_ld==validate_kas==validate_deis==True:
            if (edd.adindicator =="True" and edd.adcheck) or (edd.ldindicator =="True" and edd.ldcheck) or (edd.kasindicator =="True" and edd.kascheck):
                exceptions_useless_discrepancy_blue_list.append(edd)
            else:
                exceptions_discrepancy_yellow_list.append(edd)


    ###remove the duplicate element from section Discrepancy (data resource)
    discrepancy_device_list_temp = [e for e in discrepancy_device_list_temp if e not in duplication_discrepancy_list]

    for line in discrepancy_device_list_temp:
        for t in ticket_data:
            if t[0] == line[0]:
                line.append("I" + t[1])
                line.append(t[2])
                line.append(t[0])                                          
    ###duplicate element show in both section Discrepancy (data resource) and Discrepancy local db, but not in section Exception
    logger.info("exceptions_discrepancy_yellow_list:%s"%exceptions_discrepancy_yellow_list) 

    dde_list = [ ddl[0] for ddl in duplication_discrepancy_list if ddl[0] not in [edl.devicename for edl in (exceptions_discrepancy_yellow_list+exceptions_useless_discrepancy_blue_list)]]
    dde_device_list = [edd for edd in exceptions_discrepancy_device if edd.devicename in dde_list]
    for ddl in dde_device_list:
        #if [ddl.adindicator,ddl.ldindicator,ddl.kasindicator,ddl.deisindicator] != [e for e in duplication_discrepancy_list if ddl.devicename in e][0][2:]:
        #    ddl.adindicator,ddl.ldindicator,ddl.kasindicator,ddl.deisindicator = tuple([e for e in duplication_discrepancy_list if ddl.devicename in e][0][2:])
        if discrepancy_type=="Workstation":
            if [ddl.adindicator,ddl.ldindicator,ddl.kasindicator,ddl.deisindicator] != [e for e in duplication_discrepancy_list if ddl.devicename in e][0][2:-1]:
                ddl.adindicator,ddl.ldindicator,ddl.kasindicator,ddl.deisindicator = tuple([e for e in duplication_discrepancy_list if ddl.devicename in e][0][2:-1])
                ddl.save()
                #db.session.commit()
        else:
            if [ddl.adindicator,ddl.ldindicator,ddl.kasindicator,ddl.deisindicator] != [e for e in duplication_discrepancy_list if ddl.devicename in e][0][2:]:
                ddl.adindicator,ddl.ldindicator,ddl.kasindicator,ddl.deisindicator = tuple([e for e in duplication_discrepancy_list if ddl.devicename in e][0][2:])
                ddl.save()
                #db.session.commit()
    
    #converting list discrepancy_device_list_temp to dictionary with keys
    keys = ['devicename', 'ipaddress', 'adindicator', 'ldindicator', 'kasindicator', 'deisindicator','last_user']
    discrepancy_device_list = []
    for values in discrepancy_device_list_temp:
        dictionary = dict(zip(keys, values))
        discrepancy_device_list.append(dictionary)
    
    #Remove keys which are unnecessary
    remove_keys = ['_state', 'id', 'adcheck', 'ldcheck', 'kascheck', 'deischeck', 'datehistory', 'lockby', 'last_update']
    
    for i in range(len(dde_device_list)):  
        discrepancy_device_list.append({key: dde_device_list[i].__dict__[key] for key in dde_device_list[i].__dict__ if key not in remove_keys})
    
    exceptions_list = []    
    for i in range(len(exceptions_discrepancy_device)):  
        exceptions_list.append({key: exceptions_discrepancy_device[i].__dict__[key] for key in exceptions_discrepancy_device[i].__dict__ if key not in remove_keys})
        
    for i in range(len(exceptions_useless_discrepancy_blue_list)):  
        exceptions_list.append({key: exceptions_useless_discrepancy_blue_list[i].__dict__[key] for key in exceptions_useless_discrepancy_blue_list[i].__dict__ if key not in remove_keys})
        
    for i in range(len(exceptions_useless_discrepancy_allok_list)):  
        exceptions_list.append({key: exceptions_useless_discrepancy_allok_list[i].__dict__[key] for key in exceptions_useless_discrepancy_allok_list[i].__dict__ if key not in remove_keys})
        
    for i in range(len(exceptions_useless_discrepancy_allko_list)):  
        exceptions_list.append({key: exceptions_useless_discrepancy_allko_list[i].__dict__[key] for key in exceptions_useless_discrepancy_allko_list[i].__dict__ if key not in remove_keys})
        
    #import pdb; pdb.set_trace()
    return render(request, 'users/discrepancy.html', {'discrepancy_type' : discrepancy_type, 'discrepancy_device_list' : discrepancy_device_list, 'exceptions_list' : exceptions_list, 'dde_device_list' : dde_device_list, 'ticket_data' : ticket_data})
 
#@login_required
def except_discrepancy(request, discrepancy_type="Workstation"):

    if request.method == 'POST':
        row_data = request.json['data']
        checks = request.json['checks']
        comments = request.json['comments']
        tabel_id = request.json['tabelid']
        click_btn = request.json['clickbtn']
        current_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        #current_app.logger.info(row_data)
        #current_app.logger.info(checks)
        #current_app.logger.info(comments)
        #current_app.logger.info(click_btn)
        #current_app.logger.info(current_time)
        validation_log = ''.join(['E' if c else 'X' for c in checks])
        #current_app.logger.info(validation_log)
        discrepancy = DiscrepancyWorkstation.query.filter_by(devicename = row_data[0]).first() if discrepancy_type=="Workstation" else DiscrepancyServer.query.filter_by(devicename = row_data[0]).first()
        #current_app.logger.info(discrepancy)
        if not discrepancy:
            #discrepancy = DiscrepancyWorkstation(row_data[0],'',row_data[1],row_data[2],row_data[3],checks[0],checks[1],checks[2],comments,current_time,'') if discrepancy_type=="Workstation" else DiscrepancyServer(row_data[0],row_data[1],row_data[2],row_data[3],row_data[4],checks[0],checks[1],checks[2],comments,current_time,comments+ " @ " + current_time)
            discrepancy = DiscrepancyWorkstation(row_data[0],row_data[1],row_data[2],row_data[3],row_data[4],row_data[5],checks[0],checks[1],checks[2],False,comments,current_time,comments + " ("+validation_log+") "+" by "+ request.user.username+ " @ " + current_time,request.user.username) if discrepancy_type=="Workstation" else DiscrepancyServer(row_data[0],row_data[1],row_data[2],row_data[3],row_data[4],row_data[5],checks[0],checks[1],checks[2],False,comments,current_time,comments+" ("+validation_log+") "+" by "+request.user.username+ " @ " + current_time,request.user.username)
            db.session.add(discrepancy)
            db.session.commit()
        else:
            if click_btn == "Delete" and discrepancy_type=="Workstation":
                DiscrepancyWorkstation.query.filter_by(devicename = row_data[0]).delete()
                db.session.commit()
                return JsonResponse({'status':'OK'})
            elif click_btn == "Delete" and discrepancy_type=="Server": 
                DiscrepancyServer.query.filter_by(devicename = row_data[0]).delete()
                db.session.commit()
                return JsonResponse({'status':'OK'})
                
            #current_app.logger.info(discrepancy.comments)
            if click_btn == "Validate" and comments:
                discrepancy.comments = comments
                discrepancy.commenthistory = comments + " ("+validation_log+") "+" by "+ reqiest.user.username + " @ " + current_time + "<br/>" +  discrepancy.commenthistory
                discrepancy.datehistory = current_time + "<br/>" +  discrepancy.datehistory
                discrepancy.adcheck = checks[0]
                discrepancy.ldcheck = checks[1]
                discrepancy.kascheck = checks[2]
                discrepancy.adindicator = row_data[2]
                discrepancy.ldindicator = row_data[3]
                discrepancy.kasindicator = row_data[4]
                discrepancy.deisindicator = row_data[5]
                db.session.commit()
        updated_discrepancy = DiscrepancyWorkstation.query.filter_by(devicename = row_data[0]).first() if discrepancy_type=="Workstation" else DiscrepancyServer.query.filter_by(devicename = row_data[0]).first()
        validate_ad = True if (updated_discrepancy.adindicator =="False" and updated_discrepancy.adcheck) else True if updated_discrepancy.adindicator =="True" else False 
        validate_ld = True if (updated_discrepancy.ldindicator =="False" and updated_discrepancy.ldcheck) else True if updated_discrepancy.ldindicator =="True" else False
        validate_kas = True if (updated_discrepancy.kasindicator =="False" and updated_discrepancy.kascheck) else True if updated_discrepancy.kasindicator =="True" else False
        exception_add_row = []
        if tabel_id == "discrepancy-table": 
            if validate_ad==validate_ld==validate_kas==True:
                #exception_add_row.append("""<input id="toggle-"""+updated_discrepancy.devicename+"""\" type="checkbox" data-toggle="toggle" data-on="<i class='fa fa-lock'></i> Lock" data-off="<i class='fa fa-unlock'></i> Unlock" data-size="mini" checked>""")
                exception_add_row.append(updated_discrepancy.devicename)
                exception_add_row.append(updated_discrepancy.ipaddress)
                exception_add_row.append(updated_discrepancy.adindicator)
                exception_add_row.append(updated_discrepancy.ldindicator)
                exception_add_row.append(updated_discrepancy.kasindicator)
                exception_add_row.append(updated_discrepancy.deisindicator)
                exception_add_row.append("""<span class="label label-success">OK</span>""" if not (updated_discrepancy.adcheck) else """<span class="label label-warning">KO</span>""")
                exception_add_row.append("""<span class="label label-success">OK</span>""" if not updated_discrepancy.ldcheck else """<span class="label label-warning">KO</span>""")
                exception_add_row.append("""<span class="label label-success">OK</span>""" if not updated_discrepancy.kascheck else """<span class="label label-warning">KO</span>""")
                exception_add_row.append("""<span class="label label-success">OK</span>""" if updated_discrepancy.deisindicator=="True" else """<span class="label label-danger">KO</span>""")
                exception_add_row.append("""<input type="checkbox" id=\""""+updated_discrepancy.devicename+"""_check_ad_exc" class="editor-active" checked="checked">""" if updated_discrepancy.adcheck else """<input type="checkbox" id=\""""+updated_discrepancy.devicename+"""_check_ad_exc" class="editor-active" disabled="">""")
                exception_add_row.append("""<input type="checkbox" id=\""""+updated_discrepancy.devicename+"""_check_ld_exc" class="editor-active" checked="checked">""" if updated_discrepancy.ldcheck else """<input type="checkbox" id=\""""+updated_discrepancy.devicename+"""_check_ld_exc" class="editor-active" disabled="">""")
                exception_add_row.append("""<input type="checkbox" id=\""""+updated_discrepancy.devicename+"""_check_kas_exc" class="editor-active" checked="checked">""" if updated_discrepancy.kascheck else """<input type="checkbox" id=\""""+updated_discrepancy.devicename+"""_check_kas_exc" class="editor-active" disabled="">""")
                #exception_add_row.append(updated_discrepancy.datehistory)
                exception_add_row.append(updated_discrepancy.commenthistory)
                exception_add_row.append("""<textarea id=\""""+updated_discrepancy.devicename+"""\">"""+updated_discrepancy.comments+"""</textarea>""")
                exception_add_row.append("""<button type="button" class="btn btn-info">Validate</button><br><br><button type="button" class="btn btn-info">Delete</button>""")
        else:
            #exception_add_row.append("""<input id="toggle-"""+updated_discrepancy.devicename+"""\" type="checkbox" data-toggle="toggle" data-on="<i class='fa fa-lock'></i> Lock" data-off="<i class='fa fa-unlock'></i> Unlock" data-size="mini" checked>""")
            exception_add_row.append(updated_discrepancy.devicename)
            exception_add_row.append(updated_discrepancy.ipaddress)
            exception_add_row.append(updated_discrepancy.adindicator)
            exception_add_row.append(updated_discrepancy.ldindicator)
            exception_add_row.append(updated_discrepancy.kasindicator)
            exception_add_row.append(updated_discrepancy.deisindicator)
            exception_add_row.append("""<span class="label label-success">OK</span>""" if updated_discrepancy.adindicator=="True" else """<span class="label label-danger">KO</span>""")
            exception_add_row.append("""<span class="label label-success">OK</span>""" if updated_discrepancy.ldindicator=="True" else """<span class="label label-danger">KO</span>""")
            exception_add_row.append("""<span class="label label-success">OK</span>""" if updated_discrepancy.kasindicator=="True" else """<span class="label label-danger">KO</span>""")
            exception_add_row.append("""<span class="label label-success">OK</span>""" if updated_discrepancy.deisindicator=="True" else """<span class="label label-danger">KO</span>""")
            exception_add_row.append("""<input type="checkbox" id=\""""+updated_discrepancy.devicename+"""_check_ad" class="editor-active" checked>""" if updated_discrepancy.adindicator=="False" and updated_discrepancy.adcheck else """<input type="checkbox" id=\""""+updated_discrepancy.devicename+"""_check_ad" class="editor-active" disabled="">""" if updated_discrepancy.adindicator=="True" else """<input type="checkbox" id=\""""+updated_discrepancy.devicename+"""_check_ad" class="editor-active">""")
            exception_add_row.append("""<input type="checkbox" id=\""""+updated_discrepancy.devicename+"""_check_ld" class="editor-active" checked>""" if updated_discrepancy.ldindicator=="False" and updated_discrepancy.ldcheck else """<input type="checkbox" id=\""""+updated_discrepancy.devicename+"""_check_ld" class="editor-active" disabled="">""" if updated_discrepancy.ldindicator=="True" else """<input type="checkbox" id=\""""+updated_discrepancy.devicename+"""_check_ld" class="editor-active">""")
            exception_add_row.append("""<input type="checkbox" id=\""""+updated_discrepancy.devicename+"""_check_kas" class="editor-active" checked>""" if updated_discrepancy.kasindicator=="False" and updated_discrepancy.kascheck else """<input type="checkbox" id=\""""+updated_discrepancy.devicename+"""_check_kas" class="editor-active" disabled="">""" if updated_discrepancy.kasindicator=="True" else """<input type="checkbox" id=\""""+updated_discrepancy.devicename+"""_check_kas" class="editor-active">""")
            exception_add_row.append("""<textarea id=\""""+updated_discrepancy.devicename+"""\">"""+updated_discrepancy.comments+"""</textarea>""")
            exception_add_row.append("""<button type="button" class="btn btn-info" id="btn-"""+updated_discrepancy.devicename+"""\">Validate</button>""")

            #current_app.logger.info(exception_add_row)

    return JsonResponse({'status':'OK','exception_add_row':exception_add_row})

''' TODO
@login_required
def discrepancy_add_comment(reques, discrepancy_type="Workstation"):
    
    if request.method == 'POST':
        devicename = request.json['devicename']
        comments = request.json['comments']
        current_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    
        discrepancy = DiscrepancyWorkstation.query.filter_by(devicename = devicename).first() if discrepancy_type=="Workstation" else DiscrepancyServer.query.filter_by(devicename = devicename).first()
        
    if discrepancy:
        discrepancy.comments = comments
        discrepancy.commenthistory = comments + " by " + current_user.username + " @ " + current_time + "<br/>" +  discrepancy.commenthistory
        discrepancy.datehistory = current_time + "<br/>" +  discrepancy.datehistory
        db.session.commit()
        
    else:
        discrepancy = DiscrepancyWorkstation(row_data[0],row_data[1],row_data[2],row_data[3],row_data[4],row_data[5],checks[0],checks[1],checks[2],False,comments,current_time,comments + " ("+validation_log+") "+" by "+ request.user.username+ " @ " + current_time,request.user.username) if discrepancy_type=="Workstation" else DiscrepancyServer(row_data[0],row_data[1],row_data[2],row_data[3],row_data[4],row_data[5],checks[0],checks[1],checks[2],False,comments,current_time,comments+" ("+validation_log+") "+" by "+request.user.username+ " @ " + current_time,request.user.username)
        db.session.add(discrepancy)
        db.session.commit()
        
    return jsonify({'status':'OK'})
'''    
 
@login_required
def ticket_status(request):
    request_data = json.loads(request.body)
     
    if request_data is None:
        return JsonResponse({"status":'NO DATA'})
  
    workstation = request_data['workstation']
    ad = request_data['ad']
    ld = request_data['ld']
    kav = request_data['kav']

    status = ticketstatus.get_ticket_status(workstation, ad, ld, kav)

    if status:
        return JsonResponse({"status":'status'})
    else:
        return JsonResponse({"status":'ERROR'})
    
@login_required
def ostype(request, server_type=None):
    form = GeneralForm(request.GET)
    #last hour data, trace data from history file
    project_upload_path = settings.UPLOAD_FOLDER
    
    os_data_file= os.path.join(project_upload_path, settings.SEC_OS_DETAILS_DATA_FILE)
    with open(os_data_file,"r") as os_data_file_in:
        os_data=os_data_file_in.readlines()

    version_list_temp = ast.literal_eval(os_data[0])["version_wise"]
    version_wise_list= {}
    os_type_list = ['Windows XP', 'Windows 7', 'Windows 8', 'Windows 10', 'Unknown']
    worldwide, americas, emea, asia, unspecified = 0, 0, 0, 0, 0
    #import pdb ; pdb.set_trace()
    for i in os_type_list:
        version_wise_list[i] = {'type':i, 'worldwide':version_list_temp['AMERICAS'][i] + version_list_temp['EMEA'][i] + version_list_temp['ASIA'][i] + version_list_temp['unspecified'][i], 'americas': version_list_temp['AMERICAS'][i], 'emea':version_list_temp['EMEA'][i], 'asia':version_list_temp['ASIA'][i], 'unspecified':version_list_temp['unspecified'][i]}
        
    '''for i in version_wise_list:
        worldwide +=  i['worldwide']
        americas += i['americas']
        emea += i['emea']
        asia +=i['asia']
        unspecified +=i['unspecified']
        
    version_wise_list['Total'] = {'type': 'Total', 'worldwide': worldwide, 'americas': americas, 'emea':emea, 'asia':asia, 'unspecified':unspecified}    '''   

    country_list=ast.literal_eval(os_data[0])["country_wise_list"]
    release_build=ast.literal_eval(os_data[0])["release_build"]
    
    release_build_worldwide={}
    
    
    
    release_build_americas = {}
    for k, v in release_build['AMERICAS'].items():
        if k not in release_build_worldwide:
            release_build_worldwide[k] = {'version' : k, 'count' : 0, 'percentage' : 0}
        release_build_worldwide[k]['count'] += v
        release_build_worldwide[k]['percentage'] = round((release_build_worldwide[k]['count']/version_wise_list['Windows 10']['worldwide'])*100,2 )
        release_build_americas[k] = {'version' : k, 'count' : v, 'percentage' : round((v/version_wise_list['Windows 10']['worldwide'])*100,2 )}
    
    release_build_emea = {}
    for k, v in release_build['EMEA'].items():
        if k not in release_build_worldwide:
            release_build_worldwide[k] = {'version' : k, 'count' : 0, 'percentage' : 0}
        release_build_worldwide[k]['count'] += v
        release_build_worldwide[k]['percentage'] = round((release_build_worldwide[k]['count']/version_wise_list['Windows 10']['worldwide'])*100,2 )
        release_build_emea[k] = {'version' : k, 'count' : v, 'percentage' : round((v/version_wise_list['Windows 10']['worldwide'])*100,2 )}
        
    release_build_asia = {}
    for k, v in release_build['ASIA'].items():
        if k not in release_build_worldwide:
            release_build_worldwide[k] = {'version' : k, 'count' : 0, 'percentage' : 0}
        release_build_worldwide[k]['count'] += v
        release_build_worldwide[k]['percentage'] = round((release_build_worldwide[k]['count']/version_wise_list['Windows 10']['worldwide'])*100,2 )
        release_build_asia[k] = {'version' : k, 'count' : v, 'percentage' : round((v/version_wise_list['Windows 10']['worldwide'])*100,2 )}
        
    release_build_unspecified = {}
    for k, v in release_build['unspecified'].items():
        if k not in release_build_worldwide:
            release_build_worldwide[k] = {'version' : k, 'count' : 0, 'percentage' : 0}
        release_build_worldwide[k]['count'] += v
        release_build_worldwide[k]['percentage'] = round((release_build_worldwide[k]['count']/version_wise_list['Windows 10']['worldwide'])*100,2 )
        release_build_unspecified[k] = {'version' : k, 'count' : v, 'percentage' : round((v/version_wise_list['Windows 10']['worldwide'])*100,2 )}
    
    
    '''for build_values in release_build.values():
        for key in set(build_values) | set(release_build_worldwide):
            release_build_worldwide[key] = build_values.get(key,0) + release_build_worldwide.get(key,0)
    total_worldwide_count=0    
    for i in ['ASIA','AMERICAS','EMEA','unspecified']:
	    total_worldwide_count+=version_wise_list[i]["Windows XP"]+version_wise_list[i]["Windows 7"]+version_wise_list[i]["Windows 8"]+version_wise_list[i]["Windows 10"]+version_wise_list[i]["Unknown"]
    logger.info('+=+=+=release_build_worldwide : %s' % release_build_worldwide)
    logger.info('+=+=+=total_worldwide_count : %s' % total_worldwide_count)'''
    
    #import pdb ; pdb.set_trace()
    '''vwl_worldwide_windowsxp = version_wise_list["AMERICAS"]["Windows XP"]+version_wise_list["EMEA"]["Windows XP"]+version_wise_list["ASIA"]["Windows XP"]+version_wise_list[None]["Windows XP"]
    vwl_worldwide_windows7 = version_wise_list["AMERICAS"]["Windows 7"]+version_wise_list["EMEA"]["Windows 7"]+version_wise_list["ASIA"]["Windows 7"]+version_wise_list[None]["Windows 7"]
    vwl_worldwide_windows8 = version_wise_list["AMERICAS"]["Windows 8"]+version_wise_list["EMEA"]["Windows 8"]+version_wise_list["ASIA"]["Windows 8"]+version_wise_list[None]["Windows 8"]
    vwl_worldwide_windows8
    vwl_worldwide_windows8
    vwl_worldwide_windows8'''
    return render(request, 'users/ostype.html', {'form' : form, 'country_list' : country_list, 'version_wise_list' : version_wise_list, 'os_type_list' : os_type_list, 'release_build' : release_build,'release_build_worldwide' : release_build_worldwide, 'release_build_americas' : release_build_americas, 'release_build_emea' : release_build_emea, 'release_build_asia' : release_build_asia, 'release_build_unspecified' : release_build_unspecified})


@login_required
def cmdb(request):
    form = GeneralForm(request.GET)
    #import pdb;pdb.set_trace()
    integrity_data = cmdbrpt._get_cmdb_report_integrity()
    backup_data = cmdbrpt._get_cmdb_report_backup()
    monitoring_data = cmdbrpt._get_cmdb_report_monitoring()
    data_list = cmdbrpt._get_cmdb_device()
    oneretail_data_list = cmdbrpt._get_oneretail_device()
    oneretail_integrity_data = cmdbrpt._get_oneretail_report_integrity()
    oneretail_backup_data = cmdbrpt._get_oneretail_report_backup()
    oneretail_monitoring_data = cmdbrpt._get_oneretail_report_monitoring()
    logger.info("one retail integrity_data:%s"%oneretail_integrity_data)
    
    
    #exclusion server list
    exclude_dict = collections.defaultdict(list)
    json_url = os.path.join(settings.UPLOAD_FOLDER,'exclude_list.json')
    with open(json_url, 'r') as excludefile:
        exclusion_list= json.load(excludefile)
    #exclusion_type = {"0":"Wrong Backup non AS400 - Datacenter Internal (no Chic)","1":"Wrong Backup non AS400 - NAS & MYITStoreBox","2":"Wrong Monitoring non AS400 - Datacenter Internal (no Chic)","3":"Wrong Monitoring non AS400 - Datacenter External SAP"}
    exclusion_type = {"0":"No Location","1":"No CI Zone","2":"Wrong Status","3":"No CI Environement","4":"No Configuration Desc","5":"No Criticality - No Application Linked","6":"No Backup","7":"Wrong Backup AS400 Prod","8":"Wrong Backup AS400 Non Prod","9":"Wrong Backup non AS400 - Datacenter Internal (no Chic)","10":"Wrong Backup non AS400 - Datacenter External Chic","11":"Wrong Backup non AS400 - NAS & MYITStoreBox","12":"Wrong Backup non AS400 - Datacenter External SAP","13":"No Monitoring","14":"Wrong Monitoring AS400 Production","15":"Wrong Monitoring non AS400 - Datacenter Internal (no Chic)","16":"Wrong Monitoring non AS400 - Datacenter External Chic","17":"Wrong Monitoring non AS400 - NAS","18":"Wrong Monitoring non AS400 - Datacenter External SAP"}
    for key,value in exclusion_list.items():
        for el in value:
            pass
            exclude_dict[key].append(el+":"+exclusion_type[el])
    #exclusion server list for oneretail
    exclude_dict_oneretail = collections.defaultdict(list)
    json_url = os.path.join(settings.UPLOAD_FOLDER,'oneretail_exclude_list.json')
    with open(json_url, 'r') as excludefile_oneretail:
        exclusion_list = json.load(excludefile_oneretail)
    oneretail_exclusion_type = {"0":"No Location","1":"No CI Zone","2":"Wrong Status","3":"No CI Environement","4":"No Configuration Desc","5":"No Criticality - No Application Linked","6":"No Configuration ID","7":"Wrong Configuration ID","8":"No Host Function","9":"No Server Location","10":"No Technical Leader","11":"No Reboot Plan","12":"No Backup","13":"Wrong Backup ID","14":"No Monitoring","15":"Wrong Monitoring"}
    for key,value in exclusion_list.items():
        for el in value:
            pass
            exclude_dict_oneretail[key].append(el+":"+oneretail_exclusion_type[el])

    #import pdb; pdb.set_trace()        
    return render(request, 'users/cmdbreport.html', {'exclude_list' : dict(exclude_dict), 'exclude_list_oneretail' : dict(exclude_dict_oneretail), 'form' : form, 'cmdb_data' : dict(integrity_data), 'monitoring_data' : dict(monitoring_data), 'backup_data' : dict(backup_data), 'data_list' : data_list, 'oneretail_integrity_data' : dict(oneretail_integrity_data), 'oneretail_backup_data' : dict(oneretail_backup_data), 'oneretail_monitoring_data' : dict(oneretail_monitoring_data)})
    
@login_required
def exclusion_json():
    json_url = os.path.join(settings.UPLOAD_FOLDER,'exclude_list.json')
    request_data = json.loads(request.body) 
    import pdb;pdb.set_trace()
    exclude_server = request_data['exclude_server']
    exclude_type = request_data['exclude_type']
    #current_app.logger.info("exclude_server:{0}, exclude_type:{1}".format(exclude_server,exclude_type))
    with open(json_url) as jsonfile:
        data = json.load(jsonfile)
    if exclude_server in data.keys():
        if exclude_type not in data[exclude_server]:
            data[exclude_server].append(exclude_type)
    else:
        data[exclude_server] = list()
        data[exclude_server].append(exclude_type)
    with open(json_url,'w') as outputfile:
        json.dump(data,outputfile)
    res = updateCondtion(int(exclude_type))
    current_app.logger.info("res:%s"%res)
    return JsonResponse(res) 

@login_required
def oneretail_exclusion_json():
    json_url = os.path.join(settings.UPLOAD_FOLDER,'oneretail_exclude_list.json')
    request_data = json.loads(request.body) 
    import pdb;pdb.set_trace()
    exclude_server = request_data['oneretail_exclude_server']
    exclude_type = request_data['exclude_type']
    print("inside exclustion oneretail")
    print(exclude_server)
    #current_app.logger.info("exclude_server:{0}, exclude_type:{1}".format(exclude_server,exclude_type))
    with open(json_url) as jsonfile:
        data = json.load(jsonfile)
    if exclude_server in data.keys():
        if exclude_type not in data[exclude_server]:
            data[exclude_server].append(exclude_type)
    else:
        data[exclude_server] = list()
        data[exclude_server].append(exclude_type)
    with open(json_url,'w') as outputfile:
        json.dump(data,outputfile)
    res = oneretail_updateCondtion(int(exclude_type))
    current_app.logger.info("res:%s"%res)
    print(res)
    return JsonResponse(res)    

@login_required
def oneretail_delete_json():
    json_url = os.path.join(settings.UPLOAD_FOLDER,'oneretail_exclude_list.json')
    request_data = json.loads(request.body) 
    import pdb;pdb.set_trace()
    dname = request_data['data']
    
    #current_app.logger.info("dname:%s"%dname)
    with open(json_url, 'r') as excludefile:
        list = json.load(excludefile)
    test = dname.split("|")
    name = test[0]
    number = dname[-1:]
    if len(test[2]) == 2 :
        number = dname[-2:]
    if name in list.keys():
        if number in list[name]:
            if len( list[name] ) == 0:
                pass #error without pass, delete whole table from json
                list = [element for element in list if name not in element]
            else:
                list[name].remove(number)
    with open(json_url,'w') as outputfile:
        json.dump(list,outputfile)
    int_n = int(number)
    res = oneretail_updateCondtion(int_n)
    return JsonResponse(res)  

@login_required
def delete_json():
    json_url = os.path.join(settings.UPLOAD_FOLDER,'exclude_list.json')
    request_data = json.loads(request.body) 
    import pdb;pdb.set_trace()
    dname = request_datan['data']
    
    #current_app.logger.info("dname:%s"%dname)
    with open(json_url, 'r') as excludefile:
        list = json.load(excludefile)
    test = dname.split("|")
    name = test[0]
    number = dname[-1:]
    if len(test[2]) == 2 :
        number = dname[-2:]
    if name in list.keys():
        if number in list[name]:
            if len( list[name] ) == 0:
                pass #error without pass, delete whole table from json
                list = [element for element in list if name not in element]
            else:
                list[name].remove(number)
    with open(json_url,'w') as outputfile:
        json.dump(list,outputfile)
    int_n = int(number)
    res = updateCondtion(int_n)
    return JsonResponse(res)  

@login_required
def oneretail_device_information(request):
    request_data = json.loads(request.body) 
    #import pdb;pdb.set_trace()
    data = request_data['data']
    stri = str(data)
    print(stri)
    pos = 0;
    if stri[0] == 'y':
        pos = pos + 12
    elif stri[0] == 'z':
        pos = pos + 2 + 12
    pos = pos + int(stri[1:]) - 1
    spos = str(pos)
    print(pos)
    res = cmdbrpt._get_oneretail_device_name(pos)
    return JsonResponse(res, safe=False)    

@login_required
def device_information(request):
    request_data = json.loads(request.body) 
    #import pdb;pdb.set_trace()
    data = request_data['data']
    stri = str(data)
    pos = 0;
    if stri[0] == 'b':
        pos = pos + 6
    elif stri[0] == 'm':
        pos = pos + 7 + 6
    pos = pos + int(stri[1]) - 1
    spos = str(pos)
    res = cmdbrpt._get_cmdb_device_name(pos)
    return JsonResponse(res, safe=False)
    

@login_required
def quality(request):

    form = RCAForm(request.GET)
    rca_report_form = RCAReportForm(request.GET) 
    action_form = ActionForm()
    #action_form.actionNum = ""
    action_form.actionContent = ""
    action_form.actionOwner = ""
    action_form.actionTargetDate = ""
    action_form.actionTargetCheckbox = ""

    rca_report_form.actions.append_entry(action_form)
    rca_data = rca.get_all_rca()
    rca_list = rca_data['rca_list']
    rca_sla_chart = rca_data['rca_sla_chart']
    rca_group_chart = rca_data['rca_group_chart']
    
    #import pdb; pdb.set_trace()
    return render(request, 'users/quality.html', {'form' : form, 'rca_report_form' : rca_report_form, 'rca_list' : SafeString(rca_list), 'rca_sla_chart' : rca_sla_chart, 'rca_group_chart' : rca_group_chart})
 
@login_required
def save_rca(request):
    #import pdb; pdb.set_trace()
    request_data = json.loads(request.body)
    rca_id = request_data['rcaid']
    rca_content = json.loads(request_data['content'])
    rca_report = json.loads(request_data['report'])
    rca_status = request_data['status']
    rca_mode = request_data['mode']
    
    content_mandatory_fields = settings.QUALITY_RCA_MANDATORY_FIELDS
    for f in content_mandatory_fields:
        if f not in rca_content.keys():
            return JsonResponse({"status":'ERROR', 'message':'Missing Fields'})
    
    if rca_mode == 'rca-report':
        rca_report = rca.format_report(rca_report)

    rca_id_cell = """<div><a href="#" data-toggle="modal" data-target="#rcaRowModal" id=\""""+rca_id+"""-edit" class="click_rcareport">"""+rca_id+"""</a></div>"""
    rca_outage_title_cell = """<div><a href="#" data-toggle="modal" data-target="#rcaReportModal" id=\""""+rca_id+"""\" class="click_rcareport">"""+rca_content['outageTitle']+"""</a></div>"""
    
    rca_data_list = [rca_id_cell, rca_content['week'], rca_content['zone'], rca_outage_title_cell, rca_content['rcaStatus'], rca_content['startDate'], rca_content['endDate'], rca_content['dueDate'], rca_content['incidentStartDate'], rca_content['incidentEndDate'], rca_content['incidentDuration'], rca_content['rcaOwner'], rca_content['problemId'], rca_content['im'], rca_content['group'], rca_content['comments']]
    
    ok = rca.save_rca(rca_id, request_data['content'], json.dumps(rca_report), rca_status)
    
    in_sla_check = datetime.datetime.strptime(rca_content['dueDate'], '%Y-%m-%d %H:%M') > datetime.datetime.now()
    
    rca_chart_data = rca.get_all_rca()
    
    status = "OK" if ok else "ERROR"
    
    return JsonResponse({"status":status, 'new_row': rca_content, 'in_sla_check' : in_sla_check, 'rca_sla_chart' : rca_chart_data['rca_sla_chart'], 'rca_group_chart' : rca_chart_data['rca_group_chart'], 'new_row_list': rca_data_list, 'rca_list' : rca_chart_data['rca_list'] })
    
@login_required
def delete_rca(request):
    #import pdb; pdb.set_trace()
    rca_id = json.loads(request.body)['rcaid']
   
    ok = rca.delete_rca(rca_id)
    
    rca_chart_data = rca.get_all_rca()
    
    status = "OK" if ok else "ERROR"
    
    return JsonResponse({"status":status, 'rca_sla_chart' : rca_chart_data['rca_sla_chart'], 'rca_group_chart' : rca_chart_data['rca_group_chart'], 'rca_list' : rca_chart_data['rca_list'] })

@login_required
def edit_rcareport_quality(quality_type=None): #may not be required check with anu
    import pdb;pdb.set_trace()
    project_upload_path = current_app.config.get('UPLOAD_FOLDER')
    rcareport_source_json = os.path.join(project_upload_path, current_app.config.get('RCA_REPORT'))
    with open(rcareport_source_json,"r") as rcareport_source_file:
        rcareport_source_list = json.load(rcareport_source_file)
    if request.method == 'POST':
        ticketNumber = request.form['ticketNumber']
        rcaId = request.form['rcaId'].encode('utf-8')
        problemId = request.form['problemId']
        reportStartDate = request.form['reportStartDate']
        reportEndDate = request.form['reportEndDate']
        preparedBy = request.form['preparedBy']
        contributors = request.form['contributors']
        description = request.form['description']
        impact = request.form['impact']
        rootcause = request.form['rootcause']
        detection = request.form['detection']
        restore = request.form['restore']
        preventiveAction = request.form['preventiveAction']
        actions = []
        current_app.logger.info(request.form)
        #actionCount = (len(request.form) - 13) / 3
        actionCount = int(request.form['actionsCount'])
        current_app.logger.info("=== ACTIONCOUNT %s"%type(actionCount))
        for i in range(actionCount):
            #actions.append([request.form['actions-'+str(i)+'-actionNum'],request.form['actions-'+str(i)+'-actionContent'],request.form['actions-'+str(i)+'-actionOwner'],request.form['actions-'+str(i)+'-actionTargetDate']])
            if 'actions-'+str(i)+'-actionTargetCheckbox' in request.form:
                checkboxAction = 'true'
            else:
                checkboxAction = 'false'
            actions.append([request.form['actions-'+str(i)+'-actionContent'],request.form['actions-'+str(i)+'-actionOwner'],request.form['actions-'+str(i)+'-actionTargetDate'], checkboxAction])
        current_app.logger.info("=== ACTIONS :%s"%actions)
        rca_key = rcaId
        rca_row = [ticketNumber,rcaId,problemId,reportStartDate,reportEndDate,preparedBy,contributors,description,impact,rootcause,detection,restore,preventiveAction,actions]
        if rca_key is not None and rca_key != "":
            rcareport_source_list[rca_key] = rca_row
            with open(rcareport_source_json,"wb") as rcareport_source_file:
                json.dump(rcareport_source_list, rcareport_source_file,indent=4,encoding="utf-8")
    return JsonResponse({"status":"OK"})    

 
def format_esurvey_data(data, satisfaction_group_by):
    by_zone_dic = {}
    by_country_dic = {}
    worldwide_count = 0
    for d in data:
        if d[1] is '':
            continue
        if d[1] not in by_zone_dic:
            by_zone_dic[d[1]] = 0
        by_zone_dic[d[1]] += d[0]
        worldwide_count += d[0]

        if satisfaction_group_by == 'country':
            if d[2] is '':
                continue
            if d[2] not in by_country_dic:
                by_country_dic[d[2]] = 0
            by_country_dic[d[2]] += d[0]    

    if worldwide_count > 0:
        by_zone_dic['WORLDWIDE'] = worldwide_count
    by_zone_dic.update(by_country_dic)

    return by_zone_dic 
 
def format_graph_data_by_country(verysatisfied_data, satisfied_data, unsatisfied_data, veryunsatisfied_data):

    project_upload_path = settings.UPLOAD_FOLDER
    country_details_json = os.path.join(project_upload_path, settings.REPORTING_COUNTRY_DETAILS)
    with open(country_details_json,"r") as country_details_file:
        country_details_list = json.load(country_details_file)

    graph_dic = {}
    if verysatisfied_data != []:
        for d in verysatisfied_data:
            if d[1] is '' or d[2] is '':
                continue
            #check if zone in dic
            if d[1] not in graph_dic:
                graph_dic[d[1]] = {}
            #check if country set for zone
            countryName = country_details_list[d[2]]['area']['customData'] if d[2] in country_details_list else d[2]
            if countryName not in graph_dic[d[1]]:
                graph_dic[d[1]][countryName] = {}
                graph_dic[d[1]][countryName]['code'] = d[2]
            if 'satisfied' not in graph_dic[d[1]][countryName]:
                graph_dic[d[1]][countryName]['satisfied'] = 0
                
            graph_dic[d[1]][countryName]['satisfied'] += d[0]
     
    if satisfied_data != []:
        for d in satisfied_data:
            if d[1] is '' or d[2] is '':
                continue
            #check if zone in dic
            if d[1] not in graph_dic:
                graph_dic[d[1]] = {}
            #check if country set for zone
            countryName = country_details_list[d[2]]['area']['customData'] if d[2] in country_details_list else d[2]
            if countryName not in graph_dic[d[1]]:
                graph_dic[d[1]][countryName] = {}
                graph_dic[d[1]][countryName]['code'] = d[2]
            if 'satisfied' not in graph_dic[d[1]][countryName]:
                graph_dic[d[1]][countryName]['satisfied'] = 0
                
            graph_dic[d[1]][countryName]['satisfied'] += d[0]
     
    if unsatisfied_data != []:
        for d in unsatisfied_data:
            if d[1] is '' or d[2] is '':
                continue
            #check if zone in dic
            if d[1] not in graph_dic:
                graph_dic[d[1]] = {}
            #check if country set for zone
            countryName = country_details_list[d[2]]['area']['customData'] if d[2] in country_details_list else d[2]
            if countryName not in graph_dic[d[1]]:
                graph_dic[d[1]][countryName] = {}
                graph_dic[d[1]][countryName]['code'] = d[2]
            if 'unsatisfied' not in graph_dic[d[1]][countryName]:
                graph_dic[d[1]][countryName]['unsatisfied'] = 0
                
            graph_dic[d[1]][countryName]['unsatisfied'] += d[0]
        
    if veryunsatisfied_data != []:
        for d in veryunsatisfied_data:
            if d[1] is '' or d[2] is '':
                continue
            #check if zone in dic
            if d[1] not in graph_dic:
                graph_dic[d[1]] = {}
            #check if country set for zone
            countryName = country_details_list[d[2]]['area']['customData'] if d[2] in country_details_list else d[2]
            if countryName not in graph_dic[d[1]]:
                graph_dic[d[1]][countryName] = {}
                graph_dic[d[1]][countryName]['code'] = d[2]
            if 'veryunsatisfied' not in graph_dic[d[1]][countryName]:
                graph_dic[d[1]][countryName]['veryunsatisfied'] = 0
                
            graph_dic[d[1]][countryName]['veryunsatisfied'] += d[0]
        
    return graph_dic


def format_graph_data_by_zone(total_satisfied_data, formatted_unsatisfied_data, formatted_veryunsatisfied_data, total_closed_tickets_count):

    project_upload_path = settings.UPLOAD_FOLDER
    country_details_json = os.path.join(project_upload_path, settings.REPORTING_COUNTRY_DETAILS)
    with open(country_details_json,"r") as country_details_file:
        country_details_list = json.load(country_details_file)

    graph_pie_values = {}
    total_survey_count = {}
    for key, value in total_satisfied_data.items():
        if key not in graph_pie_values:
            graph_pie_values[key] = []
        graph_pie_values[key].append({"category": "Satisfied", "value": value, "color" : "#84b761"})
        if key not in total_survey_count:
            total_survey_count[key] = 0
        total_survey_count[key] += value

    for key, value in formatted_unsatisfied_data.items():
        if key not in graph_pie_values:
            graph_pie_values[key] = []
        graph_pie_values[key].append({"category": "Unsatisfied", "value": value, "color" : "#ff8738"})
        if key not in total_survey_count:
            total_survey_count[key] = 0
        total_survey_count[key] += value

    for key, value in formatted_veryunsatisfied_data.items():
        if key not in graph_pie_values:
            graph_dic[key] = []
        graph_pie_values[key].append({"category": "Very Unsatisfied", "value": value, "color" : "#cc0000"})
        if key not in total_survey_count:
            total_survey_count[key] = 0
        total_survey_count[key] += value

    graph_dic = {"areas" : [], "country_images": [], "zone_images" : [], "satisfaction_rate_zone_table" : {}, "answer_rate_zone_table" : {}}
    logger.info("=========== ESURVEY ZONE SATISFIED :: %s"%total_satisfied_data)
    if 'NA' in graph_pie_values and 'image' in country_details_list['NA']:
        na_image = country_details_list['NA']["image"]
        na_image['pie']['dataProvider'] = graph_pie_values['NA']
        graph_dic["satisfaction_rate_zone_table"]["AMERICAS"] = total_satisfied_data['NA'] * 100.0 / total_closed_tickets_count[0] if total_closed_tickets_count[0] != 0 else 0
        graph_dic["answer_rate_zone_table"]["AMERICAS"] = total_survey_count['NA'] * 100.0 / total_closed_tickets_count[0] if total_closed_tickets_count[0] != 0 else 0
        graph_dic["zone_images"].append(na_image)

    if 'EMEA' in graph_pie_values and 'image' in country_details_list['EMEA']:
        emea_image = country_details_list['EMEA']['image']
        emea_image['pie']['dataProvider'] = graph_pie_values['EMEA']
        graph_dic["satisfaction_rate_zone_table"]["EMEA"] = total_satisfied_data['EMEA'] * 100.0 / total_closed_tickets_count[2] if total_closed_tickets_count[2] != 0 else 0
        graph_dic["answer_rate_zone_table"]["EMEA"] = total_survey_count['EMEA'] * 100.0 / total_closed_tickets_count[2] if total_closed_tickets_count[2] != 0 else 0
        graph_dic["zone_images"].append(emea_image)

    if 'APAC' in graph_pie_values and 'image' in country_details_list['APAC']:
        apac_image = country_details_list['APAC']['image']
        apac_image['pie']['dataProvider'] = graph_pie_values['APAC']
        graph_dic["satisfaction_rate_zone_table"]["APAC"] = total_satisfied_data['APAC'] * 100.0 / total_closed_tickets_count[1] if total_closed_tickets_count[1] != 0 else 0
        graph_dic["answer_rate_zone_table"]["APAC"] = total_survey_count['APAC'] * 100.0 / total_closed_tickets_count[1] if total_closed_tickets_count[1] != 0 else 0
        graph_dic["zone_images"].append(apac_image)

    if 'WORLDWIDE' in graph_pie_values and 'image' in country_details_list['WORLDWIDE']:
        worldwide_image = country_details_list['WORLDWIDE']['image']
        worldwide_image['pie']['dataProvider'] = graph_pie_values['WORLDWIDE']
        graph_dic["satisfaction_rate_zone_table"]["WORLDWIDE"] = total_satisfied_data['WORLDWIDE'] * 100.0 / total_closed_tickets_count[3] if total_closed_tickets_count[3] != 0 else 0
        graph_dic["answer_rate_zone_table"]["WORLDWIDE"] = total_survey_count['WORLDWIDE'] * 100.0 / total_closed_tickets_count[3] if total_closed_tickets_count[3] != 0 else 0
        graph_dic["zone_images"].append(worldwide_image)
    
    return graph_dic

    
def format_graph_data_by_closegroup(verysatisfied_data, satisfied_data, unsatisfied_data, veryunsatisfied_data):
    
    group_dic = {}
    if verysatisfied_data != []:
        for d in verysatisfied_data:
            if d[1] is '':
                continue
            #check if zone in dic
            if d[1] not in group_dic:
                group_dic[d[1]] = {}
            if 'satisfied' not in group_dic[d[1]]:
                group_dic[d[1]]['satisfied'] = 0
                
            group_dic[d[1]]['satisfied'] += d[0]
     
    if satisfied_data != []:
        for d in satisfied_data:
            if d[1] is '':
                continue
            #check if zone in dic
            if d[1] not in group_dic:
                group_dic[d[1]] = {}
            if 'satisfied' not in group_dic[d[1]]:
                group_dic[d[1]]['satisfied'] = 0
                
            group_dic[d[1]]['satisfied'] += d[0]
     
    if unsatisfied_data != []:
        for d in unsatisfied_data:
            if d[1] is '':
                continue
            #check if zone in dic
            if d[1] not in group_dic:
                group_dic[d[1]] = {}
            if 'unsatisfied' not in group_dic[d[1]]:
                group_dic[d[1]]['unsatisfied'] = 0
                
            group_dic[d[1]]['unsatisfied'] += d[0]
        
    if veryunsatisfied_data != []:
        for d in veryunsatisfied_data:
            if d[1] is '':
                continue
            #check if zone in dic
            if d[1] not in group_dic:
                group_dic[d[1]] = {}
            if 'veryunsatisfied' not in group_dic[d[1]]:
                group_dic[d[1]]['veryunsatisfied'] = 0
                
            group_dic[d[1]]['veryunsatisfied'] += d[0]
    
    formatted_dic = {}
    for group, data in group_dic.items():
        formatted_dic[group] = []
        if('satisfied' in data):
            formatted_dic[group].append({"category" : "Satisfied", "value" : data['satisfied'], "color" : "#84b761", "group" : group})
        if('unsatisfied' in data):
            formatted_dic[group].append({"category" : "Unsatisfied", "value" : data['unsatisfied'], "color" : "#ff8738", "group" : group})
        if('veryunsatisfied' in data):
            formatted_dic[group].append({"category" : "Very Unsatisfied", "value" : data['veryunsatisfied'], "color" : "#cc0000", "group" : group})

    logger.info("====== ESURVEY :: GROUP DIC :: %s"%group_dic)
    
    TEAMS = settings.TEAMS
    tsc_teams = [item for x in TEAMS['TSC'] for item in x['team']] #['WW_TM_L3_TSC_AS400', 'WW_TM_L3_TSC_BI_DATABASE', 'WW_TM_L3_TSC_MIDDLEWARE', 'WW_TM_L1_SOC', 'WW_TM_L3_TSC_WEB_APPLIS', 'WW_TM_L3_TSC_WINDOWS_SAFE_SCOL', 'WW_TM_L3_TSC_UNIX']
    itl4_teams = [item for x in TEAMS['ITL4'] for item in x['team']]
    sd_teams = [item for x in TEAMS['SERVICE_DESK'] for item in x['team']]
    proxi_emea_teams = [item for x in TEAMS['PROXI_EMEA'] for item in x['team']]
    proxi_america_teams = [item for x in TEAMS['PROXI_AMERICA'] for item in x['team']]
    proxi_global_asia_teams = [item for x in TEAMS['PROXI_GLOBAL_ASIE'] for item in x['team']]
    proxi_berluti_teams = [item for x in TEAMS['PROXI_BERLUTI'] for item in x['team']]
    proxi_fondation_teams = [item for x in TEAMS['PROXI_FONDATION'] for item in x['team']]
    ams_teams = [item for x in TEAMS['AMS'] for item in x['team']]
    network_teams = [item for x in TEAMS['NETWORK'] for item in x['team']]
 
    graph_dic = {}
    
    for group in formatted_dic.keys():
        if group in tsc_teams:
            if 'tsc' not in graph_dic:
                graph_dic['tsc'] = {}
            graph_dic['tsc'][group] = {"chart_data" : formatted_dic[group], "group_name" : group.replace('_',' ')} #[x['name'] for x in TEAMS['TSC'] if group in x['team']][0]  - to get group['name] from settings
        elif group in sd_teams:
            if 'service desk' not in graph_dic:
                graph_dic['service desk'] = {}
            graph_dic['service desk'][group] = {"chart_data" : formatted_dic[group], "group_name" : group.replace('_',' ')}
        elif group in proxi_emea_teams:
            if 'proximity emea' not in graph_dic:
                graph_dic['proximity emea'] = {}
            graph_dic['proximity emea'][group] = {"chart_data" : formatted_dic[group], "group_name" : group.replace('_',' ')}
        elif group in proxi_america_teams:
            if 'proximity america' not in graph_dic:
                graph_dic['proximity america'] = {}
            graph_dic['proximity america'][group] = {"chart_data" : formatted_dic[group], "group_name" : group.replace('_',' ')}
        elif group in proxi_global_asia_teams:
            if 'proximity asia' not in graph_dic:
                graph_dic['proximity asia'] = {}
            graph_dic['proximity asia'][group] = {"chart_data" : formatted_dic[group], "group_name" : group.replace('_',' ')}
        elif group in proxi_berluti_teams:
            if 'proximity berluti' not in graph_dic:
                graph_dic['proximity berluti'] = {}
            graph_dic['proximity berluti'][group] = {"chart_data" : formatted_dic[group], "group_name" : group.replace('_',' ')}
        elif group in proxi_fondation_teams:
            if 'proximity fondation' not in graph_dic:
                graph_dic['proximity fondation'] = {}
            graph_dic['proximity fondation'][group] = {"chart_data" : formatted_dic[group], "group_name" : group.replace('_',' ')}
        elif group in itl4_teams:
            if 'itl4' not in graph_dic:
                graph_dic['itl4'] = {}
            graph_dic['itl4'][group] = {"chart_data" : formatted_dic[group], "group_name" : group.replace('_',' ')}
        elif group in ams_teams:
            if 'ams' not in graph_dic:
                graph_dic['ams'] = {}
            graph_dic['ams'][group] = {"chart_data" : formatted_dic[group], "group_name" : group.replace('_',' ')}
        elif group in network_teams:
            if 'network' not in graph_dic:
                graph_dic['network'] = {}
            graph_dic['network'][group] = {"chart_data" : formatted_dic[group], "group_name" : group.replace('_',' ')}
        else:
            if 'others' not in graph_dic:
                graph_dic['others'] = {}
            graph_dic['others'][group] = {"chart_data" : formatted_dic[group], "group_name" : group.replace('_',' ')}
            
    return graph_dic 
 
@login_required
def reporting_esurvey(request):
    
    #import pdb;pdb.set_trace()
    if not request.body:
        satisfaction_group_by = "country"
    elif "satisfactionGroupBy" not in json.loads(request.body):
        satisfaction_group_by = "country"
    else:
        satisfaction_group_by = json.loads(request.body)['satisfactionGroupBy']
       
    #satisfaction_group_by = "country" if request.json not None or "satisfactionGroupBy" not in request.json else request.json["satisfactionGroupBy"]
    logger.info("======= ESURVEY DATA :: satisfaction_group_by %s"%satisfaction_group_by)
    #startDate = (datetime.datetime.now() - datetime.timedelta(days=1)).date() if not request.body else json.loads(request.body)['startDate']
    #endDate = datetime.datetime.now().date() if not request.body else json.loads(request.body)['endDate']
    startDate = datetime.date(2018, 12, 1) if not request.body else json.loads(request.body)['startDate']
    endDate = datetime.date(2018, 12, 31) if not request.body else json.loads(request.body)['endDate']
    logger.info("=================== START DATE***************** :: %s"%startDate)
   
    
    verysatisfied_data = esurvey.get_esurvey_data_summary('Very satisfied', startDate, endDate, satisfaction_group_by)
    satisfied_data = esurvey.get_esurvey_data_summary('Satisfied', startDate, endDate, satisfaction_group_by)
    unsatisfied_data = esurvey.get_esurvey_data_summary('Unsatisfied', startDate, endDate, satisfaction_group_by)
    veryunsatisfied_data = esurvey.get_esurvey_data_summary('Very unsatisfied', startDate, endDate, satisfaction_group_by)
    
    if satisfaction_group_by == 'country':
        graph_dic = format_graph_data_by_country(verysatisfied_data, satisfied_data, unsatisfied_data, veryunsatisfied_data)
        stats_data = esurvey.get_stats_data_by_zone(startDate, endDate)
        logger.info("=================== ESURVEY STATS :: %s"%stats_data)
        if stats_data:
            graph_dic['stats'] = stats_data
    elif satisfaction_group_by == 'zone':
        
        #create the map data
        formatted_verysatisfied_data = format_esurvey_data(verysatisfied_data, satisfaction_group_by)
        formatted_satisfied_data = format_esurvey_data(satisfied_data, satisfaction_group_by)
        formatted_unsatisfied_data = format_esurvey_data(unsatisfied_data, satisfaction_group_by)
        formatted_veryunsatisfied_data = format_esurvey_data(veryunsatisfied_data, satisfaction_group_by)
        logger.info("======= ESURVEY DATA :: %s"%formatted_verysatisfied_data) 
        
        #combine very satisfied and satisfied data
        total_satisfied_data = {}
        for key, value in formatted_verysatisfied_data.items():
            if key not in total_satisfied_data:
                total_satisfied_data[key] = 0
            total_satisfied_data[key] += value
        for key, value in formatted_satisfied_data.items():
            if key not in total_satisfied_data:
                total_satisfied_data[key] = 0
            total_satisfied_data[key] += value
       
        total_closed_tickets_count = esurvey.get_total_closed_tickets_count(startDate, endDate)
 
        graph_dic = format_graph_data_by_zone(total_satisfied_data, formatted_unsatisfied_data, formatted_veryunsatisfied_data, total_closed_tickets_count)
    else:
        graph_dic = format_graph_data_by_closegroup(verysatisfied_data, satisfied_data, unsatisfied_data, veryunsatisfied_data)

    if request.method == "GET":
        return render(request, 'users/reporting_esurvey.html', {'graph_data' : graph_dic, 'satisfaction_group_by' : satisfaction_group_by})
    else: 
        return JsonResponse({"status":'OK', "graph_data": graph_dic, "satisfaction_group_by": satisfaction_group_by}) 

@login_required
def get_esurvey_details(request):
    request_data = json.loads(request.body)
    mode = request_data['mode']
    mode_param = request_data['modeParam']
    satisfaction_level = request_data['satisfactionLevel']
    start_date = request_data['startDate']
    end_date = request_data['endDate']
    
    details_list = esurvey.get_esurvey_data_details(mode, mode_param, satisfaction_level, start_date, end_date)

    return JsonResponse({"status":'OK', "details_list": details_list})        
        
@login_required
def closed_tickets(request):
    dt_now = datetime.datetime.now()
    local_tz = request.GET.get('local_tz', None)
    # get current month
    month_now = int(datetime.datetime.now().strftime("%m"))
    # get valid months
    #r_months = [i for i in range(12) if i<month_now and i!=0 ]
    r_months = [i for i in range(1,13)]

    months_list = dict((k,v) for k,v in enumerate(calendar.month_abbr) if k in r_months)
    # sort months
    months_list = collections.OrderedDict(sorted(months_list.items()))
    
    # get applications list
    applications = ct.get_applications_list(request)
    open_checkbox, close_checkbox, bounce_check = '', '', ''
    closed_list,open_selected_grp,closed_selected_grp,assigned_selected_grp,applications_selected_grp = [],[],[],[],[]
    sla_selection = 'allsla'
    
    search_by_type = 'date'
    form = GeneralForm(request.GET)
    
    if request.method == 'POST':

        #bounce_check = request.form.get('bounce_check')
        form = GeneralForm(data=request.POST)
        open_check_tickets = request.POST.get('open_check_tickets') 
        close_check_tickets = request.POST.get('close_check_tickets') 
        open_search_date = request.POST.get('open_search_date')
        close_search_date = request.POST.get('close_search_date')
        logger.info("OPEN SEARCH DATE:%s"%open_search_date)
        logger.info("CLOSE SEARCH DATE:%s"%close_search_date)
        open_start_date = datetime.datetime.strptime(open_search_date.split(' - ')[0], '%d/%m/%Y') if open_search_date else None
        open_end_date = datetime.datetime.strptime(open_search_date.split(' - ')[1], '%d/%m/%Y') + datetime.timedelta(days=1) if open_search_date else None
        close_start_date = datetime.datetime.strptime(close_search_date.split(' - ')[0], '%d/%m/%Y') if close_search_date else None
        close_end_date = datetime.datetime.strptime(close_search_date.split(' - ')[1], '%d/%m/%Y') + datetime.timedelta(days=1) if close_search_date else None
        
        # group_id = request.form.getlist('groups_by_date')
        groups_by_closedgroup = request.POST.getlist('groups_by_closedgroup')
        groups_by_opengroup = request.POST.getlist('groups_by_opengroup')
        groups_by_assignedgroup = request.POST.getlist('groups_by_assignedgroup')
        groups_by_applications = request.POST.getlist('groups_by_applications')
        sla_selection = request.POST.get('sla_selection')
        
        filters_dic = {}
        #filters_dic["bounce_check"] = bounce_check
        filters_dic["open_check_tickets"] = open_check_tickets
        filters_dic["close_check_tickets"] = close_check_tickets
        filters_dic["open_start_date"] = open_start_date
        filters_dic["open_end_date"] = open_end_date
        filters_dic["close_start_date"] = close_start_date
        filters_dic["close_end_date"] = close_end_date
        filters_dic["groups_by_closedgroup"] = groups_by_closedgroup
        filters_dic["groups_by_opengroup"] = groups_by_opengroup
        filters_dic["groups_by_assignedgroup"] = groups_by_assignedgroup
        filters_dic["groups_by_applications"] = groups_by_applications
        filters_dic["sla_selection"] = sla_selection
        
        #current_app.logger.info("FILTERS DIC:%s"%filters_dic)
        
        #selected_grp = map(int, group_id)
        open_checkbox = open_check_tickets
        close_checkbox = close_check_tickets
        open_selected_grp = [int(i) for i in groups_by_opengroup] #map(int, groups_by_opengroup)
        closed_selected_grp = [int(i) for i in groups_by_closedgroup] #map(int, groups_by_closedgroup)
        assigned_selected_grp = [int(i) for i in groups_by_assignedgroup] #map(int, groups_by_assignedgroup)
        applications_selected_grp = [str(i) for i in groups_by_applications] #map(str, groups_by_applications)
 
        #closed_list = ct.get_closed_tickets(request, filters_dic, local_tz)
        closed_list = ct.get_closed_tickets(request, filters_dic, local_tz,'itsm')

        ##merge data from deis and itsm
        #closed_list['OSLA_DATA'].update(closed_list_itsm['OSLA_DATA'])  
        osla_stats = ct.get_osla_stats(closed_list['OSLA_DATA'])
        
        #closed_list['SLA1'] = closed_list['SLA1'] +closed_list_itsm['SLA1']
        #closed_list['SLA2'] = closed_list['SLA2'] +closed_list_itsm['SLA2']
        #closed_list['OSLA'] = closed_list['OSLA'] +closed_list_itsm['OSLA']
        closed_list['SLA1_P'] = "%.2f" % (float(closed_list['SLA1']) /(closed_list['SLA1']+closed_list['SLA2']+closed_list['OSLA'] )*100) if (closed_list['SLA1']+closed_list['SLA2']+closed_list['OSLA'] ) else 0
        closed_list['SLA2_P'] = "%.2f" % (float(closed_list['SLA2']) /(closed_list['SLA1']+closed_list['SLA2']+closed_list['OSLA'] )*100) if (closed_list['SLA1']+closed_list['SLA2']+closed_list['OSLA'] ) else 0
        closed_list['OSLA_P'] = "%.2f" % (float(closed_list['OSLA']) /(closed_list['SLA1']+closed_list['SLA2']+closed_list['OSLA'] )*100) if (closed_list['SLA1']+closed_list['SLA2']+closed_list['OSLA'] ) else 0
        closed_list['OSLA_DATA'] = osla_stats
        closed_list['closed_data'] = closed_list['closed_data'] #+ closed_list_itsm['closed_data']
        closed_list['open_tickets_count'] = closed_list['open_tickets_count'] #+ closed_list_itsm['open_tickets_count']
        closed_list['closed_tickets_count'] = closed_list['closed_tickets_count'] #+ closed_list_itsm['closed_tickets_count']
        logger.info("closed_list:%s"%closed_list['OSLA_DATA'])
      
    return render(request, 'users/closed_tickets.html', {'form' : form, 'dt_now' : dt_now, 'closed_list' : closed_list, 'open_checkbox' : open_checkbox, 'close_checkbox' : close_checkbox, 'bounce_check' : bounce_check, 'open_selected_grp' : open_selected_grp, 'closed_selected_grp' : closed_selected_grp, 'assigned_selected_grp' : assigned_selected_grp, 'applications_selected_grp' : applications_selected_grp, 'applications' : applications, 'sla_selection' : sla_selection})


@login_required
def reporting_topten(request):
    if not request.body:
        zone = "worldwide"
        month = datetime.datetime.now().strftime("%Y%m%d")
        month = '20181101'
    else:
        request_data = json.loads(request.body) 
        zone = request_data['zone']
        month= '20181101' #request_data['month']
    data = topten.get_topten_data(zone,month)

    if request.method == "GET":
        return render(request, 'users/reporting_topten.html', {'data' : data})
    else:
        return JsonResponse({"status":'OK',"data": data})
        
@login_required
def save_topten(request):
    if not request.body:
        return JsonResponse({"status":'NO DATA'})

    request_data = json.loads(request.body) 
    row_id = request_data['id']
    reasons = request_data['reason']
    actions = request_data['action']
    owners = request_data['owner']

    status = topten.save_topten_data(row_id, reasons, actions, owners)

    if status:
        return JsonResponse({"status":'OK'})
    else:
        return JsonResponse({"status":'ERROR'})        

@login_required
def reporting_ticket_analysis(request, ticket_analysis_type=None):
    dt_now = datetime.datetime.now()
    local_tz = request.GET.get('local_tz', None)
    source_selected_grp, target_selected_grp = [], []
    ticket_analysis_data = {}

    form = GeneralForm(request.GET)
    logger.info('MethodType:%s'%request.method)
    logger.info('Ticket analysis type:%s'%ticket_analysis_type)
    
    #Default data to be shown on first page load
    if request.method == 'GET':
        if ticket_analysis_type == "escalation":
            source_selected_grp = [0, 1, 2, 3, 5, 97]
            target_selected_grp = [42, 43, 45, 44, 47, 48, 49, 95, 96, 87]
            start_date = datetime.datetime(2019, 1, 1, 0, 0)
            end_date = datetime.datetime(2019, 2, 1, 0, 0)
            ticket_analysis_data = tanalysis.escalation(request, source_selected_grp,target_selected_grp,start_date,end_date,local_tz)
    
    
    if request.method == 'POST':
        form = GeneralForm(data=request.POST)
        date_range = request.POST.get('search_date')
        logger.info('frontend_data DN : %s' % form.data)
        source_grp = request.POST.getlist('groups_by_sourcegroup') if request.POST.getlist('groups_by_sourcegroup') != '' else []
        target_grp = request.POST.getlist('groups_by_targetgroup') if request.POST.getlist('groups_by_targetgroup') != '' else []
        
        source_selected_grp = [int(i) for i in source_grp] if source_grp else []
        target_selected_grp = [int(i) for i in target_grp] if target_grp else []

        #start_date = datetime.datetime.strptime(date_range.split(' - ')[0], '%d/%m/%Y').strftime('%Y-%m-%d') if date_range else None
        #end_date = datetime.datetime.strptime(date_range.split(' - ')[1], '%d/%m/%Y').strftime('%Y-%m-%d') if date_range else None
        logger.info('local_tz : %s' % local_tz)
        start_date = datetime.datetime.strptime(date_range.split(' - ')[0], '%d/%m/%Y') if date_range else None
        end_date = datetime.datetime.strptime(date_range.split(' - ')[1], '%d/%m/%Y') + datetime.timedelta(days=1) if date_range else None
        
        # TODO
        #start_date = cmr._convert_to_local_time(local_tz, settings.TIMEZONE_IVANTI_DB_INC, start_date)
        #end_date = cmr._convert_to_local_time(local_tz, settings.TIMEZONE_IVANTI_DB_INC, end_date)

        #import pdb;pdb.set_trace()
        if ticket_analysis_type == "escalation":
            ticket_analysis_data = tanalysis.escalation(request, source_selected_grp,target_selected_grp,start_date,end_date,local_tz)
        if ticket_analysis_type == "reopen":
            ticket_analysis_data = tanalysis.reopen(request, source_selected_grp,target_selected_grp,start_date,end_date,local_tz)
        if ticket_analysis_type == "bouncing":
            ticket_analysis_data = tanalysis.bouncing(request, source_selected_grp,start_date,end_date,local_tz)
        if ticket_analysis_type == "callback":
            ticket_analysis_data = tanalysis.callback(request, source_selected_grp,target_selected_grp,start_date,end_date,local_tz)
    
    src_or_trg = source_selected_grp or target_selected_grp
    
    #import pdb;pdb.set_trace()
    return render(request, 'users/reporting_ticket_analysis.html', {'form' : form, 'source_selected_grp' : source_selected_grp, 'target_selected_grp' : target_selected_grp, 'ticket_analysis_type' : ticket_analysis_type, 'ticket_analysis_data' : ticket_analysis_data, 'src_or_trg' : src_or_trg})   


@login_required
def reporting_ticket_stats(request):

    dt_now = datetime.datetime.now()
    local_tz = request.GET.get('local_tz', None)
    
    transfer_ticket_check = 1;
    #stats_data, ticket_stats_selected_grp = [], []
    
    form = GeneralForm(request.GET)
    
    #Default data to be shown on first page load
    if request.method == 'GET':
        transfer_ticket_check = 'on'
        ticket_stats_selected_grp = [0, 1, 2, 3, 5, 97]
        filters_dic = {'transfer_ticket_check': 'on', 'start_date': datetime.datetime(2018, 12, 1, 0, 0), 'end_date': datetime.datetime(2019, 1, 1, 0, 0), 'ticket_stats_group': [0, 1, 2, 3, 5, 97]}
        stats_data = tstats.get_stats_data_summary(filters_dic, local_tz)
    
    if request.method == 'POST':

        form = GeneralForm(data=request.POST)
        #bounce_check = request.form.get('bounce_check')
        transfer_ticket_check = request.POST.get('transfer_ticket_check') 
        search_date = request.POST.get('search_date')
        logger.info("SEARCH DATE:%s"%search_date)
        start_date = datetime.datetime.strptime(search_date.split(' - ')[0], '%d/%m/%Y') if search_date else None
        end_date = datetime.datetime.strptime(search_date.split(' - ')[1], '%d/%m/%Y') + datetime.timedelta(days=1) if search_date else None
        
        #convert dates based on timezone
        logger.info('========= TIMEZONE :: %s' % local_tz)
        
        # group_id = request.form.getlist('groups_by_date')
        ticket_stats_group = request.POST.getlist('ticket_stats_group')
        ticket_stats_selected_grp = [int(i) for i in ticket_stats_group] #map(int, ticket_stats_group)
        
        filters_dic = {}
        filters_dic["transfer_ticket_check"] = transfer_ticket_check
        filters_dic["start_date"] = start_date
        filters_dic["end_date"] = end_date
        filters_dic["ticket_stats_group"] = ticket_stats_selected_grp
        
        logger.info("FILTERS DIC:%s"%filters_dic)
        
        stats_data = tstats.get_stats_data_summary(filters_dic, local_tz)
    
    return render(request, 'users/reporting_ticket_stats.html', {'form' : form, 'dt_now' : dt_now, 'stats_data' : stats_data, 'transfer_ticket_check' : transfer_ticket_check, 'ticket_stats_selected_grp' : ticket_stats_selected_grp})
    
@login_required
def reporting_integration_view(request, view_mode = 'backlog'):
    dt_now = datetime.datetime.now()
    # default local tz set to Paris time
    local_tz = request.GET.get('local_tz', 'Europe/Paris') if request.method == 'GET' else json.loads(request.body)['local_tz']
    logger.info("======== local_tz :: %s"%local_tz)
    data = []
    
    #if request.method == 'GET':
    data = intview.get_integration_view_data(request, view_mode, local_tz)
    stats = intview.get_integration_view_stats(request, local_tz)
    return render(request, 'users/reporting_integration_view.html', {'data' : data, 'stats' : stats, 'view_mode' : view_mode})    
    
@login_required
def reporting_tsccalendar(request):
    #data = changecalendar.get_change_calendar_data('TSC')
    non_validated_data = [] #data['non_validated']
    validated_data = [] #data['validated']
   
    return render(request, 'users/reporting_tsccalendar.html', {'validated' : validated_data, 'non_validated' : SafeString(non_validated_data)})

@login_required
def reporting_nestcalendar(request):
    data = changecalendar.get_change_calendar_data('NEST')
    non_validated_data = data['non_validated']
    validated_data = data['validated']
    
    return render(request, 'users/reporting_nestcalendar.html', {'validated' : validated_data, 'non_validated' : non_validated_data})    
    
@login_required
def hypervision_oneretail(request):
    url = settings.ONERETAIL_MAP_URL
    return render(request, 'users/hypervision_oneretail.html', {'url' : url})    
    
@login_required
def query_management(request):
    
    if request.method == 'GET':
        query_list = qm.get_user_accessable_query_list(request)
        return render(request, 'users/query_management.html', {'query_list' : query_list})
    
    query_data = qm.get_query_by_title(request.json['title'])
    
    return JsonResponse({"data": query_data})  
    
@login_required
def query_save(request):
    #import pdb;pdb.set_trace()
    request_data = json.loads(request.body)
    query_settings = request_data['query_settings']
    query_list = []
    
    remove_comments1 = re.sub(r"[--](.*)[\r\n]", " ", query_settings['querytext']) #remove comments starting with --
    remove_comments2 = re.sub(r"/\*[^\*]+\*/", " ", remove_comments1) #remove comments like /* OS LANGUAGE */ and multi-line comments
    #remove_comments2 = re.sub(r"[/*](.*)[*/]", " ", remove_comments1) #remove comments like /* OS LANGUAGE */
    
    #check query for any sql injection
    #restricted_words = ['create', 'show', 'use', 'describe', 'drop', 'insert', 'set', 'update', 'delete', 'flush', 'privileges', 'alter', 'load', 'dump', 'grant', 'truncate', 'commit', 'rollback', 'desc']
    
    #query_text = [rw for rw in restricted_words if rw in query_settings['querytext'].lower()]
    query_text = re.search(r'(\bcreate\b|\bshow\b|\buse\b|\bdescribe\b|\bdrop\b|\binsert\b|\bset\b|\bupdate\b|\bdelete\b|\bflush\b|\bprivileges\b|\balter\b|\bload\b|\bdump\b|\bgrant\b|\btruncate\b|\bcommit\b|\brollback\b|\bdesc\b)', remove_comments2.lower())
    print(query_text)
    query_text = query_text.groups() if query_text else query_text
    if query_text:
        restricted_words = list(query_text)
        result = {"status" : 'ERROR', "restricted_words" : restricted_words }
    else:
        save_status = qm.save_query_settings(query_settings)
        
        status = "OK" if save_status else "FAIL"
        query_list = qm.get_user_accessable_query_list(request)
        
        result = {"status" : status, "query_list" : query_list}
    return JsonResponse(result)    
    
@login_required
def query_edit(request):
    request_data = json.loads(request.body)
    query_title = request_data['query_title']
    query_details = qm.get_query_details_by_title(query_title)
    
    result = {"status" : "OK", "query_details" : query_details}
    return JsonResponse(result)  

@login_required
def query_delete(request):
    request_data = json.loads(request.body)
    query_title = request_data['query_title']
    delete_status = qm.delete_query(query_title)
    
    status = "OK" if delete_status else "FAIL"
    query_list = qm.get_user_accessable_query_list(request)
    
    result = {"status" : status, "query_list" : query_list}
    return JsonResponse(result)    

@login_required
def content_management(request):
    
    if request.method == 'GET':
        content_list = cm.get_user_accessable_content_list(request)
        return render(request, 'users/content_management.html', {'content_list' : content_list})
    
    request_data = json.loads(request.body)
    title = request_data['title']
    content_data = cm.get_content_by_title(title)
    
    return JsonResponse({"data": content_data})   
    
    
@login_required
def content_save(request):
    request_data = json.loads(request.body)
    content_settings = request_data['content_settings']
    content_list = []
    
    save_status = cm.save_content_settings(content_settings)
    
    status = "OK" if save_status else "ERROR"
    content_list = cm.get_user_accessable_content_list(request)
    
    result = {"status" : status, "content_list" : content_list}
    
    return JsonResponse(result)
    
@login_required
def content_edit(request):
    request_data = json.loads(request.body)
    content_title = request_data['content_title']
    content_details = cm.get_content_details_by_title(content_title)
    
    result = {"status" : "OK", "content_details" : content_details}
    return JsonResponse(result)
    
@login_required
def content_delete(request):
    request_data = json.loads(request.body)
    content_title = request_data['content_title']
    delete_status = cm.delete_content(content_title)
    
    status = "OK" if delete_status else "FAIL"
    content_list = cm.get_user_accessable_content_list(request)
    
    result = {"status" : status, "content_list" : content_list}
    return JsonResponse(result)   

@login_required
def cmdb_json(request):
    json_url = os.path.join(settings.UPLOAD_FOLDER,'device_list.json')
    data = json.load(open(json_url))
    return JsonResponse(data, safe=False) 
    #return JsonResponse(serializers.serialize('json', data), safe=False)
    
@login_required
def oneretail_json(request):
    json_url = os.path.join(settings.UPLOAD_FOLDER,'oneretail_device_list.json')
    data = json.load(open(json_url))
    return JsonResponse(data, safe=False)  
    
@login_required
def tickets_notes_json(request):

    notes = None
    local_tz = request.GET.get('local_tz', None)

    if request.method == 'POST':
        request_data = json.loads(request.body)
        ticket_no = request_data['ticket_no']
        ticket_seq = request_data['ticket_seq']
        isclosed = request_data['isclosed']
        logger.info("==== TICKETS NOTES JSON :: %s"%request_data) 
        # notes = ot.get_notes(ticket_no, isclosed, local_tz, 'deis') if len(ticket_no)==7 else ot.get_notes(ticket_no, isclosed, local_tz, 'itsm')
        notes = ot.get_notes(request, ticket_no, ticket_seq, isclosed, local_tz, 'itsm')

    logger.info('noted: %s' % json.dumps(notes, indent=4, sort_keys=True, default=lambda x:str(x)))
    return JsonResponse(notes, safe=False) 
    #return JsonResponse({'notes' : notes}) 
    #return json.dumps(notes, indent=4, sort_keys=True, default=lambda x:str(x))    
  
  
''' Dashboard module'''  
  
def backlogs(request, view_type=None, user_state=None):
    """ Gets all the team summary backlogs  """
    
    if not user_state:
        user_state = classicdash.get_user_dashboard_filters(request, request.user.username)
    
    start_time = datetime.datetime.now()
    data = {}
    if view_type == 'sla':
        backlogs = {}
        if 'tsc' in user_state['curr_open_states']:
            '''data['tsc']=ot.get_backlogs_sla_summary_groups(request, 'all_tsc', None, 'deis')
            data['tsc_itsm']=ot.get_backlogs_sla_summary_groups(request, 'all_tsc', None, 'itsm')
            backlogs['tsc'] = cmt._merge_deis_and_itsm_data(data['tsc'],data['tsc_itsm']) #merge deis and itsm data and assign to tsc'''
            backlogs['tsc'] = ot.get_backlogs_sla_summary_groups(request, 'all_tsc', None, 'itsm')
            logger.info("backlogs_sla_tsc:********************************:%s"% backlogs['tsc'])
            
        if 'service_desk' in user_state['curr_open_states']:
            '''data['service_desk']=ot.get_backlogs_sla_summary_groups(request, 'all_sd', None, 'deis')
            data['fo_itsm']=ot.get_backlogs_sla_summary_groups(request, 'all_sd', None, 'itsm')
            backlogs['service_desk'] = cmt._merge_deis_and_itsm_data(data['service_desk'],data['fo_itsm']) #merge deis and itsm data and assign to tsc'''
            backlogs['service_desk']=ot.get_backlogs_sla_summary_groups(request, 'all_sd', None, 'itsm')
            logger.info("backlogs_sla_fo:********************************:%s"% backlogs['service_desk'])
        
        if 'itl4' in user_state['curr_open_states']:
            '''data['itl4']=ot.get_backlogs_sla_summary_groups(request, 'all_itl4', None, 'deis')
            data['itl4_itsm']=ot.get_backlogs_sla_summary_groups(request, 'all_itl4', None, 'itsm')
            backlogs['itl4'] = cmt._merge_deis_and_itsm_data(data['itl4'],data['itl4_itsm']) #merge deis and itsm data'''
            backlogs['itl4']=ot.get_backlogs_sla_summary_groups(request, 'all_itl4', None, 'itsm')
            logger.info("backlogs_sla_itl4:********************************:%s"% backlogs['itl4'])
            
        if 'network' in user_state['curr_open_states']:
            '''data['network']=ot.get_backlogs_sla_summary_groups(request, 'all_network', None, 'deis')
            data['network_itsm']=ot.get_backlogs_sla_summary_groups(request, 'all_network', None, 'itsm')
            backlogs['network'] = cmt._merge_deis_and_itsm_data(data['network'],data['network_itsm']) #merge deis and itsm data'''
            backlogs['network']=ot.get_backlogs_sla_summary_groups('all_network', None, 'itsm')
            logger.info("backlogs_sla_network:********************************:%s"% backlogs['network'])
            
        if 'ams' in user_state['curr_open_states']:
            '''data['ams']=ot.get_backlogs_sla_summary_groups(request, 'all_ams', None, 'deis')
            data['ams_itsm']=ot.get_backlogs_sla_summary_groups(request, 'all_ams', None, 'itsm')
            backlogs['ams'] = cmt._merge_deis_and_itsm_data(data['ams'],data['ams_itsm']) #merge deis and itsm data'''
            backlogs['ams']=ot.get_backlogs_sla_summary_groups(request, 'all_ams', None, 'itsm')
            logger.info("backlogs_sla_ams:********************************:%s"% backlogs['ams'])

        if 'proxi_emea' in user_state['curr_open_states']:
            '''data['proxiemea']=ot.get_backlogs_sla_summary_groups(request, 'all_emea', None, 'deis')
            data['proxiemea_itsm']=ot.get_backlogs_sla_summary_groups(request, 'all_emea',None,'itsm')
            backlogs['proxi_emea'] = cmt._merge_deis_and_itsm_data(data['proxiemea'],data['proxiemea_itsm']) #merge deis and itsm data'''
            backlogs['proxi_emea']=ot.get_backlogs_sla_summary_groups(request, 'all_emea',None,'itsm')
            logger.info("backlogs_sla_proxiemea:********************************:%s"% backlogs['proxi_emea'])

        if 'proxi_america' in user_state['curr_open_states']:
            '''data['proxius']=ot.get_backlogs_sla_summary_groups(request, 'all_america_ds', None, 'deis')
            data['proxius_itsm']=ot.get_backlogs_sla_summary_groups(request, 'all_america_ds',None,'itsm')
            backlogs['proxi_america'] = cmt._merge_deis_and_itsm_data(data['proxius'],data['proxius_itsm']) #merge deis and itsm data'''
            backlogs['proxi_america']=ot.get_backlogs_sla_summary_groups(request, 'all_america_ds',None,'itsm')
            logger.info("backlogs_sla_proxi_america_ds:********************************:%s"% backlogs['proxi_america'])
                
        if 'proxi_global_asie' in user_state['curr_open_states']:
            '''data['proxigba']=ot.get_backlogs_sla_summary_groups(request, 'all_global_asie_ds', None, 'deis')
            data['proxigba_itsm']=ot.get_backlogs_sla_summary_groups(request, 'all_global_asie_ds',None,'itsm')
            backlogs['proxi_global_asie'] = cmt._merge_deis_and_itsm_data(data['proxigba'],data['proxigba_itsm']) #merge deis and itsm data'''
            backlogs['proxi_global_asie']=ot.get_backlogs_sla_summary_groups(request, 'all_global_asie_ds',None,'itsm')
            logger.info("backlogs_sla_proxi_global_asie_ds:********************************:%s"% backlogs['proxi_global_asie'])
                
        if 'proxi_berluti' in user_state['curr_open_states']:
            '''data['proxiblt']=ot.get_backlogs_sla_summary_groups(request, 'all_berluti_ds', None, 'deis')
            data['proxiblt_itsm']=ot.get_backlogs_sla_summary_groups(request, 'all_berluti_ds',None,'itsm')
            backlogs['proxi_berluti'] = cmt._merge_deis_and_itsm_data(data['proxiblt'],data['proxiblt_itsm']) #merge deis and itsm data'''
            backlogs['proxi_berluti']=ot.get_backlogs_sla_summary_groups(request, 'all_berluti_ds',None,'itsm')
            logger.info("backlogs_sla_proxi_berluti_ds:********************************:%s"% backlogs['proxi_berluti'])
                
        if 'proxi_fondation' in user_state['curr_open_states']:
            '''data['proxifdt']=ot.get_backlogs_sla_summary_groups(request, 'all_fondation_ds', None, 'deis')
            data['proxifdt_itsm']=ot.get_backlogs_sla_summary_groups(request, 'all_fondation_ds',None,'itsm')
            backlogs['proxi_fondation'] = cmt._merge_deis_and_itsm_data(data['proxifdt'],data['proxifdt_itsm']) #merge deis and itsm data'''
            backlogs['proxi_fondation'] = ot.get_backlogs_sla_summary_groups(request, 'all_fondation_ds',None,'itsm')
            logger.info("backlogs_sla_proxi_fondation_ds:********************************:%s"% backlogs['proxi_fondation'])
                
        
    #view_type == 'priority':       
    else:
        backlogs = {}
        if 'tsc' in user_state['curr_open_states']:
            '''data['tsc']=ot.get_backlogs_prio_summary_groups(request, 'all_tsc',None,'priority','deis')
            data['tsc_itsm']=ot.get_backlogs_prio_summary_groups(request, 'all_tsc',None,'priority','itsm')
            backlogs['tsc'] = cmt._merge_deis_and_itsm_data(data['tsc'],data['tsc_itsm']) #merge deis and itsm data'''
            backlogs['tsc'] = ot.get_backlogs_prio_summary_groups(request, 'all_tsc',None,'priority','itsm')
            logger.info("backlogs_prio_tsc:********************************:%s"% backlogs['tsc'])
            
        if 'service_desk' in user_state['curr_open_states']:
            '''data['service_desk']=ot.get_backlogs_prio_summary_groups(request, 'all_sd',None,'priority','deis')
            data['fo_itsm']=ot.get_backlogs_prio_summary_groups(request, 'all_sd',None,'priority','itsm')
            backlogs['service_desk'] = cmt._merge_deis_and_itsm_data(data['service_desk'],data['fo_itsm']) #merge deis and itsm data'''
            backlogs['service_desk'] = ot.get_backlogs_prio_summary_groups(request, 'all_sd',None,'priority','itsm')
            logger.info("backlogs_prio_fo:********************************:%s"% backlogs['service_desk'])
    
        if 'proxi_emea' in user_state['curr_open_states']:
            '''data['proxiemea']=ot.get_backlogs_prio_summary_groups(request, 'all_emea',None, None,'deis')
            data['proxiemea_itsm']=ot.get_backlogs_prio_summary_groups(request, 'all_emea',None,'priority','itsm')
            backlogs['proxi_emea'] = cmt._merge_deis_and_itsm_data(data['proxiemea'],data['proxiemea_itsm']) #merge deis and itsm data'''
            backlogs['proxi_emea'] = ot.get_backlogs_prio_summary_groups(request, 'all_emea',None,'priority','itsm')
            logger.info("backlogs_prio_proxi_emea:********************************:%s"% backlogs['proxi_emea'])
            
        if 'proxi_america' in user_state['curr_open_states']:
            '''data['proxius']=ot.get_backlogs_prio_summary_groups(request, 'all_america_ds',None, None,'deis')
            data['proxius_itsm']=ot.get_backlogs_prio_summary_groups(request, 'all_america_ds',None, None,'itsm')
            backlogs['proxi_america'] = cmt._merge_deis_and_itsm_data(data['proxius'],data['proxius_itsm']) #merge deis and itsm data'''
            backlogs['proxi_america'] = ot.get_backlogs_prio_summary_groups(request, 'all_america_ds',None, None,'itsm')
            logger.info("backlogs_proxi_america_ds:********************************:%s"% backlogs['proxi_america'])
            
        if 'proxi_global_asie' in user_state['curr_open_states']:
            '''data['proxigba']=ot.get_backlogs_prio_summary_groups(request, 'all_global_asie_ds',None, None,'deis')
            data['proxigba_itsm']=ot.get_backlogs_prio_summary_groups(request, 'all_global_asie_ds',None,'priority','itsm')
            backlogs['proxi_global_asie'] = cmt._merge_deis_and_itsm_data(data['proxigba'],data['proxigba_itsm']) #merge deis and itsm data'''
            backlogs['proxi_global_asie'] = ot.get_backlogs_prio_summary_groups(request, 'all_global_asie_ds',None,'priority','itsm')
            logger.info("backlogs_prio_proxi_global_asie_ds:********************************:%s"% backlogs['proxi_global_asie'])

        if 'proxi_berluti' in user_state['curr_open_states']:
            '''data['proxiblt']=ot.get_backlogs_prio_summary_groups(request, 'all_berluti_ds',None, None,'deis')
            data['proxiblt_itsm']=ot.get_backlogs_prio_summary_groups(request, 'all_berluti_ds',None,'priority','itsm')
            backlogs['proxi_berluti'] = cmt._merge_deis_and_itsm_data(data['proxiblt'],data['proxiblt_itsm']) #merge deis and itsm data'''
            backlogs['proxi_berluti'] = ot.get_backlogs_prio_summary_groups(request, 'all_berluti_ds',None,'priority','itsm')
            logger.info("backlogs_prio_proxi_berluti_ds:********************************:%s"% backlogs['proxi_berluti'])
            
        if 'proxi_fondation' in user_state['curr_open_states']:
            '''data['proxifdt']=ot.get_backlogs_prio_summary_groups(request, 'all_fondation_ds',None, None,'deis')
            data['proxifdt_itsm']=ot.get_backlogs_prio_summary_groups(request, 'all_fondation_ds',None,'priority','itsm')
            backlogs['proxi_fondation'] = cmt._merge_deis_and_itsm_data(data['proxifdt'],data['proxifdt_itsm']) #merge deis and itsm data'''
            backlogs['proxi_fondation'] = ot.get_backlogs_prio_summary_groups(request, 'all_fondation_ds',None,'priority','itsm')
            logger.info("backlogs_prio_proxi_fondation_ds:********************************:%s"% backlogs['proxi_fondation'])
                
        if 'itl4' in user_state['curr_open_states']:
            '''data['itl4']=ot.get_backlogs_prio_summary_groups(request, 'all_itl4',None, None,'deis')
            data['itl4_itsm']=ot.get_backlogs_prio_summary_groups(request, 'all_itl4',None,'priority','itsm')
            backlogs['itl4'] = cmt._merge_deis_and_itsm_data(data['itl4'],data['itl4_itsm']) #merge deis and itsm data'''
            backlogs['itl4'] = ot.get_backlogs_prio_summary_groups(request, 'all_itl4',None,'priority','itsm')
            logger.info("backlogs_prio_itl4:********************************:%s"% backlogs['itl4'])
            
        if 'network' in user_state['curr_open_states']:
            '''data['network']=ot.get_backlogs_sla_summary_groups(request, 'all_network', None, 'deis')
            data['network_itsm']=ot.get_backlogs_sla_summary_groups(request, 'all_network', None, 'itsm')
            backlogs['network'] = cmt._merge_deis_and_itsm_data(data['network'],data['network_itsm']) #merge deis and itsm data'''
            backlogs['network'] = ot.get_backlogs_sla_summary_groups(request, 'all_network', None, 'itsm')
            logger.info("backlogs_sla_network:********************************:%s"% backlogs['network'])

        if 'ams' in user_state['curr_open_states']:
            '''data['ams']=ot.get_backlogs_prio_summary_groups(request, 'all_ams',None, None,'deis')
            data['ams_itsm']=ot.get_backlogs_prio_summary_groups(request, 'all_ams',None,'priority','itsm')
            backlogs['ams'] = cmt._merge_deis_and_itsm_data(data['ams'],data['ams_itsm']) #merge deis and itsm data'''
            backlogs['ams'] = ot.get_backlogs_prio_summary_groups(request, 'all_ams',None,'priority','itsm')
            logger.info("backlogs_prio_ams:********************************:%s"% backlogs['ams'])

    #logger.info("**************************************:%s"%backlogs)
    return backlogs

def get_highticket_data(request, view_type=None, local_tz=None, user_state=None):
    
    if not user_state:
        user_state = classicdash.get_user_dashboard_filters(request, request.user.username)
        
    ticket_type = None
    criticality_high = settings.TICKET_CRITICALITY['HIGH']
    high_crit_tickets = {}
    team_trends = {}
    
    # Get all high tickets
    if 'tsc' in user_state['curr_open_states']:
        #tsc_tickets=ot.get_backlogs_data(request, 'all_tsc',local_tz, ticket_type, criticality_high)
        tsc_tickets_itsm=ot.get_backlogs_data(request, 'all_tsc',local_tz, ticket_type, criticality_high, None, 'itsm')
        tsc_high_crit_tickets=  tsc_tickets_itsm['isla']+tsc_tickets_itsm['oosla']+tsc_tickets_itsm['soosla']
        high_crit_tickets['tsc'] = tsc_high_crit_tickets
        team_trends['tsc'] = tt.team_trends_data(request, 'all_tsc')
        
    if 'service_desk' in user_state['curr_open_states']:
        #fo_tickets=ot.get_backlogs_data(request, 'all_sd',local_tz, ticket_type, criticality_high)
        fo_tickets_itsm=ot.get_backlogs_data(request, 'all_sd',local_tz, ticket_type, criticality_high, None, 'itsm')
        fo_high_crit_tickets = fo_tickets_itsm['isla']+fo_tickets_itsm['oosla']+fo_tickets_itsm['soosla']
        high_crit_tickets['service_desk'] = fo_high_crit_tickets
        team_trends['service_desk'] = tt.team_trends_data(request, 'all_sd')
        
    
    if 'proxi_america' in user_state['curr_open_states']:
        #proxius_tickets=ot.get_backlogs_data(request, 'all_america_ds',local_tz, ticket_type, criticality_high)
        proxius_tickets_itsm=ot.get_backlogs_data(request, 'all_america_ds',local_tz, ticket_type, criticality_high, None, 'itsm')
        proxius_high_crit_tickets = proxius_tickets_itsm['isla']+proxius_tickets_itsm['oosla']+proxius_tickets_itsm['soosla']
        high_crit_tickets['proxi_america'] = proxius_high_crit_tickets
        team_trends['proxi_america'] = tt.team_trends_data(request, 'all_america_ds')
        
    if 'proxi_emea' in user_state['curr_open_states']:
        #proxiemea_tickets=ot.get_backlogs_data(request, 'all_emea',local_tz, ticket_type, criticality_high)
        proxiemea_tickets_itsm=ot.get_backlogs_data(request, 'all_emea',local_tz, ticket_type, criticality_high, None, 'itsm')
        proxiemea_high_crit_tickets = proxiemea_tickets_itsm['isla']+proxiemea_tickets_itsm['oosla']+proxiemea_tickets_itsm['soosla']
        high_crit_tickets['proxi_emea'] = proxiemea_high_crit_tickets
        team_trends['proxi_emea'] = tt.team_trends_data(request, 'all_emea') 
        
    if 'proxi_global_asie' in user_state['curr_open_states']:
        #proxigba_tickets=ot.get_backlogs_data(request, 'all_global_asie_ds',local_tz, ticket_type, criticality_high)
        proxigba_tickets_itsm=ot.get_backlogs_data(request, 'all_global_asie_ds',local_tz, ticket_type, criticality_high, None, 'itsm')
        proxigba_high_crit_tickets = proxigba_tickets_itsm['isla']+proxigba_tickets_itsm['oosla']+proxigba_tickets_itsm['soosla']
        high_crit_tickets['proxi_global_asie'] = proxigba_high_crit_tickets
        team_trends['proxi_global_asie'] = tt.team_trends_data(request, 'all_global_asie_ds')
        
    if 'proxi_berluti' in user_state['curr_open_states']:
        #proxiblt_tickets=ot.get_backlogs_data(request, 'all_berluti_ds',local_tz, ticket_type, criticality_high)
        proxiblt_tickets_itsm=ot.get_backlogs_data(request, 'all_berluti_ds',local_tz, ticket_type, criticality_high, None, 'itsm')
        proxiblt_high_crit_tickets = proxiblt_tickets_itsm['isla']+proxiblt_tickets_itsm['oosla']+proxiblt_tickets_itsm['soosla']
        high_crit_tickets['proxi_berluti'] = proxiblt_high_crit_tickets
        team_trends['proxi_berluti'] = tt.team_trends_data(request, 'all_berluti_ds')
   
    if 'proxi_fondation' in user_state['curr_open_states']:
        #proxifdt_tickets=ot.get_backlogs_data(request, 'all_fondation_ds',local_tz, ticket_type, criticality_high)
        proxifdt_tickets_itsm=ot.get_backlogs_data(request, 'all_fondation_ds',local_tz, ticket_type, criticality_high, None, 'itsm')
        proxifdt_high_crit_tickets = proxifdt_tickets_itsm['isla']+proxifdt_tickets_itsm['oosla']+proxifdt_tickets_itsm['soosla']
        high_crit_tickets['proxi_fondation'] = proxifdt_high_crit_tickets
        team_trends['proxi_fondation'] = tt.team_trends_data(request, 'all_fondation_ds')
           
    if 'itl4' in user_state['curr_open_states']:
        #itl4_tickets=ot.get_backlogs_data(request, 'all_itl4',local_tz, ticket_type, criticality_high)
        itl4_tickets_itsm=ot.get_backlogs_data(request, 'all_itl4',local_tz, ticket_type, criticality_high, None, 'itsm')
        itl4_high_crit_tickets = itl4_tickets_itsm['isla']+itl4_tickets_itsm['oosla']+itl4_tickets_itsm['soosla']
        high_crit_tickets['itl4'] = itl4_high_crit_tickets
        team_trends['itl4'] = tt.team_trends_data(request, 'all_itl4')
        
    if 'network' in user_state['curr_open_states']:
        #network_tickets=ot.get_backlogs_data(request, 'all_network',local_tz, ticket_type, criticality_high)
        network_tickets_itsm=ot.get_backlogs_data(request, 'all_network',local_tz, ticket_type, criticality_high, None, 'itsm')
        network_high_crit_tickets = network_tickets_itsm['isla']+network_tickets_itsm['oosla']+network_tickets_itsm['soosla']
        high_crit_tickets['network'] = network_high_crit_tickets
        team_trends['network'] = tt.team_trends_data(request, 'all_network')
        
    if 'ams' in user_state['curr_open_states']:
        #ams_tickets=ot.get_backlogs_data(request, 'all_ams',local_tz, ticket_type, criticality_high)
        ams_tickets_itsm=ot.get_backlogs_data(request, 'all_ams',local_tz, ticket_type, criticality_high, None, 'itsm')
        ams_high_crit_tickets = ams_tickets_itsm['isla']+ams_tickets_itsm['oosla']+ams_tickets_itsm['soosla']
        high_crit_tickets['ams'] = ams_high_crit_tickets
        team_trends['ams'] = tt.team_trends_data(request, 'all_ams')
     
    return {'high_crit_tickets' : high_crit_tickets, 'team_trends' : team_trends}  
  
    
@login_required
def dashboard(request):
    """Home for users."""
    
    user_state = classicdash.get_user_dashboard_filters(request, request.user.username)

    logger.info('***************test:%s***************'%request.session)
    if 'view' not in request.session:
        request.session['view'] = 'sla'

    backlogs_data = backlogs(request, request.session['view'], user_state)

    local_tz = request.GET.get('local_tz', None)

    hightickets_data = get_highticket_data(request, request.session['view'], local_tz, user_state)
    
    logger.info('high_crit_tickets:%s***************'%hightickets_data['high_crit_tickets'])
    return render(request, 'users/home.html', {'high_crit_tickets' : hightickets_data['high_crit_tickets'], 'backlogs_data' : backlogs_data, 'team_trends' : hightickets_data['team_trends'], 'user_state' : user_state['curr_dashboard_state']})
    
@login_required
def get_team_trends(request):
    local_timezone = request.GET.get('local_tz', None)
    
    team_list = json.loads(request.body)
    data = tt.team_trends(request, team_list)

    return JsonResponse(data, safe=False) 

@login_required
def classic_dashboard_load_group(request):

    request_data = json.loads(request.body)
    group = request_data['group']
    
    if 'view' not in request.session:
        request.session['view'] = 'sla'
        
    local_tz = request.GET.get('local_tz', None)
        
    user_state = {'curr_open_states' : [group]}
        
    backlog_data = backlogs(request, request.session['view'], user_state) #check
    
    hightickets_data = get_highticket_data(request, request.session['view'], local_tz, user_state)
    
    result = {'data' : backlog_data, 'high_crit_tickets' : hightickets_data['high_crit_tickets'], 'team_trends' : hightickets_data['team_trends']}
    return JsonResponse(result)

@login_required
def classic_dashboard_state_save(request):
    
    request_data = json.loads(request.body)
    user_state= request_data['user_state']
    
    logger.info("========== DEBUG :: classic_dashboard_state_save :: user_state :: ")
    logger.info(user_state)
    
    resp = classicdash.save_dashboard_state(request, user_state)
    
    status = "OK" if resp else "ERROR"
    
    result = {"status" : status}
    return JsonResponse(result)

@login_required
def ticket_search(request):
    form = request.GET
    local_tz = request.GET.get('local_tz', None)
    if request.method == 'POST':
        #import pdb;pdb.set_trace()
        ticket_no = request.POST.get('ticket_no')
        ticket_itsm = cmt._get_ticket(request, ticket_no, local_tz, 'itsm')
        ticket = cmt._get_ticket(request, ticket_no, local_tz)
        ticket = ticket_itsm + ticket 
        logger.info('ticket_itsm:%s'%ticket_itsm)

    return render(request,'users/ticket_search.html', {'form' : form, 'ticket' : ticket, 'ticket_no' : ticket_no})    

@login_required
def control_plan_global(request):
    
    data = cp.get_control_data("global")
    
    return render(request, 'users/control_plan_global.html', {'data' : data})

@login_required
def control_plan_ko(request):
    
    data = cp.get_control_data("ko")
    
    return render(request, 'users/control_plan_ko.html', {'data' : data})   
    
@login_required
def control_plan_domain(request):
    
    data = cp.get_control_data("domain")
    
    return render(request, 'users/control_plan_domain.html', {'data' : data})

@login_required
def control_plan_businessflag(request):
    
    data = cp.get_control_data("business_flag")
    
    return render(request, 'users/control_plan_businessflag.html', {'data' : data})   
        
@login_required
def control_plan_management(request):
    
    return render(request, 'users/control_plan_management.html')   
    
@login_required
def custom_dashboard(request):
    if 'view' not in request.session:
        request.session['view'] = 'sla'
        
    local_tz = request.GET.get('local_tz', None)
    
    logger.info("custom_dashboard :: local_tz :: %s"%local_tz)
    
    #import pdb;pdb.set_trace()    
    if request.body:
        user_filters_list = json.loads(request.body)
        print("custom_dashboard :: REQUEST JSON :: %s"%user_filters_list)
    else:
        user_filters_list = get_user_custom_dashboard_filters(request)
        print("custom_dashboard :: USER FILTERS :: %s"%user_filters_list)
    
    dashboard_data = {'dashboard_data' : {}, 'open_selected_grp' : []}
    if user_filters_list:
        dashboard_data = get_custom_dashboard_data(request, request.session['view'], user_filters_list, local_tz)
    
    if request.method == "GET":
        return render(request, 'users/custom_dashboard.html', {'user_filters_list' : user_filters_list, 'dashboard_data' : dashboard_data['dashboard_data'], 'open_selected_grp' : dashboard_data['open_selected_grp'], 'type' : type, 'isinstance' : isinstance})
    else:
        #return HttpResponseRedirect(reverse('team', args="WW_TM_L3_TSC_AS400"))
        return JsonResponse({"status":"OK", "data": dashboard_data})        
    
def get_user_custom_dashboard_filters(request):
    filters = cd.get_user_dashboard_filters(request.user.username)
    return filters
    
def get_custom_dashboard_data(request, view_type, filters_list, local_tz):
    #filters = request.json['data']
    dashboard_data = {}
    open_selected_grp = []
    print("get_custom_dashboard_data :: filter_list :: %s"%filters_list)
    for filter in filters_list.values():
        #if not isinstance(filter,dict):
        #    filter = filter.__dict__
        #    del filter['_sa_instance_state']
        print("get_custom_dashboard_data :: filter :: %s"%filter['selected_team'])
        selected_team = filter['selected_team']
        
        team = selected_team['team']
        group = selected_team['group']
        groupname = selected_team['groupname']
        
        if groupname.startswith('All'):
            open_selected_grp.append(groupname.replace(" ", "_")[4:]) #[4:] to remove "All " from the groupname to match with frontend
            group = group.replace(" ", "_")
        else:
            open_selected_grp.append(groupname)
        
        if view_type == 'sla':
            data = cd.get_sla_summary_by_team(request, team, group, local_tz, None, 'itsm')
        else:
            data = []
        dashboard_data[groupname] = {}
        dashboard_data[groupname]['data'] = data
        dashboard_data[groupname]['group'] = group
        dashboard_data[groupname]['team'] = team
        dashboard_data[groupname]['groupname'] = groupname
        #dashboard_data[filter['title']]['details_url'] = url_for('user.team', tsc_team=json.dumps(filter['group']))
        group = group.lower() if group.startswith('All') else group
        dashboard_data[groupname]['details_url'] = "/tickets/open/team/" + group
      
    return {"dashboard_data" : dashboard_data, "open_selected_grp" : open_selected_grp}   

@login_required
def save_settings(request):

    if request.body:
        cd.save_user_settings(request, json.loads(request.body))
    return JsonResponse({"status":'OK'})    
    