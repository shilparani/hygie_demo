from django import template
register = template.Library()

@register.filter(name='div')
def div(value, div):
    return round((value / div) * 100, 2)
