from django import template

register = template.Library()

@register.filter(name='getkey')
def getkey(value, arg):
    print("VALUE:--",value)
    print("ARGS:--",arg)
    return value[arg]
