from django import template
register = template.Library()

@register.filter(name='list_sort')
def list_sort(list_value):
    result = list_value.sort()
    return list_value
