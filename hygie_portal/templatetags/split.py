from django import template
register = template.Library()

@register.filter(name='split')
def split(value, key):
    result = value.split(key)
    return result[1]
