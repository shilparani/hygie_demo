from django import template
register = template.Library()

@register.filter(name='add')
def add(a, b):
    if type(a) == list:
        a = len(a)
    if type(b) == list:
        b = len(b)
    return a+b
