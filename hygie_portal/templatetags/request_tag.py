from django import template

register = template.Library()

@register.filter(name='request_tag')
def request_tag(request):
    path = request.get_full_path
    return path
