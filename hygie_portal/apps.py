from django.apps import AppConfig


class HygiePortalConfig(AppConfig):
    name = 'hygie_portal'
