#!/var/www/html/.pyenv/versions/3.6.5/envs/venv_hygie_prd/bin/python3.6
# -*- coding: utf-8 -*-
import pyodbc,ldap
import os
import datetime
import time

def _get_connection_security_ld():
    try:
        conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format('NLVCP1714P','1575','MSLDSK6PRD','GSQL_LANDESK_PRD','h4LuuDrD5x'))
        return conn
    except pyodbc.Error as err:
        print('Error connecting to security landesk DB:', err)
        return None

def _get_connection_kaspsky_americas():
    try:
        conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format('NLVNA310P','1523','MSKLNA1PRD','SQL_GKSP_PRD','Ne27ksp59y'))
        return conn
    except pyodbc.Error as err:
        print('Error connecting to kaspsky americas DB:', err)
        return None

def _get_connection_kaspsky_europe():
    try:
        #conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format('nlvcp473p','1525','MSKLEM1PRD','SQL_GKSP_PRD','Ne27ksp59y'))
        conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format('LVWWWIN0006P','1525','MSKLEM1PRD','SQL_GKSP_PRD','Ne27ksp59y'))
        return conn
    except pyodbc.Error as err:
        print('Error connecting to kaspsky europe DB:', err)
        return None

def _get_connection_kaspsky_asia():
    try:
        conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format('NLVAP126P','1522','MSKLAP1PRD','SQL_GKSP_PRD','Ne27ksp59y'))
        return conn
    except pyodbc.Error as err:
        print('Error connecting to kaspsky asia DB:', err)
        return None

def _get_connection_deis():
    try:
        conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format('clvcp118p-sql','1522','MSMINT1PRD','GSQL_DEIS','rx-78-2'))
        return conn
    except pyodbc.Error as err:
        print('Error connecting to security deis DB:', err)
        return None


def _close_connection(conn):
    conn.close()

def _get_landesk_device(type="WKS"):
    db_conn = _get_connection_security_ld()
    if db_conn is None:
        return None
    cursor = db_conn.cursor()
    project_upload_path = settings.UPLOAD_FOLDER

    try:
        ld_file = os.path.join(project_upload_path,"discrepancy/sql/ld_query_wks.sql") if type=="WKS" else os.path.join(project_upload_path,"discrepancy/sql/ld_query_srv.sql")
        with open(ld_file,'r') as ld_file:
            ld_query = ld_file.read()
            cursor.execute(ld_query)
            #print "landesk query %s" % ld_query
        #ld_query = "select distinct Computer.DisplayName from Computer (nolock) left join AntiVirus  (nolock) on AntiVirus.Computer_Idn=Computer.Computer_Idn and ProductName like '%Kaspersky%' WHERE Left(Computer.DisplayName,4) in ('LVNA','LVLA','LVJP','LVAP','LVCN','LVCP','LVEU')"
        #ld_query = "select DeviceName from Computer where (deviceName like 'LV%')" if type=="WKS" else "select DeviceName from Computer where (deviceName like 'NLV%' OR deviceName like 'ELV%')"
        #ld_query = "select DeviceName from Computer where deviceName like 'LV%' and deviceName not like '%EDG%' and deviceName NOT like '%ESX%' and deviceName NOT like '%HPV%' and deviceName NOT like '%MGT%' and deviceName NOT like '%NSX%' and deviceName NOT like '%PSC%' and deviceName NOT like '%RES%' and deviceName NOT like '%SAN%' and deviceName NOT like '%UNX%' and deviceName NOT like '%VCE%' and deviceName NOT like '%VLI%' and deviceName NOT like '%VOP%' and deviceName NOT like '%VRA%' and deviceName NOT like '%VRN%' and deviceName NOT like '%WDC%' and SWLastScanDate between DATEADD(DAY, -30, GETDATE()) and GETDATE()" if type=="WKS" else "select DeviceName from Computer where (deviceName like 'NLV%' OR deviceName like 'ELV%') and SWLastScanDate between DATEADD(DAY, -30, GETDATE()) and GETDATE()"   
        ld_data_file = os.path.join(project_upload_path ,"discrepancy/wks_ld_list.txt") if type=="WKS" else os.path.join(project_upload_path ,"discrepancy/srv_ld_list.txt")
        with open(ld_data_file,"w") as ld_list_file:
            for row in cursor.fetchall():
                ld_list_file.write(','.join(row)+"\n")
        # testing device last user name query
        #ld_query_lastuser = "select DeviceName,LoginName from Computer where (deviceName like 'LV%')" if type=="WKS" else "select DeviceName,LoginName from Computer where (deviceName like 'NLV%' OR deviceName like 'ELV%')"

        ld_lastuser_file = os.path.join(project_upload_path,"discrepancy/sql/ld_query_lastuser_wks.sql") if type=="WKS" else os.path.join(project_upload_path,"discrepancy/sql/ld_query_lastuser_srv.sql")
        with open(ld_lastuser_file,'r') as ld_lastuser_file:
            ld_query_lastuser = ld_lastuser_file.read()
            cursor.execute(ld_query_lastuser)
            #print "landesk lastuser query %s" % ld_query_lastuser
		#ld_query_lastuser = "select DeviceName,LoginName from Computer where deviceName like 'LV%' and deviceName not like '%EDG%' and deviceName NOT like '%ESX%' and deviceName NOT like '%HPV%' and deviceName NOT like '%MGT%' and deviceName NOT like '%NSX%' and deviceName NOT like '%PSC%' and deviceName NOT like '%RES%' and deviceName NOT like '%SAN%' and deviceName NOT like '%UNX%' and deviceName NOT like '%VCE%' and deviceName NOT like '%VLI%' and deviceName NOT like '%VOP%' and deviceName NOT like '%VRA%' and deviceName NOT like '%VRN%' and deviceName NOT like '%WDC%' and SWLastScanDate between DATEADD(DAY, -30, GETDATE()) and GETDATE()" if type=="WKS" else "select DeviceName,LoginName from Computer where (deviceName like 'NLV%' OR deviceName like 'ELV%') and SWLastScanDate between DATEADD(DAY, -30, GETDATE()) and GETDATE()"    
        ld_data_lastuser_file = os.path.join(project_upload_path ,"discrepancy/wks_ld_lastuser_list.txt") if type=="WKS" else os.path.join(project_upload_path ,"discrepancy/srv_ld_lastuser_list.txt")
        with open(ld_data_lastuser_file,"w") as ld_list_lastuser_file:
            for row in cursor.fetchall():
                d = ['-' if v is None else v for v in row]
                ld_list_lastuser_file.write(','.join(d)+"\n")
    finally:
        _close_connection(db_conn)

def _get_kapsky_device(type="WKS"):
    project_upload_path = settings.UPLOAD_FOLDER
    kas_data_file = os.path.join(project_upload_path ,"discrepancy/wks_kas_list.txt") if type=="WKS" else os.path.join(project_upload_path ,"discrepancy/srv_kas_list.txt")
    with open(kas_data_file,"w") as kas_list_file:
        for db_conn in [_get_connection_kaspsky_americas(),_get_connection_kaspsky_europe(), _get_connection_kaspsky_asia()]:
            if db_conn is not None:
                try:
                    kapsky_str_file = os.path.join(project_upload_path,"discrepancy/sql/kapsky_wks.sql") if type=="WKS" else os.path.join(project_upload_path,"discrepancy/sql/kapsky_srv.sql")
                    with open(kapsky_str_file,'r') as kapsky_str_file:
                        kapsky_query = kapsky_str_file.read()
                        cursor = db_conn.cursor()
                        cursor.execute(kapsky_query)
                        #print "kapsky query %s" % kapsky_query
                    #kapsky_query = "select distinct T1.Device from (select Upper(strWinHostName) as Device from Hosts (nolock) where tmLastNagentConnected>dateadd(day,-30,getdate()) and Left(Upper(strWinHostName),4) in ('LVNA','LVLA','LVJP','LVAP','LVCN','LVCP','LVEU') group by Upper(strWinHostName)) T1 left join (select Upper(strWinHostName) as Device, isNULL(hst_prdstates.wstrProductDisplVersion,0) as Version From Hosts (nolock) left join hst_prdstates (nolock) on Hosts.nId = hst_prdstates.nHostID left join hst_products (nolock) on hst_products.nId = hst_prdstates.nProduct  and wstrProductName in ('KES','KAVMACWKS8','KAVFSEE','KLMOBILE','KAVWKS6','KAVFS6') where tmLastNagentConnected>dateadd(day,-30,getdate()) and Left(Upper(strWinHostName),4) in ('LVNA','LVLA','LVJP','LVAP','LVCN','LVCP','LVEU') and wstrProductName is not NULL) T2 on T1.Device=T2.Device "
                    #kapsky_query = "Select strWinHostName from ads_computer where strWinHostName like 'LV%'" if type=="WKS" else "Select strWinHostName from ads_computer where strWinHostName like 'NLV%' OR strWinHostName like 'ELV%'"
                    #kapsky_str = "select distinct T1.Device from (select Upper(strWinHostName) as Device from Hosts (nolock) where tmLastNagentConnected>dateadd(day,-30,getdate()) and ({0}) group by Upper(strWinHostName)) T1 left join (select Upper(strWinHostName) as Device, isNULL(hst_prdstates.wstrProductDisplVersion,0) as Version From Hosts (nolock) left join hst_prdstates (nolock) on Hosts.nId = hst_prdstates.nHostID left join hst_products (nolock) on hst_products.nId = hst_prdstates.nProduct  and wstrProductName in ('KES','KAVMACWKS8','KAVFSEE','KLMOBILE','KAVWKS6','KAVFS6') where tmLastNagentConnected>dateadd(day,-30,getdate()) and ({1}) and wstrProductName is not NULL) T2 on T1.Device=T2.Device"
                    #kapsky_str = "select distinct T1.Device from (select Upper(strWinHostName) as Device from Hosts (nolock) where tmLastNagentConnected>dateadd(day,-30,getdate()) and ({0}) group by Upper(strWinHostName)) T1 left join (select Upper(strWinHostName) as Device, isNULL(hst_prdstates.wstrProductDisplVersion,0) as Version From Hosts (nolock) left join hst_prdstates (nolock) on Hosts.nId = hst_prdstates.nHostID left join hst_products (nolock) on hst_products.nId = hst_prdstates.nProduct  and wstrProductName in ('KES','KAVWKS6','KAVFS6','KAVFSEE','WSEE') where tmLastNagentConnected>dateadd(day,-30,getdate()) and ({1}) and wstrProductName is not NULL) T2 on T1.Device=T2.Device"
                    #kapsky_str = "select distinct T1.Device from (select Upper(strWinHostName) as Device from Hosts (nolock) where tmLastNagentConnected>dateadd(day,-30,getdate()) and ({0}) group by Upper(strWinHostName)) T1 left join (select Upper(strWinHostName) as Device, isNULL(hst_prdstates.wstrProductDisplVersion,0) as Version From Hosts (nolock) left join hst_prdstates (nolock) on Hosts.nId = hst_prdstates.nHostID left join hst_products (nolock) on hst_products.nId = hst_prdstates.nProduct  and wstrProductName in ({2}) where tmLastNagentConnected>dateadd(day,-30,getdate()) and ({1}) and wstrProductName is not NULL) T2 on T1.Device=T2.Device"
                    #kapsky_query = kapsky_str.format("strWinHostName like 'LV%'","strWinHostName like 'LV%'","'KAVWKS6','KES'") if type=="WKS" else kapsky_str.format("strWinHostName like 'NLV%' OR strWinHostName like 'ELV%'","strWinHostName like 'NLV%' OR strWinHostName like 'ELV%'","'KAVFSEE','KAVFS6','WSEE'")    
                    for row in cursor.fetchall():
                        kas_list_file.write(','.join(row)+"\n")
                finally:
                     _close_connection(db_conn)

def _get_deis_device(type="WKS"):
    db_conn = _get_connection_deis()
    if db_conn is None:
        return None
    cursor = db_conn.cursor()
    project_upload_path  = settings.UPLOAD_FOLDER

    try:
        #deis_query = "SELECT distinct [Machine Name] FROM [_SMDBA_].[Inventory Items] where [Machine Name] like 'LV%'" if type=="WKS" else "SELECT distinct [Machine Name] FROM [_SMDBA_].[Inventory Items] where ([Machine Name] like '%NLV%' OR [Machine Name] like '%ELV%') and [InActive:] =0"
        deis_query_str = os.path.join(project_upload_path ,"discrepancy/sql/deis_wks.sql") if type=="WKS" else os.path.join(project_upload_path ,"discrepancy/sql/deis_srv.sql")
        with open(deis_query_str,'r') as deis_query_str:
            deis_query = deis_query_str.read()
            cursor.execute(deis_query)
            #print "DEIS query %s" % deis_query
        deis_data_file = os.path.join(project_upload_path ,"discrepancy/wks_deis_list.txt") if type=="WKS" else os.path.join(project_upload_path ,"discrepancy/srv_deis_list.txt")
        with open(deis_data_file,"w") as deis_list_file:
            for row in cursor.fetchall():
                deis_list_file.write(','.join(row)+"\n")
    finally:
        _close_connection(db_conn)

def _get_ldap_device(type="WKS"):
    project_upload_path  = settings.UPLOAD_FOLDER
    today = datetime.datetime.now()
    DD = datetime.timedelta(days=30)
    dt = today - DD
    windows_timestamp = int((time.mktime(dt.timetuple()) + 11644473600) * 10000000) #18-digit LDAP/FILETIME: 131824189250000000
    base = "DC=D1,DC=cougar,DC=ms,DC=lvmh" 
    search_flt_str = os.path.join(project_upload_path ,"discrepancy/sql/ldap_wks.txt") if type=="WKS" else os.path.join(project_upload_path ,"discrepancy/sql/ldap_srv.txt")
    with open(search_flt_str,'r') as search_flt_str:
        search_flt = search_flt_str.read()
        search_flt = search_flt.format(windows_timestamp)
        #print "LDAP query %s" % search_flt
    #search_flt = '(&(objectClass=computer)(!(userAccountControl:1.2.840.113556.1.4.803:=2))(&(Name=lv*)(!(Name=*EDG*))(!(Name=*ESX*))(!(Name=*HPV*))(!(Name=*MGT*))(!(Name=*NSX*))(!(Name=*PSC*))(!(Name=*RES*))(!(Name=*SAN*))(!(Name=*UNX*))(!(Name=*VCE*))(!(Name=*VLI*))(!(Name=*VOP*))(!(Name=*VRA*))(!(Name=*VRN*))(!(Name=*WDC*)))(lastLogonTimestamp>={0}))'.format(windows_timestamp) if type=="WKS" else '(&(objectClass=computer)(!(userAccountControl:1.2.840.113556.1.4.803:=2))(|(Name=elv*)(Name=nlv*))(lastLogonTimestamp>={0}))'.format(windows_timestamp)
    page_size = 1000
    attr_search = ['dNSHostName','cn']

    conn = ldap.initialize('LDAP://d1.cougar.ms.lvmh')
    conn.set_option(ldap.OPT_REFERRALS,0)
    conn.set_option(ldap.OPT_PROTOCOL_VERSION, 3)
    conn.simple_bind_s('s-lvpa1web','OoT@88#4')

    #New change for Python-ldap version 3.2 Reference: https://medium.com/@alpolishchuk/pagination-of-ldap-search-results-with-python-ldap-845de60b90d2 
    page_control = ldap.controls.SimplePagedResultsControl(True, size=page_size, cookie='')
    
    # Send search request
    msgid = conn.search_ext(base, ldap.SCOPE_SUBTREE, search_flt, attr_search, serverctrls=[page_control])
    pages = 0

    json_url = os.path.join(project_upload_path ,"discrepancy/wks_ldap_list.txt") if type=="WKS" else os.path.join(project_upload_path ,"discrepancy/srv_ldap_list.txt")
    with open(json_url,'w') as wks_ldap_file:
        while True:
            pages += 1
            #print "Getting page %d" % (pages,)
            rtype, rdata, rmsgid, serverctrls = conn.result3(msgid)
            #print '%d results' % len(rdata)
            #print '%s results' % rdata
            for e in rdata:
                #print '%s ' % e[1]
                if e[0] is not None:
                    if '\n' in e[1]['cn'][0].decode('utf-8'):
                        wks_ldap_file.write(e[1]['cn'][0].decode('utf-8').split('\n')[0]+'\n')
                    else:
                        wks_ldap_file.write(e[1]['cn'][0].decode('utf-8')+'\n')
            #wks_ldap_file.write(rdata[1])
            pctrls = [ c for c in serverctrls if c.controlType == ldap.controls.SimplePagedResultsControl.controlType]
            if pctrls:
                cookie = pctrls[0].cookie
                if cookie:
                    page_control.cookie = cookie
                    msgid = conn.search_ext(base, ldap.SCOPE_SUBTREE, search_flt, attr_search, serverctrls=[page_control])
                else:
                    break
            else:
                print("Warning:  Server ignores RFC 2696 control.")
                break
    if type != "WKS":
        base = "DC=D1-INT,DC=cougar-INT,DC=ms,DC=lvmh" 
        search_flt = '(&(objectClass=computer)(!(userAccountControl:1.2.840.113556.1.4.803:=2))(|(Name=elv*)(Name=nlv*))(lastLogonTimestamp>={0}))'.format(windows_timestamp)
        page_size = 1000
        attr_search = ['dNSHostName','cn']

        conn = ldap.initialize('LDAP://d1-INT.cougar-INT.ms.lvmh')
        conn.set_option(ldap.OPT_REFERRALS,0)
        conn.set_option(ldap.OPT_PROTOCOL_VERSION, 3)
        conn.simple_bind_s('s-lvpa1sch01','##91uGY')
        
        page_control = ldap.controls.SimplePagedResultsControl(True, size=page_size, cookie='')
        
        # Send search request
        print("Loading INT")
        msgid = conn.search_ext(base, ldap.SCOPE_SUBTREE, search_flt, attr_search, serverctrls=[page_control])
        
        pages = 0

        json_url = os.path.join(project_upload_path ,"discrepancy/srv_ldap_list.txt")
        
        with open(json_url,'a') as wks_ldap_file:
            while True:
                pages += 1
                rtype, rdata, rmsgid, serverctrls = conn.result3(msgid)
                for e in rdata:
                    #print '%s ' % e[1]
                    if e[0] is not None:
                        if '\n' in e[1]['cn'][0].decode('utf-8'):
                            wks_ldap_file.write(e[1]['cn'][0].decode('utf-8').split('\n')[0]+'\n')
                        else:
                            wks_ldap_file.write(e[1]['cn'][0].decode('utf-8')+'\n')
                #wks_ldap_file.write(rdata[1])
                pctrls = [ c for c in serverctrls if c.controlType == ldap.controls.SimplePagedResultsControl.controlType]
                if pctrls:
                    cookie = pctrls[0].cookie
                    if cookie:
                        page_control.cookie = cookie
                        msgid = conn.search_ext(base, ldap.SCOPE_SUBTREE, search_flt, attr_search, serverctrls=[page_control])
                    else:
                        break
                else:
                    print("Warning:  Server ignores RFC 2696 control.")
                    break
        
        base = "DC=D1-prp,DC=cougar-prp,DC=ms,DC=lvmh" 
        search_flt = '(&(objectClass=computer)(!(userAccountControl:1.2.840.113556.1.4.803:=2))(|(Name=elv*)(Name=nlv*))(lastLogonTimestamp>={0}))'.format(windows_timestamp)
        page_size = 1000
        attr_search = ['dNSHostName','cn']

        conn = ldap.initialize('LDAP://d1-prp.cougar-prp.ms.lvmh')
        conn.set_option(ldap.OPT_REFERRALS,0)
        conn.set_option(ldap.OPT_PROTOCOL_VERSION, 3)
        conn.simple_bind_s('s-lvpa1sch01','##91uGY')
        
        page_control = ldap.controls.SimplePagedResultsControl(True, size=page_size, cookie='')

        # Send search request
        print("Loading PRP")
        msgid = conn.search_ext(base, ldap.SCOPE_SUBTREE, search_flt, attr_search, serverctrls=[page_control])
        
        pages = 0

        json_url = os.path.join(project_upload_path ,"discrepancy/srv_ldap_list.txt")
        
        with open(json_url,'a') as wks_ldap_file:
            while True:
                pages += 1
                rtype, rdata, rmsgid, serverctrls = conn.result3(msgid)
                for e in rdata:
                    #print '%s ' % e[1]
                    if e[0] is not None:
                        if '\n' in e[1]['cn'][0].decode('utf-8'):
                            wks_ldap_file.write(e[1]['cn'][0].decode('utf-8').split('\n')[0]+'\n')
                        else:
                            wks_ldap_file.write(e[1]['cn'][0].decode('utf-8')+'\n')
                #wks_ldap_file.write(rdata[1])
                pctrls = [ c for c in serverctrls if c.controlType == ldap.controls.SimplePagedResultsControl.controlType]
                if pctrls:
                    cookie = pctrls[0].cookie
                    if cookie:
                        page_control.cookie = cookie
                        msgid = conn.search_ext(base, ldap.SCOPE_SUBTREE, search_flt, attr_search, serverctrls=[page_control])
                    else:
                        break
                else:
                    print("Warning:  Server ignores RFC 2696 control.")
                    break
                    
        base = "DC=lvm-ext,DC=partners,DC=ms,DC=lvmh" 
        search_flt = '(&(objectClass=computer)(!(userAccountControl:1.2.840.113556.1.4.803:=2))(|(Name=elv*)(Name=nlv*))(lastLogonTimestamp>={0}))'.format(windows_timestamp)
        page_size = 1000
        attr_search = ['dNSHostName','cn']
        
        conn = ldap.initialize('LDAP://lvm-ext.partners.ms.lvmh')
         
        conn.set_option(ldap.OPT_REFERRALS,0)
        conn.set_option(ldap.OPT_PROTOCOL_VERSION, 3)
        conn.simple_bind_s('s-lvpa1sch01@lvm-ext.partners.ms.lvmh','VPNk2bwNM')
        
        page_control = ldap.controls.SimplePagedResultsControl(True, size=page_size, cookie='')
        
        # Send search request
        print("Loading LVM-EXT")
        msgid = conn.search_ext(base, ldap.SCOPE_SUBTREE, search_flt, attr_search, serverctrls=[page_control])
        
        pages = 0
        
        json_url = os.path.join(project_upload_path ,"discrepancy/srv_ldap_list.txt")
        
        with open(json_url,'a') as wks_ldap_file:
            while True:
                pages += 1
                rtype, rdata, rmsgid, serverctrls = conn.result3(msgid)
                for e in rdata:
                    #print '%s ' % e[1]
                    if e[0] is not None:
                        if '\n' in e[1]['cn'][0].decode('utf-8'):
                            wks_ldap_file.write(e[1]['cn'][0].decode('utf-8').split('\n')[0]+'\n')
                        else:
                            wks_ldap_file.write(e[1]['cn'][0].decode('utf-8')+'\n')
                #wks_ldap_file.write(rdata[1])
                pctrls = [ c for c in serverctrls if c.controlType == ldap.controls.SimplePagedResultsControl.controlType]
                if pctrls:
                    cookie = pctrls[0].cookie
                    if cookie:
                        page_control.cookie = cookie
                        msgid = conn.search_ext(base, ldap.SCOPE_SUBTREE, search_flt, attr_search, serverctrls=[page_control])
                    else:
                        break
                else:
                    print("Warning:  Server ignores RFC 2696 control.")
                    break
                                        

def _get_discrepancy(type="WKS"):
    project_upload_path  = settings.UPLOAD_FOLDER
    if type=="WKS":
        with open(os.path.join(project_upload_path ,"discrepancy/wks_ldap_list.txt")) as f1:
            words1 = set(x.strip().upper() for x in f1)
        with open(os.path.join(project_upload_path ,"discrepancy/wks_ld_list.txt")) as f2:
            words2 = set(x.strip().upper() for x in f2)
        with open(os.path.join(project_upload_path ,"discrepancy/wks_kas_list.txt")) as f3:
            words3 = set(x.strip().upper() for x in f3)
        #with open("/u01/tsc_portal/tsc_portal/discrepancy/data/wks_deis_list.txt") as f4:
            #words4 = set(x.strip().upper() for x in f4)
        #duplicates  = words1.intersection(words2).intersection(words3).intersection(words4)
        duplicates  = words1.intersection(words2).intersection(words3)
        #uniques = (words1-duplicates).union(words2-duplicates).union(words3-duplicates).union(words4-duplicates)
        uniques = (words1-duplicates).union(words2-duplicates).union(words3-duplicates)
        with open(os.path.join(project_upload_path ,"discrepancy/wks_discrepancy.txt"), "w") as f5:
            f5.writelines(str(x)+','+','+str(x in words1)+','+str(x in words2 or x[-1]=="M")+','+str(x in words3)+','+ 'False' +'\n' for x in uniques if not ((x in words1)==(x in words2 or x[-1]=="M")==(x in words3)==True))
    
        # test with last user
        with open(os.path.join(project_upload_path ,"discrepancy/wks_ld_lastuser_list.txt")) as flastuser2:
            wordslastuser2 = [x.strip().upper() for x in flastuser2]
            flastuser2_dic = {}
            for x in wordslastuser2:
                list = x.split(",")
                #print "lastuser::list::%s"%list
                flastuser2_dic[list[0]] = list[1]
            wordslastuser2 = set(flastuser2_dic.keys())
        #print "lastuserdic%s"%flastuser2_dic
        #print "lastuser%s"%wordslastuser2
        duplicates_lastuser = words1.intersection(wordslastuser2).intersection(words3)
        uniques_lastuser = (words1-duplicates_lastuser).union(wordslastuser2-duplicates_lastuser).union(words3-duplicates_lastuser)
        for x in uniques_lastuser:
            if str(x) not in flastuser2_dic:
                flastuser2_dic[str(x)] = '-'
        with open(os.path.join(project_upload_path ,"discrepancy/wks_lastuser_discrepancy.txt"), "w") as flastuser5:
            flastuser5.writelines(str(x)+','+','+str(x in words1)+','+str(x in wordslastuser2 or x[-1]=="M")+','+str(x in words3)+','+ 'False' +','+ flastuser2_dic[str(x)] +'\n' for x in uniques_lastuser if not ((x in words1)==(x in wordslastuser2 or x[-1]=="M")==(x in words3)==True))
    else:
        with open(os.path.join(project_upload_path ,"discrepancy/srv_ldap_list.txt")) as f1:
            words1 = set(x.strip().upper() for x in f1)
        with open(os.path.join(project_upload_path ,"discrepancy/srv_ld_list.txt")) as f2:
            words2 = set(x.strip().upper() for x in f2)
        with open(os.path.join(project_upload_path ,"discrepancy/srv_kas_list.txt")) as f3:
            words3 = set(x.strip().upper() for x in f3)
        with open(os.path.join(project_upload_path ,"discrepancy/srv_deis_list.txt")) as f4:
            words4 = set( x.strip().upper()[5:] if 'NSTK_' in x.strip().upper() else x.strip().upper() for x in f4 )
        #duplicates  = words1.intersection(words2)
        #uniques1 = words1.difference(words2).union(words2.difference(words1))
        #uniques2 = duplicates.difference(words3).union(words3.difference(duplicates))

        duplicates = words1.intersection(words2).intersection(words3).intersection(words4)
      
        uniques = (words1-duplicates).union(words2-duplicates).union(words3-duplicates).union(words4-duplicates)

        landesk_device = (words1-duplicates).union(words2-duplicates)
        kaspersky_device = (words1-duplicates).union(words3-duplicates)
        deis_device = (words1-duplicates).union(words4-duplicates)
         
        in_landesk = []
        db_conn = _get_connection_security_ld()
        if db_conn is not None:
            cursor = db_conn.cursor()
            try:
                ld_ip_query = "select Upper(Hostname), tcp.Address from dbo.TCP where tcp.Address in (select tcp.Address from Computer c inner join dbo.TCP tcp on tcp.HostName like (c.deviceName + '%') where c.deviceName in ('{0}') group by tcp.Address)".format("','".join(x for x in landesk_device))
                cursor.execute(ld_ip_query)
                for e in cursor.fetchall():
                    e[0] = e[0].split('.')[0]
                    in_landesk.append(e)
                #print "in_landesk:%s"%in_landesk
            finally:
                _close_connection(db_conn)

        in_kaspersky = []
        for db_conn in [_get_connection_kaspsky_americas(),_get_connection_kaspsky_europe(), _get_connection_kaspsky_asia()]:
            if db_conn is not None:
                try:
                    cursor = db_conn.cursor()
                    in_kapsky_query = "select strWinHostName, strAddress from Hosts (nolock) where strWinHostName in ('{0}')".format("','".join(kaspersky_device))
                    cursor.execute(in_kapsky_query)
                    for e in cursor.fetchall():
                        e[1] = e[1].split("//")[1].split(":")[0] if e[1] and "//" in e[1] else e[1]
                        in_kaspersky.append(e)
                    #print "in_kaspersky:%s"%in_kaspersky        
                finally:
                     _close_connection(db_conn) 


        in_deis = []
        db_conn = _get_connection_deis()
        if db_conn is not None:
            cursor = db_conn.cursor()
            try:
                deis_ip_query = "select Upper([Machine Name]), [InvItemText02], [Server_Location] from [_SMDBA_].[Inventory Items] where [Machine Name] in ('{0}')".format("','".join(x for x in deis_device))
                cursor.execute(deis_ip_query)
                for e in cursor.fetchall():
                    e[0] = e[0].split('.')[0]
                    in_deis.append(e)
                #print "in_deis:%s"%in_deis
            finally:
                _close_connection(db_conn)

        all_device_ip = in_landesk 
        all_device_ip.extend(in_kaspersky)
        all_device_ip.extend(in_deis)
        #print "all_device_ip:%s"%all_device_ip
        all_device_ip_dict = dict((adi[0],adi[1]) for adi in all_device_ip if adi[1] is not None)
        all_device_address_dict = dict((adi[0],adi[2]) for adi in all_device_ip if len(adi) >2 and adi[2] is not None)
        to_be_removed_device_in_location_frcclr = [key for key,value in all_device_address_dict.items() if value=='FR - Chic Chevilly la Rue']
        normal_check_device_in_location_frcclr = [ item for item in to_be_removed_device_in_location_frcclr if item in uniques]
        total_uniques = [item for item in uniques if item not in to_be_removed_device_in_location_frcclr]
        #print "all_device_address_dict:%s"%all_device_address_dict
        with open(os.path.join(project_upload_path ,"discrepancy/srv_discrepancy.txt"), "w") as f5:
            #f5.writelines(str(x)+','+str(all_device_ip_dict.get(x,'')) +','+str(x in words1)+','+str(x in words2)+','+str(x in words3)+','+str(x in words4)+'\n' for x in uniques)
            f5.writelines(str(x)+','+str(all_device_ip_dict.get(x,'')) +','+str(x in words1)+','+str(x in words2)+','+str(x in words3 or ('Chic' in all_device_address_dict.get(x,'')))+','+str(x in words4)+'\n' for x in total_uniques if not ((x in words1)==(x in words2)==(x in words3 or ('Chic' in all_device_address_dict.get(x,'')))==(x in words4)==True))
            f5.writelines(str(x)+','+str(all_device_ip_dict.get(x,'')) +','+str(x in words1)+','+str(x in words2)+','+str(x in words3)+','+str(x in words4)+'\n' for x in normal_check_device_in_location_frcclr if not ((x in words1)==(x in words2)==(x in words3)==(x in words4)==True))

if __name__=="__main__":
    if __package__ is None:
        import sys
        from os import path
        sys.path.append( path.dirname(path.dirname( path.dirname( path.abspath(__file__) ) ) ) )
        os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'hygie.settings')
        from django.conf import settings
    else:
        from django.conf import settings
    _get_ldap_device("WKS")
    _get_landesk_device("WKS")
    _get_kapsky_device("WKS")
    #_get_deis_device("WKS")
    _get_discrepancy("WKS") 
    _get_ldap_device("SRV")
    _get_landesk_device("SRV")
    _get_kapsky_device("SRV")
    _get_deis_device("SRV")
    _get_discrepancy("SRV")
    print('Last run on: ', datetime.datetime.utcnow() )
