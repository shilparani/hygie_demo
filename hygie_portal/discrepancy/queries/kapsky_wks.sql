select distinct T1.Device 
from 
(select Upper(strWinHostName) as Device 
from Hosts (nolock) 
where 
tmLastNagentConnected>dateadd(day,-30,getdate()) 
and (strWinHostName like 'LV%') 
group by Upper(strWinHostName)) T1 
left join (select Upper(strWinHostName) as Device, 
isNULL(hst_prdstates.wstrProductDisplVersion,0) as Version 
From Hosts (nolock) left join hst_prdstates (nolock) on Hosts.nId = hst_prdstates.nHostID 
left join hst_products (nolock) on hst_products.nId = hst_prdstates.nProduct  
and wstrProductName in ('KAVWKS6','KES') 
where tmLastNagentConnected>dateadd(day,-30,getdate()) 
and (strWinHostName like 'LV%') 
and wstrProductName is not NULL) T2 on T1.Device=T2.Device