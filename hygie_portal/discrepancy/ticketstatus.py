import json
#from flask import current_app
from hygie.settings import logger
from datetime import timedelta
from dateutil import tz
from math import floor
from collections import defaultdict,OrderedDict
import datetime
import pytz
import operator
import os
import requests
from datetime import datetime
import MySQLdb
import time

def verif_existing_ticket():

    ticket_data = []
    
    db_conn = MySQLdb.connect(user='top10', passwd='pwtEGu8kXOpglxgA', host='NLVCP2008P', db='autobot')
    
    cursor = db_conn.cursor()
    
    connstr = "Select workstation,ticketitsm, guiditsm from events_discrepancy where status = '0';"
   
    cursor.execute(connstr)
    
    for t in cursor.fetchall():
        t = [x if x is not None else '' for x in t]
        ticket_data.append(t)
    
    db_conn.close()
    
    return ticket_data

def get_ticket_status(workstation,ad,ld,kav):

    logger.info(workstation)
    
    workstation = workstation.replace('"','')
    ad = ad.replace('"','')
    ld = ld.replace('"','')
    kav = kav.replace('"','')
    
    check_workstation = workstation[2:4]
    groupe = "null"
    
    if "EU" in check_workstation or "CP" in check_workstation:
        groupe = "CP_SD_L1_SERVICE_DESK"
    elif "AP" in check_workstation or "CN" in check_workstation or "JP" in check_workstation:
        groupe = "AS_SD_L1_SERVICE_DESK"
    else:
        groupe = "AM_SD_L1_SERVICE_DESK"

    now = datetime.now()
    autobotid =  "AUTOBOT-5" + now.strftime("%Y%m%d%s") 

    body = """<?xml version="1.0" encoding="utf-8"?>
        <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
          <soap:Body>
            <PostEventExtended xmlns="http://touchpaper.com/tps/EventLog">
              <EventSource>Incident</EventSource>
              <EventType>Create_Incident</EventType>
              <Title>Discrepancy Workstation WORKSTATION_TO_CHANGE (AUTOBOT_ID_TO_CHANGE)</Title>
              <Description></Description>
              <ConfigurationItem>WORKSTATION_TO_CHANGE</ConfigurationItem>
              <Priority></Priority>
              <Status></Status>
              <Parameter1>GROUPE_TO_ASSIGN</Parameter1>
              <Parameter2>Staff AUTOBOT</Parameter2>
              <Parameter3>Staff AUTOBOT</Parameter3>
              <Parameter4>Staff AUTOBOT</Parameter4>
              <Parameter5>WebService</Parameter5>
              <Parameter6>Minor Degradation</Parameter6>
              <Parameter7>Application Service Level</Parameter7>
              <Parameter8>Workstation</Parameter8>
              <Parameter9>Bronze</Parameter9>
              <Parameter10></Parameter10>
              <Parameter11></Parameter11>
              <Parameter12>IS&amp;T for IS&amp;T applications - Monitoring Components - TRUESIGHT</Parameter12>
              <Parameter13>Architecture</Parameter13>
              <Parameter14>GROUPE_TO_ASSIGN</Parameter14>
              <Parameter15></Parameter15>
              <Parameter16>DESCRIPTION_TO_CHANGE</Parameter16>
              <Parameter17></Parameter17>
              <Parameter18></Parameter18>
              <Parameter19></Parameter19>
              <Parameter20>AUTOBOT_ID_TO_CHANGE</Parameter20>
            </PostEventExtended>
        </soap:Body>
        </soap:Envelope>"""

    description = workstation + " is in discrepancy on Hygie for the following object :"

    if (ad == 'KO'):
        description += " - Active directory "

    if (ld == 'KO'):
        description += " - Landesk  "

    if (kav == 'KO'):
        description += " - Kaspersky "

    body = body.replace("AUTOBOT_ID_TO_CHANGE",autobotid)

    body = body.replace("DESCRIPTION_TO_CHANGE",description)

    body = body.replace("WORKSTATION_TO_CHANGE",workstation)
    
    body = body.replace("GROUPE_TO_ASSIGN",groupe)
     
    url = "https://louisvuitton.landesk.com/LouisVuitton.FrameworkExplicit/EventLog.asmx"

    headers = {"SOAPAction" : "http://touchpaper.com/tps/EventLog/PostEventExtended"}

    #import pdb;pdb.set_trace()
    response = requests.post(url, data=body, headers=headers)

    logger.info(body)
    
    logger.info(response)
    
    time.sleep( 3 )
    
    body = """<?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Body>
        <PostEventExtended xmlns="http://touchpaper.com/tps/EventLog">
    <EventSource>Incident</EventSource>
          <EventType>Note_Incident</EventType>
          <Title>Retrieving Ticket Number</Title>
          <Description></Description>
          <ConfigurationItem></ConfigurationItem>
          <Priority></Priority>
          <Status></Status>
          <Parameter1>Private Note</Parameter1>
          <Parameter2>True</Parameter2>
          <Parameter3>False</Parameter3>
          <Parameter4>WW_TM_L1_SOC</Parameter4>
          <Parameter5>Staff AUTOBOT</Parameter5>
          <Parameter6></Parameter6>
          <Parameter7></Parameter7>
          <Parameter8></Parameter8>
          <Parameter9></Parameter9>
          <Parameter10></Parameter10>
          <Parameter11></Parameter11>
          <Parameter12></Parameter12>
          <Parameter13></Parameter13>
          <Parameter14></Parameter14>
          <Parameter15></Parameter15>
          <Parameter16></Parameter16>
          <Parameter17></Parameter17>
          <Parameter18></Parameter18>
          <Parameter19></Parameter19>
          <Parameter20>AUTOBOT_ID_TO_CHANGE</Parameter20>
        </PostEventExtended>
    </soap:Body>
    </soap:Envelope>"""

    body = body.replace("AUTOBOT_ID_TO_CHANGE",autobotid)

    response = requests.post(url, data=body, headers=headers)

    logger.info(body)
    
    logger.info(response)
    
    db_conn = MySQLdb.connect(user='top10', passwd='pwtEGu8kXOpglxgA', host='NLVCP2008P', db='autobot')

    cursor = db_conn.cursor()

    #import pdb;pdb.set_trace()
    connstr = "INSERT INTO `events_discrepancy` (`ID`, `Workstation`, `Date`, `AutobotID`, `Status`) VALUES (NULL, '{0}', NOW(), '{1}', '0');"
    connstr = connstr.format(workstation, autobotid)
    cursor.execute(connstr)

    db_conn.close()
    
    return autobotid