# -*- coding: utf-8 -*-
"""Helper utilities and decorators."""

from dateutil import tz
from math import floor
from collections import defaultdict,OrderedDict
from hygie_portal.models import Contentmanager
import datetime as dt
import pytz
import operator
import json
import ast

def get_user_accessable_content_list(request):
    roles_list =  request.user.role.split('|')
    all_content_details = Contentmanager.objects.all()
    user_accessable_content_list = list(set(row.title for row in all_content_details for role in roles_list if role in row.allowedaccess))
    
    return user_accessable_content_list

def save_content_settings(content_settings):
    print("content_settings :: %s"%content_settings)

    content_details = None
    # check if content id exists from contentManager
    if content_settings['id']:
        content_details = Contentmanager.objects.filter(id = content_settings['id']).first()
        print(content_details)
        
    if content_details:
        if 'delete' in content_settings:
            #delete content
            Contentmanager.delete(content_details)
        else:
            #update existing content settings
            #Contentmanager.update(content_details, content_settings)
            content_settings['lastupdated'] = dt.datetime.now()
            Contentmanager.objects.filter(id = content_settings['id']).update(title = content_settings['title'], description = content_settings['description'], module = content_settings['module'], contenttext = content_settings['contenttext'], lastuser = content_settings['lastuser'], allowedaccess = content_settings['allowedaccess'], lastupdated = content_settings['lastupdated'].strftime('%Y-%m-%d %H:%M:%S'))
        
    else:
        #create new settings
        print("new content details")
        new_content = Contentmanager(title = content_settings['title'], description = content_settings['description'], module = content_settings['module'], contenttext = content_settings['contenttext'], lastupdated = dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), lastuser = content_settings['lastuser'] , allowedaccess = content_settings['allowedaccess'])
        new_content.save()
        #Contentmanager.add_content(new_content)
    
    return True    

def get_content_details_by_title(content_title):
    content = Contentmanager.objects.filter(title = content_title).first().__dict__
    del content['_state']
    content['lastupdated'] = content['lastupdated'].strftime('%Y-%m-%d %H:%M:%S')
    
    return content

def get_content_by_title(content_title):
    content = Contentmanager.objects.filter(title = content_title).first().__dict__
    return content['contenttext'] 

def get_content_details_by_module(module_name):
    content_list = Contentmanager.objects.filter(module__contains=module_name).all()
    print("================= 1" )
    print(content_list[0])
    print("================= 2" )
    content = {}
    for c in content_list:
        print(c.title)
        print("================= 3" )
        content[c.title] = c.contenttext    
    
    return content

def delete_content(content_title):
    
    # check if content id exists from contentManager
    content_details = Contentmanager.objects.filter(title = content_title).first()
    print(content_details)
    if content_details:
        Contentmanager.delete(content_details)
    
    return True
    