# -*- coding: utf-8 -*-
"""Helper utilities and decorators."""

#from flask import current_app
#from flask_login import current_user
from datetime import timedelta
from dateutil import tz
from math import floor
from collections import defaultdict,OrderedDict
from hygie_portal.models import Querymanager
import datetime as dt
import pytz
import operator
import json
import ast

def get_user_accessable_query_list(request):
    roles_list =  request.user.role.split('|')
    all_query_details = Querymanager.objects.all()
    user_accessable_query_list = list(set(row.title for row in all_query_details for role in roles_list if role in row.allowedaccess))
    
    return user_accessable_query_list

def save_query_settings(query_settings_temp):
    print("query_settings_temp :: %s"%query_settings_temp)  

    query_details = None
    # check if query id exists from queryManager
    if query_settings_temp['id']:
        query_details = Querymanager.objects.filter(id = query_settings_temp['id']).first()
        print(query_details)
        
    if query_details:
        query_details = Querymanager.objects.filter(id = query_settings_temp['id']).first()
        if 'delete' in query_settings_temp:
            #delete query
            Querymanager.delete(query_details)
            #Querymanager.delete_query(query_details)
        else:
            #update existing query settings
            query_settings_temp['lastupdated'] = dt.datetime.now()
            Querymanager.objects.filter(id=query_details.id).update(title = query_settings_temp['title'], description = query_settings_temp['description'], querytext = query_settings_temp['querytext'], lastuser = query_settings_temp['lastuser'], allowedaccess = query_settings_temp['allowedaccess'], lastupdated = query_settings_temp['lastupdated'].strftime('%Y-%m-%d %H:%M:%S'))
        
    else:
        #create new settings
        print("new query details")
        new_query = Querymanager(title = query_settings_temp['title'], description = query_settings_temp['description'], querytext = query_settings_temp['querytext'], lastupdated = dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), lastuser = query_settings_temp['lastuser'] , allowedaccess = query_settings_temp['allowedaccess'])
        #Querymanager.add_query(new_query)
        new_query.save()
    
    return True    

def get_query_details_by_title(query_title):
    query = Querymanager.objects.filter(title = query_title).first().__dict__
    del query['_state']
    query['lastupdated'] = query['lastupdated'].strftime('%Y-%m-%d %H:%M:%S')
    
    return query

def get_query_by_title(query_title):
    query = Querymanager.objects.filter(title = query_title).first().__dict__
    return query['querytext'] 

def delete_query(query_title):
    
    # check if query id exists from queryManager
    query_details = Querymanager.objects.filter(title = query_title).first()
    print(query_details)
    if query_details:
        #del query_details
        Querymanager.delete(query_details)
    
    return True
    