#!/var/www/html/.pyenv/versions/3.6.5/envs/venv_hygie_prd/bin/python3.6
# -*- coding: utf-8 -*-
from datetime import timedelta

import json, datetime
import pyodbc
import os
import MySQLdb

def _get_connection_security():
    try:
        conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format('NLVCP1714P','1575','MSLDSK6PRD','GSQL_LANDESK_PRD','h4LuuDrD5x'))
        return conn
    except pyodbc.Error as err:
        print('Error connecting to security DB:', err)
        return None
    
def _get_patch_connection():
    try:
        #conn = MySQLdb.connect(user='hygie_prp', passwd='TFhj95fcht', host='nlvcp2064d', db='Hygie_prp')
        conn = MySQLdb.connect(user='esurvey', passwd='esurvey', host='nlvcp2008p', db='Hygie')
        return conn
    except MySQLdb.Error as err:
        print('Error connecting to patch DB:', err)
        return None

def _close_connection(conn):
    conn.close()

def _get_security_microsoft_patch_wks_common(patch_required_str):
    result = []
    db_conn = _get_connection_security() 
    if db_conn is None:
        return result
    cursor = db_conn.cursor()
    patch_required_wks_str = patch_required_str
    ncur_patch_required_wks= cursor.execute(patch_required_wks_str)
    result_patch_required_wks= ncur_patch_required_wks.fetchone()
    result.append(result_patch_required_wks)

    _close_connection(db_conn)
    return result

def _get_security_microsoft_patch_wks_zone(security_type=None, patch_required_str=None):

    security_microsoft_patch_count_list = []
    patch_conn = _get_patch_connection()
    if patch_conn is None:
        return security_microsoft_patch_count_list
    patch_cursor = patch_conn.cursor()
    patch_cursor.execute("select campaign_name, campaign_startdate, srv_patch_list, wks_patch_list from patchcampaigns")
    patch_data = patch_cursor.fetchall()
    _close_connection(patch_conn)
    
    default_keys = ["campaign_name","campaign_startdate","srv_patch_list","wks_patch_list"]
    
    srv_in21days_patch_list = []
    wks_in21days_patch_list = []
    for patch in patch_data:
        patch = dict(zip(default_keys, patch))
        #print patch['srv_patch_list']
        patch_date = patch['campaign_startdate']
        in21days_from_today = (datetime.datetime.now()-datetime.timedelta(days=26)).date()
        
        if patch_date < in21days_from_today:
            srv_patch_list = json.loads(patch['srv_patch_list'])
            wks_patch_list = json.loads(patch['wks_patch_list'])
            # include only active patches
            srv_in21days_patch_list.extend([x for x in srv_patch_list.keys() if srv_patch_list[x]['status']])
            wks_in21days_patch_list.extend([x for x in wks_patch_list.keys() if wks_patch_list[x]['status']])
    #print "=== PATCH :: srv_in21days_patch_list:%s"% srv_in21days_patch_list
    #print "=== PATCH :: wks_in21days_patch_list:%s"% wks_in21days_patch_list
    
    #remove duplicates in the 21 days patch list
    srv_in21days_patch_list = "','".join(set(p for p in srv_in21days_patch_list))
    wks_in21days_patch_list = "','".join(set(p for p in wks_in21days_patch_list))
    
    project_upload_path = settings.UPLOAD_FOLDER
    ## get americas data
    #patch_required_americas_str = patch_required_str.format("(Computer.ComputerLocation like '%Na-%' or Computer.ComputerLocation like '%La-%') or (Computer.DeviceName like 'LVNA%' or Computer.DeviceName like 'LVLA%')") if security_type =="WKS" else patch_required_str.format("(Computer.ComputerLocation like '%Na-%' or Computer.ComputerLocation like '%La-%') or (computer.devicename like 'NLVNA%' or computer.devicename like 'NLVLA%')")
    patch_required_americas_str = patch_required_str.format("Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Na-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/La-%Workstation%' or Computer.DisplayName like 'LVNA%' or Computer.DisplayName like 'LVLA%'",wks_in21days_patch_list) if security_type =="WKS" else patch_required_str.format("(Computer.ComputerLocation like '%Na-%' or Computer.ComputerLocation like '%La-%') or (computer.devicename like 'NLVNA%' or computer.devicename like 'NLVLA%')",srv_in21days_patch_list)
    security_count_americas = _get_security_microsoft_patch_wks_common(patch_required_americas_str)
    security_microsoft_patch_count_list.append(security_count_americas[0][0])

    ## get europe data
    #patch_required_emea_str = patch_required_str.format("(Computer.ComputerLocation like '%Cp-%' or Computer.ComputerLocation like '%Eu-%') or (Computer.DeviceName like 'LVCP%' or Computer.DeviceName like 'LVEU%')") if security_type =="WKS" else patch_required_str.format("(Computer.ComputerLocation like '%Cp-%' or Computer.ComputerLocation like '%Eu-%') or (computer.devicename like 'NLVCP%' or computer.devicename like 'ELVCP%' or computer.devicename like 'NLVEU%' or computer.devicename like 'ELVEU%')")
    patch_required_emea_str = patch_required_str.format("Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Cp-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Eu-%Workstation%' or Computer.DisplayName like 'LVEU%' or Computer.DisplayName like 'LVCP%'",wks_in21days_patch_list) if security_type =="WKS" else patch_required_str.format("(Computer.ComputerLocation like '%Cp-%' or Computer.ComputerLocation like '%Eu-%') or (computer.devicename like 'NLVCP%' or computer.devicename like 'ELVCP%' or computer.devicename like 'NLVEU%' or computer.devicename like 'ELVEU%')",srv_in21days_patch_list)
    security_count_europe = _get_security_microsoft_patch_wks_common(patch_required_emea_str)
    security_microsoft_patch_count_list.append(security_count_europe[0][0])

    ## get asia data
    #patch_required_asia_str = patch_required_str.format("(Computer.ComputerLocation like '%Ap-%' or Computer.ComputerLocation like '%Cn-%' or Computer.ComputerLocation like '%Jp-%') or  (computer.devicename like 'LVAP%' or computer.devicename like 'LVCN%' or computer.devicename like 'LVJP%')") if security_type =="WKS" else patch_required_str.format("(Computer.ComputerLocation like '%Ap-%' or Computer.ComputerLocation like '%Cn-%' or Computer.ComputerLocation like '%Jp-%') or (computer.devicename like 'NLVAP%' or computer.devicename like 'NLVCN%' or computer.devicename like 'NLVJP%' or computer.devicename like 'ELVAP%' or computer.devicename like 'ELVCN%' or computer.devicename like 'ELVJP%' or computer.devicename like 'NLVSG%')")
    patch_required_asia_str = patch_required_str.format("Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Ap-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Cn-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Jp-%Workstation%' or Computer.DisplayName like 'LVAP%' or Computer.DisplayName like 'LVCN%' or Computer.DisplayName like 'LVJP%' or Computer.DisplayName like 'LVSG%'",wks_in21days_patch_list) if security_type =="WKS" else patch_required_str.format("(Computer.ComputerLocation like '%Ap-%' or Computer.ComputerLocation like '%Cn-%' or Computer.ComputerLocation like '%Jp-%') or (computer.devicename like 'NLVAP%' or computer.devicename like 'NLVCN%' or computer.devicename like 'NLVJP%' or computer.devicename like 'ELVAP%' or computer.devicename like 'ELVCN%' or computer.devicename like 'ELVJP%' or computer.devicename like 'NLVSG%')",srv_in21days_patch_list)
    security_count_asia = _get_security_microsoft_patch_wks_common(patch_required_asia_str)
    security_microsoft_patch_count_list.append(security_count_asia[0][0])

    ## all data
    security_microsoft_patch_count_list.append(security_count_americas[0][0]+security_count_europe[0][0]+security_count_asia[0][0])

    return security_microsoft_patch_count_list 

def _save_history_data_in_txt():

    project_upload_path = settings.UPLOAD_FOLDER
    ## wks init query

    patch_required_wks_str = """
        select
            count(*) as [Impacted WKS]
        from
          (
          select distinct Computer.DisplayName
          From Computer (nolock) 
          inner join CVDetected on CVDetected.Computer_Idn=Computer.Computer_Idn
          inner join Vulnerability on CVDetected.Vulnerability_Idn=Vulnerability.Vulnerability_Idn
          Where 
             (
             Computer.Computer_Idn in 
                (
                   (select
                      Computer.Computer_Idn 
                   from
                      Computer (nolock)
                            left join ComputerVulnerability (nolock) on ComputerVulnerability.Computer_Idn=Computer.Computer_Idn 
                                  and ComputerVulnerability.PatchInstallSucceeded=2
                                  and ComputerVulnerability.PatchDetected<>1
                      where 
                         ComputerVulnerability.Computer_Idn is not NULL
                         and ComputerVulnerability.Vul_ID in ('{1}')
                   )
                   UNION
                   (select CVDetected.Computer_Idn 
                   from CVDetected (nolock) 
                      left join Computer (nolock) on Computer.Computer_Idn = CVDetected.Computer_Idn 
                      left join Vulnerability (nolock) on Vulnerability.Vulnerability_Idn=CVDetected.Vulnerability_Idn 
                   where 
                      Computer.Computer_Idn is not NULL and Vulnerability.Vulnerability_Idn is not NULL
                      and Vulnerability.Vul_ID in ('{1}')
                      and SupercededState<>2
                   group by    CVDetected.Computer_Idn
                   ) 
                   UNION
                   (select Computer.Computer_Idn 
                   from Computer (nolock) ,
                      Vulnerability2 inner join Vulnerability on Vulnerability2.Vulnerability_Idn=Vulnerability.Vulnerability_Idn
                   where 
                      charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  ) >0
                      and ((charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  )-1) % 4 )=0
                      and Vulnerability.Vul_ID in ('{1}')
                      and SupercededState<>2
                   group by    Computer.Computer_Idn
                   )
                )
             )
             and ({0})
             and (Computer.Type Not Like '%Server%')
             and datediff(hour,Computer.recorddate,getdate())>12
             and Vulnerability.Vul_ID in ('{1}')
          group by
              Computer.DisplayName
          ) T1
    """

    ## srv init query
    patch_required_srv_str_prd = """
        select
            count(*) as [Impacted WKS]
        from
          (
            select distinct Computer.DeviceName
            From Computer (nolock)
            inner join CVDetected on CVDetected.Computer_Idn=Computer.Computer_Idn
            inner join Vulnerability on CVDetected.Vulnerability_Idn=Vulnerability.Vulnerability_Idn
            Where
             (
             Computer.Computer_Idn in
                (
                   (select
                      Computer.Computer_Idn
                   from
                      Computer (nolock)
                            inner join ComputerVulnerability (nolock) on ComputerVulnerability.Computer_Idn=Computer.Computer_Idn
                                  and ComputerVulnerability.PatchInstallSucceeded=2
                                  and ComputerVulnerability.PatchDetected<>1
                      where
                         ComputerVulnerability.Computer_Idn is not NULL
                         and ComputerVulnerability.Vul_ID in ('{1}')
                   )
                   UNION
                   (select CVDetected.Computer_Idn
                   from CVDetected (nolock)
                      inner join Computer (nolock) on Computer.Computer_Idn = CVDetected.Computer_Idn
                      inner join Vulnerability (nolock) on Vulnerability.Vulnerability_Idn=CVDetected.Vulnerability_Idn
                   where
                      Computer.Computer_Idn is not NULL and Vulnerability.Vulnerability_Idn is not NULL
                      and Vulnerability.Vul_ID in ('{1}')
                      and SupercededState<>2
                   group by    CVDetected.Computer_Idn
                   )
                   UNION
                   (select Computer.Computer_Idn
                   from Computer (nolock) ,
                      Vulnerability2 inner join Vulnerability on Vulnerability2.Vulnerability_Idn=Vulnerability.Vulnerability_Idn
                   where
                      charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  ) >0
                      and ((charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  )-1) % 4 )=0
                      and Vulnerability.Vul_ID in ('{1}')
                      and SupercededState<>2
                   group by    Computer.Computer_Idn
                   )
                )
             )
            and Upper(Right(Computer.DisplayName,1)) in ('P','B')
            and (Computer.Type Like '%Server%' or Computer.Type Like '%Virtual Machine%')
            and datediff(hour,Computer.recorddate,getdate())>12
            and ({0})
            and Vulnerability.Vul_ID in ('{1}')
          ) T1
    """

    patch_required_srv_str_nonprd = """
        select
            count(*) as [Impacted WKS]
        from
          (
            select distinct Computer.DeviceName
            From Computer (nolock)
            inner join CVDetected on CVDetected.Computer_Idn=Computer.Computer_Idn
            inner join Vulnerability on CVDetected.Vulnerability_Idn=Vulnerability.Vulnerability_Idn
            Where
             (
             Computer.Computer_Idn in
                (
                   (select
                      Computer.Computer_Idn
                   from
                      Computer (nolock)
                            inner join ComputerVulnerability (nolock) on ComputerVulnerability.Computer_Idn=Computer.Computer_Idn
                                  and ComputerVulnerability.PatchInstallSucceeded=2
                                  and ComputerVulnerability.PatchDetected<>1
                      where
                         ComputerVulnerability.Computer_Idn is not NULL
                         and ComputerVulnerability.Vul_ID in ('{1}')
                   )
                   UNION
                   (select CVDetected.Computer_Idn
                   from CVDetected (nolock)
                      inner join Computer (nolock) on Computer.Computer_Idn = CVDetected.Computer_Idn
                      inner join Vulnerability (nolock) on Vulnerability.Vulnerability_Idn=CVDetected.Vulnerability_Idn
                   where
                      Computer.Computer_Idn is not NULL and Vulnerability.Vulnerability_Idn is not NULL
                      and Vulnerability.Vul_ID in ('{1}')
                      and SupercededState<>2
                   group by    CVDetected.Computer_Idn
                   )
                   UNION
                   (select Computer.Computer_Idn
                   from Computer (nolock) ,
                      Vulnerability2 inner join Vulnerability on Vulnerability2.Vulnerability_Idn=Vulnerability.Vulnerability_Idn
                   where
                      charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  ) >0
                      and ((charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  )-1) % 4 )=0
                      and Vulnerability.Vul_ID in ('{1}')
                      and SupercededState<>2
                   group by    Computer.Computer_Idn
                   )
                )
             )
            and Upper(Right(Computer.DisplayName,1)) not in ('P','B')
            and (Computer.Type Like '%Server%' or Computer.Type Like '%Virtual Machine%')
            and datediff(hour,Computer.recorddate,getdate())>12
            and ({0})
            and Vulnerability.Vul_ID in ('{1}')
          ) T1
    """


    ## wks data
    today_patch_required_wks_data = _get_security_microsoft_patch_wks_zone("WKS", patch_required_wks_str)
    ## srv data
    today_patch_required_srv_data_prd = _get_security_microsoft_patch_wks_zone("SRV", patch_required_srv_str_prd)
    today_patch_required_srv_data_nonprd = _get_security_microsoft_patch_wks_zone("SRV", patch_required_srv_str_nonprd)

    ## save data into files
    history_wks_file = os.path.join(project_upload_path ,"securities/histories/history_data_wks_v2.txt")
    history_srv_file = os.path.join(project_upload_path ,"securities/histories/history_data_srv_v2.txt")
    with open(history_wks_file,"r") as myfilein:
        lines = myfilein.readlines()
        lines.append(datetime.datetime.now().strftime('%m/%d/%Y')+":"+",".join(str(item) for item in today_patch_required_wks_data) + "\n")
    with open(history_wks_file,"w") as myfileout:
        myfileout.writelines(lines[-50:])

    with open(history_srv_file,"r") as myfilein:
        lines = myfilein.readlines()
        lines.append(datetime.datetime.now().strftime('%m/%d/%Y')+":"+",".join(str(item) for item in( today_patch_required_srv_data_prd+today_patch_required_srv_data_nonprd )) + "\n")
    with open(history_srv_file,"w") as myfileout:
        myfileout.writelines(lines[-50:])
        
if __name__=="__main__":
    if __package__ is None:
        import sys
        from os import path
        sys.path.append( path.dirname(path.dirname( path.dirname( path.abspath(__file__) ) ) ) )
        os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'hygie.settings')
        from django.conf import settings
    else:
        from django.conf import settings
    _save_history_data_in_txt()
    print('Last run on: ', datetime.datetime.utcnow())

