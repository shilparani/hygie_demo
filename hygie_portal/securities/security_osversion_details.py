#!/var/www/html/.pyenv/versions/3.6.5/envs/venv_hygie_prd/bin/python3.6
# -*- coding: utf-8 -*-
from datetime import timedelta
from collections import defaultdict

import datetime
import pyodbc
import os
import json

def _get_connection_osdetails():
    try:
        conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format('NLVCP1714P','1575','MSLDSK6PRD','GSQL_LANDESK_PRD','h4LuuDrD5x'))
        return conn
    except pyodbc.Error as err:
        print('Error connecting to security osdetails DB:', err)
        return None

def _close_connection(conn):
    conn.close()

def _get_os_details(os_details_str):
    result = []
    collected_details={}
    db_conn = _get_connection_osdetails()
    if db_conn is None:
        return collected_details
    cursor = db_conn.cursor()
   # print cursor.execute(os_details_str).fetchall()
    all_wks_details = cursor.execute(os_details_str)
    #print all_wks_details.fetchone()
    #computer_osversion_list = []
    total_asia=0
    total_americas=0
    total_emea=0
    final_collected_list={}
    #region_list={}
    release_build={"unspecified":{},'EMEA':{},'ASIA':{},'AMERICAS':{}}
    region_list={"unspecified":0,'EMEA':0,'ASIA':0,'AMERICAS':0}
    region_list_versionwise={"unspecified": {}, 'EMEA': {}, 'ASIA': {}, 'AMERICAS':{}}
    windowsXP_country_list,windows7_country_list,windows8_country_list,windows10_country_list,unknown_country_list=[],[],[],[],[]
    asia_windows_XP,asia_windows_7,asia_windows_8,asia_windows_10,asia_unknown,americas_windows_XP,americas_windows_7,americas_windows_8,americas_windows_10,americas_unknown,emea_windows_XP,emea_windows_7,emea_windows_8,emea_windows_10,emea_unknown=0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
    
    data = all_wks_details.fetchall()
    for osdetails in data:
    
        osdetails = ["unspecified" if d is None else d for d in osdetails]
        #computer_osversion_list.append(dict([('ostype',osdetails[7]),('region',osdetails[19])]))
        #osdetails[7] = osdetails[7].replace(' ', '_') if osdetails[7] else osdetails[7]
        #osdetails[19] = osdetails[19].replace(' ', '_') if osdetails[19] else osdetails[19]

        if osdetails[21] not in final_collected_list:
            final_collected_list[osdetails[21]]={"Windows XP":0,"Windows 7":0,"Windows 8":0,"Windows 10":0,"Unknown":0,"unspecified":0}
       
        if osdetails[7] not in final_collected_list[osdetails[21]]:
            final_collected_list[osdetails[21]][osdetails[7]]=0
            
        final_collected_list[osdetails[21]][osdetails[7]]+=1
        
        if osdetails[19] not in region_list:
            region_list[osdetails[19]]=0
        
        region_list[osdetails[19]]+=1
        
        if osdetails[19] not in region_list_versionwise:
            region_list_versionwise[osdetails[19]]=''
         
        #import pdb;pdb.set_trace()         
        if osdetails[7] not in region_list_versionwise[osdetails[19]]:
            #region_list_versionwise[osdetails[19]][osdetails[7]]=0
            region_list_versionwise[osdetails[19]]={"Windows XP":0,"Windows 7":0,"Windows 8":0,"Windows 10":0,"Unknown":0,"unspecified":0}

        region_list_versionwise[osdetails[19]][osdetails[7]]+=1            
        
        if osdetails[7]=="Windows 10":
            if osdetails[10] not in release_build.keys():
                release_build[osdetails[19]][osdetails[10]]=0
            release_build[osdetails[19]][osdetails[10]]+=1            
        
        #if osdetails[7]=="Windows XP":
        #    windowsXP_country_list.append(osdetails[21])
        #    if osdetails[19]=="ASIA":
        #       asia_windows_XP+=1
        #    elif osdetails[19]=="AMERICAS":
        #       americas_windows_XP+=1
        #    elif osdetails[19]=="EMEA":
        #       emea_windows_XP+=1        
        #elif osdetails[7]=="Windows 7":
        #    windows7_country_list.append(osdetails[21])
        #    if osdetails[19]=="ASIA":
        #       asia_windows_7+=1
        #    elif osdetails[19]=="AMERICAS":
        #       americas_windows_7+=1
        #    elif osdetails[19]=="EMEA":
        #       emea_windows_7+=1
        #elif osdetails[7]=="Windows 8":
        #    windows8_country_list.append(osdetails[21])
        #    if osdetails[19]=="ASIA":
        #       asia_windows_8+=1
        #    elif osdetails[19]=="AMERICAS":
        #       americas_windows_8+=1
        #    elif osdetails[19]=="EMEA":
        #       emea_windows_8+=1
        #elif osdetails[7]=="Windows 10":
        #    windows10_country_list.append(osdetails[21])
        #    if osdetails[19]=="ASIA":
        #       asia_windows_10+=1
        #    elif osdetails[19]=="AMERICAS":
        #       americas_windows_10+=1
        #    elif osdetails[19]=="EMEA":
        #       emea_windows_10+=1
        #else:
        #    unknown_country_list.append(osdetails[21])
        #    if osdetails[19]=="ASIA":
        #       asia_unknown+=1
        #    elif osdetails[19]=="AMERICAS":
        #       americas_unknown+=1
        #    elif osdetails[19]=="EMEA":
        #       emea_unknown+=1        
    #collected_details["count_asia_windows_XP"]=asia_windows_XP
    #collected_details["count_asia_windows_7"]=asia_windows_7
    #collected_details["count_asia_windows_8"]=asia_windows_8
    #collected_details["count_asia_windows_10"]=asia_windows_10
    #collected_details["count_asia_unknown"]=asia_unknown
    #collected_details["count_americas_windows_XP"]=americas_windows_XP
    #collected_details["count_americas_windows_7"]=americas_windows_7
    #collected_details["count_americas_windows_8"]=americas_windows_8
    #collected_details["count_americas_windows_10"]=americas_windows_10
    #collected_details["count_americas_unknown"]=americas_unknown
    #collected_details["count_emea_windows_XP"]=emea_windows_XP
    #collected_details["count_emea_windows_7"]=emea_windows_7
    #collected_details["count_emea_windows_8"]=emea_windows_8
    #collected_details["count_emea_windows_10"]=emea_windows_10
    #collected_details["count_emea_unknown"]=emea_unknown
    #collected_details["windowsXP_country_list"]=windowsXP_country_list
    #collected_details["windows7_country_list"]=windows7_country_list
    #collected_details["windows8_country_list"]=windows8_country_list
    #collected_details["windows10_country_list"]=windows10_country_list
    #collected_details["unknown_country_list"]=unknown_country_list
    #p=defaultdict(int)
    #for k in windowsXP_country_list:
    #    p[k] += 1
    #Count_windowsXP_country_list=dict(p)
    #collected_details["Count_windowsXP_country_list"]=Count_windowsXP_country_list
    #q=defaultdict(int)
    #for l in windows7_country_list:
    #    q[l] += 1
    #Count_windows7_country_list=dict(q)
    #collected_details["Count_windows7_country_list"]=Count_windows7_country_list
    #r=defaultdict(int)
    #for m in windows8_country_list:
    #    r[m] += 1
    #Count_windows8_country_list=dict(r)
    #collected_details["Count_windows8_country_list"]=Count_windows8_country_list
    #s=defaultdict(int)
    #for n in windows10_country_list:
    #    s[n] += 1
    #Count_windows10_country_list=dict(s)
    #collected_details["Count_windows10_country_list"]=Count_windows10_country_list
    #t=defaultdict(int)
    #for o in unknown_country_list:
    #    t[k] += 1
    #Count_unknown_country_list=dict(t)
    #collected_details["Count_unknown_country_list"]=Count_unknown_country_list
    collected_details["country_wise_list"]=final_collected_list
    collected_details["region"]=region_list
    collected_details["version_wise"]=region_list_versionwise
    collected_details["release_build"]=release_build
    #print computer_osversion_list
    _close_connection(db_conn)
    #return computer_osversion_list
    return collected_details

def _save_osdetails_data_in_txt():
    os_details_str="""
SET NOCOUNT ON
declare @MaxSoftwareScandate as INTEGER ='-30'
SET NOCOUNT ON
declare @softwaretofind  AS VARCHAR(50) ='%Mozilla Firefox%'
SET NOCOUNT ON
SELECT DISTINCT A0.DISPLAYNAME AS "Device Name",

A1.CurrentVersion,
A0.LoginName,
ldap.FirstName,
ldap.LastName,
ldap.EmailAddr as 'Email',
a1.MUILang as 'MUI',

CASE A1.CurrentVersion
    WHEN '5.1' THEN 'Windows XP'
    WHEN '6.1' THEN 'Windows 7'
    WHEN '6.3' THEN 'Windows 8'
    WHEN '10.0' THEN 'Windows 10'
    ELSE 'Unknown'
END AS 'OS version',

A1.MUILang AS "Language", /* OS LANGUAGE */
A3.OSType AS "Operating System", /* OS EDITION */

Case 
    when A1.ReleaseID IS NULL Then 'LTSB'
    else A1.ReleaseID

end as  "Release Build", /* OS BUILD */


A0.Manufacturer,/* MANUFACTURER */
A0.Model,/* MODEL */

case 
when CS.ChassisType LIKE '%Tower%' OR CS.ChassisType LIKE  'Desktop' OR CS.ChassisType LIKE  '%lunch%' OR CS.ChassisType LIKE  '%low%'OR CS.ChassisType LIKE  '%Space%' then 'Desktop'
   when CS.ChassisType LIKE '%Laptop%' OR CS.ChassisType LIKE 'Notebook' OR  CS.ChassisType LIKE '%Portable%' OR CS.ChassisType IS NULL then 'Laptop'
   when CS.ChassisType LIKE '%Server%' OR CS.ChassisType LIKE '%Rack%' then 'Server'
   when CS.ChassisType LIKE 'Other' then 'Virtual Machine'
      when CS.ChassisType LIKE 'All in One' then 'Desktop All in One'
   else 'Unknown'

end as Chassis,



CONVERT(VARCHAR(8),A0.SWLastScanDate,112)  As "Last Sofware Scan date", /* LAST SOFTWARE SCAN DATE */
MONTH(a0.SWLastScanDate) as "Last software scan month",/* LAST SOFTWARE SCAN MONTH */
CONVERT(VARCHAR(8),DATEADD(day, @MaxSoftwareScandate, getdate()),112) as "Max Software scan date",/* MAX DATE FOR QUERY */
A1.LastBootUpTime,/* LAST BOOT TIME */

CASE 
    WHEN A0.DISPLAYNAME LIKE '%LVAP%' THEN 'ASIA PACIFIC'
    WHEN A0.DISPLAYNAME LIKE '%LVCP%' THEN 'CORP'
    WHEN A0.DISPLAYNAME LIKE '%LVEU%' THEN 'EUROPE'
    WHEN A0.DISPLAYNAME LIKE '%LVNA%' THEN 'NORTH AMERICA'
    WHEN A0.DISPLAYNAME LIKE '%LVLA%' THEN 'LATIN AMERICA'
    WHEN A0.DISPLAYNAME LIKE '%LVCN%' THEN 'CHINA'
    WHEN A0.DISPLAYNAME LIKE '%LVJP%' THEN 'JAPAN'
END AS 'region',

CASE 
    WHEN A0.DISPLAYNAME LIKE '%LVAP%' THEN 'ASIA'
    WHEN A0.DISPLAYNAME LIKE '%LVCP%' THEN 'EMEA'
    WHEN A0.DISPLAYNAME LIKE '%LVEU%' THEN 'EMEA'
    WHEN A0.DISPLAYNAME LIKE '%LVNA%' THEN 'AMERICAS'
    WHEN A0.DISPLAYNAME LIKE '%LVLA%' THEN 'AMERICAS'
    WHEN A0.DISPLAYNAME LIKE '%LVCN%' THEN 'ASIA'
    WHEN A0.DISPLAYNAME LIKE '%LVJP%' THEN 'ASIA'
END AS 'Area',

CASE 
    WHEN A0.DISPLAYNAME LIKE '%LVAPAU%' THEN 'AU ' 
    WHEN A0.DISPLAYNAME LIKE '%LVAPCN%' THEN 'CN ' 
    WHEN A0.DISPLAYNAME LIKE '%LVAPGU%' THEN 'GU ' 
    WHEN A0.DISPLAYNAME LIKE '%LVAPHK%' THEN 'HK ' 
    WHEN A0.DISPLAYNAME LIKE '%LVAPID%' THEN 'ID ' 
    WHEN A0.DISPLAYNAME LIKE '%LVAPIN%' THEN 'IN ' 
    WHEN A0.DISPLAYNAME LIKE '%LVAPKR%' THEN 'KR ' 
    WHEN A0.DISPLAYNAME LIKE '%LVAPMN%' THEN 'MN ' 
    WHEN A0.DISPLAYNAME LIKE '%LVAPMO%' THEN 'MO ' 
    WHEN A0.DISPLAYNAME LIKE '%LVAPMP%' THEN 'MP ' 
    WHEN A0.DISPLAYNAME LIKE '%LVAPMY%' THEN 'MY ' 
    WHEN A0.DISPLAYNAME LIKE '%LVAPNZ%' THEN 'NZ ' 
    WHEN A0.DISPLAYNAME LIKE '%LVAPPH%' THEN 'PH ' 
    WHEN A0.DISPLAYNAME LIKE '%LVAPSG%' THEN 'SG ' 
    WHEN A0.DISPLAYNAME LIKE '%LVAPTH%' THEN 'TH ' 
    WHEN A0.DISPLAYNAME LIKE '%LVAPTW%' THEN 'TW ' 
    WHEN A0.DISPLAYNAME LIKE '%LVAPVN%' THEN 'VN ' 
    WHEN A0.DISPLAYNAME LIKE '%LVASTA%' THEN 'TA ' 
    WHEN A0.DISPLAYNAME LIKE '%LVCNCN%' THEN 'CN ' 
    WHEN A0.DISPLAYNAME LIKE '%LVCPCH%' THEN 'CH ' 
    WHEN A0.DISPLAYNAME LIKE '%LVCPES%' THEN 'ES ' 
    WHEN A0.DISPLAYNAME LIKE '%LVCPFR%' THEN 'FR ' 
    WHEN A0.DISPLAYNAME LIKE '%LVCPIT%' THEN 'IT ' 
    WHEN A0.DISPLAYNAME LIKE '%LVCPPT%' THEN 'PT ' 
    WHEN A0.DISPLAYNAME LIKE '%LVCPRO%' THEN 'RO ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUAE%' THEN 'AE ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUAT%' THEN 'AT ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUBE%' THEN 'BE ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUBH%' THEN 'BH ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUCH%' THEN 'CH ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUCZ%' THEN 'CZ ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUDE%' THEN 'DE ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUDK%' THEN 'DK ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUES%' THEN 'ES ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUFI%' THEN 'FI ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUFR%' THEN 'FR ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUGR%' THEN 'GR ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUHU%' THEN 'HU ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUIL%' THEN 'IL ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUIN%' THEN 'IN ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUIT%' THEN 'IT ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUJO%' THEN 'JO ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUKW%' THEN 'KW ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUKZ%' THEN 'KZ ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEULB%' THEN 'LB ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEULU%' THEN 'LU ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUMA%' THEN 'MA ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUNL%' THEN 'NL ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUNO%' THEN 'NO ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUPL%' THEN 'PL ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUPT%' THEN 'PT ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUQA%' THEN 'QA ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEURO%' THEN 'RO ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEURU%' THEN 'RU ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUSA%' THEN 'SA ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUSE%' THEN 'SE ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUTR%' THEN 'TR ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUUA%' THEN 'UA ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUUK%' THEN 'UK ' 
    WHEN A0.DISPLAYNAME LIKE '%LVEUZA%' THEN 'ZA ' 
    WHEN A0.DISPLAYNAME LIKE '%LVFRA0%' THEN 'A0 ' 
    WHEN A0.DISPLAYNAME LIKE '%LVJPJP%' THEN 'JP ' 
    WHEN A0.DISPLAYNAME LIKE '%LVLAAR%' THEN 'AR ' 
    WHEN A0.DISPLAYNAME LIKE '%LVLAAW%' THEN 'AW ' 
    WHEN A0.DISPLAYNAME LIKE '%LVLABR%' THEN 'BR ' 
    WHEN A0.DISPLAYNAME LIKE '%LVLACL%' THEN 'CL ' 
    WHEN A0.DISPLAYNAME LIKE '%LVLACO%' THEN 'CO ' 
    WHEN A0.DISPLAYNAME LIKE '%LVLADO%' THEN 'DO ' 
    WHEN A0.DISPLAYNAME LIKE '%LVLAMX%' THEN 'MX ' 
    WHEN A0.DISPLAYNAME LIKE '%LVLAPA%' THEN 'PA ' 
    WHEN A0.DISPLAYNAME LIKE '%LVLAPM%' THEN 'PM ' 
    WHEN A0.DISPLAYNAME LIKE '%LVLART%' THEN 'RT ' 
    WHEN A0.DISPLAYNAME LIKE '%LVMBSC%' THEN 'SC ' 
    WHEN A0.DISPLAYNAME LIKE '%LVNACA%' THEN 'CA ' 
    WHEN A0.DISPLAYNAME LIKE '%LVNAFR%' THEN 'FR ' 
    WHEN A0.DISPLAYNAME LIKE '%LVNALO%' THEN 'LO ' 
    WHEN A0.DISPLAYNAME LIKE '%LVNAMX%' THEN 'MX ' 
    WHEN A0.DISPLAYNAME LIKE '%LVNAUS%' THEN 'US ' 
    WHEN A0.DISPLAYNAME LIKE '%LVUSUM%' THEN 'UM ' 


END AS 'Country code',

CASE 
    WHEN A0.DISPLAYNAME LIKE '%LVAPAU%' THEN'AUSTRALIA'
WHEN A0.DISPLAYNAME LIKE '%LVAPCN%' THEN'CHINA'
WHEN A0.DISPLAYNAME LIKE '%LVAPGU%' THEN'GUAM'
WHEN A0.DISPLAYNAME LIKE '%LVAPHK%' THEN'HONG KONG'
WHEN A0.DISPLAYNAME LIKE '%LVAPID%' THEN'INDONESIA'
WHEN A0.DISPLAYNAME LIKE '%LVAPIN%' THEN'INDIA'
WHEN A0.DISPLAYNAME LIKE '%LVAPKR%' THEN'KOREA, REPUBLIC OF'
WHEN A0.DISPLAYNAME LIKE '%LVAPMN%' THEN'MONGOLIA'
WHEN A0.DISPLAYNAME LIKE '%LVAPMO%' THEN'MACAO'
WHEN A0.DISPLAYNAME LIKE '%LVAPMP%' THEN'NORTHERN MARIANA ISLANDS'
WHEN A0.DISPLAYNAME LIKE '%LVAPMY%' THEN'MALAYSIA'
WHEN A0.DISPLAYNAME LIKE '%LVAPNZ%' THEN'NEW ZEALAND'
WHEN A0.DISPLAYNAME LIKE '%LVAPPH%' THEN'PHILIPPINES'
WHEN A0.DISPLAYNAME LIKE '%LVAPSG%' THEN'SINGAPORE'
WHEN A0.DISPLAYNAME LIKE '%LVAPTH%' THEN'THAILAND'
WHEN A0.DISPLAYNAME LIKE '%LVAPTW%' THEN'TAIWAN, PROVINCE OF CHINA'
WHEN A0.DISPLAYNAME LIKE '%LVAPVN%' THEN'VIET NAM'
WHEN A0.DISPLAYNAME LIKE '%LVCNCN%' THEN'CHINA'
WHEN A0.DISPLAYNAME LIKE '%LVCPCH%' THEN'SWITZERLAND'
WHEN A0.DISPLAYNAME LIKE '%LVCPES%' THEN'SPAIN'
WHEN A0.DISPLAYNAME LIKE '%LVCPFR%' THEN'FRANCE'
WHEN A0.DISPLAYNAME LIKE '%LVCPIT%' THEN'ITALY'
WHEN A0.DISPLAYNAME LIKE '%LVCPPT%' THEN'PORTUGAL'
WHEN A0.DISPLAYNAME LIKE '%LVCPRO%' THEN'ROMANIA'
WHEN A0.DISPLAYNAME LIKE '%LVEUAE%' THEN'UNITED ARAB EMIRATES'
WHEN A0.DISPLAYNAME LIKE '%LVEUAT%' THEN'AUSTRIA'
WHEN A0.DISPLAYNAME LIKE '%LVEUBE%' THEN'BELGIUM'
WHEN A0.DISPLAYNAME LIKE '%LVEUBH%' THEN'BAHRAIN'
WHEN A0.DISPLAYNAME LIKE '%LVEUCH%' THEN'SWITZERLAND'
WHEN A0.DISPLAYNAME LIKE '%LVEUCZ%' THEN'CZECH REPUBLIC'
WHEN A0.DISPLAYNAME LIKE '%LVEUDE%' THEN'GERMANY'
WHEN A0.DISPLAYNAME LIKE '%LVEUDK%' THEN'DENMARK'
WHEN A0.DISPLAYNAME LIKE '%LVEUES%' THEN'SPAIN'
WHEN A0.DISPLAYNAME LIKE '%LVEUFI%' THEN'FINLAND'
WHEN A0.DISPLAYNAME LIKE '%LVEUFR%' THEN'FRANCE'
WHEN A0.DISPLAYNAME LIKE '%LVEUGR%' THEN'GREECE'
WHEN A0.DISPLAYNAME LIKE '%LVEUHU%' THEN'HUNGARY'
WHEN A0.DISPLAYNAME LIKE '%LVEUIL%' THEN'ISRAEL'
WHEN A0.DISPLAYNAME LIKE '%LVEUIN%' THEN'INDIA'
WHEN A0.DISPLAYNAME LIKE '%LVEUIT%' THEN'ITALY'
WHEN A0.DISPLAYNAME LIKE '%LVEUJO%' THEN'JORDAN'
WHEN A0.DISPLAYNAME LIKE '%LVEUKW%' THEN'KUWAIT'
WHEN A0.DISPLAYNAME LIKE '%LVEUKZ%' THEN'KAZAKHSTAN'
WHEN A0.DISPLAYNAME LIKE '%LVEULB%' THEN'LEBANON'
WHEN A0.DISPLAYNAME LIKE '%LVEULU%' THEN'LUXEMBOURG'
WHEN A0.DISPLAYNAME LIKE '%LVEUMA%' THEN'MOROCCO'
WHEN A0.DISPLAYNAME LIKE '%LVEUNL%' THEN'NETHERLANDS'
WHEN A0.DISPLAYNAME LIKE '%LVEUNO%' THEN'NORWAY'
WHEN A0.DISPLAYNAME LIKE '%LVEUPL%' THEN'POLAND'
WHEN A0.DISPLAYNAME LIKE '%LVEUPT%' THEN'PORTUGAL'
WHEN A0.DISPLAYNAME LIKE '%LVEUQA%' THEN'QATAR'
WHEN A0.DISPLAYNAME LIKE '%LVEURO%' THEN'ROMANIA'
WHEN A0.DISPLAYNAME LIKE '%LVEURU%' THEN'RUSSIAN FEDERATION'
WHEN A0.DISPLAYNAME LIKE '%LVEUSA%' THEN'SAUDI ARABIA'
WHEN A0.DISPLAYNAME LIKE '%LVEUSE%' THEN'SWEDEN'
WHEN A0.DISPLAYNAME LIKE '%LVEUTR%' THEN'TURKEY'
WHEN A0.DISPLAYNAME LIKE '%LVEUUA%' THEN'UKRAINE'
WHEN A0.DISPLAYNAME LIKE '%LVEUZA%' THEN'SOUTH AFRICA'
WHEN A0.DISPLAYNAME LIKE '%LVJPJP%' THEN'JAPAN'
WHEN A0.DISPLAYNAME LIKE '%LVLAAR%' THEN'ARGENTINA'
WHEN A0.DISPLAYNAME LIKE '%LVLAAW%' THEN'ARUBA'
WHEN A0.DISPLAYNAME LIKE '%LVLABR%' THEN'BRAZIL'
WHEN A0.DISPLAYNAME LIKE '%LVLACL%' THEN'CHILE'
WHEN A0.DISPLAYNAME LIKE '%LVLACO%' THEN'COLOMBIA'
WHEN A0.DISPLAYNAME LIKE '%LVLADO%' THEN'DOMINICAN REPUBLIC'
WHEN A0.DISPLAYNAME LIKE '%LVLAMX%' THEN'MEXICO'
WHEN A0.DISPLAYNAME LIKE '%LVLAPA%' THEN'PANAMA'
WHEN A0.DISPLAYNAME LIKE '%LVLAPM%' THEN'SAINT PIERRE AND MIQUELON'
WHEN A0.DISPLAYNAME LIKE '%LVMBSC%' THEN'SEYCHELLES'
WHEN A0.DISPLAYNAME LIKE '%LVNACA%' THEN'CANADA'
WHEN A0.DISPLAYNAME LIKE '%LVNAFR%' THEN'FRANCE'
WHEN A0.DISPLAYNAME LIKE '%LVNAMX%' THEN'MEXICO'
WHEN A0.DISPLAYNAME LIKE '%LVNAUS%' THEN'UNITED STATES'
WHEN A0.DISPLAYNAME LIKE '%LVUSUM%' THEN'UNITED STATES MINOR OUTLYING ISLANDS'
    
END AS 'Country',

CASE 
WHEN A0.DISPLAYNAME LIKE '%L08%' THEN 'Sydney Castlereagh'
WHEN A0.DISPLAYNAME LIKE '%L08%' THEN 'Sydney'
WHEN A0.DISPLAYNAME LIKE '%L12%' THEN 'Melbourne Collin Street'
WHEN A0.DISPLAYNAME LIKE '%L14%' THEN 'Brisbane'
WHEN A0.DISPLAYNAME LIKE '%L16%' THEN 'Surfers Paradise'
WHEN A0.DISPLAYNAME LIKE '%L22%' THEN 'Sydney'
WHEN A0.DISPLAYNAME LIKE '%L22%' THEN 'Cairns'
WHEN A0.DISPLAYNAME LIKE '%L24%' THEN 'Perth'
WHEN A0.DISPLAYNAME LIKE '%L26%' THEN 'Melbourne Crown Casino'
WHEN A0.DISPLAYNAME LIKE '%L26%' THEN 'Melbourne Chadstone'
WHEN A0.DISPLAYNAME LIKE '%L28%' THEN 'Sydney The Rocks'
WHEN A0.DISPLAYNAME LIKE '%L30%' THEN 'Melbourne Chadstone'
WHEN A0.DISPLAYNAME LIKE '%L32%' THEN 'Sydney Bondi'
WHEN A0.DISPLAYNAME LIKE '%L32%' THEN 'Sydney'
WHEN A0.DISPLAYNAME LIKE '%L38%' THEN 'Sydney Bondi Beach Pop Up'
WHEN A0.DISPLAYNAME LIKE '%SYD%' THEN 'Sydney'
WHEN A0.DISPLAYNAME LIKE '%WHS%' THEN 'Sydney'
WHEN A0.DISPLAYNAME LIKE '%N12%' THEN 'China World'
WHEN A0.DISPLAYNAME LIKE '%N41%' THEN 'Guangzhou La Perle'
WHEN A0.DISPLAYNAME LIKE '%N83%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%NB4%' THEN 'Changchun Charter Center'
WHEN A0.DISPLAYNAME LIKE '%NC1%' THEN 'Changsha Maison'
WHEN A0.DISPLAYNAME LIKE '%NC9%' THEN 'Dalian'
WHEN A0.DISPLAYNAME LIKE '%NE3%' THEN 'Chengdu Yanlord'
WHEN A0.DISPLAYNAME LIKE '%NG4%' THEN 'Kunming Panlong'
WHEN A0.DISPLAYNAME LIKE '%NJ2%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%NJ6%' THEN 'Harbin Charter'
WHEN A0.DISPLAYNAME LIKE '%NJ9%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%SHA%' THEN 'Shanghai'
WHEN A0.DISPLAYNAME LIKE '%WHS%' THEN 'Schenker Warehouse'
WHEN A0.DISPLAYNAME LIKE '%GUM%' THEN 'Guam'
WHEN A0.DISPLAYNAME LIKE '%N52%' THEN 'Tumon Sands Plaza'
WHEN A0.DISPLAYNAME LIKE '%N53%' THEN 'Guam DFS'
WHEN A0.DISPLAYNAME LIKE '%HKG%' THEN 'Dorset House'
WHEN A0.DISPLAYNAME LIKE '%N11%' THEN 'Peninsula'
WHEN A0.DISPLAYNAME LIKE '%N17%' THEN 'Lee Gardens'
WHEN A0.DISPLAYNAME LIKE '%N18%' THEN 'Chinachem'
WHEN A0.DISPLAYNAME LIKE '%N20%' THEN 'Canton Road'
WHEN A0.DISPLAYNAME LIKE '%N31%' THEN 'Landmark'
WHEN A0.DISPLAYNAME LIKE '%N47%' THEN 'Pacific Place'
WHEN A0.DISPLAYNAME LIKE '%NC8%' THEN 'Elements'
WHEN A0.DISPLAYNAME LIKE '%NK8%' THEN 'Times Square'
WHEN A0.DISPLAYNAME LIKE '%NQ4%' THEN 'Hong Kong Pop Up'
WHEN A0.DISPLAYNAME LIKE '%NV2%' THEN 'Canton Road Men Temp'
WHEN A0.DISPLAYNAME LIKE '%REP%' THEN 'Repair Center'
WHEN A0.DISPLAYNAME LIKE '%RET%' THEN 'Retail Office'
WHEN A0.DISPLAYNAME LIKE '%TWH%' THEN 'Kerry LV Trading'
WHEN A0.DISPLAYNAME LIKE '%WHS%' THEN 'Kerry'
WHEN A0.DISPLAYNAME LIKE '%CGK%' THEN 'Jakarta'
WHEN A0.DISPLAYNAME LIKE '%N63%' THEN 'Plaza Indonesia'
WHEN A0.DISPLAYNAME LIKE '%N63%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%N64%' THEN 'Plaza Senayan'
WHEN A0.DISPLAYNAME LIKE '%N64%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%NB6%' THEN 'Pacific Place'
WHEN A0.DISPLAYNAME LIKE '%AAB%' THEN 'AMS Bangalore'
WHEN A0.DISPLAYNAME LIKE '%POS%' THEN 'XX-Store Pilot'
WHEN A0.DISPLAYNAME LIKE '%Y00%' THEN 'New Delhi'
WHEN A0.DISPLAYNAME LIKE '%Y02%' THEN 'Mumbai'
WHEN A0.DISPLAYNAME LIKE '%Y03%' THEN 'New Delhi 2'
WHEN A0.DISPLAYNAME LIKE '%Y04%' THEN 'Bangalore'
WHEN A0.DISPLAYNAME LIKE '%Y05%' THEN 'Mumbai Landsend'
WHEN A0.DISPLAYNAME LIKE '%CSC%' THEN 'Seoul'
WHEN A0.DISPLAYNAME LIKE '%FGT%' THEN 'FG Trading'
WHEN A0.DISPLAYNAME LIKE '%N04%' THEN 'Seoul LV Building'
WHEN A0.DISPLAYNAME LIKE '%N06%' THEN 'Seoul Lotte Downtown'
WHEN A0.DISPLAYNAME LIKE '%N23%' THEN 'Gwangju Shinsegae'
WHEN A0.DISPLAYNAME LIKE '%N24%' THEN 'Busan Lotte Seomyun'
WHEN A0.DISPLAYNAME LIKE '%N32%' THEN 'Seoul Shinsegae Kangnam'
WHEN A0.DISPLAYNAME LIKE '%N35%' THEN 'Seoul Hyundai Main'
WHEN A0.DISPLAYNAME LIKE '%N36%' THEN 'Daegu Shinsegae'
WHEN A0.DISPLAYNAME LIKE '%N44%' THEN 'Seoul Hyundai Coex'
WHEN A0.DISPLAYNAME LIKE '%N72%' THEN 'Seoul Shinsegae Main'
WHEN A0.DISPLAYNAME LIKE '%N84%' THEN 'Jeju Lotte'
WHEN A0.DISPLAYNAME LIKE '%N94%' THEN 'Seoul Galleria'
WHEN A0.DISPLAYNAME LIKE '%NA4%' THEN 'Seoul Hyundai Mokdong'
WHEN A0.DISPLAYNAME LIKE '%NB3%' THEN 'Jukjeon Shinsegae'
WHEN A0.DISPLAYNAME LIKE '%NB5%' THEN 'Daejeon Galleria'
WHEN A0.DISPLAYNAME LIKE '%NB9%' THEN 'Busan Lotte Centum'
WHEN A0.DISPLAYNAME LIKE '%NC7%' THEN 'Ulsan Hyundai'
WHEN A0.DISPLAYNAME LIKE '%ND8%' THEN 'Busan Shinsegae Centum'
WHEN A0.DISPLAYNAME LIKE '%ND9%' THEN 'Seoul Shinsegae YDP'
WHEN A0.DISPLAYNAME LIKE '%NF3%' THEN 'Mail Order Korea'
WHEN A0.DISPLAYNAME LIKE '%NG7%' THEN 'Incheon Shinsegae'
WHEN A0.DISPLAYNAME LIKE '%NK2%' THEN 'Daegu Hyundai'
WHEN A0.DISPLAYNAME LIKE '%NL8%' THEN 'Seoul Lotte Jamsil'
WHEN A0.DISPLAYNAME LIKE '%NN3%' THEN 'Pangyo Hyundai'
WHEN A0.DISPLAYNAME LIKE '%NN4%' THEN 'Seoul Galleria East'
WHEN A0.DISPLAYNAME LIKE '%NQ1%' THEN 'Seoul Shinsegae Kangnam Shoes'
WHEN A0.DISPLAYNAME LIKE '%NQ3%' THEN 'Seoul Shinsegae Main DF'
WHEN A0.DISPLAYNAME LIKE '%NQ7%' THEN 'Hanam Shinsegae'
WHEN A0.DISPLAYNAME LIKE '%NQ9%' THEN 'Seoul Shinsegae Kangnam Men'
WHEN A0.DISPLAYNAME LIKE '%NV8%' THEN 'Seoul Shinsegae Main Men'
WHEN A0.DISPLAYNAME LIKE '%SEL%' THEN 'Seoul'
WHEN A0.DISPLAYNAME LIKE '%WHS%' THEN 'Country Warehouse'
WHEN A0.DISPLAYNAME LIKE '%ND6%' THEN 'Ulaanbaatar Kerry'
WHEN A0.DISPLAYNAME LIKE '%MFM%' THEN 'Macau'
WHEN A0.DISPLAYNAME LIKE '%N34%' THEN 'Wynn'
WHEN A0.DISPLAYNAME LIKE '%NA3%' THEN 'Four Seasons'
WHEN A0.DISPLAYNAME LIKE '%NA6%' THEN 'One Central'
WHEN A0.DISPLAYNAME LIKE '%NL9%' THEN 'Macau Galaxy'
WHEN A0.DISPLAYNAME LIKE '%NN8%' THEN 'City Of Dreams'
WHEN A0.DISPLAYNAME LIKE '%N82%' THEN 'Saipan'
WHEN A0.DISPLAYNAME LIKE '%KUL%' THEN 'Kuala Lumpur'
WHEN A0.DISPLAYNAME LIKE '%N68%' THEN 'Starhill'
WHEN A0.DISPLAYNAME LIKE '%N69%' THEN 'KLCC'
WHEN A0.DISPLAYNAME LIKE '%NF5%' THEN 'The Gardens'
WHEN A0.DISPLAYNAME LIKE '%AKL%' THEN 'Auckland'
WHEN A0.DISPLAYNAME LIKE '%X06%' THEN 'Auckland'
WHEN A0.DISPLAYNAME LIKE '%X10%' THEN 'Queenstown'
WHEN A0.DISPLAYNAME LIKE '%X12%' THEN 'Customhouse'
WHEN A0.DISPLAYNAME LIKE '%MNL%' THEN 'Manila'
WHEN A0.DISPLAYNAME LIKE '%N02%' THEN 'Greenbelt'
WHEN A0.DISPLAYNAME LIKE '%NN5%' THEN 'Solaire'
WHEN A0.DISPLAYNAME LIKE '%IOS%' THEN 'IOS Office'
WHEN A0.DISPLAYNAME LIKE '%N65%' THEN 'DFS Scottswalk'
WHEN A0.DISPLAYNAME LIKE '%N65%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%N71%' THEN 'Warehouse'
WHEN A0.DISPLAYNAME LIKE '%N74%' THEN 'Ngee Ann City'
WHEN A0.DISPLAYNAME LIKE '%NC3%' THEN 'Ion Orchard'
WHEN A0.DISPLAYNAME LIKE '%NF2%' THEN 'Marina Bay'
WHEN A0.DISPLAYNAME LIKE '%NF2%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%NQ6%' THEN 'Changi Airport'
WHEN A0.DISPLAYNAME LIKE '%NQ6%' THEN 'DFS Scottswalk'
WHEN A0.DISPLAYNAME LIKE '%SIN%' THEN 'Singapore'
WHEN A0.DISPLAYNAME LIKE '%WHS%' THEN 'Warehouse'
WHEN A0.DISPLAYNAME LIKE '%WHS%' THEN 'Singapore'
WHEN A0.DISPLAYNAME LIKE '%BKK%' THEN 'Bangkok'
WHEN A0.DISPLAYNAME LIKE '%BKK%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%N39%' THEN 'Gaysorn'
WHEN A0.DISPLAYNAME LIKE '%N79%' THEN 'Emporium'
WHEN A0.DISPLAYNAME LIKE '%NG6%' THEN 'Bangkok'
WHEN A0.DISPLAYNAME LIKE '%NG6%' THEN 'Paragon'
WHEN A0.DISPLAYNAME LIKE '%N08%' THEN 'Tainan Mitsukoshi'
WHEN A0.DISPLAYNAME LIKE '%N19%' THEN 'Taipei 101'
WHEN A0.DISPLAYNAME LIKE '%N37%' THEN 'Taipei Breeze'
WHEN A0.DISPLAYNAME LIKE '%N54%' THEN 'Taichung Mitsukoshi'
WHEN A0.DISPLAYNAME LIKE '%N58%' THEN 'Taipei LVB'
WHEN A0.DISPLAYNAME LIKE '%NA7%' THEN 'Kaohsiung Star Place'
WHEN A0.DISPLAYNAME LIKE '%NE1%' THEN 'Mail Order Taiwan'
WHEN A0.DISPLAYNAME LIKE '%NJ7%' THEN 'Kaohsiung Han Shin Arena'
WHEN A0.DISPLAYNAME LIKE '%NN6%' THEN 'Taipei Breeze Xinyi'
WHEN A0.DISPLAYNAME LIKE '%NQ8%' THEN 'Taipei Regent'
WHEN A0.DISPLAYNAME LIKE '%NV1%' THEN 'Taipei Sogo'
WHEN A0.DISPLAYNAME LIKE '%TPE%' THEN 'Taipei'
WHEN A0.DISPLAYNAME LIKE '%WHS%' THEN 'Taipei Warehouse'
WHEN A0.DISPLAYNAME LIKE '%HAN%' THEN 'Hanoi Office'
WHEN A0.DISPLAYNAME LIKE '%HCM%' THEN 'Ho Chi Minh Office'
WHEN A0.DISPLAYNAME LIKE '%N46%' THEN 'Ho Chi Minh'
WHEN A0.DISPLAYNAME LIKE '%NF6%' THEN 'Hanoi Trang Tien Plaza'
WHEN A0.DISPLAYNAME LIKE '%FFS%' THEN 'NY-New York-Store Lab 625 Mad'
WHEN A0.DISPLAYNAME LIKE '%N03%' THEN 'Plaza 66'
WHEN A0.DISPLAYNAME LIKE '%N14%' THEN 'Wenzhou'
WHEN A0.DISPLAYNAME LIKE '%N26%' THEN 'Hangzhou Tower'
WHEN A0.DISPLAYNAME LIKE '%N42%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%N42%' THEN 'Xian Zhong Da'
WHEN A0.DISPLAYNAME LIKE '%N83%' THEN 'Xiamen Marco Polo'
WHEN A0.DISPLAYNAME LIKE '%N96%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%N96%' THEN 'Beijing Peninsula'
WHEN A0.DISPLAYNAME LIKE '%N99%' THEN 'Nanjing Deji'
WHEN A0.DISPLAYNAME LIKE '%NA1%' THEN 'Beijing Seasons Place'
WHEN A0.DISPLAYNAME LIKE '%NA5%' THEN 'Tianjin Friendship'
WHEN A0.DISPLAYNAME LIKE '%NB1%' THEN 'Shenyang Charter'
WHEN A0.DISPLAYNAME LIKE '%NB8%' THEN 'Qingdao Hisense'
WHEN A0.DISPLAYNAME LIKE '%NC4%' THEN 'Shanghai IFC'
WHEN A0.DISPLAYNAME LIKE '%NC5%' THEN 'Wuxi Yaohan'
WHEN A0.DISPLAYNAME LIKE '%NC9%' THEN 'Dalian Times Square'
WHEN A0.DISPLAYNAME LIKE '%ND3%' THEN 'Ningbo Heyi'
WHEN A0.DISPLAYNAME LIKE '%ND7%' THEN 'Shenzhen Luohu'
WHEN A0.DISPLAYNAME LIKE '%NE6%' THEN 'Guangzhou Tianhe'
WHEN A0.DISPLAYNAME LIKE '%NE7%' THEN 'Hangzhou Mix C'
WHEN A0.DISPLAYNAME LIKE '%NG2%' THEN 'Shijiazhuang Future Plaza'
WHEN A0.DISPLAYNAME LIKE '%NG3%' THEN 'Shenyang Mixc'
WHEN A0.DISPLAYNAME LIKE '%NG5%' THEN 'Chongqing Times Square'
WHEN A0.DISPLAYNAME LIKE '%NJ1%' THEN 'Hangzhou Eurostreet'
WHEN A0.DISPLAYNAME LIKE '%NJ2%' THEN 'Shenyang LAvenue'
WHEN A0.DISPLAYNAME LIKE '%NJ9%' THEN 'Hefei Yintai'
WHEN A0.DISPLAYNAME LIKE '%NK1%' THEN 'Wuhan International Plaza'
WHEN A0.DISPLAYNAME LIKE '%NK6%' THEN 'Shanghai LAvenue'
WHEN A0.DISPLAYNAME LIKE '%NL1%' THEN 'Beijing Shin Kong'
WHEN A0.DISPLAYNAME LIKE '%NL1%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%NL2%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%NL2%' THEN 'Tianjin Galaxy'
WHEN A0.DISPLAYNAME LIKE '%NL3%' THEN 'Jinan Guihe'
WHEN A0.DISPLAYNAME LIKE '%NL4%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%NL4%' THEN 'Chengdu IFS'
WHEN A0.DISPLAYNAME LIKE '%NN1%' THEN 'Zhengzhou David Plaza'
WHEN A0.DISPLAYNAME LIKE '%NV6%' THEN 'Chongqing IFS Women'
WHEN A0.DISPLAYNAME LIKE '%NV7%' THEN 'Chongqing IFS Men'
WHEN A0.DISPLAYNAME LIKE '%NV9%' THEN 'Xian Shin Kong Place Women'
WHEN A0.DISPLAYNAME LIKE '%ME1%' THEN 'Paris - La Fabrique du Temps'
WHEN A0.DISPLAYNAME LIKE '%BA1%' THEN 'Barbera'
WHEN A0.DISPLAYNAME LIKE '%BA2%' THEN 'Barbera'
WHEN A0.DISPLAYNAME LIKE '%BA3%' THEN 'Barbera'
WHEN A0.DISPLAYNAME LIKE '%GI1%' THEN 'Girona'
WHEN A0.DISPLAYNAME LIKE '%PO1%' THEN 'Polinya'
WHEN A0.DISPLAYNAME LIKE '%A11%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%AS0%' THEN 'Asnieres'
WHEN A0.DISPLAYNAME LIKE '%AS1%' THEN 'Asnieres'
WHEN A0.DISPLAYNAME LIKE '%AS1%' THEN 'Paris'
WHEN A0.DISPLAYNAME LIKE '%AS1%' THEN 'Morlaix (Partner)'
WHEN A0.DISPLAYNAME LIKE '%AS2%' THEN 'Asnieres'
WHEN A0.DISPLAYNAME LIKE '%CE1%' THEN 'Cergy'
WHEN A0.DISPLAYNAME LIKE '%CE1%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%CE2%' THEN 'Cergy'
WHEN A0.DISPLAYNAME LIKE '%CE5%' THEN 'Cergy'
WHEN A0.DISPLAYNAME LIKE '%CM1%' THEN 'FMLV'
WHEN A0.DISPLAYNAME LIKE '%DU1%' THEN 'Ducey'
WHEN A0.DISPLAYNAME LIKE '%DU1%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%IS1%' THEN 'Issoudun'
WHEN A0.DISPLAYNAME LIKE '%MA1%' THEN 'Marsaz'
WHEN A0.DISPLAYNAME LIKE '%OR1%' THEN 'Deret'
WHEN A0.DISPLAYNAME LIKE '%PA0%' THEN 'Paris'
WHEN A0.DISPLAYNAME LIKE '%PA0%' THEN 'Paris FR'
WHEN A0.DISPLAYNAME LIKE '%PA0%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%PA0%' THEN 'Morlaix (Partner)'
WHEN A0.DISPLAYNAME LIKE '%PA0%' THEN 'Sarras'
WHEN A0.DISPLAYNAME LIKE '%PA1%' THEN 'Paris'
WHEN A0.DISPLAYNAME LIKE '%PA1%' THEN 'Pilot Store Corp'
WHEN A0.DISPLAYNAME LIKE '%PA1%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%PA1%' THEN 'Paris FR'
WHEN A0.DISPLAYNAME LIKE '%PA1%' THEN 'Morlaix (Partner)'
WHEN A0.DISPLAYNAME LIKE '%PA1%' THEN 'XX-Store Pilot'
WHEN A0.DISPLAYNAME LIKE '%PA2%' THEN 'Paris'
WHEN A0.DISPLAYNAME LIKE '%PA2%' THEN 'Paris -Les Fontaines Parfumees'
WHEN A0.DISPLAYNAME LIKE '%PA2%' THEN 'Morlaix (Partner)'
WHEN A0.DISPLAYNAME LIKE '%PA2%' THEN 'Cannes'
WHEN A0.DISPLAYNAME LIKE '%PA2%' THEN 'Paris FR'
WHEN A0.DISPLAYNAME LIKE '%PA3%' THEN 'Paris'
WHEN A0.DISPLAYNAME LIKE '%PA4%' THEN 'Paris'
WHEN A0.DISPLAYNAME LIKE '%PA5%' THEN 'Paris'
WHEN A0.DISPLAYNAME LIKE '%PA6%' THEN 'Paris'
WHEN A0.DISPLAYNAME LIKE '%PA7%' THEN 'Paris'
WHEN A0.DISPLAYNAME LIKE '%PA9%' THEN 'Paris'
WHEN A0.DISPLAYNAME LIKE '%PAD%' THEN 'Paris'
WHEN A0.DISPLAYNAME LIKE '%PAM%' THEN 'Paris'
WHEN A0.DISPLAYNAME LIKE '%PAP%' THEN 'Paris'
WHEN A0.DISPLAYNAME LIKE '%SA1%' THEN 'Sarras'
WHEN A0.DISPLAYNAME LIKE '%SA1%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%SA1%' THEN 'FMLV'
WHEN A0.DISPLAYNAME LIKE '%SD1%' THEN 'Saint Donat'
WHEN A0.DISPLAYNAME LIKE '%SD1%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%SF1%' THEN 'Sainte Florence'
WHEN A0.DISPLAYNAME LIKE '%SF1%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%SP1%' THEN 'Saint Pourcain'
WHEN A0.DISPLAYNAME LIKE '%XXX%' THEN 'Pilot Store Corp'
WHEN A0.DISPLAYNAME LIKE '%XXX%' THEN 'XX-Store Pilot'
WHEN A0.DISPLAYNAME LIKE '%FI1%' THEN 'Fiesso'
WHEN A0.DISPLAYNAME LIKE '%FI1%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%GA0%' THEN 'Gallarate'
WHEN A0.DISPLAYNAME LIKE '%GA0%' THEN 'Incisa'
WHEN A0.DISPLAYNAME LIKE '%IN0%' THEN 'Incisa'
WHEN A0.DISPLAYNAME LIKE '%PL1%' THEN 'Ponte de Lima'
WHEN A0.DISPLAYNAME LIKE '%SO1%' THEN 'Somarest 2'
WHEN A0.DISPLAYNAME LIKE '%SO1%' THEN 'Somarest'
WHEN A0.DISPLAYNAME LIKE '%SO1%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%SO2%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%SO2%' THEN 'Somarest 2'
WHEN A0.DISPLAYNAME LIKE '%SO2%' THEN 'Somarest'
WHEN A0.DISPLAYNAME LIKE '%JA1%' THEN 'Jebel Ali 1'
WHEN A0.DISPLAYNAME LIKE '%P10%' THEN 'Dubai'
WHEN A0.DISPLAYNAME LIKE '%P12%' THEN 'Mall of Dubai'
WHEN A0.DISPLAYNAME LIKE '%P13%' THEN 'Mall of Emirates'
WHEN A0.DISPLAYNAME LIKE '%P14%' THEN 'Abu Dhabi'
WHEN A0.DISPLAYNAME LIKE '%P15%' THEN 'Abu Dhabi Sowwah'
WHEN A0.DISPLAYNAME LIKE '%P16%' THEN 'Dubai Mall Shoes'
WHEN A0.DISPLAYNAME LIKE '%P17%' THEN 'Dubai'
WHEN A0.DISPLAYNAME LIKE '%P17%' THEN 'Dubai Mall Galeries Lafayette'
WHEN A0.DISPLAYNAME LIKE '%G00%' THEN 'Wien'
WHEN A0.DISPLAYNAME LIKE '%G01%' THEN 'Wien Tuchlauben'
WHEN A0.DISPLAYNAME LIKE '%G01%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%G02%' THEN 'Salzburg'
WHEN A0.DISPLAYNAME LIKE '%G03%' THEN 'Kitzbuhel'
WHEN A0.DISPLAYNAME LIKE '%FIN%' THEN 'Call Center'
WHEN A0.DISPLAYNAME LIKE '%FIN%' THEN 'Shared Service Center'
WHEN A0.DISPLAYNAME LIKE '%HUB%' THEN 'Call Center'
WHEN A0.DISPLAYNAME LIKE '%HUB%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%I00%' THEN 'Brussels'
WHEN A0.DISPLAYNAME LIKE '%I01%' THEN 'Brussels'
WHEN A0.DISPLAYNAME LIKE '%I02%' THEN 'Knokke'
WHEN A0.DISPLAYNAME LIKE '%I03%' THEN 'Antwerp'
WHEN A0.DISPLAYNAME LIKE '%P81%' THEN 'Manama'
WHEN A0.DISPLAYNAME LIKE '%C00%' THEN 'Geneve'
WHEN A0.DISPLAYNAME LIKE '%C01%' THEN 'Geneve'
WHEN A0.DISPLAYNAME LIKE '%C02%' THEN 'Lausanne'
WHEN A0.DISPLAYNAME LIKE '%C03%' THEN 'Crans'
WHEN A0.DISPLAYNAME LIKE '%C04%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%C04%' THEN 'St Moritz'
WHEN A0.DISPLAYNAME LIKE '%C05%' THEN 'Zurich'
WHEN A0.DISPLAYNAME LIKE '%C06%' THEN 'Bale'
WHEN A0.DISPLAYNAME LIKE '%C06%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%C07%' THEN 'Lugano'
WHEN A0.DISPLAYNAME LIKE '%C08%' THEN 'Gstaad'
WHEN A0.DISPLAYNAME LIKE '%211%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%G21%' THEN 'Prague'
WHEN A0.DISPLAYNAME LIKE '%J00%' THEN 'Munchen'
WHEN A0.DISPLAYNAME LIKE '%J00%' THEN 'XX-Store Pilot'
WHEN A0.DISPLAYNAME LIKE '%J00%' THEN 'Dusseldorf'
WHEN A0.DISPLAYNAME LIKE '%J01%' THEN 'Dusseldorf'
WHEN A0.DISPLAYNAME LIKE '%J02%' THEN 'Munich PADO'
WHEN A0.DISPLAYNAME LIKE '%J02%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%J03%' THEN 'Frankfurt'
WHEN A0.DISPLAYNAME LIKE '%J04%' THEN 'Berlin KuDamm'
WHEN A0.DISPLAYNAME LIKE '%J05%' THEN 'Hamburg'
WHEN A0.DISPLAYNAME LIKE '%J07%' THEN 'Koln'
WHEN A0.DISPLAYNAME LIKE '%J09%' THEN 'Stuttgart'
WHEN A0.DISPLAYNAME LIKE '%J10%' THEN 'Berlin KDW'
WHEN A0.DISPLAYNAME LIKE '%J12%' THEN 'Munchen Oberpollinger'
WHEN A0.DISPLAYNAME LIKE '%Q01%' THEN 'Copenhagen'
WHEN A0.DISPLAYNAME LIKE '%Q10%' THEN 'Copenhagen'
WHEN A0.DISPLAYNAME LIKE '%K00%' THEN 'Madrid'
WHEN A0.DISPLAYNAME LIKE '%K00%' THEN 'XX-Store Pilot'
WHEN A0.DISPLAYNAME LIKE '%K01%' THEN 'Madrid 1'
WHEN A0.DISPLAYNAME LIKE '%K01%' THEN 'Madrid 2'
WHEN A0.DISPLAYNAME LIKE '%K03%' THEN 'Valence'
WHEN A0.DISPLAYNAME LIKE '%K04%' THEN 'Barcelona 2'
WHEN A0.DISPLAYNAME LIKE '%K05%' THEN 'Puerto Banus'
WHEN A0.DISPLAYNAME LIKE '%K06%' THEN 'Madrid 2'
WHEN A0.DISPLAYNAME LIKE '%K08%' THEN 'Barcelona 3'
WHEN A0.DISPLAYNAME LIKE '%K10%' THEN 'Palma de Mallorca'
WHEN A0.DISPLAYNAME LIKE '%Q31%' THEN 'Helsinki'
WHEN A0.DISPLAYNAME LIKE '%A00%' THEN 'Paris FR'
WHEN A0.DISPLAYNAME LIKE '%A00%' THEN 'XX-Store Pilot'
WHEN A0.DISPLAYNAME LIKE '%A01%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%A02%' THEN 'Paris - Montaigne'
WHEN A0.DISPLAYNAME LIKE '%A02%' THEN 'XX-Store Pilot'
WHEN A0.DISPLAYNAME LIKE '%A03%' THEN 'Paris - St Germain'
WHEN A0.DISPLAYNAME LIKE '%A05%' THEN 'Paris - Bon Marche'
WHEN A0.DISPLAYNAME LIKE '%A06%' THEN 'Paris - Champs Elysees'
WHEN A0.DISPLAYNAME LIKE '%A06%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%A06%' THEN 'Paris - Galeries Lafayette'
WHEN A0.DISPLAYNAME LIKE '%A07%' THEN 'Paris - Galeries Lafayette'
WHEN A0.DISPLAYNAME LIKE '%A09%' THEN 'Paris - Printemps'
WHEN A0.DISPLAYNAME LIKE '%A09%' THEN 'Paris FR'
WHEN A0.DISPLAYNAME LIKE '%A11%' THEN 'Paris - Vendome'
WHEN A0.DISPLAYNAME LIKE '%A14%' THEN 'Paris - Grand Vendome'
WHEN A0.DISPLAYNAME LIKE '%A14%' THEN 'XX-Store Pilot'
WHEN A0.DISPLAYNAME LIKE '%A99%' THEN 'Pilot Store Corp'
WHEN A0.DISPLAYNAME LIKE '%A99%' THEN 'XX-Store Pilot'
WHEN A0.DISPLAYNAME LIKE '%A99%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%B01%' THEN 'Nice'
WHEN A0.DISPLAYNAME LIKE '%B02%' THEN 'Cannes'
WHEN A0.DISPLAYNAME LIKE '%B02%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%CR1%' THEN 'Cergy 1'
WHEN A0.DISPLAYNAME LIKE '%CR1%' THEN 'Cergy'
WHEN A0.DISPLAYNAME LIKE '%D01%' THEN 'Monaco'
WHEN A0.DISPLAYNAME LIKE '%HQN%' THEN 'Paris HQN'
WHEN A0.DISPLAYNAME LIKE '%HQN%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%QHN%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%R13%' THEN 'Marseille'
WHEN A0.DISPLAYNAME LIKE '%R14%' THEN 'Deauville'
WHEN A0.DISPLAYNAME LIKE '%R31%' THEN 'Toulouse'
WHEN A0.DISPLAYNAME LIKE '%R33%' THEN 'Bordeaux'
WHEN A0.DISPLAYNAME LIKE '%R59%' THEN 'Lille'
WHEN A0.DISPLAYNAME LIKE '%R67%' THEN 'Strasbourg'
WHEN A0.DISPLAYNAME LIKE '%R69%' THEN 'Lyon'
WHEN A0.DISPLAYNAME LIKE '%R69%' THEN 'XX-Store Pilot'
WHEN A0.DISPLAYNAME LIKE '%R73%' THEN 'Courchevel'
WHEN A0.DISPLAYNAME LIKE '%R75%' THEN 'Courchevel 2'
WHEN A0.DISPLAYNAME LIKE '%R83%' THEN 'St Tropez'
WHEN A0.DISPLAYNAME LIKE '%S00%' THEN 'Athens'
WHEN A0.DISPLAYNAME LIKE '%S01%' THEN 'Athens'
WHEN A0.DISPLAYNAME LIKE '%S04%' THEN 'Mykonos'
WHEN A0.DISPLAYNAME LIKE '%S10%' THEN 'Athens'
WHEN A0.DISPLAYNAME LIKE '%G31%' THEN 'Budapest'
WHEN A0.DISPLAYNAME LIKE '%P41%' THEN 'Tel Aviv'
WHEN A0.DISPLAYNAME LIKE '%H00%' THEN 'Milano'
WHEN A0.DISPLAYNAME LIKE '%H00%' THEN 'XX-Store Pilot'
WHEN A0.DISPLAYNAME LIKE '%H01%' THEN 'Milano 1'
WHEN A0.DISPLAYNAME LIKE '%H02%' THEN 'Firenze'
WHEN A0.DISPLAYNAME LIKE '%H04%' THEN 'Roma 1'
WHEN A0.DISPLAYNAME LIKE '%H04%' THEN 'Milano'
WHEN A0.DISPLAYNAME LIKE '%H05%' THEN 'Bari'
WHEN A0.DISPLAYNAME LIKE '%H06%' THEN 'Bologna'
WHEN A0.DISPLAYNAME LIKE '%H07%' THEN 'Torino'
WHEN A0.DISPLAYNAME LIKE '%H08%' THEN 'Napoli'
WHEN A0.DISPLAYNAME LIKE '%H09%' THEN 'Genova'
WHEN A0.DISPLAYNAME LIKE '%H10%' THEN 'Palermo'
WHEN A0.DISPLAYNAME LIKE '%H11%' THEN 'Portofino'
WHEN A0.DISPLAYNAME LIKE '%H12%' THEN 'Capri'
WHEN A0.DISPLAYNAME LIKE '%H14%' THEN 'Verona'
WHEN A0.DISPLAYNAME LIKE '%H15%' THEN 'Milano 2'
WHEN A0.DISPLAYNAME LIKE '%H17%' THEN 'Milano 3'
WHEN A0.DISPLAYNAME LIKE '%H18%' THEN 'Padova'
WHEN A0.DISPLAYNAME LIKE '%H20%' THEN 'Roma Etoile'
WHEN A0.DISPLAYNAME LIKE '%H21%' THEN 'Forte Dei Marmi'
WHEN A0.DISPLAYNAME LIKE '%H23%' THEN 'Roma Tritone'
WHEN A0.DISPLAYNAME LIKE '%HQS%' THEN 'Milano HQS'
WHEN A0.DISPLAYNAME LIKE '%P91%' THEN 'Amman'
WHEN A0.DISPLAYNAME LIKE '%P01%' THEN 'Kuwait'
WHEN A0.DISPLAYNAME LIKE '%P02%' THEN 'Kuwait Avenue'
WHEN A0.DISPLAYNAME LIKE '%Q81%' THEN 'Almaty'
WHEN A0.DISPLAYNAME LIKE '%Q81%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%P61%' THEN 'Beirut'
WHEN A0.DISPLAYNAME LIKE '%I04%' THEN 'Luxembourg'
WHEN A0.DISPLAYNAME LIKE '%P31%' THEN 'Marrakech'
WHEN A0.DISPLAYNAME LIKE '%P33%' THEN 'Casablanca Morocco Mall'
WHEN A0.DISPLAYNAME LIKE '%I00%' THEN 'Amsterdam de Bijenkorf'
WHEN A0.DISPLAYNAME LIKE '%I21%' THEN 'Amsterdam'
WHEN A0.DISPLAYNAME LIKE '%I22%' THEN 'Amsterdam de Bijenkorf'
WHEN A0.DISPLAYNAME LIKE '%Q21%' THEN 'Oslo'
WHEN A0.DISPLAYNAME LIKE '%G51%' THEN 'Warsaw'
WHEN A0.DISPLAYNAME LIKE '%E02%' THEN 'Lisbon'
WHEN A0.DISPLAYNAME LIKE '%P71%' THEN 'Doha'
WHEN A0.DISPLAYNAME LIKE '%G41%' THEN 'Bucharest'
WHEN A0.DISPLAYNAME LIKE '%Q60%' THEN 'Moscow'
WHEN A0.DISPLAYNAME LIKE '%Q61%' THEN 'Moscow 1'
WHEN A0.DISPLAYNAME LIKE '%Q62%' THEN 'Moscow 2'
WHEN A0.DISPLAYNAME LIKE '%Q63%' THEN 'Saint Petersburg'
WHEN A0.DISPLAYNAME LIKE '%Q64%' THEN 'Ekaterinburg'
WHEN A0.DISPLAYNAME LIKE '%Q65%' THEN 'Sochi'
WHEN A0.DISPLAYNAME LIKE '%Q66%' THEN 'Moscow 3'
WHEN A0.DISPLAYNAME LIKE '%P21%' THEN 'Jeddah'
WHEN A0.DISPLAYNAME LIKE '%P22%' THEN 'Riyadh'
WHEN A0.DISPLAYNAME LIKE '%P23%' THEN 'Riyadh Centria'
WHEN A0.DISPLAYNAME LIKE '%Q51%' THEN 'Stockholm'
WHEN A0.DISPLAYNAME LIKE '%S50%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%S50%' THEN 'Istanbul'
WHEN A0.DISPLAYNAME LIKE '%S51%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%S51%' THEN 'Istanbul 1'
WHEN A0.DISPLAYNAME LIKE '%S52%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%S52%' THEN 'Istanbul 2'
WHEN A0.DISPLAYNAME LIKE '%S53%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%S53%' THEN 'Istanbul 3'
WHEN A0.DISPLAYNAME LIKE '%S54%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%S54%' THEN 'Istanbul 4'
WHEN A0.DISPLAYNAME LIKE '%Q70%' THEN 'Kiev'
WHEN A0.DISPLAYNAME LIKE '%Q71%' THEN 'Kiev'
WHEN A0.DISPLAYNAME LIKE '%000%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%F00%' THEN 'London'
WHEN A0.DISPLAYNAME LIKE '%F00%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%F01%' THEN 'London New Bond Street'
WHEN A0.DISPLAYNAME LIKE '%F02%' THEN 'London Sloane Street'
WHEN A0.DISPLAYNAME LIKE '%F02%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%F03%' THEN 'London - Harrods 1'
WHEN A0.DISPLAYNAME LIKE '%F03%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%F04%' THEN 'London City'
WHEN A0.DISPLAYNAME LIKE '%F05%' THEN ' '
WHEN A0.DISPLAYNAME LIKE '%F05%' THEN 'London Selfridges'
WHEN A0.DISPLAYNAME LIKE '%F06%' THEN 'Manchester'
WHEN A0.DISPLAYNAME LIKE '%F06%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%F07%' THEN 'Edinburgh'
WHEN A0.DISPLAYNAME LIKE '%F08%' THEN 'Birmingham'
WHEN A0.DISPLAYNAME LIKE '%F09%' THEN 'London - Harrods 2'
WHEN A0.DISPLAYNAME LIKE '%F10%' THEN 'Leeds'
WHEN A0.DISPLAYNAME LIKE '%F11%' THEN 'White City'
WHEN A0.DISPLAYNAME LIKE '%F12%' THEN 'Harrods 3'
WHEN A0.DISPLAYNAME LIKE '%F13%' THEN 'London Selfridges'
WHEN A0.DISPLAYNAME LIKE '%F14%' THEN 'London - Harrods 3'
WHEN A0.DISPLAYNAME LIKE '%F15%' THEN 'Heathrow'
WHEN A0.DISPLAYNAME LIKE '%F15%' THEN 'London New Bond Street'
WHEN A0.DISPLAYNAME LIKE '%F17%' THEN 'London - Harrods 4'
WHEN A0.DISPLAYNAME LIKE '%F17%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%F17%' THEN 'Heathrow'
WHEN A0.DISPLAYNAME LIKE '%F50%' THEN 'Dublin'
WHEN A0.DISPLAYNAME LIKE '%FC0%' THEN 'London Clarendon'
WHEN A0.DISPLAYNAME LIKE '%P50%' THEN 'Johannesburg'
WHEN A0.DISPLAYNAME LIKE '%P51%' THEN 'Johannesburg'
WHEN A0.DISPLAYNAME LIKE '%P52%' THEN 'Capetown'
WHEN A0.DISPLAYNAME LIKE '%624%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%VHN%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%MNG%' THEN 'Hanzomon'
WHEN A0.DISPLAYNAME LIKE '%OMD%' THEN 'Hanzomon'
WHEN A0.DISPLAYNAME LIKE '%TYO%' THEN 'Hanzomon'
WHEN A0.DISPLAYNAME LIKE '%TYO%' THEN 'Kojimachi'
WHEN A0.DISPLAYNAME LIKE '%V02%' THEN 'ONE_CustomerService'
WHEN A0.DISPLAYNAME LIKE '%V05%' THEN 'OsakaHilton'
WHEN A0.DISPLAYNAME LIKE '%V06%' THEN 'GinzaNamiki'
WHEN A0.DISPLAYNAME LIKE '%V10%' THEN 'Shinsaibashi'
WHEN A0.DISPLAYNAME LIKE '%V11%' THEN 'Fukuoka'
WHEN A0.DISPLAYNAME LIKE '%V12%' THEN 'NagoyaSakae'
WHEN A0.DISPLAYNAME LIKE '%V13%' THEN 'Futakotamagawa'
WHEN A0.DISPLAYNAME LIKE '%V14%' THEN 'GinzaMatsuya'
WHEN A0.DISPLAYNAME LIKE '%V15%' THEN 'Omotesando'
WHEN A0.DISPLAYNAME LIKE '%V15%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%V16%' THEN 'Kobe'
WHEN A0.DISPLAYNAME LIKE '%V17%' THEN 'Kochi'
WHEN A0.DISPLAYNAME LIKE '%V18%' THEN 'RoppongiHills'
WHEN A0.DISPLAYNAME LIKE '%V19%' THEN 'Okinawa'
WHEN A0.DISPLAYNAME LIKE '%V20%' THEN 'NagoyaMidland'
WHEN A0.DISPLAYNAME LIKE '%V21%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%V21%' THEN 'Kashiwa'
WHEN A0.DISPLAYNAME LIKE '%V24%' THEN 'Dover Street Market Ginza'
WHEN A0.DISPLAYNAME LIKE '%V25%' THEN 'Shinjuku'
WHEN A0.DISPLAYNAME LIKE '%V27%' THEN 'OON Japan'
WHEN A0.DISPLAYNAME LIKE '%V30%' THEN 'JR Nagoya Takashimaya'
WHEN A0.DISPLAYNAME LIKE '%V31%' THEN 'SapporoMaruiImai'
WHEN A0.DISPLAYNAME LIKE '%V32%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%V32%' THEN 'SendaiFujisaki'
WHEN A0.DISPLAYNAME LIKE '%V35%' THEN 'ShibuyaSeibu'
WHEN A0.DISPLAYNAME LIKE '%V36%' THEN 'YokohamaTakashimaya'
WHEN A0.DISPLAYNAME LIKE '%V38%' THEN 'HamamatsuEntetsu'
WHEN A0.DISPLAYNAME LIKE '%V40%' THEN 'NagoyaMatsuzakaya'
WHEN A0.DISPLAYNAME LIKE '%V41%' THEN 'KyotoTakashimaya'
WHEN A0.DISPLAYNAME LIKE '%V42%' THEN 'KorinboDaiwa'
WHEN A0.DISPLAYNAME LIKE '%V43%' THEN 'OsakaTakashimaya'
WHEN A0.DISPLAYNAME LIKE '%V44%' THEN 'OkayamaTakashimaya'
WHEN A0.DISPLAYNAME LIKE '%V45%' THEN 'HatyoboriFukuya'
WHEN A0.DISPLAYNAME LIKE '%V46%' THEN 'KokuraIzutsuya'
WHEN A0.DISPLAYNAME LIKE '%V50%' THEN 'NihonbashiMitsukoshi'
WHEN A0.DISPLAYNAME LIKE '%V51%' THEN 'OitaTokiwa'
WHEN A0.DISPLAYNAME LIKE '%V54%' THEN 'NiigataMitsukoshi'
WHEN A0.DISPLAYNAME LIKE '%V57%' THEN 'TachikawaTakashimaya'
WHEN A0.DISPLAYNAME LIKE '%V58%' THEN 'UtsunomiyaTobu'
WHEN A0.DISPLAYNAME LIKE '%V60%' THEN 'TenjinDaimaru'
WHEN A0.DISPLAYNAME LIKE '%V61%' THEN 'ShinjyukuTakashimaya'
WHEN A0.DISPLAYNAME LIKE '%V62%' THEN 'ShizuokaMatsuzakaya'
WHEN A0.DISPLAYNAME LIKE '%V65%' THEN 'KoriyamaUsui'
WHEN A0.DISPLAYNAME LIKE '%V66%' THEN 'KobeSogo'
WHEN A0.DISPLAYNAME LIKE '%V67%' THEN 'TakamatsuMitsukoshi'
WHEN A0.DISPLAYNAME LIKE '%V68%' THEN 'AbenoKintetsu'
WHEN A0.DISPLAYNAME LIKE '%V69%' THEN 'WakayamaKintetsu'
WHEN A0.DISPLAYNAME LIKE '%V70%' THEN 'KagoshimaYamakataya'
WHEN A0.DISPLAYNAME LIKE '%V71%' THEN 'KyotoDaimaru'
WHEN A0.DISPLAYNAME LIKE '%V72%' THEN 'MitoKeisei'
WHEN A0.DISPLAYNAME LIKE '%V73%' THEN 'UrawaIsetan'
WHEN A0.DISPLAYNAME LIKE '%V73%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%V74%' THEN 'YokohamaSogo'
WHEN A0.DISPLAYNAME LIKE '%V76%' THEN 'IkebukuroSeibu'
WHEN A0.DISPLAYNAME LIKE '%V77%' THEN 'NaraKintetsu'
WHEN A0.DISPLAYNAME LIKE '%V78%' THEN 'UmedaHankyu'
WHEN A0.DISPLAYNAME LIKE '%V79%' THEN 'Hankyu Umeda 5F'
WHEN A0.DISPLAYNAME LIKE '%V80%' THEN 'ChibaSogo'
WHEN A0.DISPLAYNAME LIKE '%V81%' THEN 'MatsuyamaIyotetsu'
WHEN A0.DISPLAYNAME LIKE '%V83%' THEN 'HakataHankyu'
WHEN A0.DISPLAYNAME LIKE '%V84%' THEN 'EventStore'
WHEN A0.DISPLAYNAME LIKE '%V85%' THEN 'OdakyuShinjuku'
WHEN A0.DISPLAYNAME LIKE '%V86%' THEN 'TokyoDaimaru'
WHEN A0.DISPLAYNAME LIKE '%V87%' THEN 'HankyuUmeda'
WHEN A0.DISPLAYNAME LIKE '%V99%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%V99%' THEN 'Kintetsu Misato'
WHEN A0.DISPLAYNAME LIKE '%UAB%' THEN 'Buenos Aires'
WHEN A0.DISPLAYNAME LIKE '%UAR%' THEN 'Oranjestad'
WHEN A0.DISPLAYNAME LIKE '%UAR%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UBX%' THEN 'Brasilia'
WHEN A0.DISPLAYNAME LIKE '%UBX%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UBZ%' THEN 'Iguatemi'
WHEN A0.DISPLAYNAME LIKE '%UBZ%' THEN 'Sao Paulo'
WHEN A0.DISPLAYNAME LIKE '%UBZ%' THEN 'Call Center'
WHEN A0.DISPLAYNAME LIKE '%UCK%' THEN 'Curitiba'
WHEN A0.DISPLAYNAME LIKE '%UCK%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UGF%' THEN 'Goiania Flamboyant Temp'
WHEN A0.DISPLAYNAME LIKE '%UIG%' THEN 'Iguatemi'
WHEN A0.DISPLAYNAME LIKE '%UJM%' THEN 'Cidade Jardim'
WHEN A0.DISPLAYNAME LIKE '%UJM%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%URJ%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%URJ%' THEN 'Rio Ipanema'
WHEN A0.DISPLAYNAME LIKE '%URK%' THEN 'Rio Barra'
WHEN A0.DISPLAYNAME LIKE '%URK%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%URK%' THEN 'Rio Ipanema'
WHEN A0.DISPLAYNAME LIKE '%URZ%' THEN 'Luft'
WHEN A0.DISPLAYNAME LIKE '%UCS%' THEN 'Santiago'
WHEN A0.DISPLAYNAME LIKE '%UCS%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UCS%' THEN 'Sao Paulo'
WHEN A0.DISPLAYNAME LIKE '%UBG%' THEN 'Bogota'
WHEN A0.DISPLAYNAME LIKE '%UBG%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UDO%' THEN 'Santo Domingo'
WHEN A0.DISPLAYNAME LIKE '%UDO%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UCU%' THEN 'Cancun'
WHEN A0.DISPLAYNAME LIKE '%UCU%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UGD%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UGD%' THEN 'Guadalajara'
WHEN A0.DISPLAYNAME LIKE '%UGD%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%ULO%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%ULO%' THEN 'Queretaro'
WHEN A0.DISPLAYNAME LIKE '%UMP%' THEN 'SantaFe'
WHEN A0.DISPLAYNAME LIKE '%UMP%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UMP%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%UMX%' THEN 'Masaryk'
WHEN A0.DISPLAYNAME LIKE '%UMX%' THEN 'Mexico'
WHEN A0.DISPLAYNAME LIKE '%UMX%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%UMY%' THEN 'Monterrey'
WHEN A0.DISPLAYNAME LIKE '%UPN%' THEN 'Polanco'
WHEN A0.DISPLAYNAME LIKE '%UPN%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%UPS%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UPS%' THEN 'Perisur'
WHEN A0.DISPLAYNAME LIKE '%UPM%' THEN 'Panama'
WHEN A0.DISPLAYNAME LIKE '%UPM%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%MP0%' THEN 'Sao Paulo'
WHEN A0.DISPLAYNAME LIKE '%200%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%CTV%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%U15%' THEN 'POP UP CANADA'
WHEN A0.DISPLAYNAME LIKE '%UBS%' THEN 'Toronto'
WHEN A0.DISPLAYNAME LIKE '%UBS%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UHB%' THEN 'HR Bloor Street'
WHEN A0.DISPLAYNAME LIKE '%UHC%' THEN 'HR Calgary'
WHEN A0.DISPLAYNAME LIKE '%UHE%' THEN 'HR Edmonton'
WHEN A0.DISPLAYNAME LIKE '%UHY%' THEN 'HR Yorkdale'
WHEN A0.DISPLAYNAME LIKE '%UHY%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%UMT%' THEN 'Ogilvy Montreal'
WHEN A0.DISPLAYNAME LIKE '%UTO%' THEN 'Saks Toronto'
WHEN A0.DISPLAYNAME LIKE '%UTO%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UVC%' THEN 'HR Vancouver'
WHEN A0.DISPLAYNAME LIKE '%UVH%' THEN 'Vancouver-Fairmont'
WHEN A0.DISPLAYNAME LIKE '%UVH%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UVH%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%UVH%' THEN 'HR Vancouver Men'
WHEN A0.DISPLAYNAME LIKE '%UVM%' THEN 'HR Vancouver Men'
WHEN A0.DISPLAYNAME LIKE '%UVM%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UBT%' THEN 'French Indies'
WHEN A0.DISPLAYNAME LIKE '%CCA%' THEN 'CA-San Franciso Call Center'
WHEN A0.DISPLAYNAME LIKE '%DCC%' THEN 'TX-Dallas Call Center-Dallas'
WHEN A0.DISPLAYNAME LIKE '%DCC%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%DSK%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%FCA%' THEN 'CA-US Manufacturing SD2'
WHEN A0.DISPLAYNAME LIKE '%FCA%' THEN 'CA-US Manufacturing San Dimas'
WHEN A0.DISPLAYNAME LIKE '%FCA%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%GPS%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%M00%' THEN 'HW-Ala Moana'
WHEN A0.DISPLAYNAME LIKE '%M01%' THEN 'HW-Ala Moana'
WHEN A0.DISPLAYNAME LIKE '%M01%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%M04%' THEN 'HW-Waikiki'
WHEN A0.DISPLAYNAME LIKE '%M04%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%M05%' THEN 'HW-Hilton Hawaiian Village'
WHEN A0.DISPLAYNAME LIKE '%M05%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%M10%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%M10%' THEN 'HW-Whalers Village Maui'
WHEN A0.DISPLAYNAME LIKE '%M11%' THEN 'HW-Shops at Wailea'
WHEN A0.DISPLAYNAME LIKE '%MIS%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%MIS%' THEN 'NY-New York-Fifth Avenue'
WHEN A0.DISPLAYNAME LIKE '%OCA%' THEN 'CA-LV Western Regional-Beverly Hills'
WHEN A0.DISPLAYNAME LIKE '%OCN%' THEN 'CN-LV Canada and Bermuda Regional-Toronto'
WHEN A0.DISPLAYNAME LIKE '%OCN%' THEN 'NY-LVNA 1 East 57th St NYC'
WHEN A0.DISPLAYNAME LIKE '%OFL%' THEN 'FL-LV Southeast Regional'
WHEN A0.DISPLAYNAME LIKE '%OHI%' THEN 'HI-LV Hawaii Regional Office Honolulu'
WHEN A0.DISPLAYNAME LIKE '%ON0%' THEN 'NY-LVNA 625 Madison Ave NYC'
WHEN A0.DISPLAYNAME LIKE '%ON0%' THEN 'NY-LVNA 1 East 57th St NYC'
WHEN A0.DISPLAYNAME LIKE '%ON0%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%ON0%' THEN 'CN-LV Canada and Bermuda Regional-Toronto'
WHEN A0.DISPLAYNAME LIKE '%ON2%' THEN 'NY-LVNA 1 East 57th St NYC'
WHEN A0.DISPLAYNAME LIKE '%ON2%' THEN 'NY-LVNA 625 Madison Ave NYC'
WHEN A0.DISPLAYNAME LIKE '%ON3%' THEN 'NY-LVNA 1 East 57th St NYC'
WHEN A0.DISPLAYNAME LIKE '%ON3%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%ON3%' THEN 'NY-LVNA 625 Madison Ave NYC'
WHEN A0.DISPLAYNAME LIKE '%ON3%' THEN 'CA-Beverly Hills-Rodeo Drive'
WHEN A0.DISPLAYNAME LIKE '%ON4%' THEN 'NY-LVNA 1 East 57th St NYC'
WHEN A0.DISPLAYNAME LIKE '%ON4%' THEN 'NY-LVNA 625 Madison Ave NYC'
WHEN A0.DISPLAYNAME LIKE '%ON5%' THEN 'NY-LVNA 1 East 57th St NYC'
WHEN A0.DISPLAYNAME LIKE '%ON5%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%ON5%' THEN 'NY-LVNA 625 Madison Ave NYC'
WHEN A0.DISPLAYNAME LIKE '%OS0%' THEN 'MI-Southfield'
WHEN A0.DISPLAYNAME LIKE '%OSO%' THEN 'MI-Southfield'
WHEN A0.DISPLAYNAME LIKE '%OTX%' THEN 'TX-LV Central Regional-Colleyville'
WHEN A0.DISPLAYNAME LIKE '%SD2%' THEN 'CA-US Manufacturing SD2'
WHEN A0.DISPLAYNAME LIKE '%SD2%' THEN 'CA-US Manufacturing San Dimas'
WHEN A0.DISPLAYNAME LIKE '%TX1%' THEN 'TX-US Manufacturing Dallas'
WHEN A0.DISPLAYNAME LIKE '%U2R%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%U2R%' THEN 'CA-US Manufacturing San Dimas'
WHEN A0.DISPLAYNAME LIKE '%U2R%' THEN 'CA-US Manufacturing SD2'
WHEN A0.DISPLAYNAME LIKE '%U2R%' THEN 'CA-Distribution Center Ontario'
WHEN A0.DISPLAYNAME LIKE '%U3R%' THEN 'NJ-Distribution Center Cranbury'
WHEN A0.DISPLAYNAME LIKE '%U3R%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%U3R%' THEN 'CA-Distribution Center Ontario'
WHEN A0.DISPLAYNAME LIKE '%UAC%' THEN 'CO-Aspen'
WHEN A0.DISPLAYNAME LIKE '%UAF%' THEN 'FL-Aventura-Bloomingdales'
WHEN A0.DISPLAYNAME LIKE '%UAG%' THEN 'GA-Atlanta-Saks'
WHEN A0.DISPLAYNAME LIKE '%UAG%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UAP%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UAS%' THEN 'TX-Austin'
WHEN A0.DISPLAYNAME LIKE '%UAS%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UBA%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UBA%' THEN 'AL-Birmingham-Saks'
WHEN A0.DISPLAYNAME LIKE '%UBE%' THEN 'CA-San Francisco-Bloomingdales'
WHEN A0.DISPLAYNAME LIKE '%UBH%' THEN 'FL-Bal Harbour'
WHEN A0.DISPLAYNAME LIKE '%UBH%' THEN 'FL-Aventura-Bloomingdales'
WHEN A0.DISPLAYNAME LIKE '%UBH%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UBL%' THEN 'CA-Costa Mesa-Bloomingdales'
WHEN A0.DISPLAYNAME LIKE '%UBL%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UBM%' THEN 'NY-New York-Bloomingdales'
WHEN A0.DISPLAYNAME LIKE '%UBM%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%UBN%' THEN 'NY-New York-Bloomingdales'
WHEN A0.DISPLAYNAME LIKE '%UBN%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UBP%' THEN 'MA-Boston-Saks'
WHEN A0.DISPLAYNAME LIKE '%UBR%' THEN 'FL-Boca Raton'
WHEN A0.DISPLAYNAME LIKE '%UBR%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UBU%' THEN 'MD-Baltimore'
WHEN A0.DISPLAYNAME LIKE '%UBU%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UBV%' THEN 'CA-Los Angeles-Beverly Center'
WHEN A0.DISPLAYNAME LIKE '%UBV%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UBW%' THEN 'NY-New York-Bloomingdales Shoes'
WHEN A0.DISPLAYNAME LIKE '%UBW%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UCA%' THEN 'NC-Charlotte'
WHEN A0.DISPLAYNAME LIKE '%UCC%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UCC%' THEN 'MD-Chevy Chase-Saks'
WHEN A0.DISPLAYNAME LIKE '%UCE%' THEN 'NV-Las Vegas-Caesars'
WHEN A0.DISPLAYNAME LIKE '%UCE%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UCF%' THEN 'TX-Fort Worth-Clearfork'
WHEN A0.DISPLAYNAME LIKE '%UCF%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UCH%' THEN 'IL-Chicago-Michigan Avenue'
WHEN A0.DISPLAYNAME LIKE '%UCH%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UCI%' THEN 'Las Vegas-City Center'
WHEN A0.DISPLAYNAME LIKE '%UCI%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UCI%' THEN 'NV-Las Vegas Regional'
WHEN A0.DISPLAYNAME LIKE '%UCI%' THEN 'CA-LV Western Regional-Beverly Hills'
WHEN A0.DISPLAYNAME LIKE '%UCL%' THEN 'FL-Coral Gables-Neiman Marcus'
WHEN A0.DISPLAYNAME LIKE '%UCN%' THEN 'Cincinnati-Saks'
WHEN A0.DISPLAYNAME LIKE '%UCP%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UCP%' THEN 'Boston'
WHEN A0.DISPLAYNAME LIKE '%UCP%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%UCR%' THEN 'Charleston'
WHEN A0.DISPLAYNAME LIKE '%UCR%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UDC%' THEN 'Denver'
WHEN A0.DISPLAYNAME LIKE '%UDC%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UDG%' THEN 'Dallas Galleria'
WHEN A0.DISPLAYNAME LIKE '%UDG%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UDX%' THEN 'TX-Northpark'
WHEN A0.DISPLAYNAME LIKE '%UDX%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UDX%' THEN 'Houston-Neiman Marcus'
WHEN A0.DISPLAYNAME LIKE '%UEJ%' THEN 'FL-Sarasota TEMP'
WHEN A0.DISPLAYNAME LIKE '%UEJ%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UEJ%' THEN 'New York-LVNA 1 East 57th St NYC'
WHEN A0.DISPLAYNAME LIKE '%UFA%' THEN 'New York-Fifth Avenue'
WHEN A0.DISPLAYNAME LIKE '%UFA%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UFA%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%UFV%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UFV%' THEN 'CA-San Diego-Fashion Valley'
WHEN A0.DISPLAYNAME LIKE '%UGS%' THEN 'NJ-Paramus'
WHEN A0.DISPLAYNAME LIKE '%UHC%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UHE%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UHG%' THEN 'Houston Galleria'
WHEN A0.DISPLAYNAME LIKE '%UHG%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UHS%' THEN 'New York-Macys'
WHEN A0.DISPLAYNAME LIKE '%UHS%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UHX%' THEN 'XX-Blossom Store Pilot'
WHEN A0.DISPLAYNAME LIKE '%UHX%' THEN 'Houston-Saks'
WHEN A0.DISPLAYNAME LIKE '%UIN%' THEN 'Indianapolis'
WHEN A0.DISPLAYNAME LIKE '%UIN%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UJF%' THEN 'Jacksonville'
WHEN A0.DISPLAYNAME LIKE '%UJF%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UKF%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%UKF%' THEN 'NY-NEW YORK BROOKFIELD PLACE'
WHEN A0.DISPLAYNAME LIKE '%UKP%' THEN 'PA-King of Prussia'
WHEN A0.DISPLAYNAME LIKE '%UKP%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UKP%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%ULA%' THEN 'CA-Santa Monica'
WHEN A0.DISPLAYNAME LIKE '%ULA%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%ULB%' THEN 'Las Vegas-Bellagio'
WHEN A0.DISPLAYNAME LIKE '%ULB%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%ULG%' THEN 'CA-Glendale-Bloomingdales'
WHEN A0.DISPLAYNAME LIKE '%ULG%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%ULS%' THEN 'Atlanta'
WHEN A0.DISPLAYNAME LIKE '%ULS%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%ULY%' THEN 'New York-Saks Lifestyle'
WHEN A0.DISPLAYNAME LIKE '%ULZ%' THEN 'New York-Saks Shoes'
WHEN A0.DISPLAYNAME LIKE '%UM0%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UMA%' THEN 'NY-New York-Madison Ave Pop Up'
WHEN A0.DISPLAYNAME LIKE '%UMD%' THEN 'FL-Miami-Design District'
WHEN A0.DISPLAYNAME LIKE '%UMD%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UMH%' THEN 'NY-Manhasset'
WHEN A0.DISPLAYNAME LIKE '%UMH%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UMN%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UMN%' THEN 'Minneapolis Edina'
WHEN A0.DISPLAYNAME LIKE '%UMR%' THEN 'Garden City-Macys'
WHEN A0.DISPLAYNAME LIKE '%UMR%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UMT%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UMV%' THEN 'New York-SOHO Pop Up'
WHEN A0.DISPLAYNAME LIKE '%UNC%' THEN 'Chicago-Nordstom'
WHEN A0.DISPLAYNAME LIKE '%UNC%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UND%' THEN 'San Diego-Neiman Marcus'
WHEN A0.DISPLAYNAME LIKE '%UNE%' THEN 'Nashville'
WHEN A0.DISPLAYNAME LIKE '%UNF%' THEN 'CA-Newport Beach-Neiman Marcus'
WHEN A0.DISPLAYNAME LIKE '%UNF%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UNH%' THEN 'TX-Houston-Neiman Marcus'
WHEN A0.DISPLAYNAME LIKE '%UNI%' THEN 'IL-Northbrook'
WHEN A0.DISPLAYNAME LIKE '%UNJ%' THEN 'NJ-Short Hills-Neiman Marcus'
WHEN A0.DISPLAYNAME LIKE '%UNK%' THEN 'MA-Natick'
WHEN A0.DISPLAYNAME LIKE '%UNK%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UNL%' THEN 'FL-Fort Lauderdale-Neiman Marcus'
WHEN A0.DISPLAYNAME LIKE '%UNL%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UNN%' THEN 'TX-Northpark-Neiman Marcus'
WHEN A0.DISPLAYNAME LIKE '%UNR%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UNR%' THEN 'FL-Naples'
WHEN A0.DISPLAYNAME LIKE '%UNT%' THEN 'NY-Garden City-Neiman Marcus'
WHEN A0.DISPLAYNAME LIKE '%UNT%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UNV%' THEN 'NV-LasVegas-Neiman Marcus'
WHEN A0.DISPLAYNAME LIKE '%UNX%' THEN 'TX-Plano-Neiman Marcus'
WHEN A0.DISPLAYNAME LIKE '%UNX%' THEN 'TX-Dallas-Neiman Marcus'
WHEN A0.DISPLAYNAME LIKE '%UNY%' THEN 'VA-McLean-NM Tysons'
WHEN A0.DISPLAYNAME LIKE '%UNZ%' THEN 'WA-Seattle-Nordstrom'
WHEN A0.DISPLAYNAME LIKE '%UNZ%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UOC%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UOC%' THEN 'IL-Oakbrook'
WHEN A0.DISPLAYNAME LIKE '%UOG%' THEN 'OR-Portland'
WHEN A0.DISPLAYNAME LIKE '%UOG%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UOH%' THEN 'OH-Easton'
WHEN A0.DISPLAYNAME LIKE '%UOH%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UOR%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UOR%' THEN 'FL-Orlando'
WHEN A0.DISPLAYNAME LIKE '%UPB%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UPB%' THEN 'FL-Palm Beach'
WHEN A0.DISPLAYNAME LIKE '%UPD%' THEN 'CA-Palm Desert'
WHEN A0.DISPLAYNAME LIKE '%UPD%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UPF%' THEN 'MO-Frontenac'
WHEN A0.DISPLAYNAME LIKE '%UPF%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UPG%' THEN 'FL-Palm Beach Gardens'
WHEN A0.DISPLAYNAME LIKE '%UPG%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UPG%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%UPH%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UPH%' THEN 'AZ-Scottsdale'
WHEN A0.DISPLAYNAME LIKE '%UPH%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%UPK%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UPK%' THEN 'Pittsburgh'
WHEN A0.DISPLAYNAME LIKE '%UPK%' THEN 'PA-King of Prussia'
WHEN A0.DISPLAYNAME LIKE '%UPP%' THEN 'CA-Costa Mesa-South Coast Plaza Pop-Up Mens'
WHEN A0.DISPLAYNAME LIKE '%UPR%' THEN 'PR-Puerto Rico'
WHEN A0.DISPLAYNAME LIKE '%UPR%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UPV%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UPV%' THEN 'CA-Palo Alto'
WHEN A0.DISPLAYNAME LIKE '%UPZ%' THEN 'NV-Las Vegas-Palazzo Mens Temp'
WHEN A0.DISPLAYNAME LIKE '%UPZ%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UR3%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%URD%' THEN 'CA-Beverly Hills-Rodeo Drive'
WHEN A0.DISPLAYNAME LIKE '%URD%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%URD%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%URS%' THEN 'NJ-Hackensack'
WHEN A0.DISPLAYNAME LIKE '%URS%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%USA%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%USA%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%USA%' THEN 'FL-Bal Harbour-Saks'
WHEN A0.DISPLAYNAME LIKE '%USB%' THEN 'FL-Boca Raton-Saks'
WHEN A0.DISPLAYNAME LIKE '%USC%' THEN 'CA-Costa Mesa'
WHEN A0.DISPLAYNAME LIKE '%USC%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%USC%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%USC%' THEN 'NY-LVNA 1 East 57th St NYC'
WHEN A0.DISPLAYNAME LIKE '%USH%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%USH%' THEN 'NJ-Short Hills'
WHEN A0.DISPLAYNAME LIKE '%USL%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%USL%' THEN 'LA-New Orleans-Saks'
WHEN A0.DISPLAYNAME LIKE '%USM%' THEN 'FL-Miami-Saks'
WHEN A0.DISPLAYNAME LIKE '%USN%' THEN 'NY-New York-Saks'
WHEN A0.DISPLAYNAME LIKE '%USN%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%USN%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%USO%' THEN 'NY-New York-SOHO'
WHEN A0.DISPLAYNAME LIKE '%USO%' THEN 'NY-LVNA 1 East 57th St NYC'
WHEN A0.DISPLAYNAME LIKE '%USQ%' THEN 'CA-Sacramento'
WHEN A0.DISPLAYNAME LIKE '%USQ%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%USS%' THEN 'CA-San Francisco'
WHEN A0.DISPLAYNAME LIKE '%USS%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%USS%' THEN 'CA-LV Western Regional-Beverly Hills'
WHEN A0.DISPLAYNAME LIKE '%USS%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%USU%' THEN 'CA-Costa Mesa-Saks'
WHEN A0.DISPLAYNAME LIKE '%USW%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%USW%' THEN 'OH-Beachwood-Saks'
WHEN A0.DISPLAYNAME LIKE '%USX%' THEN 'TX-San Antonio-Saks'
WHEN A0.DISPLAYNAME LIKE '%USY%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%USY%' THEN 'MI-Troy-Saks'
WHEN A0.DISPLAYNAME LIKE '%UTA%' THEN 'AZ-Tuscon'
WHEN A0.DISPLAYNAME LIKE '%UTA%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UTF%' THEN 'FL-Tampa'
WHEN A0.DISPLAYNAME LIKE '%UTF%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UTH%' THEN 'Virgin Islands'
WHEN A0.DISPLAYNAME LIKE '%UTM%' THEN 'MI-Troy'
WHEN A0.DISPLAYNAME LIKE '%UTM%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UTP%' THEN 'CA-Topanga'
WHEN A0.DISPLAYNAME LIKE '%UTP%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UTX%' THEN 'TX-San Antonio-Neiman Marcus'
WHEN A0.DISPLAYNAME LIKE '%UTY%' THEN 'VA-Tysons Corner'
WHEN A0.DISPLAYNAME LIKE '%UTY%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UVF%' THEN 'CA-Valley Fair'
WHEN A0.DISPLAYNAME LIKE '%UVF%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UVG%' THEN 'NV-Las Vegas-Fashion Show Mall'
WHEN A0.DISPLAYNAME LIKE '%UVG%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UVM%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%UVT%' THEN 'VA-Tysons Corner-Bloomingdales'
WHEN A0.DISPLAYNAME LIKE '%UVT%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UVT%' THEN 'NY-LVNA 1 East 57th St NYC'
WHEN A0.DISPLAYNAME LIKE '%UWA%' THEN 'WA-Seattle-Braven'
WHEN A0.DISPLAYNAME LIKE '%UWA%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UWB%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UWB%' THEN 'CA-Beverly Hills-Saks'
WHEN A0.DISPLAYNAME LIKE '%UWC%' THEN 'NY-White Plains'
WHEN A0.DISPLAYNAME LIKE '%UWC%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UWD%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UWD%' THEN 'DC-Washington DC'
WHEN A0.DISPLAYNAME LIKE '%UWD%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%UWE%' THEN 'CT-Westfarms'
WHEN A0.DISPLAYNAME LIKE '%UWE%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UWM%' THEN 'NV-Las Vegas-Wynn Mens Store'
WHEN A0.DISPLAYNAME LIKE '%UWP%' THEN 'NY-Bloomingdales White Plains'
WHEN A0.DISPLAYNAME LIKE '%UWP%' THEN 'MA-ATT Datacenter Boston'
WHEN A0.DISPLAYNAME LIKE '%UWW%' THEN 'NY-Walt Whitman SAKS'
WHEN A0.DISPLAYNAME LIKE '%UWY%' THEN 'NV-LasVegas-Wynn Las Vegas'
WHEN A0.DISPLAYNAME LIKE '%WMA%' THEN 'IL-MWMA Regional Office-Chicago'
WHEN A0.DISPLAYNAME LIKE '%DLB%' THEN 'FL-Miami-Design District'
WHEN A0.DISPLAYNAME LIKE '%DLB%' THEN 'XX-Blossom Store Pilot'
WHEN A0.DISPLAYNAME LIKE '%-25%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%-4P%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%-64%' THEN ''
WHEN A0.DISPLAYNAME LIKE '%EC-%' THEN 'Paris'

    
END AS 'City',


CASE 
    WHEN A0.DISPLAYNAME LIKE '%C0' Then 'Office'
    WHEN A0.DISPLAYNAME LIKE '%C7' Then 'Office'
    WHEN A0.DISPLAYNAME LIKE '%C8' Then 'Office'
    WHEN A0.DISPLAYNAME LIKE '%K0' Then 'Office'
    WHEN A0.DISPLAYNAME LIKE '%K7' Then 'Office' 
    WHEN A0.DISPLAYNAME LIKE '%K8' Then 'Office' 
    WHEN A0.DISPLAYNAME LIKE '%A0' Then 'Office'
    WHEN A0.DISPLAYNAME LIKE '%A7' Then 'Office'
    WHEN A0.DISPLAYNAME LIKE '%A8' Then 'Office'
    WHEN A0.DISPLAYNAME LIKE '%B0' Then 'Office'
    WHEN A0.DISPLAYNAME LIKE '%B7' Then 'Office'
    WHEN A0.DISPLAYNAME LIKE '%B8' Then 'Office'
    WHEN A0.DISPLAYNAME LIKE '%H0' Then 'Office'
    WHEN A0.DISPLAYNAME LIKE '%H7' Then 'Office'
    WHEN A0.DISPLAYNAME LIKE '%H8' Then 'Office'
    
    WHEN A0.DISPLAYNAME LIKE '%L0' Then 'Workshop'
    WHEN A0.DISPLAYNAME LIKE '%L7' Then 'Workshop'
    WHEN A0.DISPLAYNAME LIKE '%L8' Then 'Workshop'
    WHEN A0.DISPLAYNAME LIKE '%T0' Then 'Workshop'
    WHEN A0.DISPLAYNAME LIKE '%T7' Then 'Workshop'
    WHEN A0.DISPLAYNAME LIKE '%T8' Then 'Workshop'
    WHEN A0.DISPLAYNAME LIKE '%Z0' Then 'Workshop'
    WHEN A0.DISPLAYNAME LIKE '%Z7' Then 'Workshop'
    WHEN A0.DISPLAYNAME LIKE '%Z8' Then 'Workshop'
    WHEN A0.DISPLAYNAME LIKE '%Q0' Then 'Workshop'
    WHEN A0.DISPLAYNAME LIKE '%Q7' Then 'Workshop'
    WHEN A0.DISPLAYNAME LIKE '%Q8' Then 'Workshop'
    
    WHEN A0.DISPLAYNAME LIKE '%X0' Then 'Other'
    WHEN A0.DISPLAYNAME LIKE '%X7' Then 'Other'
    WHEN A0.DISPLAYNAME LIKE '%X8' Then 'Other'

    WHEN A0.DISPLAYNAME LIKE '%F0' Then 'Store'
    WHEN A0.DISPLAYNAME LIKE '%F7' Then 'Store'
    WHEN A0.DISPLAYNAME LIKE '%F8' Then 'Store'
    WHEN A0.DISPLAYNAME LIKE '%D0' Then 'Store'
    WHEN A0.DISPLAYNAME LIKE '%D7' Then 'Store'
    WHEN A0.DISPLAYNAME LIKE '%D8' Then 'Store'
    WHEN A0.DISPLAYNAME LIKE '%XLR1%' Then 'Store'
    WHEN A0.DISPLAYNAME LIKE '%XLR2%' Then 'Store'
    WHEN A0.DISPLAYNAME LIKE '%XMSN%' Then 'Store'

END AS 'Wks Type',

CASE 
    WHEN A0.DISPLAYNAME LIKE '%C0' Then 'Office'
    WHEN A0.DISPLAYNAME LIKE '%C7' Then 'Office'
    WHEN A0.DISPLAYNAME LIKE '%C8' Then 'Office'
    WHEN A0.DISPLAYNAME LIKE '%K0' Then 'VAP'
    WHEN A0.DISPLAYNAME LIKE '%K7' Then 'VAP' 
    WHEN A0.DISPLAYNAME LIKE '%K8' Then 'VAP' 
    WHEN A0.DISPLAYNAME LIKE '%A0' Then 'Admin'
    WHEN A0.DISPLAYNAME LIKE '%A7' Then 'Admin'
    WHEN A0.DISPLAYNAME LIKE '%A8' Then 'Admin'
    WHEN A0.DISPLAYNAME LIKE '%XLR%' Then 'LR'
    WHEN A0.DISPLAYNAME LIKE '%XBR%' Then 'BR'
    WHEN A0.DISPLAYNAME LIKE '%XMSN%' Then 'XMSN'
    WHEN A0.DISPLAYNAME LIKE '%B0' Then 'Back office'
    WHEN A0.DISPLAYNAME LIKE '%B7' Then 'Back office'
    WHEN A0.DISPLAYNAME LIKE '%B8' Then 'Back office'

    WHEN A0.DISPLAYNAME LIKE '%H0' Then 'IT room XStore'
    WHEN A0.DISPLAYNAME LIKE '%H7' Then 'IT room XStore'
    WHEN A0.DISPLAYNAME LIKE '%H8' Then 'IT room XStore'
    WHEN A0.DISPLAYNAME LIKE '%L0' Then 'Lectra'
    WHEN A0.DISPLAYNAME LIKE '%L7' Then 'Lectra'
    WHEN A0.DISPLAYNAME LIKE '%L8' Then 'Lectra'
    WHEN A0.DISPLAYNAME LIKE '%T0' Then 'Trace'
    WHEN A0.DISPLAYNAME LIKE '%T7' Then 'Trace'
    WHEN A0.DISPLAYNAME LIKE '%T8' Then 'Trace'
    WHEN A0.DISPLAYNAME LIKE '%Z0' Then 'Comeltz'
    WHEN A0.DISPLAYNAME LIKE '%Z7' Then 'Comeltz'
    WHEN A0.DISPLAYNAME LIKE '%Z8' Then 'Comeltz'
    WHEN A0.DISPLAYNAME LIKE '%Q0' Then 'Qube'
    WHEN A0.DISPLAYNAME LIKE '%Q7' Then 'Qube'
    WHEN A0.DISPLAYNAME LIKE '%Q8' Then 'Qube'
    
    WHEN A0.DISPLAYNAME LIKE '%X0' Then 'Multimedia'
    WHEN A0.DISPLAYNAME LIKE '%X7' Then 'Multimedia'
    WHEN A0.DISPLAYNAME LIKE '%X8' Then 'Multimedia'

    WHEN A0.DISPLAYNAME LIKE '%F0' Then 'POS'
    WHEN A0.DISPLAYNAME LIKE '%F7' Then 'POS'
    WHEN A0.DISPLAYNAME LIKE '%F8' Then 'POS'

    WHEN A0.DISPLAYNAME LIKE '%D0' Then 'Front Office'
    WHEN A0.DISPLAYNAME LIKE '%D7' Then 'Front Office'
    WHEN A0.DISPLAYNAME LIKE '%D8' Then 'Front Office'

END AS 'Wks Usage',

CASE 
    WHEN A0.DISPLAYNAME LIKE '%C0' Then 'C0'
    WHEN A0.DISPLAYNAME LIKE '%C7' Then 'C7'
    WHEN A0.DISPLAYNAME LIKE '%C8' Then 'C8'
    WHEN A0.DISPLAYNAME LIKE '%K0' Then 'K0'
    WHEN A0.DISPLAYNAME LIKE '%K7' Then 'K7' 
    WHEN A0.DISPLAYNAME LIKE '%K8' Then 'K8' 
    WHEN A0.DISPLAYNAME LIKE '%A0' Then 'A0'
    WHEN A0.DISPLAYNAME LIKE '%A7' Then 'A7'
    WHEN A0.DISPLAYNAME LIKE '%A8' Then 'A8'
    WHEN A0.DISPLAYNAME LIKE '%XLR%' Then 'LR'
    WHEN A0.DISPLAYNAME LIKE '%XBR%' Then 'BR'
    WHEN A0.DISPLAYNAME LIKE '%XMSN%' Then 'XMSN'
    WHEN A0.DISPLAYNAME LIKE '%B0' Then 'B0'
    WHEN A0.DISPLAYNAME LIKE '%B7' Then 'B7'
    WHEN A0.DISPLAYNAME LIKE '%B8' Then 'B8'

    WHEN A0.DISPLAYNAME LIKE '%H0' Then 'H0'
    WHEN A0.DISPLAYNAME LIKE '%H7' Then 'H7'
    WHEN A0.DISPLAYNAME LIKE '%H8' Then 'H8'
    WHEN A0.DISPLAYNAME LIKE '%L0' Then 'L0'
    WHEN A0.DISPLAYNAME LIKE '%L7' Then 'L7'
    WHEN A0.DISPLAYNAME LIKE '%L8' Then 'L8'
    WHEN A0.DISPLAYNAME LIKE '%T0' Then 'T0'
    WHEN A0.DISPLAYNAME LIKE '%T7' Then 'T7'
    WHEN A0.DISPLAYNAME LIKE '%T8' Then 'T8'
    WHEN A0.DISPLAYNAME LIKE '%Z0' Then 'Z0'
    WHEN A0.DISPLAYNAME LIKE '%Z7' Then 'Z7'
    WHEN A0.DISPLAYNAME LIKE '%Z8' Then 'Z8'
    WHEN A0.DISPLAYNAME LIKE '%Q0' Then 'Q0'
    WHEN A0.DISPLAYNAME LIKE '%Q7' Then 'Q7'
    WHEN A0.DISPLAYNAME LIKE '%Q8' Then 'Q8'
    WHEN A0.DISPLAYNAME LIKE '%X0' Then 'X0'
    WHEN A0.DISPLAYNAME LIKE '%X7' Then 'X7'
    WHEN A0.DISPLAYNAME LIKE '%X8' Then 'X8'
    WHEN A0.DISPLAYNAME LIKE '%F0' Then 'F0'
    WHEN A0.DISPLAYNAME LIKE '%F7' Then 'F7'
    WHEN A0.DISPLAYNAME LIKE '%F8' Then 'F8'
    WHEN A0.DISPLAYNAME LIKE '%D0' Then 'D0'
    WHEN A0.DISPLAYNAME LIKE '%D7' Then 'D7'
    WHEN A0.DISPLAYNAME LIKE '%D8' Then 'D8'

END AS 'Wks Code',

RIGHT(a0.computerlocation,LEN(a0.computerlocation)-CHARINDEX('/',a0.computerlocation)) as "Location"




FROM Computer A0 (nolock)
				LEFT OUTER JOIN UNMODELEDDATA GROUPWIM (nolock) ON A0.Computer_Idn = GROUPWIM.Computer_Idn AND GROUPWIM.METAOBJATTRRELATIONS_IDN = 5730 
				LEFT OUTER JOIN UNMODELEDDATA IMAGESTAMP (nolock) ON A0.Computer_Idn = IMAGESTAMP.Computer_Idn AND IMAGESTAMP.METAOBJATTRRELATIONS_IDN = 5649  
		    	INNER join Operating_System A3 (nolock) ON A0.Computer_Idn = A3.Computer_Idn          
				INNER Join OSNT A1 (nolock) ON A0.Computer_Idn = A1.Computer_Idn 
				INNER Join CompSystem CS (nolock) ON  A0.Computer_Idn= CS.Computer_Idn
				INNER join LDAPUserAttr LDAP (nolock) ON A0.Computer_Idn= LDAP.Computer_Idn
				INNER join LanDesk A2 (nolock) ON A0.Computer_Idn = A2.Computer_Idn 
			    INNER join BoundAdapter Boundadapter (nolock) ON A0.Computer_Idn = Boundadapter.Computer_Idn 
				INNER join NetworkAdapter NetworkAdapter (nolock) ON A0.Computer_Idn = NetworkAdapter.Computer_Idn 
WHERE

	A1.SERVER = N'No' 
	
	/* #################  SOFTWARE SCAN EXCLUSION (> 30 days)    ################# */
	AND CONVERT(VARCHAR(8),A0.SWLastScanDate,112)  > CONVERT(VARCHAR(8),DATEADD(day, @MaxSoftwareScandate, getdate()),112)

	
	/* #################   OPERATION SYSTEM EXCLUSION    ################# */
	AND A0.Computer_Idn NOT IN 
                (SELECT Computer_Idn 
                FROM Operating_System 
                WHERE OSTYPE LIKE '%Mac OS X%')
	AND A0.Computer_Idn NOT IN 
                (SELECT Computer_Idn 
                FROM Operating_System 
                WHERE OSTYPE LIKE '%server%')
	/* #################   LOCATION EXCLUSION    ################# */
	AND A0.Computer_Idn NOT IN 
                (SELECT Computer_Idn 
                FROM Computer 
                WHERE COMPUTERLOCATION LIKE '%jp.holding.ms.lvmh%')



ORDER BY  A0.DISPLAYNAME

    """
    os_version_dict=_get_os_details(os_details_str)
    #project_root_path = current_app.config.get('PROJECT_ROOT')
    #osdetails_data_file= os.path.join(project_root_path,"tsc_portal/securities/histories/osdetails_data_file.txt")
    project_upload_path = settings.UPLOAD_FOLDER
    osdetails_data_file = os.path.join(project_upload_path ,"securities/histories/osdetails_data_file.txt")
    with open(osdetails_data_file,"w") as osdetails_data_file_out:
        osdetails_data_file_out.write('{0}\n'.format(os_version_dict))

if __name__=="__main__":
    if __package__ is None:
        import sys
        from os import path
        sys.path.append( path.dirname(path.dirname( path.dirname( path.abspath(__file__) ) ) ) )
        os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'hygie.settings')
        from django.conf import settings
    else:
        from django.conf import settings
    start_time = datetime.datetime.now()
    print(str(start_time) )
    _save_osdetails_data_in_txt()
    end_time = datetime.datetime.now()
    print(str(end_time) )
    print(str(end_time-start_time))
    print('Last run on: ', datetime.datetime.utcnow())

