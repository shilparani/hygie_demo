# -*- coding: utf-8 -*-
#from flask_login import current_user
from datetime import timedelta
from collections import defaultdict
from django.conf import settings
from hygie.settings import logger
import datetime
import pyodbc
import os
import json


def _get_connection_security_patch():
    try:
        #conn = pymysql.connect(user=settings.SECURITY_HISTORY_NEW_LANDESK_USERNAME, passwd=settings.SECURITY_HISTORY_NEW_LANDESK_PASSWORD, host=settings.SECURITY_HISTORY_NEW_LANDESK_SERVER, db=settings.SECURITY_HISTORY_NEW_LANDESK_DB, port=settings.SECURITY_HISTORY_NEW_LANDESK_PORT)
        conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format(settings.SECURITY_HISTORY_NEW_LANDESK_SERVER, settings.SECURITY_HISTORY_NEW_LANDESK_PORT, settings.SECURITY_HISTORY_NEW_LANDESK_DB, settings.SECURITY_HISTORY_NEW_LANDESK_USERNAME, settings.SECURITY_HISTORY_NEW_LANDESK_PASSWORD))
        return conn
    except pyodbc.Error as err:
        print('Error connecting to security patch DB:', err)
        return None


def _get_connection_security_americas():
    #current_app.logger.info('Americas security start query time: %s User:%s ' % (datetime.datetime.now().strftime('%m/%d/%Y %H:%M:%S'), current_user.username))
    try:
        conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format(settings.AMERICAS_SECURITY_MSQL_SERVER, settings.AMERICAS_SECURITY_MSQL_PORT, settings.AMERICAS_SECURITY_MSQL_DB, settings.AMERICAS_SECURITY_MSQL_USERNAME, settings.AMERICAS_SECURITY_MSQL_PASSWORD))
        #current_app.logger.info('Security ODBC connection made :%s' % current_user.username)
        return conn
    except pyodbc.Error as err:
        print('Error connecting to security americas DB:', err)
        return None

def _get_connection_security_europe():
    #current_app.logger.info('Europe security start query time: %s User:%s ' % (datetime.datetime.now().strftime('%m/%d/%Y %H:%M:%S'), current_user.username))
    try:
        conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format(settings.EUROPE_SECURITY_MSQL_SERVER, settings.EUROPE_SECURITY_MSQL_PORT, settings.EUROPE_SECURITY_MSQL_DB, settings.EUROPE_SECURITY_MSQL_USERNAME, settings.EUROPE_SECURITY_MSQL_PASSWORD))
        #current_app.logger.info('Security ODBC connection made :%s' % current_user.username)
        return conn
    except pyodbc.Error as err:
        print('Error connecting to security europe DB:', err)
        return None

def _get_connection_security_asia():
    #current_app.logger.info('Asia security start query time: %s User:%s ' % (datetime.datetime.now().strftime('%m/%d/%Y %H:%M:%S'), current_user.username))
    try:
        conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format(settings.ASIA_SECURITY_MSQL_SERVER, settings.ASIA_SECURITY_MSQL_PORT, settings.ASIA_SECURITY_MSQL_DB, settings.ASIA_SECURITY_MSQL_USERNAME, settings.ASIA_SECURITY_MSQL_PASSWORD))
        #current_app.logger.info('Security ODBC connection made :%s' % current_user.username)
        return conn
    except pyodbc.Error as err:
        print('Error connecting to security asia DB:', err)
        return None

def _close_connection(conn):
    #current_app.logger.info('ODBC connection closed :%s' % current_user.username)
    conn.close()
    logger.info('End query time: %s' % datetime.datetime.now().strftime('%m/%d/%Y %H:%M:%S'))

def _get_security_microsoft_patch_wks_common(db_conn):
    result = []
    db_conn = db_conn
    cursor = db_conn.cursor()
    logger.info('_get_security_microsoft_patch_wks')

    total_wks_str = "select count(*) from ( SELECT distinct(DISPLAYNAME) FROM dbo.Computer (nolock) WHERE SWLASTSCANDATE >=dateadd(day,-7,getdate()) and dbo.Computer.TYPE <>'Server') T1"
    patch_required_wks_str = "select count(*) from (select Computer.DeviceName from CVDetected left join Vulnerability on CVDetected.Vulnerability_Idn=Vulnerability.Vulnerability_Idn left join Computer on Computer.Computer_Idn = CVDetected.Computer_Idn left join Patch on Patch.UniqueFileName = CVDetected.Patch where Vulnerability.vendor = 'Microsoft' and Vulnerability.severity = 1 and Vulnerability.PublishDate<dateadd(day,-90,getdate()) and Computer.Computer_Idn is not NULL and Computer.SWLastScanDate>=dateadd(day,-7,getdate()) and Computer.DisplayName like 'LV%' and Computer.Type<>'Server' and Patch.SupercededByVulID is NULL and Patch.Patch_Idn is not NULL group by Computer.DeviceName ) T1"

    ncur_total_wks = cursor.execute(total_wks_str)
    result_total_wks = ncur_total_wks.fetchone()
    result.append(result_total_wks)

    ncur_patch_required_wks= cursor.execute(patch_required_wks_str)
    result_patch_required_wks= ncur_patch_required_wks.fetchone()
    result.append(result_patch_required_wks)

    _close_connection(db_conn)
    return result

def _get_security_microsoft_patch_wks_common_details(db_conn):
    result = []
    db_conn = db_conn
    cursor = db_conn.cursor()
    logger.info('_get_security_microsoft_patch_wks')

    total_wks_str = "select distinct(DeviceName),ComputerLocation FROM dbo.Computer (nolock) WHERE SWLASTSCANDATE >=dateadd(day,-7,getdate()) and dbo.Computer.TYPE <>'Server'"
    patch_required_wks_str = "select Computer.DeviceName, Computer.ComputerLocation from CVDetected left join Vulnerability on CVDetected.Vulnerability_Idn=Vulnerability.Vulnerability_Idn left join Computer on Computer.Computer_Idn = CVDetected.Computer_Idn left join Patch on Patch.UniqueFileName = CVDetected.Patch   where Vulnerability.vendor = 'Microsoft' and Vulnerability.severity = 1 and Vulnerability.PublishDate<dateadd(day,-90,getdate()) and Computer.Computer_Idn is not NULL and Computer.SWLastScanDate>=dateadd(day,-7,getdate()) and Computer.DisplayName like 'LV%' and Computer.Type<>'Server' and Patch.SupercededByVulID is NULL and Patch.Patch_Idn is not NULL group by Computer.DeviceName, Computer.ComputerLocation"

    ncur_total_wks = cursor.execute(total_wks_str)
    total_wks_count = 0
    null_location_total_count = 0
    computer_locations_total = []
    for wks in ncur_total_wks.fetchall():
        total_wks_count+=1
        wks = [ x for x in wks]
        if wks[1] is None or not wks[1]: 
           null_location_total_count+=1
        elif "/" in wks[1]:
           computer_locations_total_str = str(wks[1].split("/")[1])
           if "-" in computer_locations_total_str:
               computer_locations_total.append(computer_locations_total_str.split("-")[1])
           else:
               computer_locations_total.append(computer_locations_total_str)
    computer_locations_total_dict = defaultdict(int)
    for i in computer_locations_total:
        computer_locations_total_dict[i]+=1
    if null_location_total_count !=0:
        computer_locations_total_dict["NULL"] = null_location_total_count
    #current_app.logger.info("**************%s************"%computer_locations_total_dict)
    result.append(total_wks_count)

    ncur_patch_required_wks= cursor.execute(patch_required_wks_str)
    patch_required_wks_count = 0
    null_location_required_count = 0
    computer_locations_required =[]
    for wks in ncur_patch_required_wks.fetchall():
        patch_required_wks_count+=1
        wks = [ x for x in wks]
        if wks[1] is None or not wks[1]:
            null_location_required_count+=1
        elif "/" in wks[1]:
            computer_locations_required_str = str(wks[1].split("/")[1])
            if "-" in computer_locations_required_str:
                computer_locations_required.append(computer_locations_required_str.split("-")[1])
            else:
                computer_locations_required.append(computer_locations_required_str)
    computer_locations_required_dict = defaultdict(int)
    for i in computer_locations_required:
        computer_locations_required_dict[i]+=1
    if null_location_required_count !=0:
        computer_locations_required_dict["NULL"] = null_location_required_count
    #current_app.logger.info("**************%s************"%computer_locations_required_dict)
    result.append(patch_required_wks_count)

    computer_locations_total_dict = dict(computer_locations_total_dict)
    computer_locations_required_dict = dict(computer_locations_required_dict)
    for key in computer_locations_total_dict.keys():
        if key not in computer_locations_required_dict.keys():
            computer_locations_required_dict[key]=0

    computer_locations_patched_dict =  dict(computer_locations_total_dict.items() + computer_locations_required_dict.items() + [(k,computer_locations_total_dict[k]-computer_locations_required_dict[k]) for k in set(computer_locations_required_dict) & set(computer_locations_total_dict)])

    result.append(computer_locations_patched_dict)
    result.append(computer_locations_required_dict)

    _close_connection(db_conn)
    return result

#commented by Shilpa: not being used currently
'''def _compare_last_week_data(today_data_list=None):
    if today_data_list is not None:
        project_upload_path = settings.UPLOAD_FOLDER')
        history_file = os.path.join(project_upload_path,"securities/histories/history_data.txt")
        with open(history_file,"r") as myfilein:
            lines = myfilein.readlines()
        last_week_date_str = (datetime.datetime.now()-datetime.timedelta(days=7)).strftime("%m/%d/%Y")
        last_week_required_patch_data = [item for index,item in enumerate(lines) if last_week_date_str in item]
        last_week_required_patch_data_list = [int(i) for i in last_week_required_patch_data[0].split(":")[1].strip('\n').split(",")]
        trend_list = [1 if last_week_item > today_item else -1 if last_week_item<today_item else 0 for last_week_item, today_item in zip(last_week_required_patch_data_list, today_data_list)]
        return trend_list'''

#commented by Shilpa: not being used currently
'''def _get_security_microsoft_patch_wks_zone():
    #americas data
    db_conn_americas = _get_connection_security_americas()
    if db_conn_americas is not None:
        security_count_americas = _get_security_microsoft_patch_wks_common_details(db_conn_americas)
    #emea data
    db_conn_europe = _get_connection_security_europe()
    if db_conn_europe is not None:
        security_count_europe = _get_security_microsoft_patch_wks_common_details(db_conn_europe)
    #asia data
    db_conn_asia = _get_connection_security_asia()
    if db_conn_asia is not None:
        security_count_asia = _get_security_microsoft_patch_wks_common_details(db_conn_asia)

    #total data
    security_count_total = [x+y+z for x,y,z in zip(security_count_americas[:2],security_count_europe[:2],security_count_asia[:2])]
	
    #trend data
    today_required_patch_list = (security_count_americas[:2] + security_count_europe[:2] + security_count_asia[:2] + security_count_total)[1::2]
    trend_data_list = _compare_last_week_data(today_required_patch_list)
	
    security_patch_wks_result_list = security_count_americas[:2] + security_count_europe[:2] + security_count_asia[:2] + security_count_total[:2] + trend_data_list + security_count_americas[2:] + security_count_europe[2:] + security_count_asia[2:]
    #current_app.logger.info('******%s******'%security_patch_wks_result_list)
    return security_patch_wks_result_list'''
   
def _get_security_top5_microsoft_patches_wks_common(db_conn):
    db_conn = db_conn
    cursor = db_conn.cursor()
    logger.info('_get_security_top5_microsoft_patches_wks')
    top5_microsoft_patches_wks_str = "select top 5 Vul_ID, count(*) from ( select Vulnerability.Vul_ID,Computer.DeviceName from CVDetected left join Vulnerability on CVDetected.Vulnerability_Idn=Vulnerability.Vulnerability_Idn left join Computer on Computer.Computer_Idn = CVDetected.Computer_Idn left join Patch on Patch.UniqueFileName = CVDetected.Patch where Vulnerability.vendor = 'Microsoft' and Vulnerability.severity = 1 and Vulnerability.PublishDate<dateadd(day,-90,getdate()) and Computer.Computer_Idn is not NULL and Computer.SWLastScanDate>=dateadd(day,-7,getdate()) and Computer.DisplayName like 'LV%' and Computer.Type<>'Server' and Patch.SupercededByVulID is NULL and Patch.Patch_Idn is not NULL group by Vulnerability.Vul_ID,Computer.DeviceName) T1 group by Vul_ID order by count(*) desc"

    ncur_top5_microsoft_patches_wks = cursor.execute(top5_microsoft_patches_wks_str)
    result = ncur_top5_microsoft_patches_wks.fetchall()

    _close_connection(db_conn)
    return result

#commented by Shilpa: not being used currently
'''def _get_top5_microsoft_patches_wks_zone():
    db_conn_americas = _get_connection_security_americas()
    if db_conn_americas is not None:
        security_top5_microsoft_patches_wks_americas = _get_security_top5_microsoft_patches_wks_common(db_conn_americas)
    db_conn_europe= _get_connection_security_europe()
    if db_conn_europe is not None:
        security_top5_microsoft_patches_wks_europe = _get_security_top5_microsoft_patches_wks_common(db_conn_europe)
    db_conn_asia = _get_connection_security_asia()
    if db_conn_asia is not None:
        security_top5_microsoft_patches_wks_asia = _get_security_top5_microsoft_patches_wks_common(db_conn_asia)
    security_top5_microsoft_patches_wks_list = security_top5_microsoft_patches_wks_americas + security_top5_microsoft_patches_wks_europe + security_top5_microsoft_patches_wks_asia
    return security_top5_microsoft_patches_wks_list'''

#commented by Shilpa: not being used currently
'''def _get_security_details(region):
    security_details = []
    if region == "Worldwide":
        db_conn_americas = _get_connection_security_americas()
        security_details_americas = _get_security_details_per_zone(db_conn_americas)
        db_conn_emea = _get_connection_security_europe()
        security_details_emea = _get_security_details_per_zone(db_conn_emea)
        db_conn_asia = _get_connection_security_asia()
        security_details_asia = _get_security_details_per_zone(db_conn_asia)
        security_details = security_details_americas + security_details_emea + security_details_asia
    else:
        db_conn = _get_connection_security_americas() if region == "Americas" else _get_connection_security_europe() if region == "EMEA" else _get_connection_security_asia()
        security_details = _get_security_details_per_zone(db_conn)
    return security_details'''
    
def _get_security_details_per_zone(db_conn):
    cursor = db_conn.cursor()
    logger.info('_get_security_details_per_zone')
    security_details_str = "select Computer.DeviceName, Computer.Type, CVDetected.PatchInstallSucceeded, CVDetected.NumInstallTries, Vulnerability.Vul_ID, Vulnerability.PublishDate, Vulnerability.Vendor, CVDetected.Patch, CVDetected.Expected, CVDetected.Found, CVDetected.DateDetected, Computer.ComputerLocation from CVDetected left join Vulnerability on CVDetected.Vulnerability_Idn=Vulnerability.Vulnerability_Idn left join Computer on Computer.Computer_Idn = CVDetected.Computer_Idn left join Patch on Patch.UniqueFileName = CVDetected.Patch where Vulnerability.vendor = 'Microsoft' and Vulnerability.severity = 1 and Vulnerability.PublishDate<dateadd(day,-90,getdate()) and Computer.Computer_Idn is not NULL and Computer.SWLastScanDate>=dateadd(day,-7,getdate()) and Computer.DisplayName like 'LV%' and Computer.Type<>'Server' and Patch.SupercededByVulID is NULL and Patch.Patch_Idn is not NULL"
    ncur = cursor.execute(security_details_str)
    security_details_zone = [ list(n) for n in ncur.fetchall()]
    _close_connection(db_conn)
    return security_details_zone
    

def _get_patch_search_static_result(security_type,patch_list,target_list):
    patch_search_static_result = []
    db_conn_patch = _get_connection_security_patch()
    if db_conn_patch is None:
        return patch_search_static_result
    project_upload_path = settings.UPLOAD_FOLDER
    patch_search_static_file = os.path.join(project_upload_path,"securities/sql/PatchList_Filtered.sql") if security_type=="Workstation" else os.path.join(project_upload_path,"securities/sql/PatchList_Filtered_SRV.sql")
    with open(patch_search_static_file,'r') as patch_search_static_file:
        patch_query_str = patch_search_static_file.read()
    patch_list_str = "','".join(p for p in patch_list)
    target_list_str = " and Computer.DisplayName in ('{}')".format("','".join(t for t in target_list)) if len(target_list)>0 else ""
    patch_query_str = patch_query_str.format(patch_list_str,target_list_str)
    #current_app.logger.info(patch_query_str)
    cursor = db_conn_patch.cursor()
    patch_search_static_ncur = cursor.execute(patch_query_str)
    patch_search_static_result = [ list(n) for n in patch_search_static_ncur.fetchall()]
    stack_sum = [sum([x[i] for x in patch_search_static_result]) for i in range(1,4)]
    stack_sum.insert(0,u'Total')
    patch_search_static_result.append(stack_sum)
    logger.info(patch_search_static_result)
    _close_connection(db_conn_patch)
    return patch_search_static_result
    
    
def _get_patch_detail_search_result(security_type,zone,logic,filterName,patchList,targetList):
    patch_detail_search_result = []
    db_conn_patch = _get_connection_security_patch()
    if db_conn_patch is None:
        return patch_detail_search_result
    
    project_upload_path = settings.UPLOAD_FOLDER
    search_filter_json = os.path.join(project_upload_path,"securities/histories/search_filter_list_wks.json") if security_type=="Workstation" else os.path.join(project_upload_path,"securities/histories/search_filter_list_srv.json")
    with open(search_filter_json,"r") as search_filter_file:
        search_filter_list = json.load(search_filter_file)
    patch_list = search_filter_list[filterName]["patchList"].splitlines() if patchList=="None" else patchList.split(",") if "," in patchList else []
    target_list = search_filter_list[filterName]["targetList"].splitlines() if patchList=="None" else targetList.split(",") if "," in targetList else []
    patch_list_str = "','".join(p for p in patch_list)
    target_list_str = " and Computer.DisplayName in ('{}')".format("','".join(t for t in target_list)) if len(target_list)>0 else ""
    logger.info("patch_list_str:%s"%patch_list_str)

    zone_str = "Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Cn-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Jp-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Ap-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Sg-%Workstation%' or Computer.DisplayName like '%LVCN%' or Computer.DisplayName like '%LVJP%' or Computer.DisplayName like '%LVAP%' or Computer.DisplayName like '%LVSG%'" if zone=="AS" else "Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Eu-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Cp-%Workstation%' or Computer.DisplayName like '%LVEU%' or Computer.DisplayName like '%LVCP%'" if zone=="EU" else "Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Na-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/La-%Workstation%' or Computer.DisplayName like '%LVNA%' or Computer.DisplayName like '%LVLA%'" if zone=="AM" else "Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Na-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/La-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Cn-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Jp-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Ap-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Sg-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Eu-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Cp-%Workstation%' or Computer.DisplayName like '%LVNA%' or Computer.DisplayName like '%LVLA%' or Computer.DisplayName like '%LVCN%' or Computer.DisplayName like '%LVJP%' or Computer.DisplayName like '%LVAP%' or Computer.DisplayName like '%LVSG%' or Computer.DisplayName like '%LVEU%' or Computer.DisplayName like '%LVCP%'"

    if security_type == "Server":
        zone_str = "Upper(Right(Computer.DisplayName,1)) in ('P','B') " if zone=="PRD" else "Upper(Right(Computer.DisplayName,1)) not in ('P','B') " if zone=="non-PRD" else "(Upper(Right(Computer.DisplayName,1)) in ('P','B') or Upper(Right(Computer.DisplayName,1)) not in ('P','B'))" 

    patch_detail_search_sql_file = os.path.join(project_upload_path,"securities/sql/PatchList_Computer_per_patch_per_zone_filtered.sql") if logic=="KO" else os.path.join(project_upload_path ,"securities/sql/PatchList_Computer_per_patch_per_zone_ok.sql") if logic=="OK" else os.path.join(project_upload_path ,"securities/sql/PatchList_Computer_per_patch_per_zone_total.sql")
    if security_type == "Server":
        patch_detail_search_sql_file = os.path.join(project_upload_path,"securities/sql/PatchList_Computer_per_patch_per_zone_filtered_SRV.sql") if logic=="KO" else os.path.join(project_upload_path ,"securities/sql/PatchList_Computer_per_patch_per_zone_ok_SRV.sql") if logic=="OK" else os.path.join(project_upload_path ,"securities/sql/PatchList_Computer_per_patch_per_zone_total_SRV.sql")

    with open(patch_detail_search_sql_file,'r') as patch_detail_search_sql_file:
        patch_query_str = patch_detail_search_sql_file.read()
    patch_query_str = patch_query_str.format(zone_str,patch_list_str,target_list_str)
    logger.info(patch_query_str)
    cursor = db_conn_patch.cursor()
    patch_detail_search_ncur = cursor.execute(patch_query_str)
    patch_detail_search_result = [ list(n) for n in patch_detail_search_ncur.fetchall()]
    #current_app.logger.info(patch_detail_search_result)
    _close_connection(db_conn_patch)
    return patch_detail_search_result


def _get_patch_device_info_result(security_type,device_name,filterName):
    ld_device_result = {}
    db_conn_patch = _get_connection_security_patch()
    if db_conn_patch is None:
        return ld_device_result
    project_upload_path = settings.UPLOAD_FOLDER
    ld_device_info_sql_file =  os.path.join(project_upload_path,"securities/sql/LD_Device_Info.sql")
    ld_device_patch_info_sql_file =  os.path.join(project_upload_path,"securities/sql/LD_Device_Patch_Info.sql")
    ld_device_patch_not_installed_sql_file =  os.path.join(project_upload_path,"securities/sql/LD_Device_PatchNotInstalled.sql")
    ld_device_patch_not_scanned_sql_file =  os.path.join(project_upload_path,"securities/sql/LD_Device_Patch_Not_Scanned.sql")
    ld_device_patch_history_sql_file =  os.path.join(project_upload_path,"securities/sql/LD_Device_Patch_History.sql")

    with open(ld_device_info_sql_file,'r') as ld_device_info_sql_file:
        ld_device_info_sql = ld_device_info_sql_file.read()
   
    with open(ld_device_patch_info_sql_file,'r') as ld_device_patch_info_sql_file:
        ld_device_patch_info_sql = ld_device_patch_info_sql_file.read()

    with open(ld_device_patch_not_installed_sql_file,'r') as ld_device_patch_not_installed_sql_file:
        ld_device_patch_not_installed_sql= ld_device_patch_not_installed_sql_file.read()

    with open(ld_device_patch_not_scanned_sql_file,'r') as ld_device_patch_not_scanned_sql_file:
        ld_device_patch_not_scanned_sql = ld_device_patch_not_scanned_sql_file.read()

    with open(ld_device_patch_history_sql_file,'r') as ld_device_patch_history_sql_file:
        ld_device_patch_history_sql = ld_device_patch_history_sql_file.read()

    ld_device_info_sql = ld_device_info_sql.format(device_name)
    ld_device_patch_info_sql = ld_device_patch_info_sql.format(device_name)
    ld_device_patch_not_installed_sql = ld_device_patch_not_installed_sql.format(device_name)
    logger.info(ld_device_patch_not_installed_sql)
    ld_device_patch_not_scanned_sql = ld_device_patch_not_scanned_sql.format(device_name)
    ld_device_patch_history_sql = ld_device_patch_history_sql.format(device_name)

    cursor = db_conn_patch.cursor()

    ld_device_info_ncur = cursor.execute(ld_device_info_sql)
    ld_device_info = [ list(n) for n in ld_device_info_ncur.fetchall()]

    ld_device_patch_info_ncur = cursor.execute(ld_device_patch_info_sql)
    ld_device_patch_info = [ list(n) for n in ld_device_patch_info_ncur.fetchall()]

    ld_device_patch_not_installed_ncur = cursor.execute(ld_device_patch_not_installed_sql)
    ld_device_patch_not_installed = [ list(n) for n in ld_device_patch_not_installed_ncur.fetchall()]

    ld_device_patch_not_scanned_ncur = cursor.execute(ld_device_patch_not_scanned_sql)
    ld_device_patch_not_scanned = [ list(n) for n in ld_device_patch_not_scanned_ncur.fetchall()]

    ld_device_patch_history_ncur = cursor.execute(ld_device_patch_history_sql)
    ld_device_patch_history = [ list(n) for n in ld_device_patch_history_ncur.fetchall()]

    ld_device_result["ld_device_info"]=ld_device_info
    ld_device_result["ld_device_patch_info"]=ld_device_patch_info
    ld_device_result["ld_device_patch_not_installed"]=ld_device_patch_not_installed
    ld_device_result["ld_device_patch_not_scanned"]=ld_device_patch_not_scanned
    ld_device_result["ld_device_patch_history"]=ld_device_patch_history

    logger.info(ld_device_result)
    return ld_device_result
