#!/var/www/html/.pyenv/versions/3.6.5/envs/venv_hygie_prd/bin/python3.6
# -*- coding: utf-8 -*-
from datetime import timedelta
from collections import defaultdict

import datetime
import pyodbc
import os

def _get_connection_security_americas():
    try:
        conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format('nlvna310p','1523','MSKLNA1PRD','SQL_GKSP_PRD','Ne27ksp59y'))
        return conn
    except pyodbc.Error as err:
        print('Error connecting to security americas DB:', err)
        return None

def _get_connection_security_europe():
    try:
        #conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format('nlvcp473p','1525','MSKLEM1PRD','SQL_GKSP_PRD','Ne27ksp59y'))
        conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format('LVWWWIN0006P','1525','MSKLEM1PRD','SQL_GKSP_PRD','Ne27ksp59y'))
        return conn
    except pyodbc.Error as err:
        print('Error connecting to security europe DB:', err)
        return None

def _get_connection_security_asia():
    try:
        conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format('NLVAP126P','1522','MSKLAP1PRD','SQL_GKSP_PRD','Ne27ksp59y'))
        return conn
    except pyodbc.Error as err:
        print('Error connecting to security asia DB:', err)
        return None


def _close_connection(conn):
    conn.close()


def _compare_last_week_data(security_type=None, today_data_list=None):
    project_upload_path = settings.UPLOAD_FOLDER
    if today_data_list is not None:
        history_file = os.path.join(project_upload_path ,"securities/histories/history_kaspersky_wks_v2.txt") if security_type=="WKS" else os.path.join(project_upload_path ,"securities/histories/history_kaspersky_srv_v2.txt")
        with open(history_file,"r") as myfilein:
            lines = myfilein.readlines()
        last_week_date_str = (datetime.datetime.now()-datetime.timedelta(days=7)).strftime("%m/%d/%Y")
        last_week_required_patch_data = [item for index,item in enumerate(lines) if last_week_date_str in item]
        last_week_required_patch_data_list = [int(i) for i in last_week_required_patch_data[0].split(":")[1].strip('\n').split(",")]
        trend_list = [1 if last_week_item > today_item else -1 if last_week_item<today_item else 0 for last_week_item, today_item in zip(last_week_required_patch_data_list, today_data_list)]
        return trend_list

def _get_kaspersky_cont_zone(zone=None, total_wks_str=None, wks_not_update_7days=None, total_srv_str=None, srv_not_update_7days=None):

    result_count = []

    db_conn =  _get_connection_security_americas() if zone =="Americas" else _get_connection_security_europe() if zone =="Europe" else _get_connection_security_asia()
    if db_conn is None:
        return result_count
    cursor = db_conn.cursor()

    ncur_total_wks = cursor.execute(total_wks_str)
    total_wks =  ncur_total_wks.fetchone()[0]
    result_count.append(total_wks)

    ncur_wks_not_update_7days = cursor.execute(wks_not_update_7days)
    wks_not_update_7days_count =  ncur_wks_not_update_7days.fetchone()[0]
    result_count.append(wks_not_update_7days_count)

    ncur_total_srv = cursor.execute(total_srv_str)
    total_srv =  ncur_total_srv.fetchone()[0]
    result_count.append(total_srv)

    ncur_srv_not_update_7days = cursor.execute(srv_not_update_7days)
    srv_not_update_7days_count =  ncur_srv_not_update_7days.fetchone()[0]
    result_count.append(srv_not_update_7days_count)

    _close_connection(db_conn)

    return result_count 

def _get_kaspersky_details_zone(zone=None, kaspersky_detail_wks=None, kaspersky_detail_srv=None):
    kaspersky_details_zone= dict() 

    db_conn =  _get_connection_security_americas() if zone =="Americas" else _get_connection_security_europe() if zone =="Europe" else _get_connection_security_asia()
    if db_conn is None:
        return kaspersky_details_zone
    cursor = db_conn.cursor()
    ncur = cursor.execute(kaspersky_detail_wks)
    kaspersky_details_zone['wks']= [ list(n) for n in ncur.fetchall()]
    ncur = cursor.execute(kaspersky_detail_srv)
    kaspersky_details_zone['srv'] = [ list(n) for n in ncur.fetchall()]
    _close_connection(db_conn)
    return kaspersky_details_zone
    

def _save_last_hour_data_in_txt():
    security_type_wks = "WKS"
    security_type_srv = "SRV"
    
    ##americas sql query
    americas_total_wks_str = '''select count(T1."Host Name") from ( SELECT Hosts.strWinHostName "Host Name", max(hst_prdstates.tmAvbasesDate) "AV Definition Date", max(hst_products.wstrProductName) "Product Name", max(hst_products.wstrProductVersion) "Product Version", max(AdmGroups.wstrName) "Group Name" FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrProductName = 'KAVWKS6' or hst_products.wstrProductName = 'KES') group by Hosts.strWinHostName)T1'''

    americas_wks_not_update_7days = '''select count(T1."Host Name") from ( SELECT Hosts.strWinHostName "Host Name", max(hst_prdstates.tmAvbasesDate) "AV Definition Date", max(hst_products.wstrProductName) "Product Name", max(hst_products.wstrProductVersion) "Product Version", max(AdmGroups.wstrName) "Group Name" FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrProductName = 'KAVWKS6' or hst_products.wstrProductName = 'KES') and hst_prdstates.tmAvbasesDate<dateadd(day,-7,getdate()) group by Hosts.strWinHostName )T1'''

    americas_total_srv_str = '''select count(T1."Host Name") from ( SELECT Hosts.strWinHostName "Host Name", max(hst_prdstates.tmAvbasesDate) "AV Definition Date", max(hst_products.wstrProductName) "Product Name", max(hst_products.wstrProductVersion) "Product Version", max(AdmGroups.wstrName) "Group Name" FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrproductname = 'WSEE' or hst_products.wstrproductname = 'KAVFSEE') and hst_products.wstrproductversion <> '6.0.0.0' group by Hosts.strWinHostName )T1'''

    americas_srv_not_update_7days = '''select count(T1."Host Name") from ( SELECT Hosts.strWinHostName "Host Name", max(hst_prdstates.tmAvbasesDate) "AV Definition Date", max(hst_products.wstrProductName) "Product Name", max(hst_products.wstrProductVersion) "Product Version", max(AdmGroups.wstrName) "Group Name" FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrproductname = 'WSEE' or hst_products.wstrproductname = 'KAVFSEE') and hst_products.wstrproductversion <> '6.0.0.0'and hst_prdstates.tmAvbasesDate<dateadd(day,-7,getdate()) group by Hosts.strWinHostName )T1'''

    americas_details_wks_str = '''SELECT Hosts.strWinHostName, max(hst_prdstates.wstrproductdisplversion), max(hst_prdstates.tmAvbasesDate), max(AdmGroups.wstrName) FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrProductName = 'KAVWKS6' or hst_products.wstrProductName = 'KES') and hst_prdstates.tmAvbasesDate<dateadd(day,-7,getdate()) group by Hosts.strWinHostName order by Hosts.strWinHostName'''

    americas_details_srv_str = '''SELECT Hosts.strWinHostName, max(hst_prdstates.wstrproductdisplversion), max(hst_prdstates.tmAvbasesDate), max(AdmGroups.wstrName) FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrproductname = 'WSEE' or hst_products.wstrproductname = 'KAVFSEE') and hst_products.wstrproductversion <> '6.0.0.0' and hst_prdstates.tmAvbasesDate<dateadd(day,-7,getdate()) group by Hosts.strWinHostName order by Hosts.strWinHostName'''
    
    #americas_wks_total_countries = "SELECT Hosts.strWinHostName FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bavproduct=1 and AdmGroups.nParentId<>0 and AdmGroups.nParentId<>1 and (hst_prdstates.nProduct=2 OR hst_prdstates.nProduct=5 OR hst_prdstates.nProduct=8 OR hst_prdstates.nProduct=13 OR hst_prdstates.nProduct=17)"

    #americas_wks_not_update_7days_countries = "SELECT Hosts.strWinHostName FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bavproduct=1 and AdmGroups.nParentId<>0 and AdmGroups.nParentId<>1 and (hst_prdstates.nProduct=2 OR hst_prdstates.nProduct=5 OR hst_prdstates.nProduct=8 OR hst_prdstates.nProduct=13 OR hst_prdstates.nProduct=17) and hst_prdstates.tmAvbasesDate<dateadd(day,-7,getdate())"

    #americas_srv_total_countries = "SELECT Hosts.strWinHostName FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bavproduct=1 and AdmGroups.nParentId<>0 and AdmGroups.nParentId<>1 and (hst_prdstates.nProduct=4 OR hst_prdstates.nProduct=7 OR hst_prdstates.nProduct=14 OR hst_prdstates.nProduct=15 OR hst_prdstates.nProduct=18)"

    #americas_srv_not_update_7days_countries = "SELECT Hosts.strWinHostName FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bavproduct=1 and AdmGroups.nParentId<>0 and AdmGroups.nParentId<>1 and (hst_prdstates.nProduct=4 OR hst_prdstates.nProduct=7 OR hst_prdstates.nProduct=14 OR hst_prdstates.nProduct=15 OR hst_prdstates.nProduct=18) and hst_prdstates.tmAvbasesDate < dateadd(day,-7,getdate())"
    
    ##europe sql query
    europe_total_wks_str = '''select count(T1."Host Name") from ( SELECT Hosts.strWinHostName "Host Name", max(hst_prdstates.tmAvbasesDate) "AV Definition Date", max(hst_products.wstrProductName) "Product Name", max(hst_products.wstrProductVersion) "Product Version", max(AdmGroups.wstrName) "Group Name" FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrProductName = 'KAVWKS6' or hst_products.wstrProductName = 'KES') group by Hosts.strWinHostName )T1'''

    europe_wks_not_update_7days = '''select count(T1."Host Name") from ( SELECT Hosts.strWinHostName "Host Name", max(hst_prdstates.tmAvbasesDate) "AV Definition Date", max(hst_products.wstrProductName) "Product Name", max(hst_products.wstrProductVersion) "Product Version", max(AdmGroups.wstrName) "Group Name" FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrProductName = 'KAVWKS6' or hst_products.wstrProductName = 'KES') and hst_prdstates.tmAvbasesDate<dateadd(day,-7,getdate()) group by Hosts.strWinHostName )T1'''

    europe_total_srv_str = '''select count(T1."Host Name") from ( SELECT Hosts.strWinHostName "Host Name", max(hst_prdstates.tmAvbasesDate) "AV Definition Date", max(hst_products.wstrProductName) "Product Name", max(hst_products.wstrProductVersion) "Product Version", max(AdmGroups.wstrName) "Group Name" FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrproductname = 'WSEE' or hst_products.wstrproductname = 'KAVFSEE') and hst_products.wstrproductversion <> '6.0.0.0' group by Hosts.strWinHostName )T1'''
        
    europe_srv_not_update_7days = '''select count(T1."Host Name") from ( SELECT Hosts.strWinHostName "Host Name", max(hst_prdstates.tmAvbasesDate) "AV Definition Date", max(hst_products.wstrProductName) "Product Name", max(hst_products.wstrProductVersion) "Product Version", max(AdmGroups.wstrName) "Group Name" FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrproductname = 'WSEE' or hst_products.wstrproductname = 'KAVFSEE') and hst_products.wstrproductversion <> '6.0.0.0' and hst_prdstates.tmAvbasesDate<dateadd(day,-7,getdate()) group by Hosts.strWinHostName )T1'''
        
    europe_details_wks_str = '''SELECT Hosts.strWinHostName, max(hst_prdstates.wstrproductdisplversion), max(hst_prdstates.tmAvbasesDate), max(AdmGroups.wstrName) FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrProductName = 'KAVWKS6' or hst_products.wstrProductName = 'KES') and hst_prdstates.tmAvbasesDate<dateadd(day,-7,getdate()) group by hosts.strWinHostName order by hosts.strWinHostName'''

    europe_details_srv_str = '''SELECT Hosts.strWinHostName, max(hst_prdstates.wstrproductdisplversion), max(hst_prdstates.tmAvbasesDate), max(AdmGroups.wstrName) FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrproductname = 'WSEE' or hst_products.wstrproductname = 'KAVFSEE') and hst_products.wstrproductversion <> '6.0.0.0' and hst_prdstates.tmAvbasesDate<dateadd(day,-7,getdate()) group by hosts.strWinHostName order by hosts.strWinHostName'''


    ##asia sql query
    asia_total_wks_str = '''select count(T1."Host Name") from ( SELECT Hosts.strWinHostName "Host Name", max(hst_prdstates.tmAvbasesDate) "AV Definition Date", max(hst_products.wstrProductName) "Product Name", max(hst_products.wstrProductVersion) "Product Version", max(AdmGroups.wstrName) "Group Name" FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrProductName = 'KAVWKS6' or hst_products.wstrProductName = 'KES') group by Hosts.strWinHostName )T1'''

    asia_wks_not_update_7days = '''select count(T1."Host Name") from ( SELECT Hosts.strWinHostName "Host Name", max(hst_prdstates.tmAvbasesDate) "AV Definition Date", max(hst_products.wstrProductName) "Product Name", max(hst_products.wstrProductVersion) "Product Version", max(AdmGroups.wstrName) "Group Name" FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrProductName = 'KAVWKS6' or hst_products.wstrProductName = 'KES') and hst_prdstates.tmAvbasesDate<dateadd(day,-7,getdate()) group by Hosts.strWinHostName )T1'''

    asia_total_srv_str = '''select count(T1."Host Name") from ( SELECT Hosts.strWinHostName "Host Name", max(hst_prdstates.tmAvbasesDate) "AV Definition Date", max(hst_products.wstrProductName) "Product Name", max(hst_products.wstrProductVersion) "Product Version", max(AdmGroups.wstrName) "Group Name" FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrproductname = 'WSEE' or hst_products.wstrproductname = 'KAVFSEE') and hst_products.wstrproductversion <> '6.0.0.0' group by Hosts.strWinHostName )T1'''

    asia_srv_not_update_7days = '''select count(T1."Host Name") from ( SELECT Hosts.strWinHostName "Host Name", max(hst_prdstates.tmAvbasesDate) "AV Definition Date", max(hst_products.wstrProductName) "Product Name", max(hst_products.wstrProductVersion) "Product Version", max(AdmGroups.wstrName) "Group Name" FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrproductname = 'WSEE' or hst_products.wstrproductname = 'KAVFSEE') and hst_products.wstrproductversion <> '6.0.0.0' and hst_prdstates.tmAvbasesDate<dateadd(day,-7,getdate()) group by Hosts.strWinHostName )T1'''

    asia_details_wks_str = '''SELECT Hosts.strWinHostName, max(hst_prdstates.wstrproductdisplversion), max(hst_prdstates.tmAvbasesDate), max(AdmGroups.wstrName) FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrProductName = 'KAVWKS6' or hst_products.wstrProductName = 'KES') and hst_prdstates.tmAvbasesDate<dateadd(day,-7,getdate()) group by hosts.strWinHostName order by hosts.strWinHostName'''

    asia_details_srv_str = '''SELECT Hosts.strWinHostName, max(hst_prdstates.wstrproductdisplversion), max(hst_prdstates.tmAvbasesDate), max(AdmGroups.wstrName) FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrproductname = 'WSEE' or hst_products.wstrproductname = 'KAVFSEE') and hst_products.wstrproductversion <> '6.0.0.0' and hst_prdstates.tmAvbasesDate<dateadd(day,-7,getdate()) group by hosts.strWinHostName order by hosts.strWinHostName'''

    ##count data
    americas_kaspersky_count_list = _get_kaspersky_cont_zone("Americas", americas_total_wks_str, americas_wks_not_update_7days, americas_total_srv_str, americas_srv_not_update_7days)
    europe_kaspersky_count_list = _get_kaspersky_cont_zone("Europe", europe_total_wks_str, europe_wks_not_update_7days, europe_total_srv_str, europe_srv_not_update_7days)
    asia_kaspersky_count_list = _get_kaspersky_cont_zone("Asia", asia_total_wks_str, asia_wks_not_update_7days, asia_total_srv_str, asia_srv_not_update_7days)

    total_kaspersky_count_list = [i+j+k for i,j,k in zip(americas_kaspersky_count_list,europe_kaspersky_count_list,asia_kaspersky_count_list)]


    #trend data
    today_wks_not_update_7days_list = [americas_kaspersky_count_list[1],europe_kaspersky_count_list[1],asia_kaspersky_count_list[1],total_kaspersky_count_list[1]]
    wks_trend_data_list = _compare_last_week_data("WKS", today_wks_not_update_7days_list)
    
    today_srv_not_update_7days_list = [americas_kaspersky_count_list[3],europe_kaspersky_count_list[3],asia_kaspersky_count_list[3],total_kaspersky_count_list[3]]
    srv_trend_data_list = _compare_last_week_data("SRV", today_srv_not_update_7days_list)

    #print "wks_trend_data_list:%s"%wks_trend_data_list
    #print "srv_trend_data_list:%s"%srv_trend_data_list
    
    kaspersky_wks_count_list = americas_kaspersky_count_list[:2]+europe_kaspersky_count_list[:2]+asia_kaspersky_count_list[:2]+total_kaspersky_count_list[:2]+wks_trend_data_list
    kaspersky_srv_count_list = americas_kaspersky_count_list[2:]+europe_kaspersky_count_list[2:]+asia_kaspersky_count_list[2:]+total_kaspersky_count_list[2:]+srv_trend_data_list

    ##kaspersky details data

    americas_kaspersky_details_list = _get_kaspersky_details_zone("Americas", americas_details_wks_str, americas_details_srv_str)
    europe_kaspersky_details_list = _get_kaspersky_details_zone("Europe", europe_details_wks_str, europe_details_srv_str)
    asia_kaspersky_details_list = _get_kaspersky_details_zone("Asia", asia_details_wks_str, asia_details_srv_str)

    #print "americas_kaspersky_details_list:%s"%americas_kaspersky_details_list
    #print "europe_kaspersky_details_list:%s"%europe_kaspersky_details_list
    #print "asia_kaspersky_details_list:%s"%asia_kaspersky_details_list

    project_upload_path = settings.UPLOAD_FOLDER
    ##write all data into last_hour_data.txt
    last_hour_file = os.path.join(project_upload_path ,"securities/histories/last_hour_kaspersky_data_v2.txt")

    with open(last_hour_file,"w") as last_hour_file_out:
        last_hour_file_out.write('{0}\n{1}\n{2}\n{3}\n{4}\n{5}\n{6}\n{7}\n{8}\n{9}\n'.format(kaspersky_wks_count_list,americas_kaspersky_details_list['wks'],europe_kaspersky_details_list['wks'],asia_kaspersky_details_list['wks'],americas_kaspersky_details_list['wks']+europe_kaspersky_details_list['wks']+asia_kaspersky_details_list['wks'], kaspersky_srv_count_list, americas_kaspersky_details_list['srv'],europe_kaspersky_details_list['srv'],asia_kaspersky_details_list['srv'],americas_kaspersky_details_list['srv']+europe_kaspersky_details_list['srv']+asia_kaspersky_details_list['srv']))
        #last_hour_file_out.write('{0}\n{1}\n{2}\n{3}\n{4}\n{5}\n{6}\n{7}\n{8}\n{9}\n{10}\n{11}\n{12}\n{13}\n{14}\n{15}'.format(security_microsoft_patch_wks_list, security_top5_microsoft_patches_wks_list, security_details_wks_amerias, security_details_wks_emea, security_details_wks_asia, security_details_summary_wks_amerias, security_details_summary_wks_emea, security_details_summary_wks_asia, security_microsoft_patch_srv_list, security_top5_microsoft_patches_srv_list,security_details_srv_amerias,security_details_srv_emea,security_details_srv_asia,security_details_summary_srv_amerias,security_details_summary_srv_emea,security_details_summary_srv_asia))

if __name__=="__main__":
    if __package__ is None:
        import sys
        from os import path
        sys.path.append( path.dirname(path.dirname( path.dirname( path.abspath(__file__) ) ) ) )
        os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'hygie.settings')
        from django.conf import settings
    else:
        from django.conf import settings       
    start_time = datetime.datetime.now()
    print(str(start_time) )
    _save_last_hour_data_in_txt()
    end_time = datetime.datetime.now()
    print(str(end_time) )
    print(str(end_time-start_time) )
    print('Last run on: ', datetime.datetime.utcnow())
