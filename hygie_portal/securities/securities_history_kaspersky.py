#!/var/www/html/.pyenv/versions/3.6.5/envs/venv_hygie_prd/bin/python3.6
# -*- coding: utf-8 -*-
from datetime import timedelta

import datetime
import pyodbc
import os

def _get_connection_security_americas():
    try:
        conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format('nlvna310p','1523','MSKLNA1PRD','SQL_GKSP_PRD','Ne27ksp59y'))
        return conn
    except pyodbc.Error as err:
        print('Error connecting to security americas DB:', err)
        return None

def _get_connection_security_europe():
    try:
        #conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format('nlvcp473p','1525','MSKLEM1PRD','SQL_GKSP_PRD','Ne27ksp59y'))
        conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format('LVWWWIN0006P','1525','MSKLEM1PRD','SQL_GKSP_PRD','Ne27ksp59y'))
        return conn
    except pyodbc.Error as err:
        print('Error connecting to security europe DB:', err)
        return None

def _get_connection_security_asia():
    try:
        conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format('NLVAP126P','1522','MSKLAP1PRD','SQL_GKSP_PRD','Ne27ksp59y'))
        return conn
    except pyodbc.Error as err:
        print('Error connecting to security asia DB:', err)
        return None


def _close_connection(conn):
    conn.close()


def _get_kaspersky_cont_zone(zone=None, total_wks_str=None, wks_not_update_7days=None, total_srv_str=None, srv_not_update_7days=None):

    result_count = []
    
    db_conn =  _get_connection_security_americas() if zone =="Americas" else _get_connection_security_europe() if zone =="Europe" else _get_connection_security_asia()
    
    # check DB connection, if none return
    if db_conn is None:
        return result_count
    
    cursor = db_conn.cursor()

    ncur_total_wks = cursor.execute(total_wks_str)
    total_wks =  ncur_total_wks.fetchone()[0]
    result_count.append(total_wks)

    ncur_wks_not_update_7days = cursor.execute(wks_not_update_7days)
    wks_not_update_7days_count =  ncur_wks_not_update_7days.fetchone()[0]
    result_count.append(wks_not_update_7days_count)

    ncur_total_srv = cursor.execute(total_srv_str)
    total_srv =  ncur_total_srv.fetchone()[0]
    result_count.append(total_srv)

    ncur_srv_not_update_7days = cursor.execute(srv_not_update_7days)
    srv_not_update_7days_count =  ncur_srv_not_update_7days.fetchone()[0]
    result_count.append(srv_not_update_7days_count)

    _close_connection(db_conn)

    return result_count


def _save_history_data_in_txt():

    ##americas sql query
    americas_total_wks_str = '''select count(T1."Host Name") from ( SELECT Hosts.strWinHostName "Host Name", max(hst_prdstates.tmAvbasesDate) "AV Definition Date", max(hst_products.wstrProductName) "Product Name", max(hst_products.wstrProductVersion) "Product Version", max(AdmGroups.wstrName) "Group Name" FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrProductName = 'KAVWKS6' or hst_products.wstrProductName = 'KES') group by Hosts.strWinHostName)T1'''

    americas_wks_not_update_7days = '''select count(T1."Host Name") from ( SELECT Hosts.strWinHostName "Host Name", max(hst_prdstates.tmAvbasesDate) "AV Definition Date", max(hst_products.wstrProductName) "Product Name", max(hst_products.wstrProductVersion) "Product Version", max(AdmGroups.wstrName) "Group Name" FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrProductName = 'KAVWKS6' or hst_products.wstrProductName = 'KES') and hst_prdstates.tmAvbasesDate<dateadd(day,-7,getdate()) group by Hosts.strWinHostName )T1'''

    americas_total_srv_str = '''select count(T1."Host Name") from ( SELECT Hosts.strWinHostName "Host Name", max(hst_prdstates.tmAvbasesDate) "AV Definition Date", max(hst_products.wstrProductName) "Product Name", max(hst_products.wstrProductVersion) "Product Version", max(AdmGroups.wstrName) "Group Name" FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrproductname = 'WSEE' or hst_products.wstrproductname = 'KAVFSEE') and hst_products.wstrproductversion <> '6.0.0.0' group by Hosts.strWinHostName )T1'''

    americas_srv_not_update_7days = '''select count(T1."Host Name") from ( SELECT Hosts.strWinHostName "Host Name", max(hst_prdstates.tmAvbasesDate) "AV Definition Date", max(hst_products.wstrProductName) "Product Name", max(hst_products.wstrProductVersion) "Product Version", max(AdmGroups.wstrName) "Group Name" FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrproductname = 'WSEE' or hst_products.wstrproductname = 'KAVFSEE') and hst_products.wstrproductversion <> '6.0.0.0'and hst_prdstates.tmAvbasesDate<dateadd(day,-7,getdate()) group by Hosts.strWinHostName )T1'''

    ##europe sql query
    europe_total_wks_str = '''select count(T1."Host Name") from ( SELECT Hosts.strWinHostName "Host Name", max(hst_prdstates.tmAvbasesDate) "AV Definition Date", max(hst_products.wstrProductName) "Product Name", max(hst_products.wstrProductVersion) "Product Version", max(AdmGroups.wstrName) "Group Name" FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrProductName = 'KAVWKS6' or hst_products.wstrProductName = 'KES') group by Hosts.strWinHostName )T1'''

    europe_wks_not_update_7days = '''select count(T1."Host Name") from ( SELECT Hosts.strWinHostName "Host Name", max(hst_prdstates.tmAvbasesDate) "AV Definition Date", max(hst_products.wstrProductName) "Product Name", max(hst_products.wstrProductVersion) "Product Version", max(AdmGroups.wstrName) "Group Name" FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrProductName = 'KAVWKS6' or hst_products.wstrProductName = 'KES') and hst_prdstates.tmAvbasesDate<dateadd(day,-7,getdate()) group by Hosts.strWinHostName )T1'''

    europe_total_srv_str = '''select count(T1."Host Name") from ( SELECT Hosts.strWinHostName "Host Name", max(hst_prdstates.tmAvbasesDate) "AV Definition Date", max(hst_products.wstrProductName) "Product Name", max(hst_products.wstrProductVersion) "Product Version", max(AdmGroups.wstrName) "Group Name" FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrproductname = 'WSEE' or hst_products.wstrproductname = 'KAVFSEE') and hst_products.wstrproductversion <> '6.0.0.0' group by Hosts.strWinHostName )T1'''
        
    europe_srv_not_update_7days = '''select count(T1."Host Name") from ( SELECT Hosts.strWinHostName "Host Name", max(hst_prdstates.tmAvbasesDate) "AV Definition Date", max(hst_products.wstrProductName) "Product Name", max(hst_products.wstrProductVersion) "Product Version", max(AdmGroups.wstrName) "Group Name" FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrproductname = 'WSEE' or hst_products.wstrproductname = 'KAVFSEE') and hst_products.wstrproductversion <> '6.0.0.0' and hst_prdstates.tmAvbasesDate<dateadd(day,-7,getdate()) group by Hosts.strWinHostName )T1'''


    ##asia sql query
    asia_total_wks_str = '''select count(T1."Host Name") from ( SELECT Hosts.strWinHostName "Host Name", max(hst_prdstates.tmAvbasesDate) "AV Definition Date", max(hst_products.wstrProductName) "Product Name", max(hst_products.wstrProductVersion) "Product Version", max(AdmGroups.wstrName) "Group Name" FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrProductName = 'KAVWKS6' or hst_products.wstrProductName = 'KES') group by Hosts.strWinHostName )T1'''

    asia_wks_not_update_7days = '''select count(T1."Host Name") from ( SELECT Hosts.strWinHostName "Host Name", max(hst_prdstates.tmAvbasesDate) "AV Definition Date", max(hst_products.wstrProductName) "Product Name", max(hst_products.wstrProductVersion) "Product Version", max(AdmGroups.wstrName) "Group Name" FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrProductName = 'KAVWKS6' or hst_products.wstrProductName = 'KES') and hst_prdstates.tmAvbasesDate<dateadd(day,-7,getdate()) group by Hosts.strWinHostName )T1'''

    asia_total_srv_str = '''select count(T1."Host Name") from ( SELECT Hosts.strWinHostName "Host Name", max(hst_prdstates.tmAvbasesDate) "AV Definition Date", max(hst_products.wstrProductName) "Product Name", max(hst_products.wstrProductVersion) "Product Version", max(AdmGroups.wstrName) "Group Name" FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrproductname = 'WSEE' or hst_products.wstrproductname = 'KAVFSEE') and hst_products.wstrproductversion <> '6.0.0.0' group by Hosts.strWinHostName )T1'''

    asia_srv_not_update_7days = '''select count(T1."Host Name") from ( SELECT Hosts.strWinHostName "Host Name", max(hst_prdstates.tmAvbasesDate) "AV Definition Date", max(hst_products.wstrProductName) "Product Name", max(hst_products.wstrProductVersion) "Product Version", max(AdmGroups.wstrName) "Group Name" FROM Hosts INNER JOIN hst_prdstates ON Hosts.nId = hst_prdstates.nHostId INNER JOIN hst_products ON hst_prdstates.nProduct = hst_products.nid INNER JOIN AdmGroups ON Hosts.nGroup = AdmGroups.nId WHERE hst_prdstates.bAVProduct=1 and AdmGroups.wstrName <> 'Unassigned devices' and (hst_products.wstrproductname = 'WSEE' or hst_products.wstrproductname = 'KAVFSEE') and hst_products.wstrproductversion <> '6.0.0.0' and hst_prdstates.tmAvbasesDate<dateadd(day,-7,getdate()) group by Hosts.strWinHostName )T1'''

    ##count data
    americas_kaspersky_count_list = _get_kaspersky_cont_zone("Americas", americas_total_wks_str, americas_wks_not_update_7days, americas_total_srv_str, americas_srv_not_update_7days)
    europe_kaspersky_count_list = _get_kaspersky_cont_zone("Europe", europe_total_wks_str, europe_wks_not_update_7days, europe_total_srv_str, europe_srv_not_update_7days)
    asia_kaspersky_count_list = _get_kaspersky_cont_zone("Asia", asia_total_wks_str, asia_wks_not_update_7days, asia_total_srv_str, asia_srv_not_update_7days)

    today_not_updated_7days_wks_data = [americas_kaspersky_count_list[1],europe_kaspersky_count_list[1],asia_kaspersky_count_list[1],americas_kaspersky_count_list[1]+europe_kaspersky_count_list[1]+asia_kaspersky_count_list[1]]
    today_not_updated_7days_srv_data = [americas_kaspersky_count_list[3],europe_kaspersky_count_list[3],asia_kaspersky_count_list[3],americas_kaspersky_count_list[3]+europe_kaspersky_count_list[3]+asia_kaspersky_count_list[3]]

    project_upload_path = settings.UPLOAD_FOLDER
    ## save data into files
    history_wks_file = os.path.join(project_upload_path ,"securities/histories/history_kaspersky_wks_v2.txt")
    history_srv_file = os.path.join(project_upload_path ,"securities/histories/history_kaspersky_srv_v2.txt")
    with open(history_wks_file,"r") as myfilein:
        lines = myfilein.readlines()
        lines.append(datetime.datetime.now().strftime('%m/%d/%Y')+":"+",".join(str(item) for item in today_not_updated_7days_wks_data) + "\n")
    with open(history_wks_file,"w") as myfileout:
        myfileout.writelines(lines[-50:])

    with open(history_srv_file,"r") as myfilein:
        lines = myfilein.readlines()
        lines.append(datetime.datetime.now().strftime('%m/%d/%Y')+":"+",".join(str(item) for item in today_not_updated_7days_srv_data) + "\n")
    with open(history_srv_file,"w") as myfileout:
        myfileout.writelines(lines[-50:])
        
if __name__=="__main__":
    if __package__ is None:
        import sys
        from os import path
        sys.path.append( path.dirname(path.dirname( path.dirname( path.abspath(__file__) ) ) ) )
        os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'hygie.settings')
        from django.conf import settings
    else:
        from django.conf import settings
    _save_history_data_in_txt()
    print('Last run on: ', datetime.datetime.utcnow())
