#!/var/www/html/.pyenv/versions/3.6.5/envs/venv_hygie_prd/bin/python3.6
# -*- coding: utf-8 -*-
from datetime import timedelta
from collections import defaultdict

import datetime
import pyodbc
import os
import json
import MySQLdb

def _get_connection_security():
    try:
        conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format('NLVCP1714P','1575','MSLDSK6PRD','GSQL_LANDESK_PRD','h4LuuDrD5x'))
        return conn
    except pyodbc.Error as err:
        print('Error connecting to security osdetails DB:', err)
        return None

def _get_patch_connection():
    try:
        #conn = MySQLdb.connect(user='hygie_prp', passwd='TFhj95fcht', host='nlvcp2064d', db='Hygie_prp')
        conn = MySQLdb.connect(user='esurvey', passwd='esurvey', host='nlvcp2008p', db='Hygie')
        return conn
    except MySQLdb.Error as err:
        print('Error connecting to security osdetails DB:', err)
        return None

def _close_connection(conn):
    conn.close()

def _get_security_microsoft_patch_common_details(total_sql_str=None,patch_required_sql_str=None):
    result = []
    db_conn = _get_connection_security()
    if db_conn is None:
        return result
    cursor = db_conn.cursor()

    total_wks_str = total_sql_str
    patch_required_wks_str = patch_required_sql_str

    ncur_total_wks = cursor.execute(total_wks_str)
    total_wks_count = 0
    null_location_total_count = 0
    computer_locations_total = []
    for wks in ncur_total_wks.fetchall():
        total_wks_count+=1
        wks = list(wks)
        #wks = [ x for x in wks]
        if wks[1] is None or not wks[1]: 
           null_location_total_count+=1
        elif "/" in wks[1]:
           computer_locations_total_str = str(wks[1].split("/")[1])
           if "-" in computer_locations_total_str:
               computer_locations_total.append(computer_locations_total_str.split("-")[1])
           else:
               computer_locations_total.append(computer_locations_total_str)
    computer_locations_total_dict = defaultdict(int)
    for i in computer_locations_total:
        computer_locations_total_dict[i]+=1
    if null_location_total_count !=0:
        computer_locations_total_dict["NULL"] = null_location_total_count
    result.append(total_wks_count)

    ncur_patch_required_wks= cursor.execute(patch_required_wks_str)
    patch_required_wks_count = 0
    null_location_required_count = 0
    computer_locations_required =[]
    for wks in ncur_patch_required_wks.fetchall():
        patch_required_wks_count+=1
        wks = [ x for x in wks]
        if wks[1] is None or not wks[1]:
            null_location_required_count+=1
        elif "/" in wks[1]:
            computer_locations_required_str = str(wks[1].split("/")[1])
            if "-" in computer_locations_required_str:
                computer_locations_required.append(computer_locations_required_str.split("-")[1])
            else:
                computer_locations_required.append(computer_locations_required_str)
    computer_locations_required_dict = defaultdict(int)
    for i in computer_locations_required:
        computer_locations_required_dict[i]+=1
    if null_location_required_count !=0:
        computer_locations_required_dict["NULL"] = null_location_required_count
    result.append(patch_required_wks_count)

    computer_locations_total_dict = dict(computer_locations_total_dict)
    computer_locations_required_dict = dict(computer_locations_required_dict)
    for key in computer_locations_total_dict.keys():
        if key not in computer_locations_required_dict.keys():
            computer_locations_required_dict[key]=0

    computer_locations_patched_dict =  dict(list(computer_locations_total_dict.items()) + list(computer_locations_required_dict.items()) + [(k,computer_locations_total_dict[k]-computer_locations_required_dict[k]) for k in set(computer_locations_required_dict) & set(computer_locations_total_dict)])

    result.append(computer_locations_patched_dict)
    result.append(computer_locations_required_dict)

    _close_connection(db_conn)
    return result

def _compare_last_week_data(security_type=None, today_data_list=None):
    project_upload_path = settings.UPLOAD_FOLDER
    if today_data_list is not None:
        history_file = os.path.join(project_upload_path ,"securities/histories/history_data_wks_v2.txt") if security_type=="WKS" else os.path.join(project_upload_path ,"securities/histories/history_data_srv_v2.txt")
        with open(history_file,"r") as myfilein:
            lines = myfilein.readlines()
        last_week_date_str = (datetime.datetime.now()-datetime.timedelta(days=7)).strftime("%m/%d/%Y")
        last_week_required_patch_data = [item for index,item in enumerate(lines) if last_week_date_str in item]
        last_week_required_patch_data_list = [int(i) for i in last_week_required_patch_data[0].split(":")[1].strip('\n').split(",")]
        trend_list = [1 if last_week_item > today_item else -1 if last_week_item<today_item else 0 for last_week_item, today_item in zip(last_week_required_patch_data_list, today_data_list)]
        return trend_list

def _get_security_microsoft_patch_zone(security_type=None, total_sql_str=None,patch_required_sql_str=None,in21days_patch_list=None):
    #americas data
    total_sql_americas_str = total_sql_str.format(" (DisplayName like 'LVNA%' or DisplayName like 'LVLA%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/La-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Na-%Workstation%') ") if security_type=="WKS" else total_sql_str.format("(Computer.ComputerLocation like '%Na-%' or Computer.ComputerLocation like '%La-%') or (computer.devicename like 'NLVNA%' or computer.devicename like 'NLVLA%')")
    #print(total_sql_americas_str)
    patch_required_sql_americas_str = patch_required_sql_str.format("Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Na-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/La-%Workstation%' or Computer.DisplayName like 'LVNA%' or Computer.DisplayName like 'LVLA%'",in21days_patch_list) if security_type=="WKS" else patch_required_sql_str.format("(Computer.ComputerLocation like '%Na-%' or Computer.ComputerLocation like '%La-%') or (computer.devicename like 'NLVNA%' or computer.devicename like 'NLVLA%')",in21days_patch_list)
    security_count_americas = _get_security_microsoft_patch_common_details(total_sql_americas_str, patch_required_sql_americas_str)

    #emea data
    total_sql_emea_str = total_sql_str.format(" (DisplayName like 'LVCP%' or DisplayName like 'LVEU%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Cp-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Eu-%Workstation%') ") if security_type=="WKS" else total_sql_str.format("(Computer.ComputerLocation like '%Cp-%' or Computer.ComputerLocation like '%Eu-%') or (computer.devicename like 'NLVCP%' or computer.devicename like 'ELVCP%' or computer.devicename like 'NLVEU%' or computer.devicename like 'ELVEU%')")
    patch_required_sql_emea_str = patch_required_sql_str.format("Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Cp-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Eu-%Workstation%' or Computer.DisplayName like 'LVEU%' or Computer.DisplayName like 'LVCP%'",in21days_patch_list) if security_type=="WKS" else patch_required_sql_str.format("(Computer.ComputerLocation like '%Cp-%' or Computer.ComputerLocation like '%Eu-%') or (computer.devicename like 'NLVCP%' or computer.devicename like 'ELVCP%' or computer.devicename like 'NLVEU%' or computer.devicename like 'ELVEU%')",in21days_patch_list)
    #print(total_sql_emea_str)
    security_count_europe = _get_security_microsoft_patch_common_details(total_sql_emea_str, patch_required_sql_emea_str)

    #asia data
    total_sql_asia_str = total_sql_str.format(" (DisplayName like 'LVAP%' or DisplayName like 'LVCN%' or DisplayName like 'LVJP%' or DisplayName like 'LVSG%'or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Ap-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Cn-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Jp-%Workstation%') ") if security_type=="WKS" else total_sql_str.format("(Computer.ComputerLocation like '%Ap-%' or Computer.ComputerLocation like '%Cn-%' or Computer.ComputerLocation like '%Jp-%') or (computer.devicename like 'NLVAP%' or computer.devicename like 'NLVCN%' or computer.devicename like 'NLVJP%' or computer.devicename like 'ELVAP%' or computer.devicename like 'ELVCN%' or computer.devicename like 'ELVJP%' or computer.devicename like 'NLVSG%')")
    patch_required_sql_asia_str = patch_required_sql_str.format("Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Ap-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Cn-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Jp-%Workstation%' or Computer.DisplayName like 'LVAP%' or Computer.DisplayName like 'LVCN%' or Computer.DisplayName like 'LVJP%' or Computer.DisplayName like 'LVSG%'",in21days_patch_list) if security_type=="WKS" else patch_required_sql_str.format("(Computer.ComputerLocation like '%Ap-%' or Computer.ComputerLocation like '%Cn-%' or Computer.ComputerLocation like '%Jp-%') or (computer.devicename like 'NLVAP%' or computer.devicename like 'NLVCN%' or computer.devicename like 'NLVJP%' or computer.devicename like 'ELVAP%' or computer.devicename like 'ELVCN%' or computer.devicename like 'ELVJP%' or computer.devicename like 'NLVSG%')",in21days_patch_list)
    #print(total_sql_asia_str)
    security_count_asia = _get_security_microsoft_patch_common_details(total_sql_asia_str, patch_required_sql_asia_str)

    #total data
    security_count_total = [x+y+z for x,y,z in zip(security_count_americas[:2],security_count_europe[:2],security_count_asia[:2])]

    #trend data
    today_required_patch_list = (security_count_americas[:2] + security_count_europe[:2] + security_count_asia[:2] + security_count_total)[1::2]
    trend_data_list = _compare_last_week_data(security_type, today_required_patch_list)

    security_patch_wks_result_list = security_count_americas[:2] + security_count_europe[:2] + security_count_asia[:2] + security_count_total[:2] + trend_data_list + security_count_americas[2:] + security_count_europe[2:] + security_count_asia[2:]
    return security_patch_wks_result_list
   
def _get_security_top5_microsoft_patches_common(top5_microsoft_patches_sql_str=None):
    result = []
    db_conn = _get_connection_security()
    if db_conn is None:
        return result
    cursor = db_conn.cursor()
    ncur_top5_microsoft_patches = cursor.execute(top5_microsoft_patches_sql_str)
    result = ncur_top5_microsoft_patches.fetchall()

    _close_connection(db_conn)
    return result

def _get_top5_microsoft_patches_zone(security_type=None, top5_microsoft_patches_sql_str=None,in21days_patch_list=None):
    ##americas TOP5
    top5_microsoft_patches_sql_americas_str = top5_microsoft_patches_sql_str.format("'LVNA','LVLA'",in21days_patch_list) if security_type=="WKS" else top5_microsoft_patches_sql_str.format("(Computer.ComputerLocation like '%Na-%' or Computer.ComputerLocation like '%La-%') or (computer.devicename like 'NLVNA%' or computer.devicename like 'NLVLA%')",in21days_patch_list)
    security_top5_microsoft_patches_wks_americas = _get_security_top5_microsoft_patches_common(top5_microsoft_patches_sql_americas_str)

    ## emea TOP5
    top5_microsoft_patches_sql_emea_str = top5_microsoft_patches_sql_str.format("'LVEU','LVCP'",in21days_patch_list) if security_type =="WKS" else top5_microsoft_patches_sql_str.format("(Computer.ComputerLocation like '%Cp-%' or Computer.ComputerLocation like '%Eu-%') or (computer.devicename like 'NLVCP%' or computer.devicename like 'ELVCP%' or computer.devicename like 'NLVEU%' or computer.devicename like 'ELVEU%')",in21days_patch_list)
    security_top5_microsoft_patches_wks_europe = _get_security_top5_microsoft_patches_common(top5_microsoft_patches_sql_emea_str)

    ## asia TOP5
    top5_microsoft_patches_sql_asia_str = top5_microsoft_patches_sql_str.format("'LVAP','LVCN','LVJP','LVSG'",in21days_patch_list) if security_type =="WKS" else top5_microsoft_patches_sql_str.format("(Computer.ComputerLocation like '%Ap-%' or Computer.ComputerLocation like '%Cn-%' or Computer.ComputerLocation like '%Jp-%') or (computer.devicename like 'NLVAP%' or computer.devicename like 'NLVCN%' or computer.devicename like 'NLVJP%' or computer.devicename like 'ELVAP%' or computer.devicename like 'ELVCN%' or computer.devicename like 'ELVJP%' or computer.devicename like 'NLVSG%')",in21days_patch_list)
    security_top5_microsoft_patches_wks_asia = _get_security_top5_microsoft_patches_common(top5_microsoft_patches_sql_asia_str)

    security_top5_microsoft_patches_wks_list = {'americas':security_top5_microsoft_patches_wks_americas,'emea':security_top5_microsoft_patches_wks_europe,'asia':security_top5_microsoft_patches_wks_asia}
    return security_top5_microsoft_patches_wks_list

def _get_security_details(security_type=None, region=None, security_details_str=None,in21days_patch_list=None):
    security_details = []
    if region == "Americas":
        security_details_americas_str = security_details_str.format("Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Na-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/La-%Workstation%' or Computer.DisplayName like 'LVNA%'  or Computer.DisplayName like 'LVLA%'",in21days_patch_list) if security_type =="WKS" else security_details_str.format("(Computer.ComputerLocation like '%Na-%' or Computer.ComputerLocation like '%La-%') or (computer.devicename like 'NLVNA%' or computer.devicename like 'NLVLA%')",in21days_patch_list)
        security_details = _get_security_details_per_zone(security_details_americas_str)
    elif region == "EMEA":
        security_details_emea_str= security_details_str.format("Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Cp-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Eu-%Workstation%' or Computer.DisplayName like 'LVEU%'  or Computer.DisplayName like 'LVCP%'",in21days_patch_list) if security_type =="WKS" else security_details_str.format("(Computer.ComputerLocation like '%Cp-%' or Computer.ComputerLocation like '%Eu-%') or (computer.devicename like 'NLVCP%' or computer.devicename like 'ELVCP%' or computer.devicename like 'NLVEU%' or computer.devicename like 'ELVEU%')",in21days_patch_list)
        security_details = _get_security_details_per_zone(security_details_emea_str)
    elif region == "Asia":
        security_details_asia_str= security_details_str.format("Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Ap-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Cn-%Workstation%' or Computer.ComputerLocation like 'D1.cougar.ms.lvmh/Jp-%Workstation%' or Computer.DisplayName like 'LVAP%' or Computer.DisplayName like 'LVCN%'  or Computer.DisplayName like 'LVJP%' or Computer.DisplayName like 'LVSG%'",in21days_patch_list) if security_type =="WKS" else security_details_str.format("(Computer.ComputerLocation like '%Ap-%' or Computer.ComputerLocation like '%Cn-%' or Computer.ComputerLocation like '%Jp-%') or (computer.devicename like 'NLVAP%' or computer.devicename like 'NLVCN%' or computer.devicename like 'NLVJP%' or computer.devicename like 'ELVAP%' or computer.devicename like 'ELVCN%' or computer.devicename like 'ELVJP%' or computer.devicename like 'NLVSG%')",in21days_patch_list)
        security_details = _get_security_details_per_zone(security_details_asia_str)
    return security_details
    
def _get_security_details_per_zone(security_details_str=None):
    security_details_zone = []
    db_conn = _get_connection_security()
    if db_conn is None:
        return security_details_zone
    cursor = db_conn.cursor()
    ncur = cursor.execute(security_details_str)
    security_details_zone = [ list(n) for n in ncur.fetchall()]
    _close_connection(db_conn)
    return security_details_zone
    

def _save_last_hour_data_in_txt():
    security_type_wks = "WKS"
    security_type_srv = "SRV"
    
    patch_conn = _get_patch_connection()
    if patch_conn is None:
        return None
    patch_cursor = patch_conn.cursor()
    patch_cursor.execute("select campaign_name, campaign_startdate, srv_patch_list, wks_patch_list from patchcampaigns")
    patch_data = patch_cursor.fetchall()
    _close_connection(patch_conn)
    
    default_keys = ["campaign_name","campaign_startdate","srv_patch_list","wks_patch_list"]
    
    srv_in21days_patch_list = []
    wks_in21days_patch_list = []
    for patch in patch_data:
        patch = dict(zip(default_keys, patch))
        #print patch['srv_patch_list']
        patch_date = patch['campaign_startdate']
        in21days_from_today = (datetime.datetime.now()-datetime.timedelta(days=26)).date()
        
        if patch_date < in21days_from_today:
            srv_patch_list = json.loads(patch['srv_patch_list'])
            wks_patch_list = json.loads(patch['wks_patch_list'])
            # include only active patches
            srv_in21days_patch_list.extend([x for x in srv_patch_list.keys() if srv_patch_list[x]['status']])
            wks_in21days_patch_list.extend([x for x in wks_patch_list.keys() if wks_patch_list[x]['status']])
    #print "=== PATCH :: srv_in21days_patch_list:%s"% srv_in21days_patch_list
    #print "=== PATCH :: wks_in21days_patch_list:%s"% wks_in21days_patch_list
    
    #remove duplicates in the 21 days patch list
    srv_in21days_patch_list = "','".join(set(p for p in srv_in21days_patch_list))
    wks_in21days_patch_list = "','".join(set(p for p in wks_in21days_patch_list))
    
    
    project_upload_path = settings.UPLOAD_FOLDER
    '''
    srv_search_filter_json = os.path.join(settings.UPLOAD_FOLDER,"securities/histories/search_filter_list_srv.json")
=======

    app_path = current_app.config.get('APP_DIR')
    srv_search_filter_json = app_path + current_app.config.get('SEC_SRV_SEARCH_FILTER_LIST'))
>>>>>>> refs/remotes/origin/backend_revamp
    with open(srv_search_filter_json,"r") as srv_search_filter_file:
        srv_search_filter_list = json.load(srv_search_filter_file)
    srv_in21days_patch_list = []
    for v in srv_search_filter_list.values():
        patch_date = datetime.datetime.strptime(v['patchReleaseDate'],'%m/%d/%Y').date()
        in21days_from_today = (datetime.datetime.now()-datetime.timedelta(days=26)).date()
        if patch_date < in21days_from_today:
            srv_in21days_patch_list.extend(v['patchList'].splitlines())
    print "srv_in21days_patch_list:%s"% srv_in21days_patch_list
    srv_in21days_patch_list = "','".join(p for p in srv_in21days_patch_list)

<<<<<<< HEAD
    wks_search_filter_json = os.path.join(settings.UPLOAD_FOLDER,"securities/histories/search_filter_list_wks.json")
=======
    wks_search_filter_json = app_path + current_app.config.get('SEC_WKS_SEARCH_FILTER_LIST')
>>>>>>> refs/remotes/origin/backend_revamp
    with open(wks_search_filter_json,"r") as wks_search_filter_file:
        wks_search_filter_list = json.load(wks_search_filter_file)
    wks_in21days_patch_list = []
    for v in wks_search_filter_list.values():
        patch_date = datetime.datetime.strptime(v['patchReleaseDate'],'%m/%d/%Y').date()
        in21days_from_today = (datetime.datetime.now()-datetime.timedelta(days=26)).date()
        if patch_date < in21days_from_today:
            wks_in21days_patch_list.extend(v['patchList'].splitlines())
    print "wks_in21days_patch_list:%s"% wks_in21days_patch_list
    wks_in21days_patch_list = "','".join(p for p in wks_in21days_patch_list)
    '''
    ##workstation sql query
    total_wks_str = """
        select
             DeviceName, ComputerLocation
        from Computer (nolock) 
        where 
              {0}
              and (Computer.Type Not Like '%Server%')
              and datediff(hour,Computer.recorddate,getdate())>12
    """
    patch_required_wks_str = """
        select distinct Computer.DisplayName, Computer.ComputerLocation
        from Computer (nolock) 
        left join CVDetected on CVDetected.Computer_Idn=Computer.Computer_Idn
        left join Vulnerability on CVDetected.Vulnerability_Idn=Vulnerability.Vulnerability_Idn
        where 
         (
         Computer.Computer_Idn in 
            (
               (select
                  Computer.Computer_Idn 
               from
                  Computer (nolock)
                        left join ComputerVulnerability (nolock) on ComputerVulnerability.Computer_Idn=Computer.Computer_Idn 
                              and ComputerVulnerability.PatchInstallSucceeded=2
                              and ComputerVulnerability.PatchDetected<>1
                  where 
                     ComputerVulnerability.Computer_Idn is not NULL
                     and ComputerVulnerability.Vul_ID in ('{1}')
               )
               UNION
               (select CVDetected.Computer_Idn 
               from CVDetected (nolock) 
                  left join Computer (nolock) on Computer.Computer_Idn = CVDetected.Computer_Idn 
                  left join Vulnerability (nolock) on Vulnerability.Vulnerability_Idn=CVDetected.Vulnerability_Idn 
               where 
                  Computer.Computer_Idn is not NULL and Vulnerability.Vulnerability_Idn is not NULL
                  and Vulnerability.Vul_ID in ('{1}')
                  and SupercededState<>2
               group by    CVDetected.Computer_Idn
               ) 
               UNION
               (select Computer.Computer_Idn 
               from Computer (nolock) ,
                  Vulnerability2 inner join Vulnerability on Vulnerability2.Vulnerability_Idn=Vulnerability.Vulnerability_Idn
               where 
                  charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  ) >0
                  and ((charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  )-1) % 4 )=0
                  and Vulnerability.Vul_ID in ('{1}')
                  and SupercededState<>2
               group by    Computer.Computer_Idn
               )
            )
         )
         and ({0})
         and (Computer.Type Not Like '%Server%')
         and datediff(hour,Computer.recorddate,getdate())>12
    """
    #top5_microsoft_patches_wks_str = "select top 5 Vul_ID, count(*) from ( select Vulnerability.Vul_ID,Computer.DeviceName from CVDetected left join Vulnerability on CVDetected.Vulnerability_Idn=Vulnerability.Vulnerability_Idn left join Computer on Computer.Computer_Idn = CVDetected.Computer_Idn left join Patch on Patch.UniqueFileName = CVDetected.Patch left outer join OSNT on Computer.computer_idn = OSNT.computer_idn where Vulnerability.vendor = 'Microsoft' and Vulnerability.severity = 1 and Vulnerability.PublishDate<dateadd(day,-28,getdate()) and Computer.Computer_Idn is not NULL and ({0}) and OSNT.Server='No' and Patch.SupercededByVulID is NULL and Patch.Patch_Idn is not NULL group by Vulnerability.Vul_ID,Computer.DeviceName) T1 group by Vul_ID order by count(*) desc"
    top5_microsoft_patches_wks_str = """
        select top 5 T1.Vul_ID as Vulnerability, count(Computer.DisplayName) as [WKS Impacted]
        from Computer (nolock) ,  
            (
               (select
                  Computer.Computer_Idn, ComputerVulnerability.Vul_ID as Vul_ID
               from
                  Computer (nolock)
                        left join ComputerVulnerability (nolock) on ComputerVulnerability.Computer_Idn=Computer.Computer_Idn 
                              and ComputerVulnerability.PatchInstallSucceeded=2
                              and ComputerVulnerability.PatchDetected<>1
                  where 
                     ComputerVulnerability.Computer_Idn is not NULL
                     and ComputerVulnerability.Vul_ID in ('{1}')
               )
               UNION
               (select CVDetected.Computer_Idn , Vulnerability.Vul_ID as Vul_ID
               from CVDetected (nolock) 
                  left join Computer (nolock) on Computer.Computer_Idn = CVDetected.Computer_Idn 
                  left join Vulnerability (nolock) on Vulnerability.Vulnerability_Idn=CVDetected.Vulnerability_Idn 
               where 
                  Computer.Computer_Idn is not NULL and Vulnerability.Vulnerability_Idn is not NULL
                  and Vulnerability.Vul_ID in ('{1}')
                  and SupercededState<>2
               group by    CVDetected.Computer_Idn, Vulnerability.Vul_ID
               ) 
               UNION
               (select Computer.Computer_Idn , Vulnerability.Vul_ID as Vul_ID
               from Computer (nolock) ,
                  Vulnerability2 inner join Vulnerability on Vulnerability2.Vulnerability_Idn=Vulnerability.Vulnerability_Idn
               where 
                  charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  ) >0
                  and ((charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  )-1) % 4 )=0
                  and Vulnerability.Vul_ID in ('{1}')
                  and SupercededState<>2
               group by    Computer.Computer_Idn, Vulnerability.Vul_ID
               )
            ) T1
        where
        Computer.Computer_Idn = T1.Computer_Idn
        and  Left(Computer.DisplayName,4) in ({0})
        and  (Computer.Type Not Like '%Server%')
        and datediff(hour,Computer.recorddate,getdate())>12
        group by  T1.Vul_ID
        order by  count(Computer.DisplayName) desc
    """

    security_details_wks_str = """
       select distinct DeviceName,Type,PatchInstallSucceeded,NumInstallTries,Vul_ID,PublishDate,Vendor,Patch,Expected,[Found],DateDetected ,ComputerLocation
        From 
        (
            (select
            Computer.DeviceName,Computer.Type,
            Convert(varchar(10),ComputerVulnerability.PatchInstallSucceeded) as PatchInstallSucceeded,
            'unknown' as NumInstallTries,
            ComputerVulnerability.Vul_ID as Vul_ID,
            'unknown' as PublishDate,
            'unknown' as Vendor,
            ComputerVulnerability.Patch as Patch,
            ComputerVulnerability.Expected as Expected,
            ComputerVulnerability.Found as Found ,
            Convert(varchar(30),ComputerVulnerability.DateDetected) as DateDetected ,
            Computer.ComputerLocation
            from
                Computer (nolock)
            inner join ComputerVulnerability (nolock) on ComputerVulnerability.Computer_Idn=Computer.Computer_Idn
            and ComputerVulnerability.PatchInstallSucceeded=2
            and ComputerVulnerability.PatchDetected<>1
            where
                ComputerVulnerability.Computer_Idn is not NULL
                and ComputerVulnerability.Vul_ID in ('{1}')
            and ({0})
            and (Computer.Type Not Like '%Server%')
            and datediff(hour,Computer.recorddate,getdate())>12
                   )
                   UNION
                   (select 
            Computer.DeviceName,
            Computer.Type,
            convert(varchar(10),CVDetected.PatchInstallSucceeded) as PatchInstallSucceeded,
            convert(varchar(10),CVDetected.NumInstallTries)  as NumInstallTries,
            Vulnerability.Vul_ID as Vul_ID,
            convert(varchar(20),Vulnerability.PublishDate) as PublishDate,
            Vulnerability.Vendor as Vendor,
            CVDetected.Patch  as Patch,
            CVDetected.Expected  as Expected,
            CVDetected.Found  as Found ,
            Convert(varchar(30),CVDetected.DateDetected)  DateDetected ,
            Computer.ComputerLocation
                   from CVDetected (nolock)
                      inner join Computer (nolock) on Computer.Computer_Idn = CVDetected.Computer_Idn
                      inner join Vulnerability (nolock) on Vulnerability.Vulnerability_Idn=CVDetected.Vulnerability_Idn
                   where
                      Computer.Computer_Idn is not NULL and Vulnerability.Vulnerability_Idn is not NULL
                      and Vulnerability.Vul_ID in ('{1}')
                      and SupercededState<>2
            and ({0})
            and (Computer.Type Not Like '%Server%')
            and datediff(hour,Computer.recorddate,getdate())>12
                      --and CVDetected.Computer_idn = 30849
                   )
                   UNION
                   (select 
            Computer.DeviceName,Computer.Type,
            'not_scanned' as PatchInstallSucceeded,
            'not_scanned' as NumInstallTries,
            Vulnerability.Vul_ID as Vul_ID,
            convert(varchar(20),Vulnerability.PublishDate) as PublishDate,
            'not_scanned' as Vendor,
            'not_scanned' as Patch,
            'not_scanned' as Expected,
            'not_scanned' as Found ,
            'not_scanned' as DateDetected ,
            Computer.ComputerLocation
                   from Computer (nolock) ,
                      Vulnerability2 inner join Vulnerability on Vulnerability2.Vulnerability_Idn=Vulnerability.Vulnerability_Idn
                   where
                      charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  ) >0
                      and ((charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  )-1) % 4 )=0
                      and Vulnerability.Vul_ID in ('{1}')
                      and SupercededState<>2
            and ({0})
            and (Computer.Type Not Like '%Server%')
            and datediff(hour,Computer.recorddate,getdate())>12
                   )
        ) T1
    """

    security_details_summary_wks_str = """
        select distinct Details.DeviceName, Details.Type, Details.ComputerLocation
    from(
        select distinct DeviceName,Type,PatchInstallSucceeded,NumInstallTries,Vul_ID,PublishDate,Vendor,Patch,Expected,[Found],DateDetected ,ComputerLocation
        From 
        (
            (select
            Computer.DeviceName,Computer.Type,
            Convert(varchar(10),ComputerVulnerability.PatchInstallSucceeded) as PatchInstallSucceeded,
            'unknown' as NumInstallTries,
            ComputerVulnerability.Vul_ID as Vul_ID,
            'unknown' as PublishDate,
            'unknown' as Vendor,
            ComputerVulnerability.Patch as Patch,
            ComputerVulnerability.Expected as Expected,
            ComputerVulnerability.Found as Found ,
            Convert(varchar(30),ComputerVulnerability.DateDetected) as DateDetected ,
            Computer.ComputerLocation
            from
                Computer (nolock)
            inner join ComputerVulnerability (nolock) on ComputerVulnerability.Computer_Idn=Computer.Computer_Idn
            and ComputerVulnerability.PatchInstallSucceeded=2
            and ComputerVulnerability.PatchDetected<>1
            where
                ComputerVulnerability.Computer_Idn is not NULL
                and ComputerVulnerability.Vul_ID in ('{1}')
            and ({0})
            and (Computer.Type Not Like '%Server%')
            and datediff(hour,Computer.recorddate,getdate())>12
                   )
                   UNION
                   (select 
            Computer.DeviceName,
            Computer.Type,
            convert(varchar(10),CVDetected.PatchInstallSucceeded) as PatchInstallSucceeded,
            convert(varchar(10),CVDetected.NumInstallTries)  as NumInstallTries,
            Vulnerability.Vul_ID as Vul_ID,
            convert(varchar(20),Vulnerability.PublishDate) as PublishDate,
            Vulnerability.Vendor as Vendor,
            CVDetected.Patch  as Patch,
            CVDetected.Expected  as Expected,
            CVDetected.Found  as Found ,
            Convert(varchar(30),CVDetected.DateDetected)  DateDetected ,
            Computer.ComputerLocation
                   from CVDetected (nolock)
                      inner join Computer (nolock) on Computer.Computer_Idn = CVDetected.Computer_Idn
                      inner join Vulnerability (nolock) on Vulnerability.Vulnerability_Idn=CVDetected.Vulnerability_Idn
                   where
                      Computer.Computer_Idn is not NULL and Vulnerability.Vulnerability_Idn is not NULL
                      and Vulnerability.Vul_ID in ('{1}')
                      and SupercededState<>2
            and ({0})
            and (Computer.Type Not Like '%Server%')
            and datediff(hour,Computer.recorddate,getdate())>12
                      --and CVDetected.Computer_idn = 30849
                   )
                   UNION
                   (select 
            Computer.DeviceName,Computer.Type,
            'not_scanned' as PatchInstallSucceeded,
            'not_scanned' as NumInstallTries,
            Vulnerability.Vul_ID as Vul_ID,
            convert(varchar(20),Vulnerability.PublishDate) as PublishDate,
            'not_scanned' as Vendor,
            'not_scanned' as Patch,
            'not_scanned' as Expected,
            'not_scanned' as Found ,
            'not_scanned' as DateDetected ,
            Computer.ComputerLocation
                   from Computer (nolock) ,
                      Vulnerability2 inner join Vulnerability on Vulnerability2.Vulnerability_Idn=Vulnerability.Vulnerability_Idn
                   where
                      charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  ) >0
                      and ((charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  )-1) % 4 )=0
                      and Vulnerability.Vul_ID in ('{1}')
                      and SupercededState<>2
            and ({0})
            and (Computer.Type Not Like '%Server%')
            and datediff(hour,Computer.recorddate,getdate())>12
                   )
        ) T1
    ) Details
    """

    ##server sql query
    total_srv_str = """
        Select
            distinct (DeviceName), ComputerLocation
        from Computer (nolock) 
        where 
            Upper(Right(Computer.DeviceName,1)) in ('P','B')
            and (Computer.Type Like '%Server%' or Computer.Type Like '%Virtual Machine%')
            and datediff(hour,Computer.recorddate,getdate())>12
            and ({0})
    """
    patch_required_srv_str = """
        select distinct Computer.DeviceName, Computer.ComputerLocation 
        From Computer (nolock) 
        left join CVDetected on CVDetected.Computer_Idn=Computer.Computer_Idn
        left join Vulnerability on CVDetected.Vulnerability_Idn=Vulnerability.Vulnerability_Idn
        Where 
         (
         Computer.Computer_Idn in 
            (
               (select
                  Computer.Computer_Idn 
               from
                  Computer (nolock)
                        inner join ComputerVulnerability (nolock) on ComputerVulnerability.Computer_Idn=Computer.Computer_Idn 
                              and ComputerVulnerability.PatchInstallSucceeded=2
                              and ComputerVulnerability.PatchDetected<>1
                  where 
                     ComputerVulnerability.Computer_Idn is not NULL
                     and ComputerVulnerability.Vul_ID in ('{1}')
               )
               UNION
               (select CVDetected.Computer_Idn 
               from CVDetected (nolock) 
                  inner join Computer (nolock) on Computer.Computer_Idn = CVDetected.Computer_Idn 
                  inner join Vulnerability (nolock) on Vulnerability.Vulnerability_Idn=CVDetected.Vulnerability_Idn 
               where 
                  Computer.Computer_Idn is not NULL and Vulnerability.Vulnerability_Idn is not NULL
                  and Vulnerability.Vul_ID in ('{1}')
                  and SupercededState<>2
               group by    CVDetected.Computer_Idn
               ) 
               UNION
               (select Computer.Computer_Idn 
               from Computer (nolock) ,
                  Vulnerability2 inner join Vulnerability on Vulnerability2.Vulnerability_Idn=Vulnerability.Vulnerability_Idn
               where 
                  charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  ) >0
                  and ((charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  )-1) % 4 )=0
                  and Vulnerability.Vul_ID in ('{1}')
                  and SupercededState<>2
               group by    Computer.Computer_Idn
               )
            )
         )
        and Upper(Right(Computer.DisplayName,1)) in ('P','B')
        and (Computer.Type Like '%Server%' or Computer.Type Like '%Virtual Machine%')
        and datediff(hour,Computer.recorddate,getdate())>12
        and ({0})
    """
    top5_microsoft_patches_srv_str = """
        select top 5 T1.Vul_ID as Vulnerability, count(Computer.DisplayName) as [SRV Impacted]
        From Computer (nolock) ,      
            (
               (select
                  Computer.Computer_Idn, ComputerVulnerability.Vul_ID as Vul_ID
               from
                  Computer (nolock)
                        left join ComputerVulnerability (nolock) on ComputerVulnerability.Computer_Idn=Computer.Computer_Idn 
                              and ComputerVulnerability.PatchInstallSucceeded=2
                              and ComputerVulnerability.PatchDetected<>1
                  where 
                     ComputerVulnerability.Computer_Idn is not NULL
                     and ComputerVulnerability.Vul_ID in ('{1}')
               )
               UNION
               (select CVDetected.Computer_Idn , Vulnerability.Vul_ID as Vul_ID
               from CVDetected (nolock) 
                  left join Computer (nolock) on Computer.Computer_Idn = CVDetected.Computer_Idn 
                  left join Vulnerability (nolock) on Vulnerability.Vulnerability_Idn=CVDetected.Vulnerability_Idn 
               where 
                  Computer.Computer_Idn is not NULL and Vulnerability.Vulnerability_Idn is not NULL
                  and Vulnerability.Vul_ID in ('{1}')
                  and SupercededState<>2
               group by    CVDetected.Computer_Idn, Vulnerability.Vul_ID
               ) 
               UNION
               (select Computer.Computer_Idn , Vulnerability.Vul_ID as Vul_ID
               from Computer (nolock) ,
                  Vulnerability2 inner join Vulnerability on Vulnerability2.Vulnerability_Idn=Vulnerability.Vulnerability_Idn
               where 
                  charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  ) >0
                  and ((charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  )-1) % 4 )=0
                  and Vulnerability.Vul_ID in ('{1}')
                  and SupercededState<>2
               group by    Computer.Computer_Idn, Vulnerability.Vul_ID
               )
            ) T1
        where
            Computer.Computer_Idn = T1.Computer_Idn
            and Upper(Right(Computer.DisplayName,1)) in ('P','B')
            and (Computer.Type Like '%Server%' or Computer.Type Like '%Virtual Machine%')
            and datediff(hour,Computer.recorddate,getdate())>12
            and ({0})
            group by T1.Vul_ID
            order by count(Computer.DisplayName) desc
    """
    security_details_srv_str = """
        select distinct 
            Computer.DeviceName,Computer.Type, 
            case when CVDetected.PatchInstallSucceeded is NULL then ComputerVulnerability.PatchInstallSucceeded else CVDetected.PatchInstallSucceeded end as PatchInstallSucceeded,
            case when CVDetected.NumInstallTries is NULL then 'unknown' else convert(varchar(10),CVDetected.NumInstallTries)  end NumInstallTries, 
            case when Vulnerability.Vul_ID is NULL then ComputerVulnerability.Vul_ID else Vulnerability.Vul_ID end Vul_ID, 
            case when Vulnerability.PublishDate is NULL then 'unknown' else convert(varchar(20),Vulnerability.PublishDate) end PublishDate, 
            case when Vulnerability.Vendor is NULL then 'unknown' else Vulnerability.Vendor end Vendor, 
            case when CVDetected.Patch is NULL then ComputerVulnerability.Patch else CVDetected.Patch  end Patch, 
            case when CVDetected.Expected is NULL then ComputerVulnerability.Expected else CVDetected.Expected  end Expected, 
            case when CVDetected.Found is NULL then ComputerVulnerability.Found else CVDetected.Found  end Found , 
            case when CVDetected.DateDetected is NULL then ComputerVulnerability.DateDetected else CVDetected.DateDetected  end DateDetected , 
            Computer.ComputerLocation
        From Computer (nolock)
            left join CVDetected on CVDetected.Computer_Idn=Computer.Computer_Idn
            left join ComputerVulnerability (nolock) on ComputerVulnerability.Computer_Idn=Computer.Computer_Idn
            left join Vulnerability on CVDetected.Vulnerability_Idn=Vulnerability.Vulnerability_Idn
        Where
        (
            Computer.Computer_Idn in
            (
               (select
                  Computer.Computer_Idn
               from
                  Computer (nolock)
                        inner join ComputerVulnerability (nolock) on ComputerVulnerability.Computer_Idn=Computer.Computer_Idn
                              and ComputerVulnerability.PatchInstallSucceeded=2
                              and ComputerVulnerability.PatchDetected<>1
                  where
                     ComputerVulnerability.Computer_Idn is not NULL
                     and ComputerVulnerability.Vul_ID in ('{1}')
               )
               UNION
               (select CVDetected.Computer_Idn
               from CVDetected (nolock)
                  inner join Computer (nolock) on Computer.Computer_Idn = CVDetected.Computer_Idn
                  inner join Vulnerability (nolock) on Vulnerability.Vulnerability_Idn=CVDetected.Vulnerability_Idn
               where
                  Computer.Computer_Idn is not NULL and Vulnerability.Vulnerability_Idn is not NULL
                  and Vulnerability.Vul_ID in ('{1}')
                  and SupercededState<>2
               group by    CVDetected.Computer_Idn
               )
               UNION
               (select Computer.Computer_Idn
               from Computer (nolock) ,
                  Vulnerability2 inner join Vulnerability on Vulnerability2.Vulnerability_Idn=Vulnerability.Vulnerability_Idn
               where
                  charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  ) >0
                  and ((charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  )-1) % 4 )=0
                  and Vulnerability.Vul_ID in ('{1}')
                  and SupercededState<>2
               group by    Computer.Computer_Idn
               )
            )
        )
        and Upper(Right(Computer.DisplayName,1)) in ('P','B')
        and (Computer.Type Like '%Server%' or Computer.Type Like '%Virtual Machine%')
        and datediff(hour,Computer.recorddate,getdate())>12
        and ({0})
        and (Vulnerability.Vul_ID in ('{1}') or ComputerVulnerability.Vul_ID in ('{1}') )
    """

    security_details_summary_srv_str = """
        select distinct Details.DeviceName, Details.Type, Details.ComputerLocation
        from (
            select distinct 
                Computer.DeviceName,Computer.Type,
                case when CVDetected.PatchInstallSucceeded is NULL then ComputerVulnerability.PatchInstallSucceeded else CVDetected.PatchInstallSucceeded end as PatchInstallSucceeded,
                case when CVDetected.NumInstallTries is NULL then 'unknown' else convert(varchar(10),CVDetected.NumInstallTries)  end NumInstallTries,
                case when Vulnerability.Vul_ID is NULL then ComputerVulnerability.Vul_ID else Vulnerability.Vul_ID end Vul_ID,
                case when Vulnerability.PublishDate is NULL then 'unknown' else convert(varchar(20),Vulnerability.PublishDate) end PublishDate,
                case when Vulnerability.Vendor is NULL then 'unknown' else Vulnerability.Vendor end Vendor,
                case when CVDetected.Patch is NULL then ComputerVulnerability.Patch else CVDetected.Patch  end Patch,
                case when CVDetected.Expected is NULL then ComputerVulnerability.Expected else CVDetected.Expected  end Expected,
                case when CVDetected.Found is NULL then ComputerVulnerability.Found else CVDetected.Found  end Found ,
                case when CVDetected.DateDetected is NULL then ComputerVulnerability.DateDetected else CVDetected.DateDetected  end DateDetected ,
                Computer.ComputerLocation 
            From Computer (nolock)
                left join CVDetected on CVDetected.Computer_Idn=Computer.Computer_Idn
                left join ComputerVulnerability (nolock) on ComputerVulnerability.Computer_Idn=Computer.Computer_Idn
                left join Vulnerability on CVDetected.Vulnerability_Idn=Vulnerability.Vulnerability_Idn
            Where
            (
                Computer.Computer_Idn in
                (
                   (select
                      Computer.Computer_Idn
                   from
                      Computer (nolock)
                            inner join ComputerVulnerability (nolock) on ComputerVulnerability.Computer_Idn=Computer.Computer_Idn
                                  and ComputerVulnerability.PatchInstallSucceeded=2
                                  and ComputerVulnerability.PatchDetected<>1
                      where
                         ComputerVulnerability.Computer_Idn is not NULL
                         and ComputerVulnerability.Vul_ID in ('{1}')
                   )
                   UNION
                   (select CVDetected.Computer_Idn
                   from CVDetected (nolock)
                      inner join Computer (nolock) on Computer.Computer_Idn = CVDetected.Computer_Idn
                      inner join Vulnerability (nolock) on Vulnerability.Vulnerability_Idn=CVDetected.Vulnerability_Idn
                   where
                      Computer.Computer_Idn is not NULL and Vulnerability.Vulnerability_Idn is not NULL
                      and Vulnerability.Vul_ID in ('{1}')
                      and SupercededState<>2
                   group by    CVDetected.Computer_Idn
                   )
                   UNION
                   (select Computer.Computer_Idn
                   from Computer (nolock) ,
                      Vulnerability2 inner join Vulnerability on Vulnerability2.Vulnerability_Idn=Vulnerability.Vulnerability_Idn
                   where
                      charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  ) >0
                      and ((charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  )-1) % 4 )=0
                      and Vulnerability.Vul_ID in ('{1}')
                      and SupercededState<>2
                   group by    Computer.Computer_Idn
                   )
                )
            )
            and Upper(Right(Computer.DisplayName,1)) in ('P','B')
            and (Computer.Type Like '%Server%' or Computer.Type Like '%Virtual Machine%')
            and datediff(hour,Computer.recorddate,getdate())>12
            and ({0})
            and (Vulnerability.Vul_ID in ('{1}') or ComputerVulnerability.Vul_ID in ('{1}') )
        ) Details
    """


    ##server sql query
    total_srv_str_nonprd = """
        Select
            distinct (DeviceName), ComputerLocation
        from Computer (nolock)
        where
            Upper(Right(Computer.DeviceName,1)) not in ('P','B')
            and (Computer.Type Like '%Server%' or Computer.Type Like '%Virtual Machine%')
            and datediff(hour,Computer.recorddate,getdate())>12
            and ({0})
    """
    patch_required_srv_str_nonprd = """
        select distinct Computer.DeviceName, Computer.ComputerLocation 
        From Computer (nolock) 
            left join CVDetected on CVDetected.Computer_Idn=Computer.Computer_Idn
            left join Vulnerability on CVDetected.Vulnerability_Idn=Vulnerability.Vulnerability_Idn
        Where 
         (
         Computer.Computer_Idn in 
            (
               (select
                  Computer.Computer_Idn 
               from
                  Computer (nolock)
                        inner join ComputerVulnerability (nolock) on ComputerVulnerability.Computer_Idn=Computer.Computer_Idn 
                              and ComputerVulnerability.PatchInstallSucceeded=2
                              and ComputerVulnerability.PatchDetected<>1
                  where 
                     ComputerVulnerability.Computer_Idn is not NULL
                     and ComputerVulnerability.Vul_ID in ('{1}')
               )
               UNION
               (select CVDetected.Computer_Idn 
               from CVDetected (nolock) 
                  inner join Computer (nolock) on Computer.Computer_Idn = CVDetected.Computer_Idn 
                  inner join Vulnerability (nolock) on Vulnerability.Vulnerability_Idn=CVDetected.Vulnerability_Idn 
               where 
                  Computer.Computer_Idn is not NULL and Vulnerability.Vulnerability_Idn is not NULL
                  and Vulnerability.Vul_ID in ('{1}')
                  and SupercededState<>2
               group by    CVDetected.Computer_Idn
               ) 
               UNION
               (select Computer.Computer_Idn 
               from Computer (nolock) ,
                  Vulnerability2 inner join Vulnerability on Vulnerability2.Vulnerability_Idn=Vulnerability.Vulnerability_Idn
               where 
                  charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  ) >0
                  and ((charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  )-1) % 4 )=0
                  and Vulnerability.Vul_ID in ('{1}')
                  and SupercededState<>2
               group by    Computer.Computer_Idn
               )
            )
         )
        and Upper(Right(Computer.DisplayName,1)) not in ('P','B')
        and (Computer.Type Like '%Server%' or Computer.Type Like '%Virtual Machine%')
        and datediff(hour,Computer.recorddate,getdate())>12
        and ({0})
    """
    top5_microsoft_patches_srv_str_nonprd = """
        select top 5 T1.Vul_ID as Vulnerability, count(Computer.DisplayName) as [SRV Impacted]
        From Computer (nolock) ,      
            (
               (select
                  Computer.Computer_Idn, ComputerVulnerability.Vul_ID as Vul_ID
               from
                  Computer (nolock)
                        left join ComputerVulnerability (nolock) on ComputerVulnerability.Computer_Idn=Computer.Computer_Idn 
                              and ComputerVulnerability.PatchInstallSucceeded=2
                              and ComputerVulnerability.PatchDetected<>1
                  where 
                     ComputerVulnerability.Computer_Idn is not NULL
                     and ComputerVulnerability.Vul_ID in ('{1}')
               )
               UNION
               (select CVDetected.Computer_Idn , Vulnerability.Vul_ID as Vul_ID
               from CVDetected (nolock) 
                  left join Computer (nolock) on Computer.Computer_Idn = CVDetected.Computer_Idn 
                  left join Vulnerability (nolock) on Vulnerability.Vulnerability_Idn=CVDetected.Vulnerability_Idn 
               where 
                  Computer.Computer_Idn is not NULL and Vulnerability.Vulnerability_Idn is not NULL
                  and Vulnerability.Vul_ID in ('{1}')
                  and SupercededState<>2
               group by    CVDetected.Computer_Idn, Vulnerability.Vul_ID
               ) 
               UNION
               (select Computer.Computer_Idn , Vulnerability.Vul_ID as Vul_ID
               from Computer (nolock) ,
                  Vulnerability2 inner join Vulnerability on Vulnerability2.Vulnerability_Idn=Vulnerability.Vulnerability_Idn
               where 
                  charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  ) >0
                  and ((charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  )-1) % 4 )=0
                  and Vulnerability.Vul_ID in ('{1}')
                  and SupercededState<>2
               group by    Computer.Computer_Idn, Vulnerability.Vul_ID
               )
            ) T1
        where
            Computer.Computer_Idn = T1.Computer_Idn
            and Upper(Right(Computer.DisplayName,1)) not in ('P','B')
            and (Computer.Type Like '%Server%' or Computer.Type Like '%Virtual Machine%')
            and datediff(hour,Computer.recorddate,getdate())>12
            and ({0})
            group by T1.Vul_ID
            order by count(Computer.DisplayName) desc

    """
    security_details_srv_str_nonprd = """
        select distinct 
            Computer.DeviceName,Computer.Type,
            case when CVDetected.PatchInstallSucceeded is NULL then ComputerVulnerability.PatchInstallSucceeded else CVDetected.PatchInstallSucceeded end as PatchInstallSucceeded,
            case when CVDetected.NumInstallTries is NULL then 'unknown' else convert(varchar(10),CVDetected.NumInstallTries)  end NumInstallTries,
            case when Vulnerability.Vul_ID is NULL then ComputerVulnerability.Vul_ID else Vulnerability.Vul_ID end Vul_ID,
            case when Vulnerability.PublishDate is NULL then 'unknown' else convert(varchar(20),Vulnerability.PublishDate) end PublishDate,
            case when Vulnerability.Vendor is NULL then 'unknown' else Vulnerability.Vendor end Vendor,
            case when CVDetected.Patch is NULL then ComputerVulnerability.Patch else CVDetected.Patch  end Patch,
            case when CVDetected.Expected is NULL then ComputerVulnerability.Expected else CVDetected.Expected  end Expected,
            case when CVDetected.Found is NULL then ComputerVulnerability.Found else CVDetected.Found  end Found ,
            case when CVDetected.DateDetected is NULL then ComputerVulnerability.DateDetected else CVDetected.DateDetected  end DateDetected ,
            Computer.ComputerLocation
        From Computer (nolock)
            left join CVDetected on CVDetected.Computer_Idn=Computer.Computer_Idn
            left join ComputerVulnerability (nolock) on ComputerVulnerability.Computer_Idn=Computer.Computer_Idn
            left join Vulnerability on CVDetected.Vulnerability_Idn=Vulnerability.Vulnerability_Idn
        Where
        (
            Computer.Computer_Idn in
            (
               (select
                  Computer.Computer_Idn
               from
                  Computer (nolock)
                        inner join ComputerVulnerability (nolock) on ComputerVulnerability.Computer_Idn=Computer.Computer_Idn
                              and ComputerVulnerability.PatchInstallSucceeded=2
                              and ComputerVulnerability.PatchDetected<>1
                  where
                     ComputerVulnerability.Computer_Idn is not NULL
                     and ComputerVulnerability.Vul_ID in ('{1}')
               )
               UNION
               (select CVDetected.Computer_Idn
               from CVDetected (nolock)
                  inner join Computer (nolock) on Computer.Computer_Idn = CVDetected.Computer_Idn
                  inner join Vulnerability (nolock) on Vulnerability.Vulnerability_Idn=CVDetected.Vulnerability_Idn
               where
                  Computer.Computer_Idn is not NULL and Vulnerability.Vulnerability_Idn is not NULL
                  and Vulnerability.Vul_ID in ('{1}')
                  and SupercededState<>2
               group by    CVDetected.Computer_Idn
               )
               UNION
               (select Computer.Computer_Idn
               from Computer (nolock) ,
                  Vulnerability2 inner join Vulnerability on Vulnerability2.Vulnerability_Idn=Vulnerability.Vulnerability_Idn
               where
                  charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  ) >0
                  and ((charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  )-1) % 4 )=0
                  and Vulnerability.Vul_ID in ('{1}')
                  and SupercededState<>2
               group by    Computer.Computer_Idn
               )
            )
        )
        and Upper(Right(Computer.DisplayName,1)) not in ('P','B')
        and (Computer.Type Like '%Server%' or Computer.Type Like '%Virtual Machine%')
        and datediff(hour,Computer.recorddate,getdate())>12
        and ({0})
        and (Vulnerability.Vul_ID in ('{1}') or ComputerVulnerability.Vul_ID in ('{1}') )
    """
    security_details_summary_srv_str_nonprd = """
        select distinct Details.DeviceName, Details.Type, Details.ComputerLocation
        from (
            select distinct 
            Computer.DeviceName,Computer.Type,
            case when CVDetected.PatchInstallSucceeded is NULL then ComputerVulnerability.PatchInstallSucceeded else CVDetected.PatchInstallSucceeded end as PatchInstallSucceeded,
            case when CVDetected.NumInstallTries is NULL then 'unknown' else convert(varchar(10),CVDetected.NumInstallTries)  end NumInstallTries,
            case when Vulnerability.Vul_ID is NULL then ComputerVulnerability.Vul_ID else Vulnerability.Vul_ID end Vul_ID,
            case when Vulnerability.PublishDate is NULL then 'unknown' else convert(varchar(20),Vulnerability.PublishDate) end PublishDate,
            case when Vulnerability.Vendor is NULL then 'unknown' else Vulnerability.Vendor end Vendor,
            case when CVDetected.Patch is NULL then ComputerVulnerability.Patch else CVDetected.Patch  end Patch,
            case when CVDetected.Expected is NULL then ComputerVulnerability.Expected else CVDetected.Expected  end Expected,
            case when CVDetected.Found is NULL then ComputerVulnerability.Found else CVDetected.Found  end Found ,
            case when CVDetected.DateDetected is NULL then ComputerVulnerability.DateDetected else CVDetected.DateDetected  end DateDetected ,
            Computer.ComputerLocation
            From Computer (nolock)
                left join CVDetected on CVDetected.Computer_Idn=Computer.Computer_Idn
                left join ComputerVulnerability (nolock) on ComputerVulnerability.Computer_Idn=Computer.Computer_Idn
                left join Vulnerability on CVDetected.Vulnerability_Idn=Vulnerability.Vulnerability_Idn
            Where
            (
                Computer.Computer_Idn in
                (
                   (select
                      Computer.Computer_Idn
                   from
                      Computer (nolock)
                            inner join ComputerVulnerability (nolock) on ComputerVulnerability.Computer_Idn=Computer.Computer_Idn
                                  and ComputerVulnerability.PatchInstallSucceeded=2
                                  and ComputerVulnerability.PatchDetected<>1
                      where
                         ComputerVulnerability.Computer_Idn is not NULL
                         and ComputerVulnerability.Vul_ID in ('{1}')
                   )
                   UNION
                   (select CVDetected.Computer_Idn
                   from CVDetected (nolock)
                      inner join Computer (nolock) on Computer.Computer_Idn = CVDetected.Computer_Idn
                      inner join Vulnerability (nolock) on Vulnerability.Vulnerability_Idn=CVDetected.Vulnerability_Idn
                   where
                      Computer.Computer_Idn is not NULL and Vulnerability.Vulnerability_Idn is not NULL
                      and Vulnerability.Vul_ID in ('{1}')
                      and SupercededState<>2
                   group by    CVDetected.Computer_Idn
                   )
                   UNION
                   (select Computer.Computer_Idn
                   from Computer (nolock) ,
                      Vulnerability2 inner join Vulnerability on Vulnerability2.Vulnerability_Idn=Vulnerability.Vulnerability_Idn
                   where
                      charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  ) >0
                      and ((charindex(CONVERT(VARBINARY(1), (Computer.Computer_Idn & 255))+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256) & 255)+CONVERT(VARBINARY(1), (Computer.Computer_Idn/256/256/256) & 255),NotScannedComputerIdns  )-1) % 4 )=0
                      and Vulnerability.Vul_ID in ('{1}')
                      and SupercededState<>2
                   group by    Computer.Computer_Idn
                   )
                )
            )
            and Upper(Right(Computer.DisplayName,1)) not in ('P','B')
            and (Computer.Type Like '%Server%' or Computer.Type Like '%Virtual Machine%')
            and datediff(hour,Computer.recorddate,getdate())>12
            and ({0})
            and (Vulnerability.Vul_ID in ('{1}') or ComputerVulnerability.Vul_ID in ('{1}') )
        ) Details
    """

    ##workstation data
    security_microsoft_patch_wks_list = _get_security_microsoft_patch_zone(security_type_wks, total_wks_str,patch_required_wks_str, wks_in21days_patch_list)
    security_top5_microsoft_patches_wks_list = _get_top5_microsoft_patches_zone(security_type_wks, top5_microsoft_patches_wks_str,wks_in21days_patch_list)
    security_details_wks_amerias = _get_security_details(security_type_wks, "Americas", security_details_wks_str,wks_in21days_patch_list)
    security_details_wks_emea = _get_security_details(security_type_wks, "EMEA", security_details_wks_str,wks_in21days_patch_list)
    security_details_wks_asia = _get_security_details(security_type_wks, "Asia", security_details_wks_str,wks_in21days_patch_list)
    security_details_summary_wks_amerias = _get_security_details(security_type_wks, "Americas", security_details_summary_wks_str,wks_in21days_patch_list)
    security_details_summary_wks_emea = _get_security_details(security_type_wks, "EMEA", security_details_summary_wks_str,wks_in21days_patch_list)
    security_details_summary_wks_asia = _get_security_details(security_type_wks, "Asia", security_details_summary_wks_str,wks_in21days_patch_list)

    ##server data
    security_microsoft_patch_srv_list = _get_security_microsoft_patch_zone(security_type_srv, total_srv_str,patch_required_srv_str,srv_in21days_patch_list)
    security_top5_microsoft_patches_srv_list = _get_top5_microsoft_patches_zone(security_type_srv, top5_microsoft_patches_srv_str,srv_in21days_patch_list)
    security_details_srv_amerias = _get_security_details(security_type_srv, "Americas", security_details_srv_str,srv_in21days_patch_list)
    security_details_srv_emea = _get_security_details(security_type_srv, "EMEA", security_details_srv_str,srv_in21days_patch_list)
    security_details_srv_asia = _get_security_details(security_type_srv, "Asia", security_details_srv_str,srv_in21days_patch_list)
    security_details_summary_srv_amerias = _get_security_details(security_type_srv, "Americas", security_details_summary_srv_str,srv_in21days_patch_list)
    security_details_summary_srv_emea = _get_security_details(security_type_srv, "EMEA", security_details_summary_srv_str,srv_in21days_patch_list)
    security_details_summary_srv_asia = _get_security_details(security_type_srv, "Asia", security_details_summary_srv_str,srv_in21days_patch_list)    


    ##server nonprd data
    security_microsoft_patch_srv_list_nonprd = _get_security_microsoft_patch_zone(security_type_srv, total_srv_str_nonprd,patch_required_srv_str_nonprd,srv_in21days_patch_list)
    security_top5_microsoft_patches_srv_list_nonprd = _get_top5_microsoft_patches_zone(security_type_srv, top5_microsoft_patches_srv_str_nonprd,srv_in21days_patch_list)
    security_details_srv_amerias_nonprd = _get_security_details(security_type_srv, "Americas", security_details_srv_str_nonprd,srv_in21days_patch_list)
    security_details_srv_emea_nonprd = _get_security_details(security_type_srv, "EMEA", security_details_srv_str_nonprd,srv_in21days_patch_list)
    security_details_srv_asia_nonprd = _get_security_details(security_type_srv, "Asia", security_details_srv_str_nonprd,srv_in21days_patch_list)
    security_details_summary_srv_amerias_nonprd = _get_security_details(security_type_srv, "Americas", security_details_summary_srv_str_nonprd,srv_in21days_patch_list)
    security_details_summary_srv_emea_nonprd = _get_security_details(security_type_srv, "EMEA", security_details_summary_srv_str_nonprd,srv_in21days_patch_list)
    security_details_summary_srv_asia_nonprd = _get_security_details(security_type_srv, "Asia", security_details_summary_srv_str_nonprd,srv_in21days_patch_list)


    ##write all data into last_hour_data.txt
    last_hour_file = os.path.join(project_upload_path ,"securities/histories/last_hour_data_v2.txt")
    with open(last_hour_file,"w") as last_hour_file_out:
        last_hour_file_out.write('{0}\n{1}\n{2}\n{3}\n{4}\n{5}\n{6}\n{7}\n{8}\n{9}\n{10}\n{11}\n{12}\n{13}\n{14}\n{15}\n{16}\n{17}\n{18}\n{19}\n{20}\n{21}\n{22}\n{23}'.format(security_microsoft_patch_wks_list, security_top5_microsoft_patches_wks_list, security_details_wks_amerias, security_details_wks_emea, security_details_wks_asia, security_details_summary_wks_amerias, security_details_summary_wks_emea, security_details_summary_wks_asia, security_microsoft_patch_srv_list, security_top5_microsoft_patches_srv_list,security_details_srv_amerias,security_details_srv_emea,security_details_srv_asia,security_details_summary_srv_amerias,security_details_summary_srv_emea,security_details_summary_srv_asia,security_microsoft_patch_srv_list_nonprd,security_top5_microsoft_patches_srv_list_nonprd,security_details_srv_amerias_nonprd,security_details_srv_emea_nonprd,security_details_srv_asia_nonprd,security_details_summary_srv_amerias_nonprd,security_details_summary_srv_emea_nonprd,security_details_summary_srv_asia_nonprd))

if __name__=="__main__":
    if __package__ is None:
        import sys
        from os import path
        sys.path.append( path.dirname(path.dirname( path.dirname( path.abspath(__file__) ) ) ) )
        os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'hygie.settings')
        from django.conf import settings
    else:
        from django.conf import settings
    start_time = datetime.datetime.now()
    print(str(start_time) )
    _save_last_hour_data_in_txt()
    end_time = datetime.datetime.now()
    print(str(end_time))
    print(str(end_time-start_time))
    print('Last run on: ', datetime.datetime.utcnow())
