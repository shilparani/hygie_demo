# -*- coding: utf-8 -*-
"""Application assets."""
from django_assets import Bundle, register

css = Bundle(
    'libs/bootstrap/dist/css/bootstrap.css',
    'libs/gh-theme/vendor/metisMenu/metisMenu.min.css',
    'libs/gh-theme/dist/css/sb-admin-2.css',
    'libs/font-awesome4/css/font-awesome.min.css',
    'css/style.css',
    filters='cssmin',
    output='public/css/common.css'
)

js = Bundle(
    'libs/jQuery/dist/jquery.js',
    'libs/bootstrap/dist/js/bootstrap.js',
    'js/plugins.js',
    'libs/jquery.canvasjs.min.js',
    'libs/gh-theme/dist/js/sb-admin-2.js',
    'libs/1000hz-bootstrap-validator/validator.min.js',
    'libs/sidebar/sidebar_menu.js',
    'libs/gh-theme/vendor/metisMenu/metisMenu.min.js',
    'js/moment.js',
    'js/moment-timezone.js',
    'js/moment-timezone-with-data.js',
    filters='jsmin',
    output='public/js/common.js'
)

OUTPUT_CSS = 'public/css/common.css'
OUTPUT_JS = 'public/js/common.js'

# Base bundles and variables
base_css = Bundle(
    'libs/bootstrap/dist/css/bootstrap.css',
    'libs/gh-theme/vendor/metisMenu/metisMenu.min.css',
    'libs/gh-theme/dist/css/sb-admin-2.css',
    'libs/font-awesome4/css/font-awesome.min.css',
    'css/style.css',
    'libs/sidebar/simple-sidebar.css',
)

base_js = Bundle(
    'libs/jQuery/dist/jquery.js',
    'libs/bootstrap/dist/js/bootstrap.js',
    'js/plugins.js',
    'libs/gh-theme/dist/js/sb-admin-2.js',
    'libs/1000hz-bootstrap-validator/validator.min.js',
    'libs/gh-theme/vendor/metisMenu/metisMenu.min.js',
    'js/moment.js',
    'js/moment-timezone.js',
    'js/moment-timezone-with-data.js',
    'js/handlebars.js',
)

# Side menu
menu_css = Bundle(
    'libs/gh-theme/vendor/metisMenu/metisMenu.min.css',
)

menu_js = Bundle(
    'libs/sidebar/sidebar_menu.js',
    'libs/gh-theme/vendor/metisMenu/metisMenu.min.js'
)


#Datatable files
datatable_css = Bundle(
    'css/buttons.bootstrap.min.css',
    'css/jquery.dataTables.yadcf.css'
)

datatable_js = Bundle(
    'libs/datatables/jquery.dataTables.min.js',
    'libs/datatables/dataTables.bootstrap.min.js',
    'libs/datatables/dataTables.buttons.min.js',
    'libs/datatables/jszip.min.js',
    'libs/datatables/pdfmake.min.js',
    'libs/datatables/vfs_fonts.js',
    'libs/datatables/buttons.html5.min.js',
    #'libs/datatables/buttons.flash.min.js',
    'libs/datatables/jquery.dataTables.yadcf.js'
)

# HOME PUBLIC
public_css =  Bundle(
    base_css,
    menu_css,
    filters='cssmin',
    output=OUTPUT_CSS
)

public_js = Bundle(
    base_js,
    menu_js,
    filters='jsmin',
    output=OUTPUT_JS
)

# HOME
home_css = Bundle(
    base_css,
    menu_css,
    datatable_css,
    filters='cssmin',
    output=OUTPUT_CSS
)

home_js = Bundle(
    base_js,
    menu_js,
    datatable_js,
    'libs/jquery.canvasjs.min.js',
    'libs/tether.min.js',
    'libs/loader.js',
    'libs/html5sortable-master/dist/html5sortable.js',
    filters='jsmin',
    output=OUTPUT_JS
)

# TEAM
team_css = Bundle(
    base_css,
    menu_css,
    datatable_css,
    filters='cssmin',
    output=OUTPUT_CSS
)

team_js = Bundle(
    base_js,
    menu_js,
    datatable_js,
    'libs/jquery.canvasjs.min.js',
    'libs/tether.min.js',
    'libs/loader.js',
    filters='jsmin',
    output=OUTPUT_JS
)

# SEARCH
search_css = Bundle(
    base_css,
    menu_css,
    datatable_css,
    filters='cssmin',
    output=OUTPUT_CSS
)

search_js = Bundle(
    base_js,
    menu_js,
    datatable_js,
    'libs/loader.js',
    filters='jsmin',
    output=OUTPUT_JS
)

# Admin
admin_css = Bundle( search_css, filters='cssmin', output=OUTPUT_CSS)
admin_js = Bundle( search_js, filters='jsmin', output=OUTPUT_JS)



# CLOSE TICKET
close_css = Bundle(
    base_css,
    menu_css,
    datatable_css,
    #'libs/bootstrap-select/bootstrap-select.min.css',
    'libs/bootstrap-multiselect/bootstrap-multiselect.css',
    'libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
    'https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.2/chosen.min.css',
    'libs/daterangepicker-master/dist/daterangepicker.css',
    'css/sweetalert.css',
    filters='cssmin',
    output=OUTPUT_CSS
)

close_js = Bundle(
    base_js,
    menu_js,
    datatable_js,
    'libs/jquery.canvasjs.min.js',
    #'libs/bootstrap-datetimepicker/js/moment-with-locales.js',
    'libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
    #'libs/bootstrap-select/bootstrap-select.js',
    'libs/bootstrap-multiselect/bootstrap-multiselect.js',
    'https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.2/chosen.jquery.min.js',
    'libs/d3/d3.min.js',
    'libs/loader.js',
    'js/sweetalert.min.js',
    'js/knockout-3.4.2.js',
    'libs/daterangepicker-master/dist/daterangepicker.js',
    filters='jsmin',
    output=OUTPUT_JS
)

security_css = Bundle( search_css, datatable_css, filters='cssmin', output=OUTPUT_CSS)
security_js = Bundle(
    base_js,
    menu_js,
    datatable_js,
    'libs/d3/d3.v3.min.js',
    'libs/d3/topojson.v1.min.js',
    'libs/datamaps/datamaps.world.min.js',
    'libs/amcharts/amcharts.js',
    'libs/amcharts/pie.js',
    'libs/amcharts/ammap_amcharts_extension.js',
    'libs/amcharts/continentsLow.js',
    'libs/amcharts/worldHigh.js',
    'libs/amcharts/light.js',
    filters='jsmin',
    output=OUTPUT_JS
)

cmdb_css = Bundle(
    'css/cmdb_style.css',
    'css/sweetalert.css'
)

cmdb_js = Bundle(
    'js/sweetalert.min.js'
)

datatable_min_js = Bundle(
    datatable_js,
    filters='jsmin',
    output='public/js/dt.js'
)

# LOGIN
login_css = Bundle(
    base_css,
    filters='cssmin',
    output=OUTPUT_CSS
)

login_js = Bundle(
    base_js,
    filters='jsmin',
    output=OUTPUT_JS
)

# Quality
quality_css = Bundle(
    base_css,
    menu_css,
    datatable_css,
    'libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css',
    #'libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
    'css/sweetalert.css',
    filters='cssmin',
    output=OUTPUT_CSS
)

quality_js = Bundle(
    base_js,
    menu_js,
    datatable_js,
    'libs/jquery.canvasjs.min.js',
    'libs/bootstrap/js/transition.js',
    'libs/bootstrap/js/collapse.js',
    'libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js',
    #'libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
    'js/FileSaver.js',
    'js/jquery.wordexport.js',
    'js/sweetalert.min.js',
    filters='jsmin',
    output=OUTPUT_JS
)

reporting_esurvey_css = Bundle( 
    search_css, 
    datatable_css,
    'libs/daterangepicker-master/dist/daterangepicker.css',
    filters='cssmin', 
    output=OUTPUT_CSS
)
reporting_esurvey_js = Bundle(
    base_js,
    menu_js,
    datatable_js,
    'libs/d3/d3.v3.min.js',
    'libs/d3/topojson.v1.min.js',
    'libs/datamaps/datamaps.world.min.js',
    'libs/amcharts/amcharts.js',
    'libs/amcharts/pie.js',
    'libs/amcharts/ammap_amcharts_extension.js',
    'libs/amcharts/continentsLow.js',
    'libs/amcharts/worldHigh.js',
    'libs/amcharts/light.js',
    'libs/amcharts/serial.js',
    #'libs/amcharts/xy.js',
    'js/knockout-3.4.2.js',
    'libs/daterangepicker-master/dist/daterangepicker.js',
    filters='jsmin',
    output=OUTPUT_JS
)

reporting_topten_css = Bundle(
    search_css,
    datatable_css,
    'libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
    filters='cssmin',
    output=OUTPUT_CSS
)
reporting_topten_js = Bundle(
    base_js,
    menu_js,
    datatable_js,
    'libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
    'libs/amcharts/amcharts.js',
    'libs/amcharts/ammap_amcharts_extension.js',
    'libs/amcharts/light.js',
    'libs/amcharts/serial.js',
    filters='jsmin',
    output=OUTPUT_JS
)

reporting_ticket_analysis_js = Bundle(
    base_js,
    menu_js,
    datatable_js,
    'js/knockout-3.4.2.js',
    'libs/daterangepicker-master/dist/daterangepicker.js',
    'libs/bootstrap-multiselect/bootstrap-multiselect.js',
    'libs/jquery.canvasjs.min.js',
    'libs/loader.js',
    #'libs/amcharts/amcharts.js',
    #'libs/amcharts/ammap_amcharts_extension.js',
    #'libs/amcharts/light.js',
    #'libs/amcharts/serial.js',
    filters='jsmin',
    output=OUTPUT_JS
)
reporting_ticket_analysis_css = Bundle(
    base_css,
    search_css,
    datatable_css,
    'libs/bootstrap-multiselect/bootstrap-multiselect.css',
    'libs/daterangepicker-master/dist/daterangepicker.css',
    filters='cssmin',
    output=OUTPUT_CSS
)

reporting_tsccalendar_css = Bundle(
    base_css,
    'libs/fullcalendar/fullcalendar.min.css',
    filters='cssmin',
    output=OUTPUT_CSS
)
reporting_tsccalendar_js = Bundle(
    base_js,
    menu_js,
    'libs/fullcalendar/fullcalendar.js',
    filters='jsmin',
    output=OUTPUT_JS
)

reporting_ticket_stats_css = Bundle(
    base_css,
    #menu_css,
    #'libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
    #'https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.2/chosen.min.css',
    'libs/bootstrap-multiselect/bootstrap-multiselect.css',
    'libs/daterangepicker-master/dist/daterangepicker.css',
    filters='cssmin',
    output=OUTPUT_CSS
)
reporting_ticket_stats_js = Bundle(
    base_js,
    menu_js,
    datatable_js,
    #'libs/jquery.canvasjs.min.js',
    #'libs/bootstrap-datetimepicker/js/moment-with-locales.js',
    #'libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
    #'libs/bootstrap-select/bootstrap-select.js',
    'libs/bootstrap-multiselect/bootstrap-multiselect.js',
    #'https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.2/chosen.jquery.min.js',
    #'libs/d3/d3.min.js',
    #'libs/loader.js',
    #'js/sweetalert.min.js',
    'js/knockout-3.4.2.js',
    'libs/daterangepicker-master/dist/daterangepicker.js',
    filters='jsmin',
    output=OUTPUT_JS
)

reporting_integration_view_css = Bundle(
    base_css,
    menu_css,
    datatable_css,
    'https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.2/chosen.min.css',
    #'libs/bootstrap-multiselect/bootstrap-multiselect.css',
    #'libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
    #'https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.2/chosen.min.css',
    #'libs/daterangepicker-master/dist/daterangepicker.css',
    #'css/sweetalert.css',
    filters='cssmin',
    output=OUTPUT_CSS
)
reporting_integration_view_js = Bundle(
    base_js,
    menu_js,
    datatable_js,
    'libs/jquery.canvasjs.min.js',
    'https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.2/chosen.jquery.min.js',
    #'libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
    #'libs/bootstrap-multiselect/bootstrap-multiselect.js',
    #'libs/d3/d3.min.js',
    'libs/loader.js',
    filters='jsmin',
    output=OUTPUT_JS
)

patch_management_css = Bundle(
    base_css,
    'libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
    'css/sweetalert.css',
    filters='cssmin',
    output=OUTPUT_CSS
)

patch_management_js = Bundle(
    base_js,
    menu_js,
    'libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
    'js/handlebars.js',
    'js/sweetalert.min.js',
    filters='jsmin',
    output=OUTPUT_JS
)

patch_followup_css = Bundle(
    base_css,
    datatable_css,
    'https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.2/chosen.min.css',
    filters='cssmin',
    output=OUTPUT_CSS
)

patch_followup_js = Bundle(
    base_js,
    menu_js,
    datatable_js,
    'libs/jquery.canvasjs.min.js',
    filters='jsmin',
    output=OUTPUT_JS
)

query_management_css = Bundle(
    base_css,
    menu_css,
    'libs/bootstrap-multiselect/bootstrap-multiselect.css',
    filters='cssmin',
    output=OUTPUT_CSS
)

query_management_js = Bundle(
    base_js,
    menu_js,
    'libs/bootstrap-multiselect/bootstrap-multiselect.js',
    'https://cdnjs.cloudflare.com/ajax/libs/ace/1.4.2/ace.js',
    filters='jsmin',
    output=OUTPUT_JS
)

content_management_css = Bundle(
    base_css,
    menu_css,
    'libs/bootstrap-multiselect/bootstrap-multiselect.css',
    filters='cssmin',
    output=OUTPUT_CSS
)

content_management_js = Bundle(
    base_js,
    menu_js,
    'libs/bootstrap-multiselect/bootstrap-multiselect.js',
    #'https://cloud.tinymce.com/5/tinymce.min.js',
    filters='jsmin',
    output=OUTPUT_JS
)

control_plan_css = Bundle(
    base_css,
    datatable_css,
    filters='cssmin',
    output=OUTPUT_CSS
)

control_plan_js = Bundle(
    base_js,
    datatable_js,
    "https://code.highcharts.com/highcharts.js",
    "https://code.highcharts.com/highcharts-more.js",
    "https://code.highcharts.com/modules/solid-gauge.js",
    'commonjs/control_plan_common.js',
    filters='jsmin',
    output=OUTPUT_JS
)

register('js_public', public_js)
register('css_public', public_css)

register('js_home', home_js)
register('css_home', home_css)

register('js_team', team_js)
register('css_team', team_css)

register('js_search', search_js)
register('css_search', search_css)

register('js_admin', admin_js)
register('css_admin', admin_css)

register('js_close', close_js)
register('css_close', close_css)

register('js_datatable', datatable_min_js)

register('js_login', login_js)
register('css_login', login_css)

register('js_all', base_js)
register('css_all', base_css)

register('js_security', security_js)
register('css_security', security_css)

register('js_cmdb', cmdb_js)
register('css_cmdb', cmdb_css)

register('js_quality', quality_js)
register('css_quality', quality_css)

register('js_reporting_esurvey', reporting_esurvey_js)
register('css_reporting_esurvey', reporting_esurvey_css)

register('js_reporting_topten', reporting_topten_js)
register('css_reporting_topten', reporting_topten_css)

register('js_reporting_ticket_analysis', reporting_ticket_analysis_js)
register('css_reporting_ticket_analysis', reporting_ticket_analysis_css)

register('js_reporting_tsccalendar', reporting_tsccalendar_js)
register('css_reporting_tsccalendar', reporting_tsccalendar_css)

register('js_reporting_ticket_stats', reporting_ticket_stats_js)
register('css_reporting_ticket_stats', reporting_ticket_stats_css)

register('js_reporting_integration_view', reporting_integration_view_js)
register('css_reporting_integration_view', reporting_integration_view_css)

register('js_patch_management', patch_management_js)
register('css_patch_management', patch_management_css)

register('js_patch_followup', patch_followup_js)
register('css_patch_followup', patch_followup_css)

register('js_query_management', query_management_js)
register('css_query_management', query_management_css)

register('js_content_management', content_management_js)
register('css_content_management', content_management_css)

register('js_control_plan', control_plan_js)
register('css_control_plan', control_plan_css)
