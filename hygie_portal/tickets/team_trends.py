# -*- coding: utf-8 -*-
from django.conf import settings
from hygie_portal.tickets import common as f


def team_trends(request, team):
    result = []
    data = {}
    
    db_conn = f._get_team_trends_connection(request)
    cursor = db_conn.cursor()
    
    #last 7days range : (8,1)
    #last 2 7days range : (15,8)
    #date_range = [(15,8) , (8,1)] #oldest date range to be the first element
    date_range = [(1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,1), (16,9), (24,17), (32,25), (31,1), (62,32)] #Newest date range to be the first element
   
    team = ",".join("'{0}'".format(w) for w in team) 
    for dates in date_range:
        # create the queries for the different date ranges
        #conn_str = "SELECT sum(count_in), sum(count_out) from trend_tickets where groupe in ({2}) and date(date) between curdate() - ({0}) and curdate() - ({1})"
        conn_str = "SELECT sum(count_in), sum(count_out) from trend_tickets where groupe in ({2}) and date(date) between DATE_SUB(CURDATE(), INTERVAL {0} DAY) and DATE_SUB(CURDATE(), INTERVAL {1} DAY)"     
        conn_str = conn_str.format(dates[0],dates[1],team)

        cursor.execute(conn_str) 
        db_data=cursor.fetchall()[0]
        
        #convert Decimal SQL result to int
        if db_data is not None:
            data_result =  [int(row) if row is not None else 0 for row in db_data]
        else:
            [0,0]
        result.append({"dates" : dates, "incoming" : data_result[0], "outgoing" : data_result[1], "difference" : data_result[1] - data_result[0]})
    f._close_connection(request, db_conn)

    return result

def team_trends_data(request, team):
    team_list = []
    data_result = {}
    TEAMS = settings.TEAMS
    db_conn = f._get_team_trends_connection(request)
    cursor = db_conn.cursor()
    
    if team == 'all_tsc':

        for teams in TEAMS['TSC']:
            team_list.extend(teams['team'])  
        
        conn_str = "SELECT groupe, sum(count_in), sum(count_out) from trend_tickets where groupe in ({0}) and date(date) between DATE_SUB(CURDATE(), INTERVAL 8 DAY) and DATE_SUB(CURDATE(), INTERVAL 1 DAY) group by groupe"  
        GRP = ",".join("'{0}'".format(w) for w in team_list)
        conn_str = conn_str.format(GRP)  
        
        cursor.execute(conn_str) 
        db_data=cursor.fetchall()
        
        #subtract the incoming_tickets and outgoing ticket and assign to subteam
        for data in db_data:
            if data is not None:
                data_result[data[0]] = int(data[2]) - int(data[1])
        
        #Add '0' to team_list which do not have data in DB     
        for t in team_list:
            if t not in data_result.keys():
                data_result[t] = 0
        
    if team == 'all_sd':
        
        for teams in TEAMS['SERVICE_DESK']:
            team_list.extend(teams['team'])
        
        conn_str = "SELECT groupe, sum(count_in), sum(count_out) from trend_tickets where groupe in ({0}) and date(date) between DATE_SUB(CURDATE(), INTERVAL 8 DAY) and DATE_SUB(CURDATE(), INTERVAL 1 DAY) group by groupe" 
        GRP = ",".join("'{0}'".format(w) for w in team_list)
        conn_str = conn_str.format(GRP)  
        
        cursor.execute(conn_str) 
        db_data=cursor.fetchall()
        
        #subtract the incoming_tickets and outgoing ticket and assign to subteam
        for data in db_data:
            if data is not None:
                data_result[data[0]] = int(data[2]) - int(data[1])
        
        #Add '0' to team_list which do not have data in DB     
        for t in team_list:
            if t not in data_result.keys():
                data_result[t] = 0
        
    if team == 'all_proximity_ds':
        
        for x in TEAMS['PROXI_EMEA']:
            team_list.extend(x['team']) 
        
        conn_str = "SELECT groupe, sum(count_in), sum(count_out) from trend_tickets where groupe in ({0}) and date(date) between DATE_SUB(CURDATE(), INTERVAL 8 DAY) and DATE_SUB(CURDATE(), INTERVAL 1 DAY) group by groupe"  
        GRP = ",".join("'{0}'".format(w) for w in team_list)
        conn_str = conn_str.format(GRP)  
        
        cursor.execute(conn_str) 
        db_data=cursor.fetchall()
        
        #subtract the incoming_tickets and outgoing ticket and assign to subteam
        for data in db_data:
            if data is not None:
                data_result[data[0]] = int(data[2]) - int(data[1])
        
        #Add '0' to team_list which do not have data in DB     
        for t in team_list:
            if t not in data_result.keys():
                data_result[t] = 0
        
    if team == 'all_america_ds':
        
        for x in TEAMS['PROXI_AMERICA']:
            team_list.extend(x['team'])
        
        conn_str = "SELECT groupe, sum(count_in), sum(count_out) from trend_tickets where groupe in ({0}) and date(date) between DATE_SUB(CURDATE(), INTERVAL 8 DAY) and DATE_SUB(CURDATE(), INTERVAL 1 DAY) group by groupe"  
        GRP = ",".join("'{0}'".format(w) for w in team_list)
        conn_str = conn_str.format(GRP)  
        
        cursor.execute(conn_str) 
        db_data=cursor.fetchall()
        
        #subtract the incoming_tickets and outgoing ticket and assign to subteam
        for data in db_data:
            if data is not None:
                data_result[data[0]] = int(data[2]) - int(data[1])
        
        #Add '0' to team_list which do not have data in DB     
        for t in team_list:
            if t not in data_result.keys():
                data_result[t] = 0
        
    if team == 'all_emea':
        
        for x in TEAMS['PROXI_EMEA']:
            team_list.extend(x['team']) 
        
        conn_str = "SELECT groupe, sum(count_in), sum(count_out) from trend_tickets where groupe in ({0}) and date(date) between DATE_SUB(CURDATE(), INTERVAL 8 DAY) and DATE_SUB(CURDATE(), INTERVAL 1 DAY) group by groupe"  
        GRP = ",".join("'{0}'".format(w) for w in team_list)
        conn_str = conn_str.format(GRP)  
        
        cursor.execute(conn_str) 
        db_data=cursor.fetchall()
        
        #subtract the incoming_tickets and outgoing ticket and assign to subteam
        for data in db_data:
            if data is not None:
                data_result[data[0]] = int(data[2]) - int(data[1])
        
        #Add '0' to team_list which do not have data in DB     
        for t in team_list:
            if t not in data_result.keys():
                data_result[t] = 0
        
    if team == 'all_global_asie_ds':
        
        for x in TEAMS['PROXI_GLOBAL_ASIE']:
            team_list.extend(x['team']) 
        
        conn_str = "SELECT groupe, sum(count_in), sum(count_out) from trend_tickets where groupe in ({0}) and date(date) between DATE_SUB(CURDATE(), INTERVAL 8 DAY) and DATE_SUB(CURDATE(), INTERVAL 1 DAY) group by groupe"  
        GRP = ",".join("'{0}'".format(w) for w in team_list)
        conn_str = conn_str.format(GRP)  
        
        cursor.execute(conn_str) 
        db_data=cursor.fetchall()
        
        #subtract the incoming_tickets and outgoing ticket and assign to subteam
        for data in db_data:
            if data is not None:
                data_result[data[0]] = int(data[2]) - int(data[1])
        
        #Add '0' to team_list which do not have data in DB     
        for t in team_list:
            if t not in data_result.keys():
                data_result[t] = 0
        
    if team == 'all_berluti_ds':
        
        for x in TEAMS['PROXI_BERLUTI']:
            team_list.extend(x['team'])
        
        conn_str = "SELECT groupe, sum(count_in), sum(count_out) from trend_tickets where groupe in ({0}) and date(date) between DATE_SUB(CURDATE(), INTERVAL 8 DAY) and DATE_SUB(CURDATE(), INTERVAL 1 DAY) group by groupe"  
        GRP = ",".join("'{0}'".format(w) for w in team_list)
        conn_str = conn_str.format(GRP)  
        
        cursor.execute(conn_str) 
        db_data=cursor.fetchall()
        
        #subtract the incoming_tickets and outgoing ticket and assign to subteam
        for data in db_data:
            if data is not None:
                data_result[data[0]] = int(data[2]) - int(data[1])
        
        #Add '0' to team_list which do not have data in DB     
        for t in team_list:
            if t not in data_result.keys():
                data_result[t] = 0
        
    if team == 'all_fondation_ds':
        
        for x in TEAMS['PROXI_FONDATION']:
            team_list.extend(x['team'])
 
        conn_str = "SELECT groupe, sum(count_in), sum(count_out) from trend_tickets where groupe in ({0}) and date(date) between DATE_SUB(CURDATE(), INTERVAL 8 DAY) and DATE_SUB(CURDATE(), INTERVAL 1 DAY) group by groupe" 
        GRP = ",".join("'{0}'".format(w) for w in team_list)
        conn_str = conn_str.format(GRP)  
        
        cursor.execute(conn_str) 
        db_data=cursor.fetchall()
        
        #subtract the incoming_tickets and outgoing ticket and assign to subteam
        for data in db_data:
            if data is not None:
                data_result[data[0]] = int(data[2]) - int(data[1])
        
        #Add '0' to team_list which do not have data in DB     
        for t in team_list:
            if t not in data_result.keys():
                data_result[t] = 0
        
    if team == 'all_itl4':
        
        for x in TEAMS['ITL4']:
            team_list.extend(x['team'])
        
        conn_str = "SELECT groupe, sum(count_in), sum(count_out) from trend_tickets where groupe in ({0}) and date(date) between DATE_SUB(CURDATE(), INTERVAL 8 DAY) and DATE_SUB(CURDATE(), INTERVAL 1 DAY) group by groupe" 
        GRP = ",".join("'{0}'".format(w) for w in team_list)
        conn_str = conn_str.format(GRP)  
        
        cursor.execute(conn_str) 
        db_data=cursor.fetchall()
        
        for data in db_data:
            if data is not None:
                data_result[data[0]] = int(data[2]) - int(data[1])

        #Add '0' to team_list which do not have data in DB
        for t in team_list:
            if t not in data_result.keys():
                data_result[t] = 0
                
        
    if team == 'all_ams':
        
        for x in TEAMS['AMS']:
            if isinstance(x['team'],str):
                team_list.append(x['team'])
            else:
                for e in x['team']:
                    if isinstance(e,str):
                        team_list.append(e)
                    else:
                        team_list.extend(e['team'])  
        
        conn_str = "SELECT groupe, sum(count_in), sum(count_out) from trend_tickets where groupe in ({0}) and date(date) between DATE_SUB(CURDATE(), INTERVAL 8 DAY) and DATE_SUB(CURDATE(), INTERVAL 1 DAY) group by groupe"  
        GRP = ",".join("'{0}'".format(w) for w in team_list)
        conn_str = conn_str.format(GRP)  
        
        cursor.execute(conn_str) 
        db_data=cursor.fetchall()
        
        #subtract the incoming_tickets and outgoing ticket and assign to subteam
        for data in db_data:
            if data is not None:
                data_result[data[0]] = int(data[2]) - int(data[1])
        
        #Add '0' to team_list which do not have data in DB     
        for t in team_list:
            if t not in data_result.keys():
                data_result[t] = 0

    f._close_connection(request, db_conn)    
    return data_result 
