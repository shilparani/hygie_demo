# -*- coding: utf-8 -*-
"""Helper utilities and decorators."""

from datetime import timedelta
from dateutil import tz
from math import floor
from collections import defaultdict,OrderedDict
import datetime
import pytz
from django.conf import settings
from hygie.settings import logger
from hygie_portal.tickets import common as f
import operator

from hygie_portal.querymanager import querymanager as qm

import json

# Constants
WHITE = "#ffffff"
BLACK = "#000000"

def get_backlogs_prio_data(request, teams=None, local_tz=None, ticket_type=None, ticket_crit=None, assigned_to=None, source='deis'):
    
    logger.info('get_backlogs_prio_data')
    teams_dict = settings.ALL_TEAMS_MAPPING
    TEAMS = settings.TEAMS
    
    groups_dict = {}
    GRP_TEAMS =[]
    result_temp =[]
    result = {}
    sla_grp='ITL4'
    
    if isinstance(teams, list):
        for sub_team in teams:
            for t in TEAMS.values():
                for t1 in t:
                    if sub_team in t1['team']:
                        GRP_TEAMS.append(sub_team)
                        groups_dict[sub_team] = t1['grp_name']
    elif teams == 'all_proximity_ds':
        keys = ['PROXI_EMEA', 'PROXI_AMERICA', 'PROXI_GLOBAL_ASIE', 'PROXI_BERLUTI', 'PROXI_FONDATION']
        for k in keys:
            for data in TEAMS[k]:
                GRP_TEAMS.extend(data['team'])
                groups_dict[", ".join(data['team'])] = data['grp_name']
    elif teams in teams_dict.keys():
        team_data = teams_dict[teams] if (teams== 'all_ams_cgi' or teams=='all_ams_valtech') else TEAMS[teams_dict[teams]]
        for data in team_data:
            for team in data['team']:
               GRP_TEAMS.append(team)
               groups_dict[team] = data['grp_name']

    logger.info('GROUP_TEAMS***************:%s'%GRP_TEAMS)

    ticket_type = ticket_type if ticket_type else 'ALL'
    TICKET_STATUS = ",".join("'{0}'".format(w) for w in settings.TICKET_STATUS)
    TICKET_TYPES = ",".join("'{0}'".format(w) for w in settings.TICKET_TYPES[ticket_type])
    TICKET_CRITICALITY = ",".join("'{0}'".format(w) for w in ticket_crit)
    
    
    #db_view_query = qm.get_query_by_title('_TELMASTE_HYGIE_Open')
    view = '_telmaste_hygie_open' #'({0})'.format(db_view_query)
        
    conn_str = "select `DATE OPEN` , Urgency_desc, `DUE_DATE:`, NUM_TICKET , DESCRIPTION, group_desc, assign_desc , Subject_desc, Client_desc, TTYPE as TicketType, lastmodified as LastModified, lastmodified as LastDate, status_desc as third_party, `SEQUENCE`, `SEQUENCE_Parent`, title, type as type, Client_desc as Client, User_Location as Location, '-' as problem_id, '-' as problem_sequence, '-' as problem_status from {4} as details where `CLOSED ON` is null and group_desc in ({3}) and urgency_desc in ({2}) and upper(ttype) in ({1}) and 'OPEN' in ({0});"
        
    GRP = ",".join("'{0}'".format(w) for w in GRP_TEAMS)
    conn_str = conn_str.format(TICKET_STATUS, TICKET_TYPES, TICKET_CRITICALITY, GRP) if source == 'deis' else conn_str.format(TICKET_STATUS, TICKET_TYPES, TICKET_CRITICALITY, GRP, view)
    logger.info('conn_str:%s'%conn_str)   
    db_conn = f._get_connection(request) if source=='deis' else f._get_connection_itsm(request)   
    
    result["backlogs_data"]={}
    result["GRP"]=sla_grp
    
    # check DB connection, if none return
    if db_conn is None:
        return result
        
    cursor = db_conn.cursor()

    if assigned_to:
        conn_str = conn_str + " where [Login ID Assigned To] = upper('{0}')".format(assigned_to)

    cursor.execute(conn_str)
    now_date = datetime.datetime.now(pytz.timezone(settings.TIMEZONE))
    TZ = pytz.timezone(settings.TIMEZONE)
    from_zone = tz.gettz(settings.TIMEZONE)
    to_zone = tz.gettz(local_tz)
    data=cursor.fetchall()
    f._close_connection(request, db_conn) #close DB connection
    
    for t in data:
        backlogs_data = {}  
        comp_due_date, is_oosla, is_planned, is_soosla = None, 0, 0, 0  
        # convert dbrows to list  and replace None in data to ''
        t = ['' if d is None else d for d in t]
        
        if source=='deis':
            t.extend(['Without', '', '', '', 'Incident', '', '', '', '']) #append the missing 9 columns making it similar to itsm data
            
        default_keys = ["open_date","criticality","due_date","ticket_number","description","raw_group_name","taken_by","category","open_by","ticket_type","is_sleeping_ticket",
                        "duration_at_group","ticket_status","itsm_url","itsm_parent_url","title","task_type","client","location","problem_id","problem_sequence","problem_status"]
 
        backlogs_data = dict(zip(default_keys, t)) #form dict with default_keys and DB data(t)
        
        backlogs_data["problem_url"] = "https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/Object/Open.rails?class_name=ProblemManagement.Problem&key=" + backlogs_data["problem_sequence"] if backlogs_data["problem_sequence"] else ''
        

        backlogs_data["sequence"] = backlogs_data["itsm_url"]
        #change the sla group
        if backlogs_data["raw_group_name"] in settings.SLA_GRP_AMS_CGI:
            sla_grp = 'AMS_CGI'
        elif backlogs_data["raw_group_name"] in settings.SLA_GRP_AMS_VALTECH:
            sla_grp = 'AMS_VALTECH'
        else:
            for key, value in TEAMS.items():
                for v in value:
                    if backlogs_data["raw_group_name"] in v['team']:
                        if key in settings.SLA_GRP_PROXIMITY:
                            sla_grp = 'EMEA'
                        else:
                            sla_grp = key
        
        # temporary "due_date" assignment for specific format
        due_date_temp = backlogs_data["due_date"]
        open_date_temp = backlogs_data["open_date"]
        
        #match 'team' to 'name' in settings.py 
        backlogs_data["group_name"] = [x for x in settings.ALL_TEAMS if backlogs_data["raw_group_name"] in x['team']][0]['name']

        # taken by set to '' if none
        backlogs_data["taken_by"] = backlogs_data["taken_by"] if backlogs_data["taken_by"] else ''

        # sleeping ticket, ticket not updated the past 24 hrs
        backlogs_data["is_sleeping_ticket"] = json.dumps(TZ.localize(backlogs_data["is_sleeping_ticket"]) < (now_date - timedelta(hours=24)))
        
        # duration at group
        if(backlogs_data["duration_at_group"] is None):
            if(local_tz!=settings.TIMEZONE):
                backlogs_data["duration_at_group"] = datetime.datetime.strptime(backlogs_data["open_date"], "%Y-%m-%d %H:%M:%S")
            else:
                backlogs_data["duration_at_group"] = backlogs_data["open_date"]

        if((now_date - TZ.localize(backlogs_data["duration_at_group"]))>timedelta(days = 7)):
            backlogs_data["is_duration_at_group_oosla"] = 1
        else:
            backlogs_data["is_duration_at_group_oosla"] = 0
       
        backlogs_data["duration_at_group"] = f._format_timedelta(now_date - TZ.localize(backlogs_data["duration_at_group"])) 
        user_assign = str(request.user.first_name) + " " + str(request.user.last_name)
        
        if backlogs_data["task_type"] is None:
            backlogs_data["task_type"] = "Request Task"
        elif "User" in backlogs_data["task_type"]:
            backlogs_data["task_type"] = "USL"
        elif "Application" in backlogs_data["task_type"]:
            backlogs_data["task_type"] = "ASL"  
            
        # Create the clickable link                           
        if source!='deis':
            if "I" in backlogs_data["ticket_number"]:
                if "-" in backlogs_data["ticket_number"]:
                    backlogs_data["itsm_add_note_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Task&key=' + backlogs_data["itsm_url"] + '&function_name=AddNote'
                    backlogs_data["itsm_reassign_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Task&key=' + backlogs_data["itsm_url"] + '&function_name=AddLVReassignTask'
                    
                    if user_assign != backlogs_data["taken_by"]:
                        backlogs_data["itsm_assign_to_me"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Task&key=' + backlogs_data["itsm_url"] + '&function_name=AssigntoMe'
                        
                    backlogs_data["itsm_resolve_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Task&key=' + backlogs_data["itsm_url"] + '&function_name=Complete'    
                    backlogs_data["itsm_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/open.rails?class_name=IncidentManagement.Task&key=' + backlogs_data["itsm_url"] + '&parent_class_name=IncidentManagement.Incident&parent_key=' + backlogs_data["itsm_parent_url"] + '&parent_attribute=Tasks'
                    backlogs_data["itsm_parent_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/Object/Open.rails?class_name=IncidentManagement.Incident&key=' + backlogs_data["itsm_parent_url"]
                    
                else:
                    backlogs_data["itsm_add_note_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Incident&key=' + backlogs_data["itsm_url"] + '&function_name=AddNote'
                    backlogs_data["itsm_reassign_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Incident&key=' + backlogs_data["itsm_url"] + '&function_name=AddLVAssignment'
                    
                    if user_assign != backlogs_data["taken_by"]:
                        backlogs_data["itsm_assign_to_me"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Incident&key=' + backlogs_data["itsm_url"] + '&function_name=AssignToMe'
                        
                    backlogs_data["itsm_resolve_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Incident&key=' + backlogs_data["itsm_url"] + '&function_name=Resolve'    
                    backlogs_data["itsm_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/Object/Open.rails?class_name=IncidentManagement.Incident&key=' + backlogs_data["itsm_url"]
                    backlogs_data["itsm_parent_url"] = ''

            if 'S' in backlogs_data["ticket_number"]:
                backlogs_data["itsm_add_note_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=RequestManagement.Task&key=' + backlogs_data["itsm_url"] + '&function_name=AddNote'
                backlogs_data["itsm_reassign_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=RequestManagement.Task&key=' + backlogs_data["itsm_url"] + '&function_name=AddTaskReassign1'
                
                if user_assign != backlogs_data["taken_by"]:
                    backlogs_data["itsm_assign_to_me"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=RequestManagement.Task&key=' + backlogs_data["itsm_url"] + '&function_name=AssigntoMyself'
                    
                backlogs_data["itsm_resolve_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=RequestManagement.Task&key=' + backlogs_data["itsm_parent_url"]    
                backlogs_data["itsm_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/open.rails?class_name=RequestManagement.Task&key=' + backlogs_data["itsm_url"]
                backlogs_data["itsm_parent_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/open.rails?class_name=RequestManagement.Request&key=' + backlogs_data["itsm_parent_url"] + '&puvt=8L-7vIWb8Kc5IxCCYqVjWTPMz7IiS7yz_9pIdMSTLuY1'
                
        ## PLANNED TICKETS HAVE DIFF OSLA LOGIC ##
        # check if planned
        # comp due date = planned date
        if backlogs_data["criticality"] in ['PLANNED','P_PLANNED']:
            is_planned = 1
            backlogs_data["due_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(due_date_temp)
            if(local_tz!=settings.TIMEZONE):
                backlogs_data["due_date"] = f._convert_to_local_time(from_zone, to_zone, backlogs_data["due_date"])
                backlogs_data["open_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(backlogs_data["open_date"])
                backlogs_data["open_date"] = f._convert_to_local_time(from_zone, to_zone, backlogs_data["open_date"])
            if now_date > TZ.localize(due_date_temp):
                # expired since
                is_oosla = 1
                backlogs_data["time_remaining"] = f._format_timedelta(now_date - TZ.localize(due_date_temp))
                backlogs_data["is_time_remaining_oosla"] = is_oosla
                #oosla.append(backlogs_data)
            if not is_oosla and due_date_temp and (now_date > TZ.localize(due_date_temp - timedelta(hours=4))):
                is_soosla = 1
                backlogs_data["time_remaining"] = f._format_timedelta(f._time_remaining(TZ.localize(due_date_temp), now_date, sla_grp))
                backlogs_data["is_time_remaining_oosla"] = is_oosla
                #soosla.append(backlogs_data)
        else:
            comp_due_date = TZ.localize(f._comp_duedate_95(backlogs_data["criticality"], backlogs_data["open_date"], sla_grp, backlogs_data["ticket_type"]))

        ## OUT OF SLA ##
        # check if Out of SLA
        if not is_planned and comp_due_date and now_date > comp_due_date:
            is_oosla = 1
            backlogs_data["due_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(comp_due_date)
            if(local_tz!=settings.TIMEZONE):
                backlogs_data["due_date"] = f._convert_to_local_time(from_zone, to_zone, backlogs_data["due_date"])
                backlogs_data["open_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(backlogs_data["open_date"])
                backlogs_data["open_date"] = f._convert_to_local_time(from_zone, to_zone, backlogs_data["open_date"])
            # expired since
            backlogs_data["time_remaining"] = f._format_timedelta(now_date - comp_due_date)
            backlogs_data["is_time_remaining_oosla"] = is_oosla
            backlogs_data["open_date"] = backlogs_data["open_date"].strftime('%Y-%m-%d %H:%M:%S')
            #oosla.append(backlogs_data)

        ## SOON OUT OF SLA ##
        # check if Soon OoSLA
        if not is_planned and not is_oosla and comp_due_date and (now_date > f._comp_soosla_date(backlogs_data["criticality"], comp_due_date, sla_grp, backlogs_data["ticket_type"])):
            is_soosla = 1
            # due date overide
            backlogs_data["due_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(comp_due_date) 
            if(local_tz!=settings.TIMEZONE):
                backlogs_data["due_date"] = f._convert_to_local_time(from_zone, to_zone, backlogs_data["due_date"])
                backlogs_data["open_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(backlogs_data["open_date"])
                backlogs_data["open_date"] = f._convert_to_local_time(from_zone, to_zone, backlogs_data["open_date"])
            # add time left, convert row to list then append
            backlogs_data["time_remaining"] = f._format_timedelta(f._time_remaining(comp_due_date, now_date, sla_grp))
            backlogs_data["is_time_remaining_oosla"] = is_oosla
            #soosla.append(backlogs_data)

        #combine if else statement with check for TZ.localize(due_date) and comp_due_date
        # check if ordinary ticket
        if not is_oosla and not is_soosla:
            # if planned use normal due date
            if is_planned:
                backlogs_data["due_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(due_date_temp) 
                if(local_tz!=settings.TIMEZONE):
                    backlogs_data["due_date"] = f._convert_to_local_time(from_zone, to_zone, backlogs_data["due_date"])
                    backlogs_data["open_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(open_date_temp)
                    backlogs_data["open_date"] = f._convert_to_local_time(from_zone, to_zone, backlogs_data["open_date"])
                backlogs_data["time_remaining"] = f._format_timedelta(f._time_remaining(TZ.localize(due_date_temp), now_date, sla_grp))
                backlogs_data["is_time_remaining_oosla"] = is_oosla
            else:
                backlogs_data["due_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(comp_due_date)
                if(local_tz!=settings.TIMEZONE):
                    backlogs_data["due_date"] = f._convert_to_local_time(from_zone, to_zone, backlogs_data["due_date"])
                    backlogs_data["open_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(open_date_temp)
                    backlogs_data["open_date"] = f._convert_to_local_time(from_zone, to_zone, backlogs_data["open_date"])
                backlogs_data["time_remaining"] = f._format_timedelta(f._time_remaining(comp_due_date, now_date, sla_grp))
                backlogs_data["is_time_remaining_oosla"] = is_oosla
            #isla.append(backlogs_data)
        
        result_temp.append(backlogs_data) if bool(backlogs_data) else result_temp
        result["GRP"]=sla_grp
        
    result["backlogs_data"]=result_temp     
    return result

def get_backlogs_prio_summary_groups(request, teams=None, ticket_type=None, mode=None,source='deis'):
    logger.info('get_backlogs_prio_summary_groups')
    teams_dict = settings.ALL_TEAMS_MAPPING
    TEAMS = settings.TEAMS
    
    groups_dict = {}
    GRP_TEAMS =[]
    return_dic = {}
    
    if isinstance(teams, list):
        for sub_team in teams:
            for t in TEAMS.values():
                for t1 in t:
                    if sub_team in t1['team']:
                        GRP_TEAMS.append(sub_team)
                        groups_dict[sub_team] = t1['grp_name']
    elif teams == 'all_proximity_ds':
        keys = ['PROXI_EMEA', 'PROXI_AMERICA', 'PROXI_GLOBAL_ASIE', 'PROXI_BERLUTI', 'PROXI_FONDATION']
        for k in keys:
            for data in TEAMS[k]:
                GRP_TEAMS.extend(data['team'])
                groups_dict[", ".join(data['team'])] = data['grp_name']
    elif teams in teams_dict.keys():
        team_data = teams_dict[teams] if (teams== 'all_ams_cgi' or teams=='all_ams_valtech') else TEAMS[teams_dict[teams]]
        for data in team_data:
            for team in data['team']:
               GRP_TEAMS.append(team)
               groups_dict[team] = data['grp_name']

    logger.info('GROUP_TEAMS***************:%s'%GRP_TEAMS)

    ticket_type = ticket_type if ticket_type else 'ALL'

    TICKET_STATUS = ",".join("'{0}'".format(w) for w in settings.TICKET_STATUS)
    TICKET_TYPES = ",".join("'{0}'".format(w) for w in settings.TICKET_TYPES[ticket_type])
    
    #db_view_query = qm.get_query_by_title('_TELMASTE_HYGIE_Open')
    view = '_telmaste_hygie_open' #'({0})'.format(db_view_query)
    
    conn_str = "select group_desc as GroupName, urgency_desc as Urgency from {3} as details where `CLOSED ON` is null and ('OPEN') in ({0}) and group_desc in ({2}) and upper(ttype) in ({1});"
    
    GRP = ",".join("'{0}'".format(w) for w in GRP_TEAMS)
    
    conn_str = conn_str.format(TICKET_STATUS, TICKET_TYPES, GRP) if source == 'deis' else conn_str.format(TICKET_STATUS, TICKET_TYPES, GRP, view)
        
    db_conn = f._get_connection(request) if source=='deis' else f._get_connection_itsm(request)
    
    # check DB connection, if none return
    if db_conn is None:
        for grp_name in groups_dict.values():
            return_dic[grp_name] = []
        return_dic['all'] = []
        return return_dic
   
    cursor = db_conn.cursor()    
    logger.info("conn_str:%s"%conn_str)
    cursor.execute(conn_str)
    data=cursor.fetchall()
    f._close_connection(request, db_conn) #close DB connection
    
    low_total, med_total, high_total, plan_total = 0,0,0,0

    for t in data:
        grp_name = t[0]
        criticality = t[1]
        low, med, high, plan = 0,0,0,0
        if criticality in settings.TICKET_CRITICALITY['HIGH']:
            high += 1
        if criticality in settings.TICKET_CRITICALITY['MEDIUM']:
            med += 1
        if criticality in settings.TICKET_CRITICALITY['LOW']:
            low += 1
        if criticality in settings.TICKET_CRITICALITY['PLANNED']:
            plan += 1

        #add the grp_name to return_dic if not exists
        if groups_dict[grp_name] not in return_dic.keys():
            return_dic[groups_dict[grp_name]] = {}
            
        if high:
            #check if 'Critical/High' dict is set for the current group
            if 'Critical/High' not in return_dic[groups_dict[grp_name]].keys():
                #create new key
                return_dic[groups_dict[grp_name]]['Critical/High'] = {'label':'Critical/High', 'y':0, 'color': settings.COLOR_SCHEME['HIGH'], 'indexLabelFontColor': BLACK}
            #increase the count
            return_dic[groups_dict[grp_name]]['Critical/High']['y'] += 1
            high_total+=high
            
        if med:
            #check if Medium' dict is set for the current group
            if 'Medium' not in return_dic[groups_dict[grp_name]].keys():
                #create new key
                return_dic[groups_dict[grp_name]]['Medium'] = {'label':'Medium', 'y':0, 'color': settings.COLOR_SCHEME['MEDIUM'], 'indexLabelFontColor': BLACK}
            #increase the count
            return_dic[groups_dict[grp_name]]['Medium']['y'] += 1
            med_total+=med
            
        if low:
            #check if 'Low' dict is set for the current group
            if 'Low' not in return_dic[groups_dict[grp_name]].keys():
                #create new key
                return_dic[groups_dict[grp_name]]['Low'] = {'label':'Low', 'y':0, 'color': settings.COLOR_SCHEME['LOW'], 'indexLabelFontColor': BLACK}
            #increase the count
            return_dic[groups_dict[grp_name]]['Low']['y'] += 1
            low_total+=low
            
        if plan:
            #check if 'Planned' dict is set for the current group
            if 'Planned' not in return_dic[groups_dict[grp_name]].keys():
                #create new key
                return_dic[groups_dict[grp_name]]['Planned'] = {'label':'Planned', 'y':0, 'color': settings.COLOR_SCHEME['PLANNED'], 'indexLabelFontColor': BLACK}
            #increase the count
            return_dic[groups_dict[grp_name]]['Planned']['y'] += 1
            plan_total+=plan
            
    #Add 'all' key to return dict if not exists
    if 'all' not in return_dic.keys():
        return_dic['all'] = {}   
        
    if high_total: 
        return_dic['all']['Critical/High'] = {'label':'Critical/High', 'y':high_total, 'color': settings.COLOR_SCHEME['HIGH'], 'indexLabelFontColor': BLACK}  
    if med_total:
        return_dic['all']['Medium'] = {'label':'Medium', 'y':med_total, 'color': settings.COLOR_SCHEME['MEDIUM'], 'indexLabelFontColor': BLACK}
    if low_total:
        return_dic['all']['Low'] = {'label':'Low', 'y':low_total, 'color': settings.COLOR_SCHEME['LOW'], 'indexLabelFontColor': BLACK}
    if plan_total:
        return_dic['all']['Planned'] = {'label':'Planned', 'y':plan_total, 'color': settings.COLOR_SCHEME['PLANNED'], 'indexLabelFontColor': BLACK}
        
    #Remove the 'Critical/High','Medium','Low','Planned' keys from return_dic
    for groupname, groupdata in return_dic.items():
        return_dic[groupname] = [x for x in groupdata.values()]
        
    #append empty list to the team in return_dic that do not have data from the DB
    groups_dict_val = groups_dict.values()
    for val in groups_dict_val:
        if val not in return_dic.keys():
            return_dic[val] = []
        
    return return_dic

def get_backlogs_data(request, teams=None, local_tz=None, ticket_type=None, ticket_crit=None, assigned_to=None, source='itsm'):
    logger.info('get_backlogs_data')
    teams_dict = settings.ALL_TEAMS_MAPPING
    TEAMS = settings.TEAMS
    
    groups_dict = {}
    GRP_TEAMS =[]
    result = {}
    sla_grp='ITL4'
    
    if isinstance(teams, list):
        for sub_team in teams:
            for t in TEAMS.values():
                for t1 in t:
                    if sub_team in t1['team']:
                        GRP_TEAMS.append(sub_team)
                        groups_dict[sub_team] = t1['grp_name']
    elif teams == 'all_proximity_ds':
        keys = ['PROXI_EMEA', 'PROXI_AMERICA', 'PROXI_GLOBAL_ASIE', 'PROXI_BERLUTI', 'PROXI_FONDATION']
        for k in keys:
            for data in TEAMS[k]:
                GRP_TEAMS.extend(data['team'])
                groups_dict[", ".join(data['team'])] = data['grp_name']
    elif teams in teams_dict.keys():
        team_data = teams_dict[teams] if (teams== 'all_ams_cgi' or teams=='all_ams_valtech') else TEAMS[teams_dict[teams]]
        for data in team_data:
            for team in data['team']:
                GRP_TEAMS.append(team)
                groups_dict[team] = data['grp_name']

    logger.info('GROUP_TEAMS***************:%s'%GRP_TEAMS)
    
    ticket_type = ticket_type if ticket_type else 'ALL'
    TICKET_STATUS = ",".join("'{0}'".format(w) for w in settings.TICKET_STATUS)
    TICKET_TYPES = ",".join("'{0}'".format(w) for w in settings.TICKET_TYPES[ticket_type])
    TICKET_CRITICALITY = ",".join("'{0}'".format(w) for w in ticket_crit)
    

    #db_view_query = qm.get_query_by_title('_TELMASTE_HYGIE_Open')
    view = '_telmaste_hygie_open' #'({0})'.format(db_view_query)

    conn_str = "select `DATE OPEN` , Urgency_desc, `DUE_DATE:`, NUM_TICKET , DESCRIPTION, group_desc, assign_desc , Subject_desc, Client_desc, TTYPE as TicketType, lastmodified as LastModified, lastmodified as LastDate, status_desc as third_party, `SEQUENCE`, `SEQUENCE_Parent`, title, type as type, Client_desc as Client, User_Location as Location, '-' as problem_id, '-' as problem_sequence, '-' as problem_status from {4} as details where `CLOSED ON` is null and group_desc in ({3}) and urgency_desc in ({2}) and upper(ttype) in ({1}) and 'OPEN' in ({0});"
    
    GRP = ",".join("'{0}'".format(w) for w in GRP_TEAMS) if GRP_TEAMS else "''"
    conn_str = conn_str.format(TICKET_STATUS, TICKET_TYPES, TICKET_CRITICALITY, GRP) if source == 'deis' else conn_str.format(TICKET_STATUS, TICKET_TYPES, TICKET_CRITICALITY, GRP, view)
    
    db_conn = f._get_connection(request) if source=='deis' else f._get_connection_itsm(request)
    
    logger.info('After DB connection')
    
    isla, oosla, soosla, third_party = [], [], [], []
    result["isla"]=isla
    result["oosla"]=oosla
    result["soosla"]=soosla
    result["3party"]=third_party
    result["GRP"]=sla_grp
    
    # check DB connection, if none return
    if db_conn is None:
        return result
        
    cursor = db_conn.cursor()
    
    if assigned_to:
        conn_str = conn_str + " where [Login ID Assigned To] = upper('{0}')".format(assigned_to)

    logger.info("conn_str_list:%s"%conn_str)
    cursor.execute(conn_str)
    
    logger.info('After Dexecuting query')
    
    now_date = datetime.datetime.now(pytz.timezone(settings.TIMEZONE))
    TZ = pytz.timezone(settings.TIMEZONE)
    from_zone = tz.gettz(settings.TIMEZONE)
    to_zone = tz.gettz(local_tz)
    #import pdb;pdb.set_trace()
    incident_list = list() # incident list init for calculation current group time duration
    data=cursor.fetchall()
    f._close_connection(request,db_conn)
    
    from_zone_ivanti_sr = tz.gettz(settings.TIMEZONE_IVANTI_DB_SR)
    from_zone_ivanti_inc = tz.gettz(settings.TIMEZONE_IVANTI_DB_INC)

    #import pdb;pdb.set_trace()
    for t in data:   
        backlogs_data = {}
        # convert dbrows to list and replace None in data to ''
        t = ['' if d is None else d for d in t]
                
        if source=='deis':
            t.extend(['Without', '', '', '', 'Incident', '', '', '', '']) #append the missing 9 columns making it similar to itsm data
		
        default_keys = ["open_date","criticality","due_date","ticket_number","description","raw_group_name","taken_by","category","open_by","ticket_type","is_sleeping_ticket",
                        "duration_at_group","ticket_status","itsm_url","itsm_parent_url","title","task_type","client","location","problem_id","problem_sequence","problem_status"]	
        
        backlogs_data = dict(zip(default_keys, t)) #form dict with default_keys and DB data(t)
        
        backlogs_data["sequence"] = backlogs_data["itsm_url"]
        
        backlogs_data["problem_url"] = "https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/Object/Open.rails?class_name=ProblemManagement.Problem&key=" + backlogs_data["problem_sequence"] if backlogs_data["problem_sequence"] else ''
            
        #change the sla group
        if backlogs_data["raw_group_name"] in settings.SLA_GRP_AMS_CGI:
            sla_grp = 'AMS_CGI'
        elif backlogs_data["raw_group_name"] in settings.SLA_GRP_AMS_VALTECH:
            sla_grp = 'AMS_VALTECH'
        else:
            for key, value in TEAMS.items():
                for v in value:
                    if backlogs_data["raw_group_name"] in v['team']:
                        if key in settings.SLA_GRP_PROXIMITY:
                            sla_grp = 'EMEA'
                        else:
                            sla_grp = key
        
        # temporary "due_date" assignment for specific format
        due_date_temp = backlogs_data["due_date"]
        open_date_temp = backlogs_data["open_date"]

        backlogs_data["group_name"] = [x for x in settings.ALL_TEAMS if backlogs_data["raw_group_name"] in x['team']][0]['name']

        # taken by set to '' if none
        backlogs_data["taken_by"] = backlogs_data["taken_by"] if backlogs_data["taken_by"] else ''

        comp_due_date, is_oosla, is_planned, is_soosla, is_third_party = None, 0, 0, 0, 0
        backlogs_data["ticket_type"] = backlogs_data["ticket_type"] if backlogs_data["ticket_type"] else 'Incident'

        # sleeping ticket, ticket not updated the past 24 hrs  
        #json dumps to convert python False to false to pass safely to frontend
        backlogs_data["is_sleeping_ticket"] = json.dumps(TZ.localize(backlogs_data["is_sleeping_ticket"]) > (now_date - timedelta(hours=24))) 

        user_assign = str(request.user.first_name) + " " + str(request.user.last_name)
        
        if backlogs_data["task_type"] is '':
            backlogs_data["task_type"] = "Request Task"
        elif "User" in backlogs_data["task_type"]:
            backlogs_data["task_type"] = "USL"
        elif "Application" in backlogs_data["task_type"]:
            backlogs_data["task_type"] = "ASL"
        
        # Create the clickable link                                    
        if source!='deis':
            if "I" in backlogs_data["ticket_number"]:
                if "-" in backlogs_data["ticket_number"]:
                    backlogs_data["itsm_add_note_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Task&key=' + backlogs_data["itsm_url"] + '&function_name=AddNote'
                    backlogs_data["itsm_reassign_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Task&key=' + backlogs_data["itsm_url"] + '&function_name=AddLVReassignTask'
                    
                    if user_assign != backlogs_data["taken_by"]:
                        backlogs_data["itsm_assign_to_me"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Task&key=' + backlogs_data["itsm_url"] + '&function_name=AssigntoMe'
                    
                    if backlogs_data["ticket_status"] == 'With 3rd Party':
                        backlogs_data["itsm_back_from_thirdparty_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Task&key=' + backlogs_data["itsm_url"] + '&function_name=BackFrom3rdParty'
                        backlogs_data["itsm_resolve_url"] = ''
                    else:
                        backlogs_data["itsm_resolve_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Task&key=' + backlogs_data["itsm_url"] + '&function_name=Complete'
                        backlogs_data["itsm_back_from_thirdparty_url"] = ''
                        
                    #backlogs_data["itsm_resolve_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Task&key=' + backlogs_data["itsm_url"] + '&function_name=Complete'
                    backlogs_data["itsm_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/open.rails?class_name=IncidentManagement.Task&key=' + backlogs_data["itsm_url"] + '&parent_class_name=IncidentManagement.Incident&parent_key=' + backlogs_data["itsm_parent_url"] + '&parent_attribute=Tasks'
                    backlogs_data["itsm_parent_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/Object/Open.rails?class_name=IncidentManagement.Incident&key=' + backlogs_data["itsm_parent_url"]
                    
                else:
                    backlogs_data["itsm_add_note_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Incident&key=' + backlogs_data["itsm_url"] + '&function_name=AddNote'
                    backlogs_data["itsm_reassign_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Incident&key=' + backlogs_data["itsm_url"] + '&function_name=AddLVAssignment'
                    
                    if user_assign != backlogs_data["taken_by"]:
                        backlogs_data["itsm_assign_to_me"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Incident&key=' + backlogs_data["itsm_url"] + '&function_name=AssignToMe'
                        
                    if backlogs_data["ticket_status"] == 'With 3rd Party':
                        backlogs_data["itsm_back_from_thirdparty_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Incident&key=' + backlogs_data["itsm_url"] + '&function_name=BackFrom3rdParty'
                        backlogs_data["itsm_resolve_url"] = ''
                    else:
                        backlogs_data["itsm_back_from_thirdparty_url"] = ''
                        backlogs_data["itsm_resolve_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Incident&key=' + backlogs_data["itsm_url"] + '&function_name=Resolve'
                    
                    backlogs_data["itsm_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/Object/Open.rails?class_name=IncidentManagement.Incident&key=' + backlogs_data["itsm_url"]
                    backlogs_data["itsm_parent_url"] = ''
                               
            if 'S' in backlogs_data["ticket_number"]:
                backlogs_data["itsm_add_note_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=RequestManagement.Task&key=' + backlogs_data["itsm_url"] + '&function_name=AddNote'
                backlogs_data["itsm_reassign_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=RequestManagement.Task&key=' + backlogs_data["itsm_url"] + '&function_name=AddTaskReassign1'
                
                if user_assign != backlogs_data["taken_by"]:
                    backlogs_data["itsm_assign_to_me"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=RequestManagement.Task&key=' + backlogs_data["itsm_url"] + '&function_name=AssigntoMyself'
                    
                backlogs_data["itsm_resolve_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=RequestManagement.Task&key=' + backlogs_data["itsm_url"] + '&function_name=Complete'
                # Create the clickable link 
                backlogs_data["itsm_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/open.rails?class_name=RequestManagement.Task&key=' + backlogs_data["itsm_url"]
                backlogs_data["itsm_parent_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/open.rails?class_name=RequestManagement.Request&key=' + backlogs_data["itsm_parent_url"] + '&puvt=8L-7vIWb8Kc5IxCCYqVjWTPMz7IiS7yz_9pIdMSTLuY1'

        ## PLANNED TICKETS HAVE DIFF OSLA LOGIC ##
        # check if planned
        # comp due date = planned date
        if backlogs_data["criticality"] in ['PLANNED','P_PLANNED']:
            is_planned = 1
            backlogs_data["due_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(due_date_temp)
            backlogs_data["open_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(open_date_temp)
            if source!='deis' and "I" in backlogs_data["ticket_number"]:
                backlogs_data["due_date"] = f._convert_to_local_time(from_zone_ivanti_inc, to_zone, backlogs_data["due_date"]) 
                backlogs_data["open_date"] = f._convert_to_local_time(from_zone_ivanti_inc, to_zone, backlogs_data["open_date"])
            if source!='deis' and "S" in backlogs_data["ticket_number"]:
                backlogs_data["due_date"] = f._convert_to_local_time(from_zone_ivanti_sr, to_zone, backlogs_data["due_date"]) 
                backlogs_data["open_date"] = f._convert_to_local_time(from_zone_ivanti_sr, to_zone, backlogs_data["open_date"])
            if now_date > TZ.localize(due_date_temp):
                # expired since
                is_oosla = 1
                backlogs_data["time_remaining"] = f._format_timedelta(now_date - TZ.localize(due_date_temp))
                backlogs_data["is_time_remaining_oosla"] = is_oosla
                oosla.append(backlogs_data)
            if not is_oosla and due_date_temp and (now_date > TZ.localize(due_date_temp - timedelta(hours=4))):
                is_soosla = 1
                backlogs_data["time_remaining"] = f._format_timedelta(f._time_remaining(TZ.localize(due_date_temp), now_date, sla_grp))
                backlogs_data["is_time_remaining_oosla"] = is_oosla
                soosla.append(backlogs_data)
        else:
            backlogs_data["open_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(open_date_temp)
            comp_due_date = TZ.localize(f._comp_duedate_95(backlogs_data["criticality"], open_date_temp, sla_grp, backlogs_data["ticket_type"]))
            due_date_temp = f._comp_duedate_95(backlogs_data["criticality"], open_date_temp, sla_grp, backlogs_data["ticket_type"])
            backlogs_data["due_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(due_date_temp)
            if source!='deis' and "I" in backlogs_data["ticket_number"]:
                backlogs_data["due_date"] = f._convert_to_local_time(from_zone_ivanti_inc, to_zone, backlogs_data["due_date"]) 
                backlogs_data["open_date"] = f._convert_to_local_time(from_zone_ivanti_inc, to_zone, backlogs_data["open_date"])
            if source!='deis' and "S" in backlogs_data["ticket_number"]:
                backlogs_data["due_date"] = f._convert_to_local_time(from_zone_ivanti_sr, to_zone, backlogs_data["due_date"]) 
                backlogs_data["open_date"] = f._convert_to_local_time(from_zone_ivanti_sr, to_zone, backlogs_data["open_date"])

        ## OUT OF SLA ##
        # check if Out of SLA
        if not is_planned and comp_due_date and now_date > comp_due_date and backlogs_data["ticket_status"] != 'With 3rd Party':
            is_oosla = 1 
            # expired since
            backlogs_data["time_remaining"] = f._format_timedelta(now_date - comp_due_date)
            backlogs_data["is_time_remaining_oosla"] = is_oosla
            oosla.append(backlogs_data)

        ## SOON OUT OF SLA ##
        # check if Soon OoSLA
        if not is_planned and not is_oosla and comp_due_date and (now_date > f._comp_soosla_date(backlogs_data["criticality"], comp_due_date, sla_grp, backlogs_data["ticket_type"])) and backlogs_data["ticket_status"] != 'With 3rd Party':
            is_soosla = 1
            # add time left, convert row to list then append
            backlogs_data["time_remaining"] = f._format_timedelta(f._time_remaining(comp_due_date, now_date, sla_grp))
            backlogs_data["is_time_remaining_oosla"] =  is_oosla
            soosla.append(backlogs_data)
            
        ## third Party ##
        # check if third party
        if backlogs_data["ticket_status"] == 'With 3rd Party':
            is_third_party = 1
            # add time left, convert row to list then append
            if (now_date > comp_due_date):
                backlogs_data["time_remaining"] = f._format_timedelta(now_date - comp_due_date)
                is_oosla = 1
            else:
                backlogs_data["time_remaining"] = f._format_timedelta(f._time_remaining(comp_due_date, now_date, sla_grp))

            backlogs_data["is_time_remaining_oosla"] =  is_oosla
            third_party.append(backlogs_data)
            
        # check if ordinary ticket
        if not is_oosla and not is_soosla and not is_third_party:
            # if planned use normal due date
            if is_planned:
                backlogs_data["time_remaining"] = f._format_timedelta(f._time_remaining(TZ.localize(due_date_temp), now_date, sla_grp))
                backlogs_data["is_time_remaining_oosla"] = is_oosla
            else:
                backlogs_data["time_remaining"] = f._format_timedelta(f._time_remaining(comp_due_date, now_date, sla_grp))
                backlogs_data["is_time_remaining_oosla"] = is_oosla
            isla.append(backlogs_data)
            
            
        # duration at group
        if(backlogs_data["duration_at_group"] is None):
            if(local_tz!=settings.TIMEZONE):
                backlogs_data["duration_at_group"] = datetime.datetime.strptime(backlogs_data["open_date"], "%Y-%m-%d %H:%M:%S")
            else:
                backlogs_data["duration_at_group"] = backlogs_data["open_date"]
        duration_at_group = now_date - TZ.localize(backlogs_data["duration_at_group"])
        backlogs_data["duration_at_group"] = f._format_timedelta(duration_at_group)
        
        if(duration_at_group>timedelta(days = 7)):
            backlogs_data["is_duration_at_group_oosla"] = 1
        else:
            backlogs_data["is_duration_at_group_oosla"] = 0
                  
        result["isla"]=isla
        result["oosla"]=oosla
        result["soosla"]=soosla
        result["3party"]=third_party
        result["GRP"]=sla_grp
    return result

def get_backlogs_sla_summary_groups(request, teams=None, ticket_type=None, source='itsm'): 
    logger.info('get_backlogs_summary_groups')
    teams_dict = settings.ALL_TEAMS_MAPPING
    #groups_dict = {'WW_TM_L3_TSC_AS400':'as400', 'WW_TM_L3_TSC_WINDOWS_SAFE_SCOL':'winix' , 'WW_TM_L3_TSC_UNIX':'winix' ,'WW_TM_L3_TSC_WEB_APPLIS':'webapps','WW_TM_L3_TSC_BI_DATABASE':'bi_db', 'WW_TM_L3_TSC_MIDDLEWARE':'middleware', 
    #               'WW_TM_L1_SOC':'soc', 'ALL_TEAMS':'all'}
    TEAMS = settings.TEAMS
   
    groups_dict = {}
    GRP_TEAMS =[]
    return_dic={}
    sla_grp="ITL4"
    
    #individual team list calls like [WW_TM_L3_TSC_AS400], [WW_TM_L3_TSC_BI_DATABASE] etc..
    if isinstance(teams, list):
        for sub_team in teams:
            for t in TEAMS.values():
                for t1 in t:
                    if sub_team in t1['team']:
                        GRP_TEAMS.append(sub_team)
                        groups_dict[sub_team] = t1['grp_name']
    #all_proximity_ds teams                        
    elif teams == 'all_proximity_ds':
        keys = ['PROXI_EMEA', 'PROXI_AMERICA', 'PROXI_GLOBAL_ASIE', 'PROXI_BERLUTI', 'PROXI_FONDATION']
        for k in keys:
            for data in TEAMS[k]:
                if len(data['team']) > 1:
                    for t1 in data['team']:
                        GRP_TEAMS.extend(t1)
                        groups_dict[", ".join(t1)] = data['grp_name']
                else:
                    GRP_TEAMS.extend(data['team'])
                    groups_dict[", ".join(data['team'])] = data['grp_name']
    #all_teams like all_tsc, all_itl4, all_service_desk, all_ams etc
    elif teams in teams_dict.keys():    
        team_data = teams_dict[teams] if (teams== 'all_ams_cgi' or teams=='all_ams_valtech') else TEAMS[teams_dict[teams]]
        for data in team_data:
            for team in data['team']:
                GRP_TEAMS.append(team)
                groups_dict[team] = data['grp_name']

    logger.info('GROUP_TEAMS***************:%s'%GRP_TEAMS)
    
    ticket_type = ticket_type if ticket_type else 'ALL'
    TICKET_STATUS = ",".join("'{0}'".format(w) for w in settings.TICKET_STATUS)
    TICKET_TYPES = ",".join("'{0}'".format(w) for w in settings.TICKET_TYPES[ticket_type])


    #db_view_query = qm.get_query_by_title('_TELMASTE_HYGIE_Open')
    view = '_telmaste_hygie_open' #'({0})'.format(db_view_query)
        
    conn_str = "select `DATE OPEN`, urgency_desc as Urgency, `DUE_DATE:` , TTYPE, group_desc  as GroupName, Status_desc as third_party from {3} as details where `CLOSED ON` is null and ('OPEN') in ({0}) and group_desc in ({2}) and upper(ttype) in ({1});"

    GRP = ",".join("'{0}'".format(w) for w in GRP_TEAMS)
    conn_str = conn_str.format(TICKET_STATUS, TICKET_TYPES, GRP) if source == 'deis' else conn_str.format(TICKET_STATUS, TICKET_TYPES, GRP, view)
    logger.info('conn_str:%s'%conn_str)

    db_conn = f._get_connection(request) if source=='deis' else f._get_connection_itsm(request)
    
    # check DB connection, if none return
    if db_conn is None:
        for grp_name in groups_dict.values():
            return_dic[grp_name] = []
        return_dic['all'] = []
        return return_dic
        
    insla_count_all, oosla_count_all, soosla_count_all, third_party_count_all  = 0, 0, 0, 0
    cursor = db_conn.cursor()
    cursor.execute(conn_str)
    
    now_date = datetime.datetime.now(pytz.timezone(settings.TIMEZONE))
    TZ = pytz.timezone(settings.TIMEZONE)
    
    data=cursor.fetchall()
    f._close_connection(request, db_conn) #close DB connection
    
    for t in data:
        insla_count, oosla_count, soosla_count, third_party_count = 0, 0, 0, 0
        open_date, criticality, due_date, comp_due_date, is_oosla, is_planned, is_soosla, is_third_party, ttype, grp_name, third_party  = t[0], t[1], t[2], None, False, False, False, False, t[3], t[4], t[5]

        #Make PROXIMITY teams to 'EMEA' to select SLA_HOURS
        if grp_name in settings.SLA_GRP_AMS_CGI:
            sla_grp = 'AMS_CGI'
        elif grp_name in settings.SLA_GRP_AMS_VALTECH:
            sla_grp = 'AMS_VALTECH'
        else:
            for key, value in TEAMS.items():
                for v in value:
                    if grp_name in v['team']:
                        if key in settings.SLA_GRP_PROXIMITY:
                            sla_grp = 'EMEA'
                        else:
                            sla_grp = key
      
        if criticality in ['PLANNED','P_PLANNED']:
            is_planned = True
            if now_date > TZ.localize(due_date):
                is_oosla = True
                oosla_count += 1
            if not is_oosla and due_date and (now_date > TZ.localize(due_date - timedelta(hours=4))):
                is_soosla = True
                soosla_count += 1
        else:
            comp_due_date = TZ.localize(f._comp_duedate_95(criticality, open_date, sla_grp, ttype))
                
        # check if Out of SLA
        if not is_planned and comp_due_date and now_date > comp_due_date and third_party != 'With 3rd Party':
            is_oosla = True
            oosla_count += 1

        # check if Soon OoSLA
        if not is_planned and not is_oosla and comp_due_date and (now_date > f._comp_soosla_date(criticality, comp_due_date, sla_grp, ttype)) and third_party != 'With 3rd Party':
            is_soosla = True
            soosla_count += 1

        # check if with third party
        if third_party =='With 3rd Party':
            is_third_party = True
            third_party_count += 1
                
        # check if ordirary ticket
        if not is_oosla and not is_soosla and not is_third_party:
            insla_count += 1

        #add the grp_name to return_dic if not exists
        if groups_dict[grp_name] not in return_dic.keys():
            return_dic[groups_dict[grp_name]] = {}

        if insla_count:
            #check if insla dict is set for the current group
            if 'insla' not in return_dic[groups_dict[grp_name]].keys():
                #create new key
                return_dic[groups_dict[grp_name]]['insla'] = {'label':'In SLA', 'y': 0, 'color': settings.COLOR_SCHEME['SLA'], 'indexLabelFontColor': BLACK}
            #increase the count
            return_dic[groups_dict[grp_name]]['insla']['y'] += 1
            insla_count_all+=insla_count
            
        if oosla_count:
            #check if oosla dict is set for the current group
            if 'oosla' not in return_dic[groups_dict[grp_name]].keys():
                #create new key
                return_dic[groups_dict[grp_name]]['oosla'] = {'label':'OoSLA', 'y': 0, 'color': settings.COLOR_SCHEME['OSLA'], 'indexLabelFontColor': BLACK}
            #increase the count
            return_dic[groups_dict[grp_name]]['oosla']['y'] += 1
            oosla_count_all+=oosla_count
                
        if soosla_count:
            #check if soosla dict is set for the current group
            if 'soosla' not in return_dic[groups_dict[grp_name]].keys():
                #create new key
                return_dic[groups_dict[grp_name]]['soosla'] = {'label':'SOoSLA', 'y': 0, 'color': settings.COLOR_SCHEME['SOOSLA'], 'indexLabelFontColor': BLACK}
            #increase the count
            return_dic[groups_dict[grp_name]]['soosla']['y'] += 1
            soosla_count_all+=soosla_count
        
        if third_party_count:
            #check if third_party dict is set for the current group
            if '3rdparty' not in return_dic[groups_dict[grp_name]].keys():
                #create new key
                return_dic[groups_dict[grp_name]]['3rdparty'] = {'label':'3rd Party', 'y': 0, 'color': settings.COLOR_SCHEME['THIRDPARTY'], 'indexLabelFontColor': BLACK}
            #increase the count
            return_dic[groups_dict[grp_name]]['3rdparty']['y'] += 1
            third_party_count_all+=third_party_count

    #Add 'all' key to return dict if not exists
    if 'all' not in return_dic.keys():
        return_dic['all'] = {}

    #Add the total insla, oosla, soosla, 3rdparty count
    if insla_count_all:
        return_dic['all']['insla'] = {'label':'In SLA', 'y': insla_count_all, 'color': settings.COLOR_SCHEME['SLA'], 'indexLabelFontColor': BLACK}
    if oosla_count_all:
        return_dic['all']['oosla'] = {'label':'OoSLA', 'y': oosla_count_all, 'color': settings.COLOR_SCHEME['OSLA'], 'indexLabelFontColor': BLACK}
    if soosla_count_all:
        return_dic['all']['soosla'] = {'label':'SOoSLA', 'y': soosla_count_all, 'color': settings.COLOR_SCHEME['SOOSLA'], 'indexLabelFontColor': BLACK}
    if third_party_count_all:
        return_dic['all']['3rdparty'] = {'label':'3rd Party', 'y': third_party_count_all, 'color': settings.COLOR_SCHEME['THIRDPARTY'], 'indexLabelFontColor': BLACK}

    #Remove the 'insla','oosla','soosla','3rdparty' keys from return_dic
    for groupname, groupdata in return_dic.items():
        return_dic[groupname] = [x for x in groupdata.values()]

    #append empty list to the team in return_dic that do not have data from the DB
    groups_dict_val = groups_dict.values()
    for val in groups_dict_val:
        if val not in return_dic.keys():
            return_dic[val] = []

    return return_dic

'''
#uses only ticket number
def get_notes(ticket_no, isclosed, local_tz, source):
    return f._get_notes(ticket_no, isclosed, local_tz, source)
'''
#uses ticket sequence to retrieve data
def get_notes(request, ticket_no, ticket_seq, isclosed, local_tz, source):
    return f._get_notes(request, ticket_no, ticket_seq, isclosed, local_tz, source)

def combine_dicts(a, b, op=operator.add):
    return dict(a.items() + b.items() + [(k, op(a[k], b[k])) for k in set(b) & set(a)])

class MyDict(dict):
    def __add__(self, oth):
        r = self.copy()

        try:
            for key, val in oth.items():
                if key in r:
                    r[key] += val  # You can custom it here
                else:
                    r[key] = val
        except AttributeError:  # In case oth isn't a dict
            return NotImplemented  # The convention when a case isn't handled

        return r

