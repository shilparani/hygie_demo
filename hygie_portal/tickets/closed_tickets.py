# -*- coding: utf-8 -*-
"""Helper utilities and decorators."""
from datetime import timedelta
from dateutil import tz
import datetime
import pytz
from hygie_portal.tickets import common as f
import numpy as np
import collections
from hygie.settings import logger
from django.conf import settings
from hygie_portal.querymanager import querymanager as qm

# Constants
GREEN, YELLOW, RED, WHITE, BLACK = "#5cb85c", "#f0ad4e", "#d9534f", "#ffffff", "#000000"



def get_closed_tickets(request, filters_dic, local_tz, source='deis'):
    logger.info('get_closed_tickets')
    TEAMS = settings.TEAMS
    ALL_TEAMS =  settings.ALL_TEAMS
    
    from_zone = pytz.timezone(local_tz)
    to_zone = pytz.timezone(settings.TIMEZONE)
	
    closed_data = []
    sla_grp="ITL4"

    conn_str = "select `CLOSED ON`, `DATE OPEN`, urgency_desc as Urgency, `DUE_DATE:`, num_ticket, `DESCRIPTION`, {0}, `RESOLUTION`, subject_desc as SubjectID, `CLOSED_GROUP:`, TTYPE, `OPENED_GROUP:`, close_category, null as OZI_ZONE, OSI_CATEG_TXT01, Client_desc as Client, User_Location as Location, Group_DESC, status, `sequence`, `SEQUENCE_Parent`, '-' as problem_id, '-' as problem_sequence, '-' as problem_status, '-' as curr_assigned_group, '-' as curr_assigned_analyst, status_desc from {1} as details where "
    
    conditions_list_open = []
    conditions_list_close = []   
    
    if filters_dic['close_check_tickets']:
        condition = "`CLOSED ON` is not null"
        conditions_list_close.append(condition)
        
    if filters_dic['open_check_tickets']:
        condition = "`CLOSED ON` is null"
        conditions_list_open.append(condition)
        
    if filters_dic["open_start_date"] is not None and filters_dic["open_end_date"] is not None:
        open_start_date = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(from_zone.localize(filters_dic["open_start_date"]).astimezone(to_zone))
        open_end_date = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(from_zone.localize(filters_dic["open_end_date"]).astimezone(to_zone))
        #condition = "(([Open Date] >= CAST( '{0}' AS DATE) and [Open Date] <= CAST( '{1}' AS DATE)))".format(filters_dic["open_start_date"], filters_dic["open_end_date"]) if source=='deis' else "(CAST([DATE OPEN] AS DATE) >= CAST('{0}' AS DATE) and CAST([DATE OPEN] AS DATE) <= CAST('{1}' AS DATE))".format(filters_dic["open_start_date"], filters_dic["open_end_date"])
        #condition = "(([Open Date] >= CAST( '{0}' AS DATE) and [Open Date] < CAST( '{1}' AS DATE)))".format(open_start_date, open_end_date) if source=='deis' else "(CAST([DATE OPEN] AS DATE) >= CAST('{0}' AS DATE) and CAST([DATE OPEN] AS DATE) < CAST('{1}' AS DATE))".format(open_start_date, open_end_date)
        condition = "(`DATE OPEN` between '{0}' and '{1}')".format(open_start_date, open_end_date)
        conditions_list_open.append(condition)
        conditions_list_close.append(condition)

    if filters_dic["close_start_date"] is not None and filters_dic["close_end_date"] is not None:
        close_start_date = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(from_zone.localize(filters_dic["close_start_date"]).astimezone(to_zone))
        close_end_date = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(from_zone.localize(filters_dic["close_end_date"]).astimezone(to_zone))
        #condition = "(([Close Date] >= CAST( '{0}' AS DATE) and [Close Date] <= CAST( '{1}' AS DATE)))".format(filters_dic["close_start_date"], filters_dic["close_end_date"]) if source=='deis' else "(CAST([CLOSED ON] AS DATE) >= CAST('{0}' AS DATE) and CAST([CLOSED ON] AS DATE) <= CAST('{1}' AS DATE))".format(filters_dic["close_start_date"], filters_dic["close_end_date"])
        #condition = "(([Close Date] >= CAST( '{0}' AS DATE) and [Close Date] < CAST( '{1}' AS DATE)))".format(close_start_date, close_end_date) if source=='deis' else "(CAST([CLOSED ON] AS DATE) >= CAST('{0}' AS DATE) and CAST([CLOSED ON] AS DATE) < CAST('{1}' AS DATE))".format(close_start_date, close_end_date)
        condition = "(`CLOSED ON` between '{0}' and '{1}')".format(close_start_date, close_end_date)
        conditions_list_close.append(condition)
        
    if filters_dic["groups_by_closedgroup"]:
        groups_by_closedgroup = [int(i) for i in filters_dic["groups_by_closedgroup"]] #map(int, filters_dic["groups_by_closedgroup"])
        closegroup_selected = [g['team'] for g in ALL_TEAMS if g['id'] in groups_by_closedgroup]    
        #if 'FO_ACCESS_COLLAB' in closegroup_selected:
        #    closegroup_selected.extend(['WW_TM_L2_ACCESS_RIGHTS', 'WW_TM_L2_COLLABORATIVE_TOOLS'])
        closegroup_selected = flatten(closegroup_selected)
        
        # if others is selected
        if -1 in groups_by_closedgroup:
            others_closegroup_selected = [g['team'] for g in ALL_TEAMS if g['team'] not in closegroup_selected]
            others_closegroup_selected = flatten(others_closegroup_selected)
            others_closegroup_selected_str = ",".join("'{0}'".format(w) for w in others_closegroup_selected)
            condition = "`CLOSED_GROUP:` not in (" + others_closegroup_selected_str + ")"
        else:
            closegroup_selected_str = ",".join("'{0}'".format(w) for w in closegroup_selected)
            condition = "`CLOSED_GROUP:` in (" + closegroup_selected_str + ")"
        conditions_list_close.append(condition)
    
    if filters_dic["groups_by_opengroup"]:
        groups_by_opengroup = [int(i) for i in filters_dic["groups_by_opengroup"]] #map(int, filters_dic["groups_by_opengroup"])
        opengroup_selected = [g['team'] for g in ALL_TEAMS if g['id'] in groups_by_opengroup]                                                                         
        opengroup_selected = flatten(opengroup_selected)
        
        # if others is selected
        if -1 in groups_by_opengroup:
            others_opengroup_selected = [g['team'] for g in ALL_TEAMS if g['team'] not in opengroup_selected]                                                                                         
            others_opengroup_selected = flatten(others_opengroup_selected)
            others_opengroup_selected_str = ",".join("'{0}'".format(w) for w in others_opengroup_selected)
            condition = "`OPENED_GROUP:` not in (" + others_opengroup_selected_str + ")"
        else:
            opengroup_selected_str = ",".join("'{0}'".format(w) for w in opengroup_selected)
            condition = "`OPENED_GROUP:` in (" + opengroup_selected_str + ")"
        conditions_list_open.append(condition)
        conditions_list_close.append(condition)
        
    if filters_dic["groups_by_assignedgroup"]:
        groups_by_assignedgroup = [int(i) for i in filters_dic["groups_by_assignedgroup"]] #map(int, filters_dic["groups_by_assignedgroup"])
        assignedgroup_selected = [g['team'] for g in ALL_TEAMS if g['id'] in groups_by_assignedgroup]    
        assignedgroup_selected = flatten(assignedgroup_selected)
        assignedgroup_selected_str = ",".join("'{0}'".format(w) for w in assignedgroup_selected)
        condition = "`Group_DESC` in (" + assignedgroup_selected_str + ")"
        conditions_list_open.append(condition)
        
    if filters_dic["groups_by_applications"]:
        groups_by_applications = [str(i) for i in filters_dic["groups_by_applications"]] #map(str, filters_dic["groups_by_applications"])
        applications_selected_str = ",".join("'{0}'".format(w) for w in groups_by_applications)    
        condition = "`Subject_DESC` in (" + applications_selected_str + ")"
        logger.info("conn_query_sql TICKET SEARCH APPLICATIONS LIST: %s" %condition)
        conditions_list_open.append(condition)
        conditions_list_close.append(condition)
    
    
    #current_app.logger.info("CONDITIONS LIST CLOSE :: ",conditions_list_close)
    #current_app.logger.info("CONDITIONS LIST OPEN :: ",conditions_list_open)
    
    ''' TO BE ADDED LATER '''    
    #if filters_dic["bounce_check"]:
    #    condition = "BOUNCING > 0"
    #    conditions_list_open.append(condition)
    #    conditions_list_close.append(condition)
    
    #conn_str = conn_str + " and ".join(conditions_list)
    
    db_conn = f._get_connection(request) if source=='deis' else f._get_connection_itsm(request)
    return_data = {
        "closed_data" : [],
        "SLA1" : 0,
        "SLA1_P" : '0.00',
        "SLA2" : 0,
        "SLA2_P" : '0.00',
        "OSLA" : 0,
        "OSLA_P" : '0.00',
        "OSLA_DATA" : {},
        "open_tickets_count" : 0,
        "closed_tickets_count" : 0
    }
    
    # check DB connection, if none return
    if db_conn is None:
        return return_data
    
    cursor = db_conn.cursor()
	
    open_tickets, closed_tickets, tickets_list = [], [], []
    if filters_dic['open_check_tickets']:
        #db_view_query = qm.get_query_by_title('_TELMASTE_HYGIE_Open')
        view = '_telmaste_hygie_open' #'({0})'.format(db_view_query)
        conn_str_open = conn_str.format('`LASTUSER`',view) 
        conn_str_open = conn_str_open + " and ".join(conditions_list_open) + " LIMIT 5000;"
        logger.info("conn_query_sql TICKET SEARCH OPEN TICKETS: %s" %conn_str_open)
        cursor.execute(conn_str_open)
        open_tickets = cursor.fetchall()
    
    if filters_dic['close_check_tickets']:
        #db_view_query = qm.get_query_by_title('_TELMASTE_HYGIE_')
        view = '_telmaste_hygie_' #'({0})'.format(db_view_query)
        conn_str_close = conn_str.format('`CLOSE_BY`',view)
        conn_str_close = conn_str_close + " and ".join(conditions_list_close) + " LIMIT 5000;"
        logger.info("conn_query_sql TICKET SEARCH CLOSE TICKETS: %s" %conn_str_close)
        cursor.execute(conn_str_close)
        closed_tickets = cursor.fetchall()
        
    f._close_connection(request, db_conn) #close DB connection
    
    open_tickets =  list(open_tickets)
    closed_tickets =  list(closed_tickets)
    tickets_list = open_tickets + closed_tickets
    
    open_tickets_count = len(open_tickets)
    closed_tickets_count =  len(closed_tickets)

    now_date = datetime.datetime.now()
    TZ = pytz.timezone(settings.TIMEZONE)
    from_zone = tz.gettz(settings.TIMEZONE)
    to_zone = tz.gettz(local_tz)
   
    sla1_total, sla2_total, osla_total = 0, 0, 0
    latetime_total = 0
    l_list = {} 
    
    for t in tickets_list:
        closed_ticket_data = {}
        comp_due_date, is_oosla, is_planned, is_insla = None, False, False, False
        
        # convert dbrows to list and replace None in data to ''
        t = ['' if d is None else d for d in t]
        
        default_keys = ["close_date","open_date","criticality","due_date","ticket_number","description","taken_by","resolution","category","closed_group_raw","ticket_type","opened_group","resolution_category","zone","process","client","location","assigned_to","status","sequence","itsm_parent_url", "problem_id", "problem_sequence", "problem_status", "curr_assigned_group", "curr_assigned_analyst", "ticket_status"]
        closed_ticket_data = dict(zip(default_keys, t)) #form dict with default_keys and DB data(t)
        
        closed_ticket_data["itsm_url"] = closed_ticket_data["sequence"]
        
        close_date = closed_ticket_data["close_date"]
        open_date = closed_ticket_data["open_date"]
        due_date = closed_ticket_data["due_date"]
        criticality = closed_ticket_data["criticality"]
        ttype = closed_ticket_data["ticket_type"]
		
        #group_for_sla_calc = "closed_group_raw" if closed_ticket_data["status"] == 'C' else "assigned_to"
        group_for_sla_calc = "curr_assigned_group"
        
        closed_ticket_data["problem_url"] = "https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/Object/Open.rails?class_name=ProblemManagement.Problem&key=" + closed_ticket_data["problem_sequence"] if closed_ticket_data["problem_sequence"] else ''
        
        
		#change the sla group
        if closed_ticket_data[group_for_sla_calc] in settings.SLA_GRP_AMS_CGI:
            sla_grp = 'AMS_CGI'
        elif closed_ticket_data[group_for_sla_calc] in settings.SLA_GRP_AMS_VALTECH:
            sla_grp = 'AMS_VALTECH'
        #elif closed_ticket_data["closed_group"] in ['CP_OS_L2_HQ', 'CP_OS_L2_HQ_P9_RIVOLI', 'CP_OS_L2_HQ_BAILLEUL_34L_36L', 'CP_OS_L2_HQ_TECHBAR']:
        #    grp_check = 'CP_HQ'
        else:
            for key, value in TEAMS.items():
                for v in value:
                    if (closed_ticket_data[group_for_sla_calc] in v['team']):
                        closed_ticket_data["curr_assigned_group_name"] = v['name']
                        if key in settings.SLA_GRP_PROXIMITY:
                            sla_grp = 'EMEA'
                        else:
                            sla_grp = key                        

        # get the 'grp_name' of closed group from settings.py
        
        if closed_ticket_data["status"] == 'C':
            for t in TEAMS.values():
                for t1 in t:
                    if closed_ticket_data["closed_group_raw"] in t1['team']:
                        closed_ticket_data["closed_group"] = t1['name'] 
        else:
            closed_ticket_data["closed_group"] = closed_ticket_data["closed_group_raw"]
        
        closed_ticket_data["curr_assigned_group_name"] = closed_ticket_data["curr_assigned_group"]
        for t in TEAMS.values():
            for t1 in t:
                if closed_ticket_data["curr_assigned_group"] in t1['team']:
                    closed_ticket_data["curr_assigned_group_name"] = t1['name'] 
                                
        closed_ticket_data["close_date"] = TZ.localize(close_date).strftime("%Y-%m-%d %H:%M:%S") if close_date else ''
        
        #if mDt > open_date:
        #    open_date = mDt
        closed_ticket_data["open_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(open_date)

        if(local_tz!=settings.TIMEZONE):
            closed_ticket_data["close_date"] = f._convert_to_local_time(from_zone, to_zone, closed_ticket_data["close_date"]) if close_date else ''
            closed_ticket_data["open_date"] = f._convert_to_local_time(from_zone, to_zone, closed_ticket_data["open_date"])
            #t[3] = f._convert_to_local_time(from_zone, to_zone, t[3])

        date_for_sla_calc = close_date if close_date else now_date
        
        user_assign = str(request.user.first_name) + " " + str(request.user.last_name)
        
        # Create the clickable link                                    
        if source!='deis':
            if "I" in closed_ticket_data["ticket_number"]:
                if "-" in closed_ticket_data["ticket_number"]:
                    closed_ticket_data["itsm_add_note_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Task&key=' + closed_ticket_data["itsm_url"] + '&function_name=AddNote'
                    closed_ticket_data["itsm_reassign_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Task&key=' + closed_ticket_data["itsm_url"] + '&function_name=AddLVReassignTask'
                    
                    if user_assign != closed_ticket_data["taken_by"]:
                        closed_ticket_data["itsm_assign_to_me"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Task&key=' + closed_ticket_data["itsm_url"] + '&function_name=AssigntoMe'
                    
                    if closed_ticket_data["ticket_status"] == 'With 3rd Party':
                        closed_ticket_data["itsm_back_from_thirdparty_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Task&key=' + closed_ticket_data["itsm_url"] + '&function_name=BackFrom3rdParty'
                        closed_ticket_data["itsm_resolve_url"] = ''
                    else:
                        closed_ticket_data["itsm_resolve_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Task&key=' + closed_ticket_data["itsm_url"] + '&function_name=Complete'
                        closed_ticket_data["itsm_back_from_thirdparty_url"] = ''
                        
                    #closed_ticket_data["itsm_resolve_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Task&key=' + closed_ticket_data["itsm_url"] + '&function_name=Complete'
                    closed_ticket_data["itsm_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/open.rails?class_name=IncidentManagement.Task&key=' + closed_ticket_data["itsm_url"] + '&parent_class_name=IncidentManagement.Incident&parent_key=' + closed_ticket_data["itsm_parent_url"] + '&parent_attribute=Tasks'
                    closed_ticket_data["itsm_parent_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/Object/Open.rails?class_name=IncidentManagement.Incident&key=' + closed_ticket_data["itsm_parent_url"]
                    
                else:
                    closed_ticket_data["itsm_add_note_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Incident&key=' + closed_ticket_data["itsm_url"] + '&function_name=AddNote'
                    closed_ticket_data["itsm_reassign_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Incident&key=' + closed_ticket_data["itsm_url"] + '&function_name=AddLVAssignment'
                    
                    if user_assign != closed_ticket_data["taken_by"]:
                        closed_ticket_data["itsm_assign_to_me"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Incident&key=' + closed_ticket_data["itsm_url"] + '&function_name=AssignToMe'
                        
                    if closed_ticket_data["ticket_status"] == 'With 3rd Party':
                        closed_ticket_data["itsm_back_from_thirdparty_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Incident&key=' + closed_ticket_data["itsm_url"] + '&function_name=BackFrom3rdParty'
                        closed_ticket_data["itsm_resolve_url"] = ''
                    else:
                        closed_ticket_data["itsm_back_from_thirdparty_url"] = ''
                        closed_ticket_data["itsm_resolve_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=IncidentManagement.Incident&key=' + closed_ticket_data["itsm_url"] + '&function_name=Resolve'
                    
                    closed_ticket_data["itsm_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/Object/Open.rails?class_name=IncidentManagement.Incident&key=' + closed_ticket_data["itsm_url"]
                    closed_ticket_data["itsm_parent_url"] = ''
                               
            if 'S' in closed_ticket_data["ticket_number"]:
                closed_ticket_data["itsm_add_note_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=RequestManagement.Task&key=' + closed_ticket_data["itsm_url"] + '&function_name=AddNote'
                closed_ticket_data["itsm_reassign_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=RequestManagement.Task&key=' + closed_ticket_data["itsm_url"] + '&function_name=AddTaskReassign1'
                
                if user_assign != closed_ticket_data["taken_by"]:
                    closed_ticket_data["itsm_assign_to_me"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=RequestManagement.Task&key=' + closed_ticket_data["itsm_url"] + '&function_name=AssigntoMyself'
                    
                closed_ticket_data["itsm_resolve_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/invokeFunction.rails?class_name=RequestManagement.Task&key=' + closed_ticket_data["itsm_url"] + '&function_name=Complete'
                # Create the clickable link 
                closed_ticket_data["itsm_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/open.rails?class_name=RequestManagement.Task&key=' + closed_ticket_data["itsm_url"]
                closed_ticket_data["itsm_parent_url"] = 'https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/object/open.rails?class_name=RequestManagement.Request&key=' + closed_ticket_data["itsm_parent_url"] + '&puvt=8L-7vIWb8Kc5IxCCYqVjWTPMz7IiS7yz_9pIdMSTLuY1'

        
        if criticality in ['PLANNED','P_PLANNED']:
            # if planned, due date is planned date
            closed_ticket_data["due_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(due_date) 
            if(local_tz != settings.TIMEZONE):
                closed_ticket_data["due_date"] = f._convert_to_local_time(from_zone, to_zone, closed_ticket_data["due_date"]) 
            is_planned = True

            if TZ.localize(date_for_sla_calc) > TZ.localize(due_date):
                is_oosla = True
                # sla type
                closed_ticket_data["sla_type"] = 'OoSLA'
                osla_total += 1
                if filters_dic['sla_selection'] == 'allsla' or filters_dic['sla_selection'] == 'oosla':
                    closed_data.append(closed_ticket_data)
            else:                
                is_insla = True
                # if planned, in sla will always be SLA1
                closed_ticket_data["sla_type"] = 'SLA1'
                sla1_total += 1
                if filters_dic['sla_selection'] == 'allsla' or filters_dic['sla_selection'] == 'isla':
                    closed_data.append(closed_ticket_data)
        else:
            # overwrite due date
            #print "============= DEBUG :: SLA GROUP :: ", sla_grp
            due_date = f._comp_duedate_95(criticality, open_date, sla_grp, ttype)
            #print "============= DEBUG :: DUE_DATE :: ", due_date
            closed_ticket_data["due_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(due_date)
            if(local_tz != settings.TIMEZONE):
                closed_ticket_data["due_date"] = f._convert_to_local_time(from_zone, to_zone, closed_ticket_data["due_date"]) 

            closed_ticket_data["sla_type"] = f._check_sla_type(criticality, open_date, date_for_sla_calc, sla_grp, ttype)

            if closed_ticket_data["sla_type"] == 'OoSLA':
                is_oosla = True
                osla_total += 1
                if filters_dic['sla_selection'] == 'allsla' or filters_dic['sla_selection'] == 'oosla':
                    closed_data.append(closed_ticket_data)
            else:
                is_insla = True
                if filters_dic['sla_selection'] == 'allsla' or filters_dic['sla_selection'] == 'isla':
                    closed_data.append(closed_ticket_data)
                if closed_ticket_data["sla_type"] == 'SLA1':
                    sla1_total += 1
                elif closed_ticket_data["sla_type"] == 'SLA2':
                    sla2_total += 1

        # get out of sla latetime list 
        if is_oosla:
            l_list[ "{0}".format(closed_ticket_data["ticket_number"]) ]=(date_for_sla_calc - due_date)
        #endfor

    # compute percentage
    total_tickets = open_tickets_count + closed_tickets_count
    sla1_perc = 100 * float(sla1_total)/float(total_tickets) if total_tickets and sla1_total else 0 
    sla2_perc = 100 * float(sla2_total)/float(total_tickets) if total_tickets and sla1_total else 0
    osla_perc = 100 * float(osla_total)/float(total_tickets) if total_tickets and sla1_total else 0

    return_data["closed_data"] = closed_data
    return_data["SLA1"] = sla1_total
    return_data["SLA1_P"] = "%.2f" % sla1_perc
    return_data["SLA2"] = sla2_total
    return_data["SLA2_P"] = "%.2f" % (sla1_perc + sla2_perc)
    return_data["OSLA"] = osla_total
    return_data["OSLA_P"] = "%.2f" % osla_perc
    return_data["OSLA_DATA"] = l_list
    return_data["open_tickets_count"] = open_tickets_count
    return_data["closed_tickets_count"] = closed_tickets_count

    #current_app.logger.info("return_data: %s" %return_data)
    return return_data

def get_notes(request, ticket_no, local_tz):

    return f._get_notes(ticket_no, local_tz)

def flatten(l):
    for el in l:
        if isinstance(el, collections.Iterable) and not isinstance(el, str):
            for sub in flatten(el):
                yield sub
        else:
            yield el

def get_applications_list(request):
    applications_list = []
    
    db_conn = f._get_mysql_connection()
    
    if not db_conn:
        return applications_list
    
    cursor = db_conn.cursor()
    
    conn_str = "select name from applications"
    
    cursor.execute(conn_str)
    
    
    for t in cursor.fetchall():
        # convert dbrows to list
        t = [x for x in t]
        applications_list.extend(t)
    
    f._close_connection(request, db_conn)     
    return applications_list
    
def get_osla_stats(osla_data):
    osla_stats = {}
    if len(osla_data) > 0:
        med_sec =  np.median(sorted([f._total_seconds(td) for td in osla_data.values()]))
        osla_stats = {
                'latetime_total': f._format_timedelta(sum(osla_data.values(), timedelta())),
                'latetime_ave': f._format_timedelta(sum(osla_data.values(), timedelta())/len(osla_data)),
                'latetime_med': str(datetime.timedelta(seconds=med_sec)),
                'latetime_min': { 'ticket_no' : min(osla_data, key=osla_data.get), 'val': f._format_timedelta(min(osla_data.values())) },
                'latetime_max': { 'ticket_no' : max(osla_data, key=osla_data.get), 'val': f._format_timedelta(max(osla_data.values())) }
        }
    
    return osla_stats