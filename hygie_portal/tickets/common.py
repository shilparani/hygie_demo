# -*- coding: utf-8 -*-
#from flask import current_app
from datetime import timedelta
from math import floor
from dateutil import tz
from hygie_portal.models import Holidays
from hygie_portal.querymanager import querymanager as qm
from django.conf import settings
from hygie.settings import logger
import datetime, calendar
import pyodbc
import pytz
import os
import MySQLdb


def _get_connection(request):
    logger.info('Start query time line 18: %s ' % (datetime.datetime.now().strftime('%m/%d/%Y %H:%M:%S')))
    try:
        #conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format(settings.MSQL_SERVER, settings.MSQL_PORT, settings.MSQL_DB, settings.MSQL_USERNAME, settings.MSQL_PASSWORD))
        conn = MySQLdb.connect(user='root', passwd='Neurones@2018', host='10.236.0.3', db='demo', charset='utf8')
        logger.info('ODBC connection made :%s' % request.user.username)
        return conn
    except pyodbc.Error as err:
        logger.error(err)
        return None

def _get_connection_itsm(request):
    logger.info('Start query time: %s ' % (datetime.datetime.now().strftime('%m/%d/%Y %H:%M:%S')))
    try:
        #conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format(settings.ITSM_SERVER, settings.ITSM_PORT, settings.ITSM_DB, settings.ITSM_USERNAME, settings.ITSM_PASSWORD))
        conn = MySQLdb.connect(user='root', passwd='Neurones@2018', host='10.236.0.3', db='demo', charset='utf8')
        logger.info('Security ODBC connection made :%s' % request.user.username)
        return conn
    except pyodbc.Error as err:
        logger.error(err)
        return None

def _get_connection_itsm_dev(request):
    logger.info('Start query time: %s ' % (datetime.datetime.now().strftime('%m/%d/%Y %H:%M:%S')))
    try:
        #conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format(settings.ITSM_SERVER_DEV, settings.ITSM_PORT_DEV, settings.ITSM_DB_DEV, settings.ITSM_USERNAME_DEV, settings.ITSM_PASSWORD_DEV))
        conn = MySQLdb.connect(user='root', passwd='Neurones@2018', host='10.236.0.3', db='demo', charset='utf8')
        logger.info('Security ODBC connection made :%s' % request.user.username)
        return conn
    except pyodbc.Error as err:
        logger.error(err)
        return None

def _get_team_trends_connection(request):
    try:
        #conn = MySQLdb.connect(user='esurvey', passwd='esurvey', host='NLVCP2064D', db='Hygie_dev')
        #conn = MySQLdb.connect(user='esurvey', passwd='esurvey', host='NLVCP2008p', db='Hygie')
        conn = MySQLdb.connect(user='root', passwd='Neurones@2018', host='10.236.0.3', db='demo', charset='utf8')
        return conn
    except MySQLdb.Error as err:
        logger.info(err)
        return None

def _close_connection(request,conn):
    logger.info('ODBC connection closed :%s' % request.user.username)
    conn.close()
    logger.info('End query time: %s' % datetime.datetime.now().strftime('%m/%d/%Y %H:%M:%S'))

def _get_ticket(request, ticket_str, local_tz=None, source='deis'):

    search_tickets = []
    if not ticket_str:
        return None

    ticket_list = filter(None, [x.strip() for x in ticket_str.split(',')])
   
    if not ticket_list:
        return None

    TZ = pytz.timezone(settings.TIMEZONE)
    from_zone = tz.gettz(settings.TIMEZONE)
    to_zone = tz.gettz(local_tz)
    
    #intentional duplication of login id assigned to column for closed by for deis 

    db_view_query = qm.get_query_by_title('_TELMASTE_HYGIE_')
    view = '({0})'.format(db_view_query)
    
    conn_str = "select [Close Date & Time], [Open Date & Time], [Urgency ID:], [Due Date & Time:], [Incident #], [Incident Description], [Login ID Assigned To], [Login ID Assigned To], [Incident Resolution], [Closed Group:], [Subject ID], [Ticket Type], [Group Name], Null as sequence, null as problem_id, null as problem_sequence, null as problem_status from _SMDBA_.Incident where [InActive:] =0 and ([Incident #] like '%{0}%')".format("%' or [Incident #] like '%".join(ticket_list)) if source=='deis' else "select [CLOSED ON], [DATE OPEN] , Urgency_desc as Urgency, [DUE_DATE:], NUM_TICKET , DESCRIPTION,  [LASTUSER], [CLOSE_BY], [RESOLUTION], [CLOSED_GROUP:], Subject_desc as SubjectID, TTYPE, Group_desc as GroupName, [sequence], problem_id, problem_sequence, problem_status from {1} as details where num_ticket like '%{0}%'".format("%' or num_ticket like '%".join(ticket_list), view)

    logger.info('conn_str:%s'%conn_str)
    
    db_conn = _get_connection(request, ) if source=='deis' else _get_connection_itsm(request, )
    # check DB connection, if none return
    if db_conn is None:
        return search_tickets
    cursor = db_conn.cursor()
    cursor.execute(conn_str)
    
    data=cursor.fetchall()
    _close_connection(request, db_conn)
    
    TEAMS = settings.TEAMS
    sla_grp='ITL4'
    
    for t in data:
    
        # convert dbrows to list  and replace None in data to ''
        t = ['' if d is None else d for d in t]
        
        default_keys = ["close_date","open_date","criticality","due_date","ticket_number","description","assigned_to","closed_by","resolution","closed_team", "category","ticket_type","raw_group_name", "sequence", "problem_id", "problem_sequence", "problem_status"]  
          
        tickets_data = dict(zip(default_keys, t)) #form dict with default_keys and DB data(t)
        closed_grp = tickets_data["raw_group_name"] if tickets_data["closed_team"] is '' else tickets_data["closed_team"]
        due_date = tickets_data["due_date"]
        close_date = tickets_data["close_date"]
        open_date = tickets_data["open_date"]
         
        tickets_data["problem_url"] = "https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/Object/Open.rails?class_name=ProblemManagement.Problem&key=" + tickets_data["problem_sequence"] if tickets_data["problem_sequence"] else ''
        
        #change the sla group
        if tickets_data["raw_group_name"] in settings.SLA_GRP_AMS_CGI:
            sla_grp = 'AMS_CGI'
        elif tickets_data["raw_group_name"] in settings.SLA_GRP_AMS_VALTECH:
            sla_grp = 'AMS_VALTECH'
        else:
            for key, value in TEAMS.items():
                for v in value:
                    if tickets_data["raw_group_name"] in v['team']:
                        if key in settings.SLA_GRP_PROXIMITY:
                            sla_grp = 'EMEA'
                        else:
                            sla_grp = key

        if tickets_data["criticality"] in ['PLANNED','P_PLANNED']:
            #t[3] = TZ.localize(due_date).strftime("%Y-%m-%d %H:%M:%S")
            tickets_data["due_date"] ='{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(due_date)
            if close_date:
                tickets_data["close_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(close_date)
            tickets_data["open_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(open_date)
            if(local_tz!=settings.TIMEZONE):
                tickets_data["due_date"] = _convert_to_local_time(from_zone, to_zone, tickets_data["due_date"])
        else:
            due_date = _comp_duedate_95(tickets_data["criticality"], open_date, sla_grp, tickets_data["ticket_type"])
            #t[3] = TZ.localize(due_date).strftime("%Y-%m-%d %H:%M:%S")
            tickets_data["due_date"] ='{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(due_date)
            if close_date:
                tickets_data["close_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(close_date)
            tickets_data["open_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(open_date)
            if(local_tz!=settings.TIMEZONE):
                tickets_data["due_date"] = _convert_to_local_time(from_zone, to_zone, tickets_data["due_date"])
        if(local_tz!=settings.TIMEZONE):
            if close_date:
                #t[0] = TZ.localize(close_date).strftime("%Y-%m-%d %H:%M:%S")
                tickets_data["close_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(close_date)
                tickets_data["close_date"] = _convert_to_local_time(from_zone, to_zone, tickets_data["close_date"])
            #t[1] = TZ.localize(open_date).strftime("%Y-%m-%d %H:%M:%S")
            tickets_data["open_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(open_date)
            tickets_data["open_date"] = _convert_to_local_time(from_zone, to_zone, tickets_data["open_date"]) 
        search_tickets.append(tickets_data) 
    #t = [x for x in tickets]
    return search_tickets

# SLA 2
def _comp_duedate_100(criticality, open_datetime, group=None, ttype=None):
    comp_due_date = None

    #group = 'TSC' if not group else 'SERVICE_DESK'
    sla_hours = settings.SLA_HOURS

    hours = 0
    if group is 'TSC':
        hours = sla_hours[group]['SLA2'][criticality]
        comp_due_date = open_datetime + timedelta(hours=hours)
    elif group is 'NETWORK':
        hours = sla_hours[group]['SLA2'][criticality]
        comp_due_date = open_datetime + timedelta(hours=hours)
    elif group is 'SERVICE_DESK':
        hours = sla_hours[group]['SLA2'][ttype][criticality]
        comp_due_date = _comp_fo_duedate(open_datetime, hours)
    elif group is 'EMEA':
        hours = sla_hours[group]['SLA2'][ttype][criticality]
        comp_due_date = _comp_fo_duedate(open_datetime, hours)
    elif group is 'ITL4':
        hours = sla_hours[group]['SLA2'][criticality]
        comp_due_date = open_datetime + timedelta(hours=hours)
    elif group is 'AMS':
        hours = sla_hours[group]['SLA2'][ttype][criticality]
        comp_due_date = _comp_ams_duedate(open_datetime, hours, group)

    elif group is 'AMS_CGI':
        hours = sla_hours['AMS']['SLA1'][ttype][criticality]
        comp_due_date = _comp_ams_duedate(open_datetime, hours, group)

    elif group is 'AMS_VALTECH':
        hours = sla_hours['AMS']['SLA1'][ttype][criticality]
        comp_due_date = _comp_ams_duedate(open_datetime, hours, group)

    return comp_due_date

# SLA 1
def _comp_duedate_95(criticality, open_datetime, group=None, ttype=None):
    comp_due_date = None

    sla_hours = settings.SLA_HOURS

    hours = 0
    if group == 'TSC':
        hours = sla_hours[group]['SLA1'][criticality]
        comp_due_date = open_datetime + timedelta(hours=hours)

    elif group == 'SERVICE_DESK':
        hours = sla_hours[group]['SLA1'][ttype][criticality]
        comp_due_date = _comp_fo_duedate(open_datetime, hours)
        
    elif group == 'NETWORK':
        hours = sla_hours[group]['SLA1'][criticality]
        comp_due_date = open_datetime + timedelta(hours=hours)

    elif group == 'EMEA':
        hours = sla_hours[group]['SLA1'][ttype][criticality]
        comp_due_date = _comp_fo_duedate(open_datetime, hours)

    elif group == 'ITL4':
        hours = sla_hours[group]['SLA1'][criticality]
        comp_due_date = open_datetime + timedelta(hours=hours)

    elif group == 'AMS':
        hours = sla_hours[group]['SLA1'][ttype][criticality]
        comp_due_date = _comp_ams_duedate(open_datetime, hours, group)

    elif group == 'AMS_CGI':
        hours = sla_hours['AMS']['SLA1'][ttype][criticality]
        comp_due_date = _comp_ams_duedate(open_datetime, hours, group)

    elif group == 'AMS_VALTECH':
        hours = sla_hours['AMS']['SLA1'][ttype][criticality]
        comp_due_date = _comp_ams_duedate(open_datetime, hours, group)

    return comp_due_date

def _comp_fo_duedate(in_datetime, in_hours):
    """  Gets SERVICE_DESK due date """    
    start_time =  _get_fo_officehours(in_datetime)['start_time']
    end_time = _get_fo_officehours(in_datetime)['end_time']

    default_start_datetime = in_datetime.replace(hour=int(start_time[0]),minute=int(start_time[1]),second=00)
    default_end_datetime = in_datetime.replace(hour=int(end_time[0]),minute=int(end_time[1]),second=00)

    start = in_datetime

    if in_datetime >= default_start_datetime and in_datetime <= default_end_datetime:
        start = in_datetime
    elif in_datetime < default_start_datetime:
        start = default_start_datetime
    elif in_datetime > default_end_datetime:
        start = default_start_datetime + timedelta(days=1)
        # day incremented, check again the offic hours
        start = start.replace(hour=int(_get_fo_officehours(start)['start_time'][0]),minute=int(_get_fo_officehours(start)['start_time'][1]),second=00)

    end = start.replace(hour=int(end_time[0]),minute=int(end_time[1]),second=00)
    due_date = start + timedelta(hours=in_hours)

    while due_date > end:
        time_diff = due_date - end
        #print 'diff:%s' % time_diff

        # when start/end dates are increased, check the office hours
        start = start + timedelta(days=1)
        start_time = _get_fo_officehours(start)['start_time']
        start = start.replace(hour=int(start_time[0]),minute=int(start_time[1]),second=00)

        due_date = start + time_diff

        end = end + timedelta(days=1)
        end_time = _get_fo_officehours(end)['end_time']
        end = end.replace(hour=int(end_time[0]),minute=int(end_time[1]),second=00)

    return due_date

def _get_fo_officehours(in_datetime):

    holiday = _is_holiday(in_datetime, 'fra')

    weekdays = list(calendar.day_abbr)[:5]

    week = 'WEEK' if in_datetime.strftime('%a') in weekdays and not holiday else 'WEEKEND-PF'

    start_time = settings.FO_WORKTIME[week]['START'].split(':')
    end_time = settings.FO_WORKTIME[week]['END'].split(':')

    office_hours =  { 'start_time':start_time, 'end_time':end_time}

    return office_hours

def _comp_ams_duedate(in_datetime, in_hours, group='AMS'):
    """  Gets AMS due date """
    
    start_time = _get_ams_officehours(in_datetime, group)['start_time']
    end_time = _get_ams_officehours(in_datetime, group)['end_time']   
    default_start_datetime = in_datetime.replace(hour=int(start_time[0]),minute=int(start_time[1]),second=00)
    default_end_datetime = in_datetime.replace(hour=int(end_time[0]),minute=int(end_time[1]),second=00)
    start = in_datetime
    
    if in_datetime >= default_start_datetime and in_datetime <= default_end_datetime:
        start = in_datetime
    elif in_datetime < default_start_datetime:
        start = default_start_datetime
    elif in_datetime > default_end_datetime:
        start = default_start_datetime + timedelta(days=1)
        # day incremented, check again the offic hours
        start = start.replace(hour=int(_get_ams_officehours(start, group)['start_time'][0]),minute=int(_get_ams_officehours(start, group)['start_time'][1]),second=00)

    
    if in_datetime.weekday() in [5,6]:
        start = next_weekday(in_datetime,0).replace(hour=int(start_time[0]),minute=int(start_time[1]),second=00)
    if _is_holiday(start):
        start = (start + timedelta(days=1)).replace(hour=int(start_time[0]),minute=int(start_time[1]),second=00)
    if start.weekday() in [5,6]:
        start = next_weekday(start,0).replace(hour=int(start_time[0]),minute=int(start_time[1]),second=00)
    if _is_holiday(start):
        start = (start + timedelta(days=1)).replace(hour=int(start_time[0]),minute=int(start_time[1]),second=00)
    
    end = start.replace(hour=int(end_time[0]),minute=int(end_time[1]),second=00)
    
    due_date = start + timedelta(hours=in_hours)

    while due_date > end:
        time_diff = due_date - end
        #print time_diff
        #print 'diff:%s' % time_diff

        # when start/end dates are increased, check the office hours
        start = start + timedelta(days=1)
        start_time = _get_ams_officehours(start, group)['start_time']
        start = start.replace(hour=int(start_time[0]),minute=int(start_time[1]),second=00)

        due_date = start + time_diff

        end = end + timedelta(days=1)
        end_time = _get_ams_officehours(end, group)['end_time']
        end = end.replace(hour=int(end_time[0]),minute=int(end_time[1]),second=00)
        
    
    if due_date.weekday()==5:
        due_date = due_date + timedelta(days=2)
    elif due_date.weekday()==6:
        due_date = due_date + timedelta(days=1)    
    if _is_holiday(due_date):
        due_date = due_date + timedelta(days=1)
    if due_date.weekday()==5:
        due_date = due_date + timedelta(days=2)
    elif due_date.weekday()==6:
        due_date = due_date + timedelta(days=1) 
    
    return due_date

def _get_ams_officehours(in_datetime,group='AMS'):
    start_time = settings.AMS_WORKTIME[group]['START'].split(':')
    end_time = settings.AMS_WORKTIME[group]['END'].split(':')
    office_hours =  { 'start_time':start_time, 'end_time':end_time}
    return office_hours

def _is_holiday(in_datetime, region=None):

    holidays = Holidays.objects.filter(start_date=in_datetime.date(),region=region).first()

    is_holiday = True if holidays else False
     
    return is_holiday

def next_weekday(d, weekday):
    days_ahead = weekday - d.weekday()
    if days_ahead <= 0: # Target day already happened this week
        days_ahead += 7
    return d + timedelta(days_ahead)

def _comp_soosla_date(criticality, due_date, sla_grp, ttype):
    comp_due_date = None

    if sla_grp in ['TSC','ITL4','NETWORK']:
        logger.info("sla_group:%s************************************ttype:%s"%(sla_grp,ttype))
        if criticality in ['LOW','P_LOW']:
            comp_due_date = due_date - timedelta(hours=27)
        if criticality in ['MEDIUM','P_MEDIUM']:
            comp_due_date = due_date - timedelta(hours=6)
        if criticality in ['HIGH','P_HIGH']:
            comp_due_date = due_date - timedelta(hours=3)
        if criticality in ['CRITICAL','P_CRITICAL']:
            comp_due_date = due_date - timedelta(minutes=90)
    else:
        if ttype == 'Incident':
            logger.info("sla_group:%s************************************ttype:%s"%(sla_grp,ttype))
            if criticality in ['SSHD']:
                comp_due_date = due_date - timedelta(hours=90)
            if criticality in ['LOW','P_LOW']:
                comp_due_date = due_date - timedelta(hours=18)
            if criticality in ['MEDIUM','P_MEDIUM']:
                comp_due_date = due_date - timedelta(hours=6)
            if criticality in ['HIGH','P_HIGH','CRITICAL','P_CRITICAL']:
                comp_due_date = due_date - timedelta(hours=3)
        else:
            if criticality in ['SSHD']:
                comp_due_date = due_date - timedelta(hours=90)
            if criticality in ['LOW','P_LOW']:
                comp_due_date = due_date - timedelta(hours=90)
            if criticality in ['MEDIUM','P_MEDIUM']:
                comp_due_date = due_date - timedelta(hours=36)
            if criticality in ['HIGH','P_HIGH','CRITICAL','P_CRITICAL']:
                comp_due_date = due_date - timedelta(hours=18)

    return comp_due_date

def _time_remaining(comp_due_date, now_date, group):

    tr = comp_due_date - now_date

    return tr

# check if closed SLA1 or SLA2
def _check_sla_type(criticality, open_date, close_date, group=None, ttype=None):
    sla_type, sla1, sla2 = None, False, False

    # check if sla1 95
    if _comp_duedate_95(criticality, open_date, group, ttype) >= close_date:
        sla_type = 'SLA1'
        sla1 = True
    
    # check if sla2
    if _comp_duedate_100(criticality, open_date, group, ttype) >= close_date and not sla1:
        sla_type = 'SLA2'
        sla2 = True

    # if not sla1 or sla2 then out of sla
    if not sla1 and not sla2:
        sla_type = 'OoSLA'

    return sla_type

def _format_hrs_timedelta(in_timedelta):
    seconds = in_timedelta.total_seconds()
    hours = seconds // 3600
    minutes = (seconds % 3600) // 60
    seconds = seconds % 60
    str = '{}:{}:{}'.format(int(hours), int(minutes), int(seconds))
    return (str)

def _format_timedelta(value, time_format="{days} days, {hours2}:{minutes2}:{seconds2}"):
    if hasattr(value, 'seconds'):
        seconds = value.seconds + value.days * 24 * 3600
    else:
        seconds = int(value)

    seconds_total = seconds

    minutes = int(floor(seconds / 60))
    minutes_total = minutes
    seconds -= minutes * 60

    hours = int(floor(minutes / 60))
    hours_total = hours
    minutes -= hours * 60

    days = int(floor(hours / 24))
    days_total = days
    hours -= days * 24

    years = int(floor(days / 365))
    years_total = years

    if not days:
        time_format="{hours2}:{minutes2}:{seconds2}"

    if days == 1:
        time_format="{days} day, {hours2}:{minutes2}:{seconds2}"

    return time_format.format(**{
        'seconds': seconds,
        'seconds2': str(seconds).zfill(2),
        'minutes': minutes,
        'minutes2': str(minutes).zfill(2),
        'hours': hours,
        'hours2': str(hours).zfill(2),
        'days': days,
        'years': years,
        'seconds_total': seconds_total,
        'minutes_total': minutes_total,
        'hours_total': hours_total,
        'days_total': days_total,
        'years_total': years_total,
    })

def _total_seconds(td):
    return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 1e6) / 1e6


def _get_notes(request, ticket_no, ticket_seq, isclosed, local_tz, source='itsm'):
    
    notes_timeline=[]
    timeline=[]
    notes = []
    
    view = '_teldetai_hygie_incident'
    
    if 'S' in ticket_no:
        #view = '_TELDETAI_HYGIE__Request'
        view = '_teldetai_hygie_request_open'
    else:
        view = '_teldetai_hygie_incident_open'
        
    from_zone = tz.gettz(settings.TIMEZONE)
    to_zone = tz.gettz(local_tz)
    db_conn = _get_connection_itsm(request)
    # check DB connection, if none return
    if db_conn is None:
        return notes_timeline
    cursor = db_conn.cursor()

    logger.info('_get_notes')
    
    '''
    if isclosed == 'None' and 'S' in ticket_no:
        view = '_TELDETAI_HYGIE_Request_Open'
        
    if isclosed == 'None' and 'I' in ticket_no:
        view = '_TELDETAI_HYGIE_Incident_Open'
    '''
    notes_str = "select lastmodified, lastuser, action_desc, `to_group:`, TO_STAFF, description, `from_group:` from {1} as details where num_ticket = '{0}' order by lastmodified;".format(ticket_no, view)

    timeline_str = "select lastmodified, `from_group:`, `to_group:`, action_desc from {1} where num_ticket = '{0}' and action_desc in ('HD_OPEN','HD_FRWD_GROUP','HD_CLOSE','HD_FRWD_STAFF') order by lastmodified;".format(ticket_no,view)

    logger.info('notes_str:%s'%notes_str)
    logger.info('timeline_str:%s'%timeline_str)

    #logger.info('Start notes query time: %s User:%s ' % (datetime.datetime.now().strftime('%m/%d/%Y %H:%M:%S'), request.user.username))
    cursor.execute(notes_str)
    ncur = cursor.fetchall()
    
    from_zone_ivanti_db = tz.gettz(settings.TIMEZONE)
    
    #columns = [column[0] for column in ncur.description]
    for n in ncur:
        n_row = []
        for idx,item in enumerate(n):
            # Convert date to local time 
            if idx == 0:
                #item = _convert_to_local_time(from_zone, to_zone, item.strftime("%Y-%m-%d %H:%M:%S"))
                item = _convert_to_local_time(from_zone_ivanti_db, to_zone, item.strftime("%Y-%m-%d %H:%M:%S"))
            # To overide action column
            if idx == 2 and (item in [7,8] or item == 'HD_FRWD_STAFF' or item == 'HD_FRWD_GROUP') :
                item = 'Forwarded To'
            if idx == 2 and (item == 1146 or item == '2. PRIVATE NOTE' or item == 'TECHNICAL NOTE'):
                item = 'Private Note'
            if idx == 2 and (item == 1052 or item == '1. PUBLIC NOTE'):
                item = 'Public Note'
            if idx == 2 and item == 1054:
                item = 'Action Done'
            if idx == 2 and item == 61:
                item = 'Changed Urgency'
            if idx == 2 and (item == 10 or item == 'HD_OPEN'):
                item = 'Opened'
            if idx == 2 and (item == 4 or item == 'HD_CLOSE'):
                item = 'Closed'
            if idx == 2 and (item == 11 or item == 'HD_REOPEN'):
                item = 'Reopened'
            if idx == 2 and item == '3RD PARTY UPDATE':
                item = 'Adding 3rd Party'
            if idx == 2 and item == 'BACK FROM 3RD PARTY':
                item = 'Ending 3rd Party' 
            if idx == 2 and item == 'CALL_ADDL':
                item = 'User Call Back' 

            n_row.append(item)
        
        # move desc to note if forwarded
        if source=='deis':
            if 'Forwarded To' in n_row:
                n_row[3] = n_row[4]
            n_row.pop(4)
        if source!='deis':
            n_row[4] = n_row[4]
            if n[4] is not None and (n[2] == 'HD_FRWD_GROUP' or n[2] == 'HD_FRWD_STAFF'):
                n_row[3] = str(n[3]) + '<br>&<br>' + str(n[4])
            elif n[4] is not None:
                n_row[3] = n[4]
            else:
                n_row[3] = n[3]
            if n[2] == '2. PRIVATE NOTE' or n[2] == '1. PUBLIC NOTE' or n[2] == 'HD_OPEN' or n[2] == 'Assign Note' or n[2] == 'Unresolve Note' or n[2] == 'Resolve Note' or n[2] == 'Back From 3rd Party Note' or n[2] == '3rd Party Note' or n[2] == 'CALL_ADDL' or n[2] == 'TECHNICAL NOTE' or n[2] == 'STOP_CLOCK':
                n_row[3] = n[5]
                details = n_row[3]
                if n_row[3] is not None:
                    n_row[3] = details.replace('---------', '<br>')

        notes.append(n_row)

    #logger.info('Start notes query time: %s User:%s ' % (datetime.datetime.now().strftime('%m/%d/%Y %H:%M:%S'), request.user.username))
    cursor.execute(timeline_str)
    ncur_timeline = cursor.fetchall()
    current_element = "current"
    next_element = "next"
    action_num = 10
    cnt =0

    for n in ncur_timeline:
        n_row = []
        
        #logger.info("n:*********************%s*********"%(n))
        for idx,item in enumerate(n):
            logger.info("item:*********************%s*********"%(item))
            if idx == 0:
                #item = _convert_to_local_time(from_zone, to_zone, item.strftime("%Y-%m-%d %H:%M:%S"))
                item = _convert_to_local_time(from_zone_ivanti_db, to_zone, item.strftime("%Y-%m-%d %H:%M:%S"))
            if idx == 1 and cnt==0:
                next_element=item
            if idx == 2 and cnt!=0:
                next_element=item
            if idx == 3:
                action_num=item
            n_row.append(item)
        cnt+=1
        if current_element == next_element and current_element is not None and (action_num!=4 or action_num!="HD_CLOSE"):
            continue
        else:
            current_element = next_element
        logger.info("current_element:*********************%s*********"%(current_element))
        
        timeline.append(n_row)
    #logger.info('Start notes query time: %s User:%s ' % (datetime.datetime.now().strftime('%m/%d/%Y %H:%M:%S'), request.user.username))
    notes_timeline=[timeline,notes]
    logger.info("*********************%s*********"%(timeline))

    _close_connection(request, db_conn)

    return notes_timeline

def _convert_to_local_time(from_zone, to_zone, server_time):
    #server_time = from_zone.localize(datetime.datetime.strptime(server_time, "%Y-%m-%d %H:%M:%S"))
    server_time = datetime.datetime.strptime(server_time, "%Y-%m-%d %H:%M:%S")
    server_time = server_time.replace(tzinfo=from_zone)
    local_time = server_time.astimezone(to_zone)

    return '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(local_time) 

def _to_server_datetime(in_datetime, local_tz):
    pass

def _get_security_microsoft_patch_wks_common(db_conn):
    db_conn = db_conn
    cursor = db_conn.cursor()
    logger.info('_get_security_microsoft_patch_wks')
    result = []

    total_wks_str = "select count(*) from ( SELECT distinct(DISPLAYNAME) FROM dbo.Computer (nolock) WHERE SWLASTSCANDATE >=dateadd(day,-7,getdate()) and dbo.Computer.TYPE <>'Server') T1"
    patch_required_wks_str = "select count(*) from (select Computer.DeviceName from CVDetected left join Vulnerability on CVDetected.Vulnerability_Idn=Vulnerability.Vulnerability_Idn left join Computer on Computer.Computer_Idn = CVDetected.Computer_Idn left join Patch on Patch.UniqueFileName = CVDetected.Patch where Vulnerability.vendor = 'Microsoft' and Vulnerability.severity = 1 and Vulnerability.PublishDate<dateadd(day,-90,getdate()) and Computer.Computer_Idn is not NULL and Computer.SWLastScanDate>=dateadd(day,-7,getdate()) and Computer.DisplayName like 'LV%' and Computer.Type<>'Server' and Patch.SupercededByVulID is NULL and Patch.Patch_Idn is not NULL group by Computer.DeviceName ) T1"

    ncur_total_wks = cursor.execute(total_wks_str)
    result_total_wks = ncur_total_wks.fetchone()
    result.append(result_total_wks)

    ncur_patch_required_wks= cursor.execute(patch_required_wks_str)
    result_patch_required_wks= ncur_patch_required_wks.fetchone()
    result.append(result_patch_required_wks)

    _close_connection(request, db_conn)
    return result

#commented by Shilpa: not being used currently
'''def _get_security_microsoft_patch_wks_zone():
    security_microsoft_patch_wks_count_list = []
    db_conn_americas = _get_connection_security_americas()
    if db_conn_americas is not None:
        security_count_americas = _get_security_microsoft_patch_wks_common(db_conn_americas)
        security_microsoft_patch_wks_count_list.append(security_count_americas[0][0])
        security_microsoft_patch_wks_count_list.append(security_count_americas[1][0])
    db_conn_europe = _get_connection_security_europe()
    if db_conn_americas is not None:
        security_count_europe = _get_security_microsoft_patch_wks_common(db_conn_europe)
        security_microsoft_patch_wks_count_list.append(security_count_europe[0][0])
        security_microsoft_patch_wks_count_list.append(security_count_europe[1][0])
    db_conn_asia = _get_connection_security_asia()
    if db_conn_americas is not None:
        security_count_asia = _get_security_microsoft_patch_wks_common(db_conn_asia)
        security_microsoft_patch_wks_count_list.append(security_count_asia[0][0])
        security_microsoft_patch_wks_count_list.append(security_count_asia[1][0])
    security_microsoft_patch_wks_count_list.append(security_count_americas[0][0]+security_count_europe[0][0]+security_count_asia[0][0])
    security_microsoft_patch_wks_count_list.append(security_count_americas[1][0]+security_count_europe[1][0]+security_count_asia[1][0])
    current_app.logger.info('******%s******'%security_microsoft_patch_wks_count_list)
    return security_microsoft_patch_wks_count_list '''
   
def _get_security_top5_microsoft_patches_wks_common(db_conn):
    db_conn = db_conn
    cursor = db_conn.cursor()
    logger.info('_get_security_top5_microsoft_patches_wks')
    top5_microsoft_patches_wks_str = "select top 5 Vul_ID, count(*) from ( select Vulnerability.Vul_ID,Computer.DeviceName from CVDetected left join Vulnerability on CVDetected.Vulnerability_Idn=Vulnerability.Vulnerability_Idn left join Computer on Computer.Computer_Idn = CVDetected.Computer_Idn left join Patch on Patch.UniqueFileName = CVDetected.Patch where Vulnerability.vendor = 'Microsoft' and Vulnerability.severity = 1 and Vulnerability.PublishDate<dateadd(day,-90,getdate()) and Computer.Computer_Idn is not NULL and Computer.SWLastScanDate>=dateadd(day,-7,getdate()) and Computer.DisplayName like 'LV%' and Computer.Type<>'Server' and Patch.SupercededByVulID is NULL and Patch.Patch_Idn is not NULL group by Vulnerability.Vul_ID,Computer.DeviceName) T1 group by Vul_ID order by count(*) desc"

    ncur_top5_microsoft_patches_wks = cursor.execute(top5_microsoft_patches_wks_str)
    result = ncur_top5_microsoft_patches_wks.fetchall()

    _close_connection(request, db_conn)
    return result

#commented by Shilpa: not being used currently
'''def _get_top5_microsoft_patches_wks_zone():
    db_conn_americas = _get_connection_security_americas()
    security_top5_microsoft_patches_wks_americas = _get_security_top5_microsoft_patches_wks_common(db_conn_americas)
    db_conn_europe= _get_connection_security_europe()
    security_top5_microsoft_patches_wks_europe = _get_security_top5_microsoft_patches_wks_common(db_conn_europe)
    db_conn_asia = _get_connection_security_asia()
    security_top5_microsoft_patches_wks_asia = _get_security_top5_microsoft_patches_wks_common(db_conn_asia)
    security_top5_microsoft_patches_wks_list = security_top5_microsoft_patches_wks_americas + security_top5_microsoft_patches_wks_europe + security_top5_microsoft_patches_wks_asia
    return security_top5_microsoft_patches_wks_list'''

def _merge_deis_and_itsm_data(deis,itsm):
    merge_data = deis
    for key, value in itsm.items():
        for v in value:
            labels = []
            for e in merge_data[key]:
                labels.append(e['label'])
                if v['label'] ==e['label']:
                    e['y'] = e['y']+v['y']
            if v['label'] not in labels:
                merge_data[key].append(v)
    return merge_data

def _merge_deis_and_itsm_team_count_data(deis,itsm):
    temp_deis = deis
    for tbci in itsm:
        labels = []
        for tbc in temp_deis:
            if tbci['label'] == tbc['label']:
                tbc['y'] = tbci['y'] + tbc['y']
            labels.append(tbc['label'])
        if tbci['label'] not in labels:
            temp_deis.append(tbci)
    return temp_deis

def _get_mysql_connection():
    try:
        #conn = MySQLdb.connect(user='esurvey', passwd='esurvey', host='NLVCP2008P', db='hygie')
        conn = MySQLdb.connect(user='root', passwd='Neurones@2018', host='10.236.0.3', db='demo', charset='utf8')
        return conn
    except MySQLdb.Error as err:
        #conn.close()
        logger.info(err)
        return None     
