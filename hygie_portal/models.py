from django.db import models

# Create your models here.
from django.db import models
from django import forms
from wtforms import TextField, PasswordField
from wtforms.validators import InputRequired
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser
from django.db.models.signals import post_save
from django.dispatch import receiver
import bcrypt
from datetime import datetime
#import ldap

class Applications(models.Model):
    name = models.CharField(unique=True, max_length=200)

    class Meta:
        managed = True
        db_table = 'applications'
        
class Contentmanager(models.Model):
    title = models.CharField(max_length=80, blank=True, null=True)
    description = models.CharField(max_length=80, blank=True, null=True)
    module = models.CharField(max_length=80, blank=True, null=True)
    contenttext = models.TextField(blank=True, null=True)
    lastupdated = models.DateTimeField(blank=True, null=True)
    lastuser = models.CharField(max_length=80, blank=True, null=True)
    allowedaccess = models.CharField(max_length=128, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'contentmanager'        
        
class CustomUser(AbstractUser):
    #user = models.OneToOneField(User, on_delete=models.CASCADE)
    #is_active = models.IntegerField(blank=True, null=True)
    is_admin = models.IntegerField(blank=True, null=True)
    role = models.CharField(max_length=256, blank=True, null=True)
    
    #USERNAME_FIELD = 'user'
    #REQUIRED_FIELDS = []
    
    '''def is_active(self):
        return True'''
        
    def set_is_admin(self, is_admin):
        self.is_admin = is_admin

    def set_role(self, role):
        self.last_update = datetime.now()
        self.role = role
        
class Holidays(models.Model):
    uid = models.CharField(unique=True, max_length=80)
    summary = models.CharField(max_length=80)
    region = models.CharField(max_length=80)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    last_update = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'holidays'

        
  

class Patchcampaigns(models.Model):
    campaign_name = models.CharField(unique=True, max_length=50)
    campaign_startdate = models.DateField(blank=True, null=True)
    campaign_target_enddate = models.DateField(blank=True, null=True)
    patch_release_date = models.DateField(blank=True, null=True)
    srv_patch_list = models.TextField(blank=True, null=True)
    wks_patch_list = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'patchcampaigns'
        
class Querymanager(models.Model):
    title = models.CharField(max_length=80, blank=True, null=True)
    description = models.CharField(max_length=80, blank=True, null=True)
    lastupdated = models.DateTimeField(blank=True, null=True)
    lastuser = models.CharField(max_length=80, blank=True, null=True)
    allowedaccess = models.CharField(max_length=128, blank=True, null=True)
    querytext = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'querymanager'


class Rca(models.Model):
    rcaid = models.CharField(unique=True, max_length=80)
    content = models.TextField(blank=True, null=True)
    report = models.TextField(blank=True, null=True)
    status = models.CharField(max_length=80, blank=True, null=True)
    lastupdated = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'rca'


class DiscrepancyServer(models.Model):
    devicename = models.CharField(unique=True, max_length=80)
    ipaddress = models.CharField(max_length=80)
    adindicator = models.CharField(max_length=80)
    ldindicator = models.CharField(max_length=80)
    kasindicator = models.CharField(max_length=80)
    deisindicator = models.CharField(max_length=80, blank=True, null=True)
    adcheck = models.IntegerField(blank=True, null=True)
    ldcheck = models.IntegerField(blank=True, null=True)
    kascheck = models.IntegerField(blank=True, null=True)
    deischeck = models.IntegerField(blank=True, null=True)
    comments = models.TextField()
    datehistory = models.TextField()
    commenthistory = models.TextField()
    lockby = models.CharField(max_length=80)
    last_update = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'srvdiscre'


class TrendTickets(models.Model):
    date = models.DateTimeField()
    groupe = models.CharField(max_length=200)
    count_in = models.IntegerField()
    count_out = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'trend_tickets'


class DiscrepancyWorkstation(models.Model):
    devicename = models.CharField(unique=True, max_length=80)
    ipaddress = models.CharField(max_length=80)
    adindicator = models.CharField(max_length=80)
    ldindicator = models.CharField(max_length=80)
    kasindicator = models.CharField(max_length=80)
    deisindicator = models.CharField(max_length=80, blank=True, null=True)
    adcheck = models.IntegerField(blank=True, null=True)
    ldcheck = models.IntegerField(blank=True, null=True)
    kascheck = models.IntegerField(blank=True, null=True)
    deischeck = models.IntegerField(blank=True, null=True)
    comments = models.TextField()
    datehistory = models.TextField()
    commenthistory = models.TextField()
    lockby = models.CharField(max_length=80)
    last_update = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'wksdiscre'    

class classicDashboardState(models.Model):
    username = models.CharField(max_length=80, blank=True, null=True)
    dashboard_state = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'classicdashboardstate'   
    
class ControlPlanManager(models.Model):
    control_id = models.CharField(primary_key=True, unique=True, max_length=80)
    category = models.CharField(max_length=80)
    domain = models.CharField(max_length=80)
    business_flag = models.CharField(max_length=80)
    zone = models.CharField(max_length=80)
    name = models.CharField(max_length=80)
    control_description = models.CharField(max_length=300)
    team_accountable = models.CharField(max_length=80)
    control_owner = models.CharField(max_length=80)
    
    control_processing_trigger_mode = models.CharField(max_length=80)
    control_processing_execution = models.CharField(max_length=80)
    control_processing_frequency = models.CharField(max_length=80)
    control_processing_repetition = models.CharField(max_length=80)
    control_processing_method = models.CharField(max_length=128)
    control_processing_responsible = models.CharField(max_length=80)
    
    control_data_type = models.CharField(max_length=80)
    control_data_description = models.CharField(max_length=128)
    control_data_target = models.CharField(max_length=80)
    control_data_sla = models.CharField(max_length=80)
    control_data_accountable = models.CharField(max_length=80)
    
    data_management_recording = models.CharField(max_length=80)
    data_management_storage = models.CharField(max_length=80)
    data_management_history = models.CharField(max_length=80)
    
    control_valuation_implementation = models.CharField(max_length=80)
    control_valuation_reliability = models.CharField(max_length=80)
    control_valuation_expectation = models.CharField(max_length=80)
    control_valuation_criticity = models.CharField(max_length=80)
    control_valuation_global_value = models.CharField(max_length=80)
    
    comments = models.TextField()
    
    last_updated = models.DateTimeField(auto_now=True)
    table_name = models.CharField(max_length=128)
    function_name = models.CharField(max_length=128)
    template_function = models.CharField(max_length=128)

    class Meta:
        managed = True
        db_table = 'controlplanmanager'
        indexes = [
            models.Index(fields=['control_processing_responsible'], name='responsible_idx'),
            models.Index(fields=['control_description'], name='description_idx'),
            models.Index(fields=['team_accountable'], name='accountable_idx'),
            models.Index(fields=['control_owner'], name='owner_idx'),
            models.Index(fields=['name'], name='name_idx'),
        ]
        
class customDashboard(models.Model):
    widget = models.CharField(max_length=80, primary_key=True)
    username = models.CharField(max_length=80)
    dashboard = models.CharField(max_length=80)
    title = models.CharField(max_length=80)
    selected_team = models.CharField(max_length=500)
    chart_size = models.TextField()
    widget_pos = models.TextField()

    class Meta:
        managed = True
        db_table = 'customdashboard'