# -*- coding: utf-8 -*-
from datetime import timedelta
from collections import defaultdict
from django.conf import settings
from hygie.settings import logger
import collections
import json,os
import pyodbc
import datetime

##############################################################################################

def _get_connection():
    logger.info('Start query time line 18: %s ' % (datetime.datetime.now().strftime('%m/%d/%Y %H:%M:%S')))
    try:
        conn = pyodbc.connect('DRIVER=FreeTDS;SERVER={0};PORT={1};DATABASE={2};UID={3};PWD={4};TDS_Version=8.0;unicode_results=True;CHARSET=UTF8'.format(settings.MSQL_SERVER, settings.MSQL_PORT, settings.MSQL_DB, settings.MSQL_USERNAME, settings.MSQL_PASSWORD))
        return conn
    except pyodbc.Error as err:
        logger.error(err)
        return None
        
def _close_connection(conn):
    conn.close()
    logger.info('End query time: %s' % datetime.datetime.now().strftime('%m/%d/%Y %H:%M:%S')) 

def _get_cmdb_report_integrity():
    db_conn = _get_connection()
    cursor = db_conn.cursor()
    logger.info('get_cmdb_report_no_location')

    integrity_data = collections.OrderedDict() 
    
    conn_str = "select count(distinct [Machine Name]) from _SMDBA_.[Inventory Items] left outer join _SMDBA_.Srv_Cat_Link on (SEQ_ITEM_SERVER = _SMDBA_.[Inventory Items].Sequence) left outer join _SMDBA_._SUBJECTS_ on (_SMDBA_.Srv_Cat_Link.SEQ_CATEGORY = _SMDBA_._SUBJECTS_.SEQUENCE) left outer join _SMDBA_._PERSONNEL_ on (_SMDBA_.Srv_Cat_Link.SEQ_STAFF = _SMDBA_._PERSONNEL_.SEQUENCE) left outer join _SMDBA_.CATCOMPLEX on (_SMDBA_._SUBJECTS_.SEQ_CATCOMPLEX = _SMDBA_.CATCOMPLEX.SEQUENCE) left outer join _SMDBA_._SUBJTYPES_ on (_SMDBA_._SUBJECTS_.TYPE = _SMDBA_._SUBJTYPES_.SEQUENCE) where ([CI_Assembly Desc] like 'Windows%' or [CI_Assembly Desc] like 'Unix%' or [CI_Assembly Desc] like 'Linux%'  or [CI_Assembly Desc] like 'Exadata%' or [CI_Assembly Desc] like 'AS400%' or [CI_Assembly Desc] like 'Cluster%' or [CI_Assembly Desc] like 'Blade%' or [CI_Assembly Desc] like 'To Be Changed%') and ([Machine Name] not like '%XLR%' and [Machine Name] not like '%XBR%' and [Machine Name] not like '%XMS%') and {0} and [status:] not in ('Off-line', 'To be Disposed', 'Spare','N/A') and [Inactive:]=0"
    
    c = ""
    condition_list = [("No Location","[Server_Location] is Null "+get_exclude_device(0)), 
    ("No CI Zone","[CI_Assembly_Zone] is Null "+get_exclude_device(1)), 
    ("Wrong Status","[status:] in ('Disposed','In stock','N/A') "+get_exclude_device(2)), 
    ("No CI Environement","[CI_Assembly_Env] is Null "+get_exclude_device(3)), 
    ("No Configuration Desc","([CI_Assembly Desc] = 'To be changed' or [CI_Assembly Desc] is NULL) "+get_exclude_device(4)), 
    ("No Criticality - No Application Linked","[max_cat_order] is Null "+get_exclude_device(5))]
    for condition in condition_list:
        full_query = conn_str.format(condition[1])
        logger.info("full_query:%s"%full_query)
        cursor.execute(full_query)
        result = cursor.fetchone()
        integrity_data[condition[0]] = result[0]        
    logger.info("integrity_data:%s"%integrity_data)
    _close_connection(db_conn)
    return integrity_data 


def _get_cmdb_report_backup():
    db_conn = _get_connection()
    cursor = db_conn.cursor()
    logger.info('get_cmdb_report_backup')

    back_data = collections.OrderedDict()
    
    conn_str = "select count(distinct [Machine Name]) from _SMDBA_.[Inventory Items] left outer join _SMDBA_.Srv_Cat_Link on (SEQ_ITEM_SERVER = _SMDBA_.[Inventory Items].Sequence) left outer join _SMDBA_._SUBJECTS_ on (_SMDBA_.Srv_Cat_Link.SEQ_CATEGORY = _SMDBA_._SUBJECTS_.SEQUENCE) left outer join _SMDBA_._PERSONNEL_ on (_SMDBA_.Srv_Cat_Link.SEQ_STAFF = _SMDBA_._PERSONNEL_.SEQUENCE) left outer join _SMDBA_.CATCOMPLEX on (_SMDBA_._SUBJECTS_.SEQ_CATCOMPLEX = _SMDBA_.CATCOMPLEX.SEQUENCE) left outer join _SMDBA_._SUBJTYPES_ on (_SMDBA_._SUBJECTS_.TYPE = _SMDBA_._SUBJTYPES_.SEQUENCE) where ([CI_Assembly Desc] like 'Windows%' or [CI_Assembly Desc] like 'Unix%' or [CI_Assembly Desc] like 'Linux%'  or [CI_Assembly Desc] like 'Exadata%' or [CI_Assembly Desc] like 'AS400%' or [CI_Assembly Desc] like 'Cluster%' or [CI_Assembly Desc] like 'Blade%' or [CI_Assembly Desc] like 'To Be Changed%') and ([Machine Name] not like '%XLR%' and [Machine Name] not like '%XBR%' and [Machine Name] not like '%XMS%') and {0} and [status:] not in ('Off-line', 'To be Disposed', 'Spare','N/A') and [Inactive:]=0"
    condition_list = [("No Backup","[CI_Backup_ID] is NULL "+get_exclude_device(6)),
    ("Wrong Backup AS400 Prod","([CI_Backup_ID] != 'Internal - BRMS' or [CI_Backup_ID] is null) and [CI_Assembly Desc] = 'AS400 Servers' and [CI_Assembly_Env] = 'PRD' "+get_exclude_device(7)),
    ("Wrong Backup AS400 Non Prod","[CI_Backup_ID] != 'Not required - Others' and [CI_Assembly Desc] = 'AS400 Servers' and ([CI_Assembly_Env] != 'PRD' or [CI_Assembly_Env] is null) "+get_exclude_device(8)),
    ("Wrong Backup non AS400 - Datacenter Internal (no Chic)","([CI_Backup_ID] IN ('External - CHIC','External - Others','External - SAP','Internal - BRMS','Internal - DFSR') or [CI_Backup_ID] is null) and ([CI_Assembly Desc] != 'AS400 Servers' or [CI_Assembly Desc] is null) and [Server_Location] IN ('FR - Interxion','FR - Chevilly Chic') and [Machine Name] NOT IN ('LVMHCVYONEDIR01','LVMHCVYONEDIR02','LVMHCVYONEDIR03','LVMHCVYONEDIR04','LVMHCVYONEDIR05','LVMHCVYONEDIR06','LVMHCVYONEDIR07','LVMHCVYONEDIR08','LVMHCVYONEDIR09','LVMHCVYONEDIR10','LVMHCVYONEDIR17','LVMHCVYONEDIR19','LVMHDRPONEDIR01','LVMHDRPONEDIR02','LVMHDRPONEDIR03','LVMHDRPONEDIR04','LVMHDRPONEDIR05','LVMHDRPONEDIR06','LVMHDRPONEDIR07','LVMHDRPONEDIR09','LVMHDRPONEDIR17','LVMHDRPONEDIR19','LVMHRUEONEDIR17','LVMHRUEONEDIR19') "+get_exclude_device(9)),
    ("Wrong Backup non AS400 - Datacenter External Chic","([CI_Backup_ID] != 'External - CHIC' or [CI_Backup_ID] is null) and ([CI_Assembly Desc] <> 'AS400 Servers' or [CI_Assembly Desc] is null) and [Server_Location] IN ('SG - Singapore Chic','US - Secaucus Chic','US - Atlanta Chic') "+get_exclude_device(10)),
    ("Wrong Backup non AS400 - NAS & MYITStoreBox","[CI_Backup_ID] <> 'Internal - DFSR' and [CI_Backup_ID] != 'Not Required - VM Host' and [CI_Backup_ID] not like 'Not Required % MyITStoreB' and [CI_Backup_ID] is not Null and [CI_Assembly Desc] <> 'AS400 Servers' and [CI_Assembly Desc] is not Null and [Server_Location] NOT IN ('ES - Barbera del Valles','ES - Barcelona','FR - Datacenter IBM-SAP Grabels','FR - Datacenter IBM-SAP Montpellier','FR - Interxion','FR - Chevilly Chic','IT - Fiesso','JP - Japan','RO - Somarest','RU - HQ Moscow','SG - Singapore Chic','SG - Datacenter IBM-SAP Singapore','US - Secaucus Chic','US - Atlanta Chic','SG - Defu Lane','FR - Datacenter Equinix') and [Server_Location] is not Null and [Machine Name] NOT IN ('LVMHCVYONEDIR01','LVMHCVYONEDIR02','LVMHCVYONEDIR03','LVMHCVYONEDIR04','LVMHCVYONEDIR05','LVMHCVYONEDIR06','LVMHCVYONEDIR17','LVMHCVYONEDIR19','LVMHDRPONEDIR01','LVMHDRPONEDIR02','LVMHDRPONEDIR03','LVMHDRPONEDIR04','LVMHDRPONEDIR05','LVMHDRPONEDIR06','LVMHDRPONEDIR07','LVMHDRPONEDIR09','LVMHDRPONEDIR17','LVMHDRPONEDIR19','LVMHRUEONEDIR17','LVMHRUEONEDIR19') "+get_exclude_device(11)),
    ("Wrong Backup non AS400 - Datacenter External SAP","([CI_Backup_ID] <> 'External - SAP' or [CI_Backup_ID] is null) and ([CI_Assembly Desc] <> 'AS400 Servers' or [CI_Assembly Desc] is null) and [Server_Location] IN ('FR - Datacenter IBM-SAP Grabels','FR - Datacenter IBM-SAP Montpellier','SG - Datacenter IBM-SAP Singapore') "+get_exclude_device(12))]
    for condition in condition_list:
        full_query = conn_str.format(condition[1])
        cursor.execute(full_query)
        result = cursor.fetchone()
        back_data[condition[0]] = result[0]
    logger.info("back_data:%s"%back_data)
    _close_connection(db_conn)
    return back_data	

def _get_cmdb_report_monitoring():
    db_conn = _get_connection()
    cursor = db_conn.cursor()
    logger.info('get_cmdb_report_monitoring')
    monitoring_data = collections.OrderedDict()
    conn_str = "select count(distinct [Machine Name]) from _SMDBA_.[Inventory Items] left outer join _SMDBA_.Srv_Cat_Link on (SEQ_ITEM_SERVER = _SMDBA_.[Inventory Items].Sequence) left outer join _SMDBA_._SUBJECTS_ on (_SMDBA_.Srv_Cat_Link.SEQ_CATEGORY = _SMDBA_._SUBJECTS_.SEQUENCE) left outer join _SMDBA_._PERSONNEL_ on (_SMDBA_.Srv_Cat_Link.SEQ_STAFF = _SMDBA_._PERSONNEL_.SEQUENCE) left outer join _SMDBA_.CATCOMPLEX on (_SMDBA_._SUBJECTS_.SEQ_CATCOMPLEX = _SMDBA_.CATCOMPLEX.SEQUENCE) left outer join _SMDBA_._SUBJTYPES_ on (_SMDBA_._SUBJECTS_.TYPE = _SMDBA_._SUBJTYPES_.SEQUENCE) where ([CI_Assembly Desc] like 'Windows%' or [CI_Assembly Desc] like 'Unix%' or [CI_Assembly Desc] like 'Linux%'  or [CI_Assembly Desc] like 'Exadata%' or [CI_Assembly Desc] like 'AS400%' or [CI_Assembly Desc] like 'Cluster%' or [CI_Assembly Desc] like 'Blade%' or [CI_Assembly Desc] like 'To Be Changed%') and ([Machine Name] not like '%XLR%' and [Machine Name] not like '%XBR%' and [Machine Name] not like '%XMS%') and {0} and [status:] not in ('Off-line', 'To be Disposed', 'Spare','N/A') and [Inactive:]=0"
    condition_list = [("No Monitoring","[CI_Monitor_ID] IS NULL "+get_exclude_device(13)),
    ("Wrong Monitoring AS400 Production","([CI_Monitor_ID] <> 'Internal - Tango' or [CI_Monitor_ID] is null) and [CI_Assembly Desc] = 'AS400 Servers' "+get_exclude_device(14)),
    ("Wrong Monitoring non AS400 - Datacenter Internal (no Chic)","([CI_Monitor_ID] <> 'Internal - Patrol' or [CI_Monitor_ID] is null) and ([CI_Assembly Desc] NOT IN ('AS400 Servers','EXADATA SERVERS') or [CI_Assembly Desc] is null) and [Server_Location] IN ('FR - Interxion','FR - Chevilly Chic') and [Machine Name] NOT IN ('NLVCP073D','NLVCP1192D') "+get_exclude_device(15)),
    ("Wrong Monitoring non AS400 - Datacenter External Chic","([CI_Monitor_ID] <> 'Internal - Patrol' or [CI_Monitor_ID] is null) and ([CI_Assembly Desc] <> 'AS400 Servers' or [CI_Assembly Desc] is null) and [Server_Location] IN ('SG - Singapore Chic','US - Secaucus Chic','US - Atlanta Chic') "+get_exclude_device(16)),
    ("Wrong Monitoring non AS400 - NAS","([CI_Monitor_ID] <> 'Internal - Patrol' or [CI_Monitor_ID] is null) and [CI_Assembly Desc] <> 'AS400 Servers' and [CI_Assembly Desc] is not Null and [Server_Location] NOT IN ('FR - Datacenter IBM-SAP Grabels','FR - Datacenter IBM-SAP Montpellier','SG - Datacenter IBM-SAP Singapore','FR - Interxion','FR - Chevilly Chic','SG - Singapore Chic','US - Secaucus Chic','US - Atlanta Chic') and [Server_Location] is not Null "+get_exclude_device(17)),
    ("Wrong Monitoring non AS400 - Datacenter External SAP","([CI_Monitor_ID] <> 'External' or [CI_Monitor_ID] is null) and ([CI_Assembly Desc] <> 'AS400 Servers' or [CI_Assembly Desc] is null) and [Server_Location] IN ('FR - Datacenter IBM-SAP Grabels','FR - Datacenter IBM-SAP Montpellier','SG - Datacenter IBM-SAP Singapore') "+get_exclude_device(18))]
    for condition in condition_list:
        full_query = conn_str.format(condition[1])
        cursor.execute(full_query)
        result = cursor.fetchone()
        monitoring_data[condition[0]] = result[0]
    logger.info("monitoring_data:%s"%monitoring_data)
    _close_connection(db_conn) 
    return monitoring_data

##################################################################################

def _get_cmdb_device():
    db_conn = _get_connection()
    cursor = db_conn.cursor()
    logger.info('get_cmdb_device') 
    conn_str = "select Distinct([Machine Name]) from _SMDBA_.[Inventory Items] left outer join _SMDBA_.Srv_Cat_Link on (SEQ_ITEM_SERVER = _SMDBA_.[Inventory Items].Sequence) left outer join _SMDBA_._SUBJECTS_ on (_SMDBA_.Srv_Cat_Link.SEQ_CATEGORY = _SMDBA_._SUBJECTS_.SEQUENCE) left outer join _SMDBA_._PERSONNEL_ on (_SMDBA_.Srv_Cat_Link.SEQ_STAFF = _SMDBA_._PERSONNEL_.SEQUENCE) left outer join _SMDBA_.CATCOMPLEX on (_SMDBA_._SUBJECTS_.SEQ_CATCOMPLEX = _SMDBA_.CATCOMPLEX.SEQUENCE) left outer join _SMDBA_._SUBJTYPES_ on (_SMDBA_._SUBJECTS_.TYPE = _SMDBA_._SUBJTYPES_.SEQUENCE) where ([CI_Assembly Desc] like 'Windows%' or [CI_Assembly Desc] like 'Unix%' or [CI_Assembly Desc] like 'Linux%'  or [CI_Assembly Desc] like 'Exadata%' or [CI_Assembly Desc] like 'AS400%' or [CI_Assembly Desc] like 'Cluster%' or [CI_Assembly Desc] like 'Blade%' or [CI_Assembly Desc] like 'To Be Changed%' ) and [Inactive:]=0 "
    cursor.execute(conn_str)
    result = cursor.fetchall()
    device_list= []
    for row in result:
        device_list.append(row[0])
    with open(os.path.join(settings.UPLOAD_FOLDER, 'device_list.json'),'w') as devicefile:
        json.dump(device_list,devicefile)
    _close_connection(db_conn)
    return device_list 
	
	
	
def get_exclude_device(int_n):
    n = str(int_n)
    first = True
    json_url = os.path.join(settings.UPLOAD_FOLDER,'exclude_list.json')
    with open(json_url, 'r') as excludefile:
        list = json.load(excludefile)
    listexcl = []
    for key, value in list.items():
        if n in value:
            listexcl.append(key)
    req = " AND [Machine Name] NOT IN ('"
    for device in listexcl:
        if first:
            req = req + device
            first = False
        else :
            req = req + "','" + device
    req = req+"')"
    return req
	


def _get_cmdb_report_updated(n):
    db_conn = _get_connection()
    cursor = db_conn.cursor()
    logger.info('get_cmdb_report_updated')
    update_data = collections.OrderedDict()
    c = get_exclude_device(n)

    conn_str = "select count(distinct [Machine Name]) from _SMDBA_.[Inventory Items] left outer join _SMDBA_.Srv_Cat_Link on (SEQ_ITEM_SERVER = _SMDBA_.[Inventory Items].Sequence) left outer join _SMDBA_._SUBJECTS_ on (_SMDBA_.Srv_Cat_Link.SEQ_CATEGORY = _SMDBA_._SUBJECTS_.SEQUENCE) left outer join _SMDBA_._PERSONNEL_ on (_SMDBA_.Srv_Cat_Link.SEQ_STAFF = _SMDBA_._PERSONNEL_.SEQUENCE) left outer join _SMDBA_.CATCOMPLEX on (_SMDBA_._SUBJECTS_.SEQ_CATCOMPLEX = _SMDBA_.CATCOMPLEX.SEQUENCE) left outer join _SMDBA_._SUBJTYPES_ on (_SMDBA_._SUBJECTS_.TYPE = _SMDBA_._SUBJTYPES_.SEQUENCE) where ([CI_Assembly Desc] like 'Windows%' or [CI_Assembly Desc] like 'Unix%' or [CI_Assembly Desc] like 'Linux%'  or [CI_Assembly Desc] like 'Exadata%' or [CI_Assembly Desc] like 'AS400%' or [CI_Assembly Desc] like 'Cluster%' or [CI_Assembly Desc] like 'Blade%' or [CI_Assembly Desc] like 'To Be Changed%') and {0} and [status:] not in ('Off-line', 'To be Disposed', 'Spare','N/A') and [Inactive:]=0"   
    
   
    alist = [("No Location","[Server_Location] is Null"), 
    ("No CI Zone","[CI_Assembly_Zone] is Null"), 
    ("Wrong Status","[status:] in ('Disposed','In stock','N/A')"), 
    ("No CI Environement","[CI_Assembly_Env] is Null"), 
    ("No Configuration Desc","([CI_Assembly Desc] = 'To be changed' or [CI_Assembly Desc] is NULL)"), 
    ("No Criticality - No Application Linked","[max_cat_order] is Null"),
    ("No Backup","[CI_Backup_ID] is NULL"),
    ("Wrong Backup AS400 Prod","([CI_Backup_ID] != 'Internal - BRMS' or [CI_Backup_ID] is null) and [CI_Assembly Desc] = 'AS400 Servers' and [CI_Assembly_Env] = 'PRD'"),
    ("Wrong Backup AS400 Non Prod","[CI_Backup_ID] != 'Not required - Others' and [CI_Assembly Desc] = 'AS400 Servers' and ([CI_Assembly_Env] != 'PRD' or [CI_Assembly_Env] is null)"),
    ("Wrong Backup non AS400 - Datacenter Internal (no Chic)","([CI_Backup_ID] IN ('External - CHIC','External - Others','External - SAP','Internal - BRMS','Internal - DFSR') or [CI_Backup_ID] is null) and ([CI_Assembly Desc] != 'AS400 Servers' or [CI_Assembly Desc] is null) and [Server_Location] IN ('FR - Interxion','FR - Chevilly Chic') and [Machine Name] NOT IN ('LVMHCVYONEDIR01','LVMHCVYONEDIR02','LVMHCVYONEDIR03','LVMHCVYONEDIR04','LVMHCVYONEDIR05','LVMHCVYONEDIR06','LVMHCVYONEDIR07','LVMHCVYONEDIR08','LVMHCVYONEDIR09','LVMHCVYONEDIR10','LVMHCVYONEDIR17','LVMHCVYONEDIR19','LVMHDRPONEDIR01','LVMHDRPONEDIR02','LVMHDRPONEDIR03','LVMHDRPONEDIR04','LVMHDRPONEDIR05','LVMHDRPONEDIR06','LVMHDRPONEDIR07','LVMHDRPONEDIR09','LVMHDRPONEDIR17','LVMHDRPONEDIR19','LVMHRUEONEDIR17','LVMHRUEONEDIR19')"),
    ("Wrong Backup non AS400 - Datacenter External Chic","([CI_Backup_ID] != 'External - CHIC' or [CI_Backup_ID] is null) and ([CI_Assembly Desc] <> 'AS400 Servers' or [CI_Assembly Desc] is null) and [Server_Location] IN ('SG - Singapore Chic','US - Secaucus Chic','US - Atlanta Chic')"),
    ("Wrong Backup non AS400 - NAS & MYITStoreBox","[CI_Backup_ID] <> 'Internal - DFSR' and [CI_Backup_ID] != 'Not Required - VM Host' and [CI_Backup_ID] not like 'Not Required % MyITStoreB' and [CI_Backup_ID] is not Null and [CI_Assembly Desc] <> 'AS400 Servers' and [CI_Assembly Desc] is not Null and [Server_Location] NOT IN ('ES - Barbera del Valles','ES - Barcelona','FR - Datacenter IBM-SAP Grabels','FR - Datacenter IBM-SAP Montpellier','FR - Interxion','FR - Chevilly Chic','IT - Fiesso','JP - Japan','RO - Somarest','RU - HQ Moscow','SG - Singapore Chic','SG - Datacenter IBM-SAP Singapore','US - Secaucus Chic','US - Atlanta Chic','SG - Defu Lane','FR - Datacenter Equinix') and [Server_Location] is not Null and [Machine Name] NOT IN ('LVMHCVYONEDIR01','LVMHCVYONEDIR02','LVMHCVYONEDIR03','LVMHCVYONEDIR04','LVMHCVYONEDIR05','LVMHCVYONEDIR06','LVMHCVYONEDIR17','LVMHCVYONEDIR19','LVMHDRPONEDIR01','LVMHDRPONEDIR02','LVMHDRPONEDIR03','LVMHDRPONEDIR04','LVMHDRPONEDIR05','LVMHDRPONEDIR06','LVMHDRPONEDIR07','LVMHDRPONEDIR09','LVMHDRPONEDIR17','LVMHDRPONEDIR19','LVMHRUEONEDIR17','LVMHRUEONEDIR19')"),
    ("Wrong Backup non AS400 - Datacenter External SAP","([CI_Backup_ID] <> 'External - SAP' or [CI_Backup_ID] is null) and ([CI_Assembly Desc] <> 'AS400 Servers' or [CI_Assembly Desc] is null) and [Server_Location] IN ('FR - Datacenter IBM-SAP Grabels','FR - Datacenter IBM-SAP Montpellier','SG - Datacenter IBM-SAP Singapore')"),
    ("No Monitoring","[CI_Monitor_ID] IS NULL"),
    ("Wrong Monitoring AS400 Production","([CI_Monitor_ID] <> 'Internal - Tango' or [CI_Monitor_ID] is null) and [CI_Assembly Desc] = 'AS400 Servers'"),
    ("Wrong Monitoring non AS400 - Datacenter Internal (no Chic)","([CI_Monitor_ID] <> 'Internal - Patrol' or [CI_Monitor_ID] is null) and ([CI_Assembly Desc] NOT IN ('AS400 Servers','EXADATA SERVERS') or [CI_Assembly Desc] is null) and [Server_Location] IN ('FR - Interxion','FR - Chevilly Chic') and [Machine Name] NOT IN ('NLVCP073D','NLVCP1192D')"),
    ("Wrong Monitoring non AS400 - Datacenter External Chic","([CI_Monitor_ID] <> 'Internal - Patrol' or [CI_Monitor_ID] is null) and ([CI_Assembly Desc] <> 'AS400 Servers' or [CI_Assembly Desc] is null) and [Server_Location] IN ('SG - Singapore Chic','US - Secaucus Chic','US - Atlanta Chic')"),
    ("Wrong Monitoring non AS400 - NAS","([CI_Monitor_ID] <> 'Internal - Patrol' or [CI_Monitor_ID] is null) and [CI_Assembly Desc] <> 'AS400 Servers' and [CI_Assembly Desc] is not Null and [Server_Location] NOT IN ('FR - Datacenter IBM-SAP Grabels','FR - Datacenter IBM-SAP Montpellier','SG - Datacenter IBM-SAP Singapore','FR - Interxion','FR - Chevilly Chic','SG - Singapore Chic','US - Secaucus Chic','US - Atlanta Chic') and [Server_Location] is not Null"),
    ("Wrong Monitoring non AS400 - Datacenter External SAP","([CI_Monitor_ID] <> 'External' or [CI_Monitor_ID] is null) and ([CI_Assembly Desc] <> 'AS400 Servers' or [CI_Assembly Desc] is null) and [Server_Location] IN ('FR - Datacenter IBM-SAP Grabels','FR - Datacenter IBM-SAP Montpellier','SG - Datacenter IBM-SAP Singapore')")]
    
    condition = alist[n]
    
    full_query = conn_str.format(condition[1]+" "+c)
    tot = cursor.execute(full_query)
    result = cursor.fetchone()
    logger.info("update_data:%s"%update_data)
    _close_connection(db_conn)
    tab = [n,result[0],full_query]
    return tab

def _get_cmdb_device_name(n):
    db_conn = _get_connection()
    cursor = db_conn.cursor()
    logger.info('get_cmdb_device_name') 
    
    conn_str = "select Distinct([Machine Name]) from _SMDBA_.[Inventory Items] left outer join _SMDBA_.Srv_Cat_Link on (SEQ_ITEM_SERVER = _SMDBA_.[Inventory Items].Sequence) left outer join _SMDBA_._SUBJECTS_ on (_SMDBA_.Srv_Cat_Link.SEQ_CATEGORY = _SMDBA_._SUBJECTS_.SEQUENCE) left outer join _SMDBA_._PERSONNEL_ on (_SMDBA_.Srv_Cat_Link.SEQ_STAFF = _SMDBA_._PERSONNEL_.SEQUENCE) left outer join _SMDBA_.CATCOMPLEX on (_SMDBA_._SUBJECTS_.SEQ_CATCOMPLEX = _SMDBA_.CATCOMPLEX.SEQUENCE) left outer join _SMDBA_._SUBJTYPES_ on (_SMDBA_._SUBJECTS_.TYPE = _SMDBA_._SUBJTYPES_.SEQUENCE) where ([CI_Assembly Desc] like 'Windows%' or [CI_Assembly Desc] like 'Unix%' or [CI_Assembly Desc] like 'Linux%' or [CI_Assembly Desc] like 'Exadata%' or [CI_Assembly Desc] like 'AS400%' or [CI_Assembly Desc] like 'Cluster%' or [CI_Assembly Desc] like 'Blade%' or [CI_Assembly Desc] like 'To Be Changed%' ) and ([Machine Name] not like '%XLR%' and [Machine Name] not like '%XBR%' and [Machine Name] not like '%XMS%') and {0} and [status:] not in ('Off-line', 'To be Disposed', 'Spare','N/A') and [Inactive:]=0"
    
    c = get_exclude_device(n)
     
    l_req = [("No Location","[Server_Location] is Null"), ("No CI Zone","[CI_Assembly_Zone] is Null"),("wrong Status","[status:] in ('Disposed','In stock','N/A')"), ("No CI Environement","[CI_Assembly_Env] is Null"),("No Configuration Desc","([CI_Assembly Desc] = 'To be changed' or [CI_Assembly Desc] is NULL)"),("No Criticality - No Application Linked","[max_cat_order] is Null"),("No Backup","[CI_Backup_ID] is NULL"),("Wrong Backup AS400 Prod","([CI_Backup_ID] != 'Internal - BRMS' or [CI_Backup_ID] is null) and [CI_Assembly Desc] = 'AS400 Servers' and [CI_Assembly_Env] = 'PRD'"),("Wrong Backup AS400 Non Prod","[CI_Backup_ID] != 'Not required - Others' and [CI_Assembly Desc] = 'AS400 Servers' and ([CI_Assembly_Env] != 'PRD' or [CI_Assembly_Env] is null)"),
    ("Wrong Backup non AS400 - Datacenter Internal (no Chic)","([CI_Backup_ID] IN ('External - CHIC','External - Others','External - SAP','Internal - BRMS','Internal - DFSR') or [CI_Backup_ID] is null) and ([CI_Assembly Desc] != 'AS400 Servers' or [CI_Assembly Desc] is null) and [Server_Location] IN ('FR - Interxion','FR - Chevilly Chic') and [Machine Name] NOT IN ('LVMHCVYONEDIR01','LVMHCVYONEDIR02','LVMHCVYONEDIR03','LVMHCVYONEDIR04','LVMHCVYONEDIR05','LVMHCVYONEDIR06','LVMHCVYONEDIR07','LVMHCVYONEDIR08','LVMHCVYONEDIR09','LVMHCVYONEDIR10','LVMHCVYONEDIR17','LVMHCVYONEDIR19','LVMHDRPONEDIR01','LVMHDRPONEDIR02','LVMHDRPONEDIR03','LVMHDRPONEDIR04','LVMHDRPONEDIR05','LVMHDRPONEDIR06','LVMHDRPONEDIR07','LVMHDRPONEDIR09','LVMHDRPONEDIR17','LVMHDRPONEDIR19','LVMHRUEONEDIR17','LVMHRUEONEDIR19') "),
    ("Wrong Backup non AS400 - Datacenter External Chic","([CI_Backup_ID] != 'External - CHIC' or [CI_Backup_ID] is null) and ([CI_Assembly Desc] <> 'AS400 Servers' or [CI_Assembly Desc] is null) and [Server_Location] IN ('SG - Singapore Chic','US - Secaucus Chic','US - Atlanta Chic')"),
    ("Wrong Backup non AS400 - NAS & MYITStoreBox","[CI_Backup_ID] <> 'Internal - DFSR' and [CI_Backup_ID] != 'Not Required - VM Host' and [CI_Backup_ID] not like 'Not Required % MyITStoreB' and [CI_Backup_ID] is not Null and [CI_Assembly Desc] <> 'AS400 Servers' and [CI_Assembly Desc] is not Null and [Server_Location] NOT IN ('ES - Barbera del Valles','ES - Barcelona','FR - Datacenter IBM-SAP Grabels','FR - Datacenter IBM-SAP Montpellier','FR - Interxion','FR - Chevilly Chic','IT - Fiesso','JP - Japan','RO - Somarest','RU - HQ Moscow','SG - Singapore Chic','SG - Datacenter IBM-SAP Singapore','US - Secaucus Chic','US - Atlanta Chic','SG - Defu Lane','FR - Datacenter Equinix') and [Server_Location] is not Null and [Machine Name] NOT IN ('LVMHCVYONEDIR01','LVMHCVYONEDIR02','LVMHCVYONEDIR03','LVMHCVYONEDIR04','LVMHCVYONEDIR05','LVMHCVYONEDIR06','LVMHCVYONEDIR17','LVMHCVYONEDIR19','LVMHDRPONEDIR01','LVMHDRPONEDIR02','LVMHDRPONEDIR03','LVMHDRPONEDIR04','LVMHDRPONEDIR05','LVMHDRPONEDIR06','LVMHDRPONEDIR07','LVMHDRPONEDIR09','LVMHDRPONEDIR17','LVMHDRPONEDIR19','LVMHRUEONEDIR17','LVMHRUEONEDIR19') "),
    ("Wrong Backup non AS400 - Datacenter External SAP","([CI_Backup_ID] <> 'External - SAP' or [CI_Backup_ID] is null) and ([CI_Assembly Desc] <> 'AS400 Servers' or [CI_Assembly Desc] is null) and [Server_Location] IN ('FR - Datacenter IBM-SAP Grabels','FR - Datacenter IBM-SAP Montpellier','SG - Datacenter IBM-SAP Singapore')"),("No Monitoring","[CI_Monitor_ID] IS NULL"),
    ("Wrong Monitoring AS400 Production","([CI_Monitor_ID] <> 'Internal - Tango' or [CI_Monitor_ID] is null) and [CI_Assembly Desc] = 'AS400 Servers'"),
    ("Wrong Monitoring non AS400 - Datacenter Internal (no Chic)","([CI_Monitor_ID] <> 'Internal - Patrol' or [CI_Monitor_ID] is null) and ([CI_Assembly Desc] NOT IN ('AS400 Servers','EXADATA SERVERS') or [CI_Assembly Desc] is null) and [Server_Location] IN ('FR - Interxion','FR - Chevilly Chic') and [Machine Name] NOT IN ('NLVCP073D','NLVCP1192D') "),
    ("Wrong Monitoring non AS400 - Datacenter External Chic","([CI_Monitor_ID] <> 'Internal - Patrol' or [CI_Monitor_ID] is null) and ([CI_Assembly Desc] <> 'AS400 Servers' or [CI_Assembly Desc] is null) and [Server_Location] IN ('SG - Singapore Chic','US - Secaucus Chic','US - Atlanta Chic')"),
    ("Wrong Monitoring non AS400 - NAS","([CI_Monitor_ID] <> 'Internal - Patrol' or [CI_Monitor_ID] is null) and [CI_Assembly Desc] <> 'AS400 Servers' and [CI_Assembly Desc] is not Null and [Server_Location] NOT IN ('FR - Datacenter IBM-SAP Grabels','FR - Datacenter IBM-SAP Montpellier','SG - Datacenter IBM-SAP Singapore','FR - Interxion','FR - Chevilly Chic','SG - Singapore Chic','US - Secaucus Chic','US - Atlanta Chic') and [Server_Location] is not Null"),
    ("Wrong Monitoring non AS400 - Datacenter External SAP","([CI_Monitor_ID] <> 'External' or [CI_Monitor_ID] is null) and ([CI_Assembly Desc] <> 'AS400 Servers' or [CI_Assembly Desc] is null) and [Server_Location] IN ('FR - Datacenter IBM-SAP Grabels','FR - Datacenter IBM-SAP Montpellier','SG - Datacenter IBM-SAP Singapore') ")]
    
    req = l_req[n][1]
    full_query = conn_str.format(req+" "+c)
    cursor.execute(full_query)
    result = cursor.fetchall()
    device_list= []
    for row in result:
        device_list.append(row[0])
    _close_connection(db_conn)
    
    return device_list
def _get_oneretail_report_integrity():
    db_conn = _get_connection()
    cursor = db_conn.cursor()
    logger.info('get_oneretail_report_no_location')

    oneretail_integrity_data = collections.OrderedDict() 
    
    conn_str = "select count(distinct [Machine Name]) from _SMDBA_.[Inventory Items] left outer join _SMDBA_.Srv_Cat_Link on (SEQ_ITEM_SERVER = _SMDBA_.[Inventory Items].Sequence) left outer join _SMDBA_._SUBJECTS_ on (_SMDBA_.Srv_Cat_Link.SEQ_CATEGORY = _SMDBA_._SUBJECTS_.SEQUENCE) left outer join _SMDBA_._PERSONNEL_ on (_SMDBA_.Srv_Cat_Link.SEQ_STAFF = _SMDBA_._PERSONNEL_.SEQUENCE) left outer join _SMDBA_.CATCOMPLEX on (_SMDBA_._SUBJECTS_.SEQ_CATCOMPLEX = _SMDBA_.CATCOMPLEX.SEQUENCE) left outer join _SMDBA_._SUBJTYPES_ on (_SMDBA_._SUBJECTS_.TYPE = _SMDBA_._SUBJTYPES_.SEQUENCE) where ([CI_Assembly Desc] like 'Windows%' or [CI_Assembly Desc] like 'Unix%' or [CI_Assembly Desc] like 'Linux%' or [CI_Assembly Desc] like 'Exadata%' or [CI_Assembly Desc] like 'AS400%' or [CI_Assembly Desc] like 'Cluster%' or [CI_Assembly Desc] like 'Blade%' or [CI_Assembly Desc] like 'To Be Changed%') and ([Machine Name] like '%XLR%' or [Machine Name] like '%XBR%' or [Machine Name] like '%XMS%') and {0} and ([status:] not in ('Off-line', 'To be Disposed', 'Spare','N/A') or [status:] is NULL) and [Inactive:]=0"
    
    c = ""
    condition_list_oneretail = [("No Location","[Server_Location] is Null "+get_exclude_device_oneretail(0)), 
    ("No CI Zone","[CI_Assembly_Zone] is Null "+get_exclude_device_oneretail(1)), 
    ("Wrong Status","[status:] not like 'In Production%' "+get_exclude_device_oneretail(2)), 
    ("No CI Environement","[CI_Assembly_Env] is Null "+get_exclude_device_oneretail(3)), 
    ("No Configuration Desc","([CI_Assembly Desc] = 'To be changed' or [CI_Assembly Desc] is NULL) "+get_exclude_device_oneretail(4)), 
    ("No Criticality - No Application Linked","[max_cat_order] is Null "+get_exclude_device_oneretail(5)),
    ("No Configuration ID","[Configuration ID] is Null "+get_exclude_device_oneretail(6)),
    ("Wrong Configuration ID","([Configuration ID] <> 'WINDOWS SERVERS' and  [Configuration ID] is not Null) "+get_exclude_device_oneretail(7)),
    ("No Host Function","[Server_Host Function] is Null "+get_exclude_device_oneretail(8)),
    ("Wrong CI Environment","[CI_Assembly_Env] not like 'PRD%' "+get_exclude_device_oneretail(9)),
    ("No Technical Leader","[Server_TL_Login ID] is Null "+get_exclude_device_oneretail(10)),
    ("No Reboot Plan","[Concatenation] is Null "+get_exclude_device_oneretail(11))]
    for condition in condition_list_oneretail:
        full_query = conn_str.format(condition[1])
        logger.info("full_query:%s"%full_query)
        cursor.execute(full_query)
        result = cursor.fetchone()
        oneretail_integrity_data[condition[0]] = result[0]        
    logger.info("condition_list_oneretail:%s"%condition_list_oneretail)
    _close_connection(db_conn)
    return oneretail_integrity_data

def _get_oneretail_report_backup():
    db_conn = _get_connection()
    cursor = db_conn.cursor()
    logger.info('get_oneretail_report_backup')

    oneretail_back_data = collections.OrderedDict()
    
    conn_str = "select count(distinct [Machine Name]) from _SMDBA_.[Inventory Items] left outer join _SMDBA_.Srv_Cat_Link on (SEQ_ITEM_SERVER = _SMDBA_.[Inventory Items].Sequence) left outer join _SMDBA_._SUBJECTS_ on (_SMDBA_.Srv_Cat_Link.SEQ_CATEGORY = _SMDBA_._SUBJECTS_.SEQUENCE) left outer join _SMDBA_._PERSONNEL_ on (_SMDBA_.Srv_Cat_Link.SEQ_STAFF = _SMDBA_._PERSONNEL_.SEQUENCE) left outer join _SMDBA_.CATCOMPLEX on (_SMDBA_._SUBJECTS_.SEQ_CATCOMPLEX = _SMDBA_.CATCOMPLEX.SEQUENCE) left outer join _SMDBA_._SUBJTYPES_ on (_SMDBA_._SUBJECTS_.TYPE = _SMDBA_._SUBJTYPES_.SEQUENCE) where ([CI_Assembly Desc] like 'Windows%' or [CI_Assembly Desc] like 'Unix%' or [CI_Assembly Desc] like 'Linux%' or [CI_Assembly Desc] like 'Exadata%' or [CI_Assembly Desc] like 'AS400%' or [CI_Assembly Desc] like 'Cluster%' or [CI_Assembly Desc] like 'Blade%' or [CI_Assembly Desc] like 'To Be Changed%') and ([Machine Name] like '%XLR%' or [Machine Name] like '%XBR%' or [Machine Name] like '%XMS%') and {0} and ([status:] not in ('Off-line', 'To be Disposed', 'Spare', 'N/A') or [status:] is NULL) and [Inactive:]=0"
    condition_list_oneretail = [("No Backup","[CI_Backup_ID] is NULL "+get_exclude_device_oneretail(12)),
    ("Wrong Backup ID","([CI_Backup_ID] != 'Nor Required - No Infra' and [CI_Backup_ID] is not null) "+get_exclude_device_oneretail(13))]
    for condition in condition_list_oneretail:
        full_query = conn_str.format(condition[1])
        cursor.execute(full_query)
        result = cursor.fetchone()
        oneretail_back_data[condition[0]] = result[0]
    logger.info("back_data:%s"%oneretail_back_data)
    _close_connection(db_conn)
    return oneretail_back_data

def _get_oneretail_report_monitoring():
    db_conn = _get_connection()
    cursor = db_conn.cursor()
    logger.info('get_cmdb_report_monitoring')
    oneretail_monitoring_data = collections.OrderedDict()
    conn_str = "select count(distinct [Machine Name]) from _SMDBA_.[Inventory Items] left outer join _SMDBA_.Srv_Cat_Link on (SEQ_ITEM_SERVER = _SMDBA_.[Inventory Items].Sequence) left outer join _SMDBA_._SUBJECTS_ on (_SMDBA_.Srv_Cat_Link.SEQ_CATEGORY = _SMDBA_._SUBJECTS_.SEQUENCE) left outer join _SMDBA_._PERSONNEL_ on (_SMDBA_.Srv_Cat_Link.SEQ_STAFF = _SMDBA_._PERSONNEL_.SEQUENCE) left outer join _SMDBA_.CATCOMPLEX on (_SMDBA_._SUBJECTS_.SEQ_CATCOMPLEX = _SMDBA_.CATCOMPLEX.SEQUENCE) left outer join _SMDBA_._SUBJTYPES_ on (_SMDBA_._SUBJECTS_.TYPE = _SMDBA_._SUBJTYPES_.SEQUENCE) where ([CI_Assembly Desc] like 'Windows%' or [CI_Assembly Desc] like 'Unix%' or [CI_Assembly Desc] like 'Linux%' or [CI_Assembly Desc] like 'Exadata%' or [CI_Assembly Desc] like 'AS400%' or [CI_Assembly Desc] like 'Cluster%' or [CI_Assembly Desc] like 'Blade%' or [CI_Assembly Desc] like 'To Be Changed%') and ([Machine Name] like '%XLR%' or [Machine Name] like '%XBR%' or [Machine Name] like '%XMS%') and {0} and ([status:] not in ('Off-line', 'To be Disposed', 'Spare', 'N/A') or [status:] is NULL) and [Inactive:]=0"
    condition_list_oneretail = [("No Monitoring","[CI_Monitor_ID] IS NULL "+get_exclude_device_oneretail(14)),
    ("Wrong Monitoring ","([CI_Monitor_ID] <> 'Internal - Patrol' and [CI_Monitor_ID] is not null)"+get_exclude_device_oneretail(15))]
    for condition in condition_list_oneretail:
        full_query = conn_str.format(condition[1])
        cursor.execute(full_query)
        result = cursor.fetchone()
        oneretail_monitoring_data[condition[0]] = result[0]
    logger.info("oneretail_monitoring_data:%s"%oneretail_monitoring_data)
    _close_connection(db_conn) 
    return oneretail_monitoring_data

def _get_oneretail_device():
    db_conn = _get_connection()
    cursor = db_conn.cursor()
    logger.info('get_oneretail_device') 
    conn_str = "select Distinct([Machine Name]) from _SMDBA_.[Inventory Items] left outer join _SMDBA_.Srv_Cat_Link on (SEQ_ITEM_SERVER = _SMDBA_.[Inventory Items].Sequence) left outer join _SMDBA_._SUBJECTS_ on (_SMDBA_.Srv_Cat_Link.SEQ_CATEGORY = _SMDBA_._SUBJECTS_.SEQUENCE) left outer join _SMDBA_._PERSONNEL_ on (_SMDBA_.Srv_Cat_Link.SEQ_STAFF = _SMDBA_._PERSONNEL_.SEQUENCE) left outer join _SMDBA_.CATCOMPLEX on (_SMDBA_._SUBJECTS_.SEQ_CATCOMPLEX = _SMDBA_.CATCOMPLEX.SEQUENCE) left outer join _SMDBA_._SUBJTYPES_ on (_SMDBA_._SUBJECTS_.TYPE = _SMDBA_._SUBJTYPES_.SEQUENCE) where ([CI_Assembly Desc] like 'Windows%' or [CI_Assembly Desc] like 'Unix%' or [CI_Assembly Desc] like 'Linux%' or [CI_Assembly Desc] like 'Exadata%' or [CI_Assembly Desc] like 'AS400%' or [CI_Assembly Desc] like 'Cluster%' or [CI_Assembly Desc] like 'Blade%' or [CI_Assembly Desc] like 'To Be Changed%' ) and ([Machine Name] like '%XLR%' or [Machine Name] like '%XBR%' or [Machine Name] like '%XMS%') and [Inactive:]=0 "
    cursor.execute(conn_str)
    result = cursor.fetchall()
    device_list= []
    for row in result:
        device_list.append(row[0])
    with open(os.path.join(settings.UPLOAD_FOLDER, 'oneretail_device_list.json'),'w') as devicefile:
        json.dump(device_list,devicefile)
    _close_connection(db_conn)
    return device_list 

def get_exclude_device_oneretail(int_n):
    n = str(int_n)
    first = True
    json_url = os.path.join(settings.UPLOAD_FOLDER,'oneretail_exclude_list.json')
    with open(json_url, 'r') as excludefile:
        list = json.load(excludefile)
    listexcl = []
    for key, value in list.items():
        if n in value:
            listexcl.append(key)
    req = " AND [Machine Name] NOT IN ('"
    for device in listexcl:
        if first:
            req = req + device
            first = False
        else :
            req = req + "','" + device
    req = req+"')"
    return req


def _get_oneretail_report_updated(n):
    db_conn = _get_connection()
    cursor = db_conn.cursor()
    logger.info('get_oneretail_report_updated')
    update_data = collections.OrderedDict()
    c = get_exclude_device_oneretail(n)

    conn_str = "select count(distinct [Machine Name]) from _SMDBA_.[Inventory Items] left outer join _SMDBA_.Srv_Cat_Link on (SEQ_ITEM_SERVER = _SMDBA_.[Inventory Items].Sequence) left outer join _SMDBA_._SUBJECTS_ on (_SMDBA_.Srv_Cat_Link.SEQ_CATEGORY = _SMDBA_._SUBJECTS_.SEQUENCE) left outer join _SMDBA_._PERSONNEL_ on (_SMDBA_.Srv_Cat_Link.SEQ_STAFF = _SMDBA_._PERSONNEL_.SEQUENCE) left outer join _SMDBA_.CATCOMPLEX on (_SMDBA_._SUBJECTS_.SEQ_CATCOMPLEX = _SMDBA_.CATCOMPLEX.SEQUENCE) left outer join _SMDBA_._SUBJTYPES_ on (_SMDBA_._SUBJECTS_.TYPE = _SMDBA_._SUBJTYPES_.SEQUENCE) where ([CI_Assembly Desc] like 'Windows%' or [CI_Assembly Desc] like 'Unix%' or [CI_Assembly Desc] like 'Linux%' or [CI_Assembly Desc] like 'Exadata%' or [CI_Assembly Desc] like 'AS400%' or [CI_Assembly Desc] like 'Cluster%' or [CI_Assembly Desc] like 'Blade%' or [CI_Assembly Desc] like 'To Be Changed%') and ([Machine Name] like '%XLR%' or [Machine Name] like '%XBR%' or [Machine Name] like '%XMS%') and {0} and ([status:] not in ('Off-line', 'To be Disposed', 'Spare', 'N/A') or [status:] is NULL) and [Inactive:]=0"   
    
   
    alist = [("No Location","[Server_Location] is Null"),
    ("No CI Zone","[CI_Assembly_Zone] is Null"),
    ("wrong Status","[status:] in ('Disposed','In stock','N/A')"),
    ("No CI Environement","[CI_Assembly_Env] is Null"),
    ("No Configuration Desc","([CI_Assembly Desc] = 'To be changed' or [CI_Assembly Desc] is NULL)"),
    ("No Criticality - No Application Linked","[max_cat_order] is Null"),
    ("No Configuration ID","[Configuration ID] is Null "),
    ("Wrong Configuration ID","([Configuration ID] <> 'WINDOWS SERVERS' and  [Configuration ID] is not Null) "),
    ("No Host Function","[Server_Host Function] is Null "),
    ("Wrong CI Environment","[CI_Assembly_Env] not like 'PRD%' "),
    ("No Technical Leader","[Server_TL_Login ID] is Null "),
    ("No Reboot Plan","[Concatenation] is Null "),
    ("No Backup","[CI_Backup_ID] is NULL "),
    ("Wrong Backup ID","([CI_Backup_ID] != 'Nor Required - No Infra' and [CI_Backup_ID] is not null) "),
    ("No Monitoring","[CI_Monitor_ID] IS NULL "),
    ("Wrong Monitoring ","([CI_Monitor_ID] <> 'Internal - Patrol' and [CI_Monitor_ID] is not null)")]
    
    condition = alist[n]
    
    full_query = conn_str.format(condition[1]+" "+c)
    tot = cursor.execute(full_query)
    result = cursor.fetchone()
    logger.info("update_data:%s"%update_data)
    _close_connection(db_conn)
    tab = [n,result[0],full_query]
    return tab

def _get_oneretail_device_name(n):
    db_conn = _get_connection()
    cursor = db_conn.cursor()
    logger.info('get_oneretail_device_name') 
    logger.info('n:%s'%n) 
    
    conn_str = "select Distinct([Machine Name]) from _SMDBA_.[Inventory Items] left outer join _SMDBA_.Srv_Cat_Link on (SEQ_ITEM_SERVER = _SMDBA_.[Inventory Items].Sequence) left outer join _SMDBA_._SUBJECTS_ on (_SMDBA_.Srv_Cat_Link.SEQ_CATEGORY = _SMDBA_._SUBJECTS_.SEQUENCE) left outer join _SMDBA_._PERSONNEL_ on (_SMDBA_.Srv_Cat_Link.SEQ_STAFF = _SMDBA_._PERSONNEL_.SEQUENCE) left outer join _SMDBA_.CATCOMPLEX on (_SMDBA_._SUBJECTS_.SEQ_CATCOMPLEX = _SMDBA_.CATCOMPLEX.SEQUENCE) left outer join _SMDBA_._SUBJTYPES_ on (_SMDBA_._SUBJECTS_.TYPE = _SMDBA_._SUBJTYPES_.SEQUENCE) where ([CI_Assembly Desc] like 'Windows%' or [CI_Assembly Desc] like 'Unix%' or [CI_Assembly Desc] like 'Linux%' or [CI_Assembly Desc] like 'Exadata%' or [CI_Assembly Desc] like 'AS400%' or [CI_Assembly Desc] like 'Cluster%' or [CI_Assembly Desc] like 'Blade%' or [CI_Assembly Desc] like 'To Be Changed%' ) and ([Machine Name] like '%XLR%' or [Machine Name] like '%XBR%' or [Machine Name] like '%XMS%') and {0} and ([status:] not in ('Off-line', 'To be Disposed', 'Spare', 'N/A') or [status:] is NULL) and [Inactive:]=0"
    
    c = get_exclude_device_oneretail(n)
     
    p_req = [("No Location","[Server_Location] is Null"),
    ("No CI Zone","[CI_Assembly_Zone] is Null"),
    ("wrong Status","[status:] not like 'In Production%' "),
    ("No CI Environement","[CI_Assembly_Env] is Null"),
    ("No Configuration Desc","([CI_Assembly Desc] = 'To be changed' or [CI_Assembly Desc] is NULL)"),
    ("No Criticality - No Application Linked","[max_cat_order] is Null"),
    ("No Configuration ID","[Configuration ID] is Null "),
    ("Wrong Configuration ID","([Configuration ID] <> 'WINDOWS SERVERS' and  [Configuration ID] is not Null) "),
    ("No Host Function","[Server_Host Function] is Null "),
    ("Wrong CI Environment","[CI_Assembly_Env] not like 'PRD%' "),
    ("No Technical Leader","[Server_TL_Login ID] is Null "),
    ("No Reboot Plan","[Concatenation] is Null "),
    ("No Backup","[CI_Backup_ID] is NULL "),
    ("Wrong Backup ID","([CI_Backup_ID] != 'Nor Required - No Infra' and [CI_Backup_ID] is not null) "),
    ("No Monitoring","[CI_Monitor_ID] IS NULL "),
    ("Wrong Monitoring ","([CI_Monitor_ID] <> 'Internal - Patrol' and [CI_Monitor_ID] is not null)")]
    
    req = p_req[n][1]
    full_query = conn_str.format(req+" "+c)
    cursor.execute(full_query)
    logger.info("oneretail_devicename:%s"%full_query)
    result = cursor.fetchall()
    device_list_oneretail= []
    for row in result:
        device_list_oneretail.append(row[0])
    _close_connection(db_conn)
    
    return device_list_oneretail