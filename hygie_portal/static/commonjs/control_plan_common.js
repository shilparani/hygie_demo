/**
 *  Common functions for Control Plan
 */
 
// global variable to be used
var dataList = {}; 

// set time on page last updated
$('#current-time').html(moment().format('YYYY-MM-DD HH:mm:ss'));

var gaugeOptions = {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: false,
        height: 250,
    },
    title: {
        text: '',
        align: 'center',
        y: 50,
        style: {
            fontWeight: 'bold',
            color: '#5c5c5c',
            textTransform: 'uppercase',
        }
    },
    subtitle: {
        text: '',
        align: 'center',
        verticalAlign: 'middle',
        y: 50,
        style: {
            fontSize: '15px',
            color: '#5c5c5c',
        }
    },
    tooltip: {
        pointFormat: '<b>{point.percentage:.1f}%</b>'
    },
    credits: {
        enabled: false
    },
    plotOptions: {
        pie: {
            dataLabels: {
                enabled: true,
                distance: -20,
                style: {
                    fontWeight: 'bold',
                    color: 'black',
                    textOutline: '0',
                    fontSize: '15px'
                },
                format: '{point.y}'
            },
            startAngle: -90,
            endAngle: 90,
            center: ['50%', '75%'],
            size: '110%',
            enableMouseTracking: false
        }
    },
    series: []
};

// init event handlers
function initJSHandlers(){
    $(document).on('click', '.breadcrumb-item a', function(e){
        e.stopPropagation();
        
        $('ol.breadcrumb li.breadcrumb-item').removeClass('active')
        $(this).parent().nextAll().remove();
        $(this).parent().addClass('active');
        // populate validated account list
        var type = $(this).attr('title-type');
        
        $(this).addClass("deactivate-breadcrumb")
        
        $(".control-plan-block").hide();
        $('#control-plan-' + type).show();
    });
    
    $(document).on('click', '.refresh-page', function(){
        location.reload();
    });
    
    $(document).on('click', '.control-status-label-btn', function(e){
		e.stopPropagation();
		
		$('#control-plan-modal').modal();
		
	});
}

 
Array.prototype.diff = function(a) {
    return this.filter(function(i) {return a.indexOf(i) < 0;});
};

// for global view
function categoryClick(selectedCategory){
	var categoryName = $(selectedCategory).attr('category-name');
	populateDomainBreakdown(categoryList[categoryName]['domainList']);
	// set category name
	$("#control-plan-breakdown #breakdown-category-name").html(categoryName);
    
    // create breadcrumb
    addBreadcrumb('domain', categoryName);
    
	$('#control-plan-category').hide();
}

// for global view
function domainClick(selectedDomain){
	//var categoryId = $(selectedCategory).attr('category-id');
	var categoryName = $(selectedDomain).attr('category-name');
	var domainName = $(selectedDomain).attr('domain-name');
    
    addBreadcrumb("", domainName);
    
    var controlListPath = "['" + categoryName + "']['domainList']['" + domainName + "']['controlList']";
    
	populateBreakdown(categoryList[categoryName]['domainList'][domainName]['controlList'], controlListPath);
	// set category name
	$("#control-plan-breakdown #breakdown-category-name").html(categoryName + " - " + domainName);
	$('#control-plan-category').hide();
    $('#control-plan-domain').hide();
}

// for generic view with one level of hierarchy 
function blockClick(selectedElement){
    var mode = $(selectedElement).attr('mode');
    var modeValue = $(selectedElement).attr('mode-value');
    
    var controlListPath = "['" + modeValue + "']['controlList']";
    
    populateBreakdown(dataList[modeValue]['controlList'], controlListPath);
    
    addBreadcrumb(mode, modeValue);
}

function populateCategoryBreakdown(categoryList){
    var category_block_template = Handlebars.compile(document.getElementById("control-plan-category-block-template").innerHTML); 
	
	$.each(categoryList, function(id, categorySettings){
	
		// populate category blocks
		categorySettings['id'] = 'category-' + id.split(' ').join('-');
        categorySettings['statusColor'] = (categorySettings['ok_count'] != categorySettings['total_count']) ? 'red' : 'green';
        
		$("#control-plan-category").append($(category_block_template(categorySettings)));
        /*
        // activity gauge
        Highcharts.chart('control-plan-gauge-' + categorySettings['id'], {

            chart: {
                type: 'solidgauge',
                height: 300
            },
            credits: {
                enabled: false
            },
            title: {
                text: categorySettings['name'],
            },

            tooltip: {
                enabled: false
            },

            pane: {
                startAngle: 0,
                endAngle: 360,
                background: []
            },

            yAxis: {
                min: 0,
                max: 100,
                lineWidth: 0,
                tickPositions: []
            },

            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        enabled: true,
                        useHTML: true,
                        formatter: function () { 
                            return ('<span style="font-size:2em; font-weight: bold;">' + this.key + '</span>'); 
                        },
                        borderWidth: 0,
                        padding: -15,
                    },
                    linecap: 'round',
                    stickyTracking: false,
                    rounded: true
                }
            },

            series: [{
                name: categorySettings['name'],
                data: [{
                    color: categorySettings['statusColor'],
                    radius: '100%',
                    innerRadius: '88%',
                    y: Math.floor(categorySettings['ok_count'] / categorySettings['total_count'] * 100),
                    name: categorySettings['ok_count'] + "/" + categorySettings['total_count']
                }]
            }]
        });
        */
        
        // The gauge
        // The gauge
        gaugeOptions['title']['text'] = categorySettings['name'];
        gaugeOptions['subtitle']['text'] = categorySettings['ok_count'] + "/" + categorySettings['total_count'];
        var seriesData = [];
        if(parseInt(categorySettings['total_count'] - categorySettings['ok_count']) !== 0){
            seriesData.push({
                name: 'KO',
                color: '#DF5353',
                y: categorySettings['total_count'] - categorySettings['ok_count'],
                dataLabels: {
                    enabled: true
                }
            });
        }
        
        if(parseInt(categorySettings['ok_count']) !== 0){
            seriesData.push({
                name: 'OK',
                color: '#55BF3B',
                y: categorySettings['ok_count'],
                dataLabels: {
                    enabled: true
                }
            });
        }
        
        gaugeOptions['series'] = [{
            type: 'pie',
            name: categorySettings['name'],
            innerSize: '50%',
            data: seriesData
        }];
        var chart = Highcharts.chart('control-plan-gauge-' + categorySettings['id'], gaugeOptions);
		
		//$('#control-plan-category #' + categorySettings['id']).css('border-left-color', categorySettings['statusColor']);
		
	});
}

function populateDomainBreakdown(domainList){
    console.log(domainList);
    $("#control-plan-domain").empty();
    // populate category controls
	var domain_block_template = Handlebars.compile(document.getElementById("control-plan-domain-block-template").innerHTML); 
    
    $.each(domainList, function(id, domainSettings){
	
		// populate category blocks
		domainSettings['id'] = 'domain-' + id.split(' ').join('-');
        domainSettings['statusColor'] = (domainSettings['ok_count'] != domainSettings['total_count']) ? 'red' : 'green';
        
		$("#control-plan-domain").append($(domain_block_template(domainSettings)));
        
        /*
        // Activity gauge
        Highcharts.chart('control-plan-gauge-' + domainSettings['id'], {

            chart: {
                type: 'solidgauge',
                height: 300
            },
            credits: {
                enabled: false
            },
            title: {
                text: domainSettings['name'],
            },

            tooltip: {
                enabled: false
            },

            pane: {
                startAngle: 0,
                endAngle: 360,
                background: []
            },

            yAxis: {
                min: 0,
                max: 100,
                lineWidth: 0,
                tickPositions: []
            },

            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        enabled: true,
                        useHTML: true,
                        formatter: function () { 
                            return ('<span style="font-size:2em; font-weight: bold;">' + this.key + '</span>'); 
                        },
                        borderWidth: 0,
                        padding: -15,
                    },
                    linecap: 'round',
                    stickyTracking: false,
                    rounded: true
                }
            },

            series: [{
                name: domainSettings['name'],
                data: [{
                    color: domainSettings['statusColor'],
                    radius: '100%',
                    innerRadius: '88%',
                    y: Math.floor(domainSettings['ok_count'] / domainSettings['total_count'] * 100),
                    name: domainSettings['ok_count'] + "/" + domainSettings['total_count']
                }]
            }]
        });
		*/
        
        

        // The gauge
        gaugeOptions['title']['text'] = domainSettings['name'];
        gaugeOptions['subtitle']['text'] = parseInt(domainSettings['total_count'] - domainSettings['ok_count']) + "/" + domainSettings['total_count'];
        
        var seriesData = [];
        if(parseInt(domainSettings['total_count'] - domainSettings['ok_count']) !== 0){
            seriesData.push({
                name: 'KO',
                color: '#DF5353',
                y: domainSettings['total_count'] - domainSettings['ok_count'],
                dataLabels: {
                    enabled: true
                }
            });
        }
        
        if(parseInt(domainSettings['ok_count']) !== 0){
            seriesData.push({
                name: 'OK',
                color: '#55BF3B',
                y: domainSettings['ok_count'],
                dataLabels: {
                    enabled: true
                }
            });
        }
        
        gaugeOptions['series'] = [{
            type: 'pie',
            name: domainSettings['name'],
            innerSize: '50%',
            data: seriesData
        }];
        var chart = Highcharts.chart('control-plan-gauge-' + domainSettings['id'], gaugeOptions);
		//$('#control-plan-domain #' + domainSettings['id']).css('border-left-color', domainSettings['statusColor']);
		
	});
    
    $("#control-plan-domain").show();
}

function populateBusinessFlagBreakdown(businessflagList){
    console.log(businessflagList);
    $("#control-plan-businessflag").empty();
    // populate category controls
	var block_template = Handlebars.compile(document.getElementById("control-plan-businessflag-block-template").innerHTML); 
    
    $.each(businessflagList, function(id, businessflagSettings){
	
		// populate category blocks
		businessflagSettings['id'] = 'businessflag-' + id.split(' ').join('-');
        businessflagSettings['statusColor'] = (businessflagSettings['ok_count'] != businessflagSettings['total_count']) ? 'red' : 'green';
        
		$("#control-plan-businessflag").append($(block_template(businessflagSettings)));
        
        // The gauge
        gaugeOptions['title']['text'] = businessflagSettings['name'];
        gaugeOptions['subtitle']['text'] = parseInt(businessflagSettings['total_count'] - businessflagSettings['ok_count']) + "/" + businessflagSettings['total_count'];
        
        var seriesData = [];
        if(parseInt(businessflagSettings['total_count'] - businessflagSettings['ok_count']) !== 0){
            seriesData.push({
                name: 'KO',
                color: '#DF5353',
                y: businessflagSettings['total_count'] - businessflagSettings['ok_count'],
                dataLabels: {
                    enabled: true
                }
            });
        }
        
        if(parseInt(businessflagSettings['ok_count']) !== 0){
            seriesData.push({
                name: 'OK',
                color: '#55BF3B',
                y: businessflagSettings['ok_count'],
                dataLabels: {
                    enabled: true
                }
            });
        }
        
        gaugeOptions['series'] = [{
            type: 'pie',
            name: businessflagSettings['name'],
            innerSize: '50%',
            data: seriesData
        }];
        var chart = Highcharts.chart('control-plan-gauge-' + businessflagSettings['id'], gaugeOptions);
		
		$('#control-plan-businessflag #' + businessflagSettings['id']).css('border-left-color', businessflagSettings['statusColor']);
		
	});
    
    $("#control-plan-businessflag").show();
}

function populateBreakdown(controlList, controlListPath){
	
    $("#control-plan-breakdown #accordion").empty();
    
	// populate category controls
	var breakdown_card_template = Handlebars.compile(document.getElementById("control-plan-breakdown-card-template").innerHTML); 
	
	$.each(controlList, function(i, controlSettings){
		controlSettings['control-index'] = i;
		controlSettings['category-index'] = controlSettings['categoryId'];
		controlSettings['id'] = 'control-' + i;
		controlSettings['button-color-class'] = (controlSettings['status'].toLowerCase() == 'ko') ? 'danger' : 'success';
        controlSettings['onclickFunction'] = controlSettings['template_function'] + "('" + i + "', dataList" + controlListPath + "['" + i + "'])";
		
		$("#control-plan-breakdown #accordion").append($(breakdown_card_template(controlSettings)));
		$("#control-plan-breakdown #" + controlSettings['id'] + '-card').css('border-left-color', controlSettings['statusColor']);
		
		//init arrow change for collapse
		$("#" + controlSettings['id'] + "-card-content").on('show.bs.collapse', function(e){
			e.stopPropagation();
			$(this).parent().find('#' + controlSettings['id'] + '-card-header span.glyphicon:first').removeClass('glyphicon-triangle-bottom');
			$(this).parent().find('#' + controlSettings['id'] + '-card-header span.glyphicon:first').addClass('glyphicon-triangle-top');
		});
		
		$("#" + controlSettings['id'] + "-card-content").on('hide.bs.collapse', function(e){
			e.stopPropagation();
			$(this).parent().find('#' + controlSettings['id'] + '-card-header span.glyphicon:first').removeClass('glyphicon-triangle-top');
			$(this).parent().find('#' + controlSettings['id'] + '-card-header span.glyphicon:first').addClass('glyphicon-triangle-bottom');
		});
		
	});
	
	$('#control-plan-breakdown').show();
}

function addBreadcrumb(titleType, label){
    
    // remove all active class
    $('ol.breadcrumb .breadcrumb-item').removeClass('active');
    $('ol.breadcrumb .breadcrumb-item a').removeClass('deactivate-breadcrumb');
    
    // add new breadcrumb
    $('ol.breadcrumb').append($('<li></li>').addClass('breadcrumb-item active').append($('<a></a>').attr({"title-type":titleType,"href":"#"}).html(label)));
    
    // deactivate last breadcrumb
    $('ol.breadcrumb .breadcrumb-item:last a').addClass('deactivate-breadcrumb');
}

function populateAdminValidationTemplate(controlIndex, controlData){

    var validatedAccountList = controlData['validatedList'].sort();
    var currentAccountList = controlData['currentList'].sort();
    var missingAccountList = validatedAccountList.diff(currentAccountList).sort();
	var extraAccountList = currentAccountList.diff(validatedAccountList).sort();
	var last5LogList = controlData['logList'];
	
	var control_template = Handlebars.compile(document.getElementById("control-plan-admin-validation-template").innerHTML); 
	
	$('#control-plan-modal #modal-control-name').html(controlData['name']);
	
	$('#control-plan-modal .modal-body').empty();
	
    var settings = {
        "offset_class" : ((missingAccountList.length == 0 && extraAccountList.length == 0 && extraAccountList.length == 0) ? "col-lg-offset-2" : ""),
        "extra_class" : ((missingAccountList.length == 0 && extraAccountList.length == 0 && extraAccountList.length == 0) ? "hidden" : "col-lg-4")
    }
    
	$('#control-plan-modal .modal-body').append($(control_template(settings)));
	
	// populate validated accounts
	$.each(validatedAccountList, function(i, name){
		$('#control-plan-modal #validated-accounts ul').append('<li>' + name + '</li>');
	});
	
	// populate current active accounts
	$.each(currentAccountList, function(i, name){
		$('#control-plan-modal #current-active-accounts ul').append('<li>' + name + '</li>');
	});
	
	// populate missing accounts
	$.each(missingAccountList, function(i, name){
		$('#control-plan-modal #missing-accounts ul').append('<li>' + name + '</li>');
	});
	
	// populate extra accounts
	$.each(extraAccountList, function(i, name){
		$('#control-plan-modal #extra-accounts ul').append('<li>' + name + '</li>');
	});
	
    // populate logs list
	$.each(last5LogList, function(i, log){
		$('<tr></tr>')
		.append($('<td></td>').html(log['creation_date']))
		.append($('<td></td>').html(log['cn']))
		.append($('<td></td>').html(log['action']))
		.appendTo($('#control-plan-modal #last-5-logs-list table tbody'))
	});
	
}

function populateCertValidationTemplate(controlIndex, controlData){

    var unknownList = controlData['unknownList'].sort();
    var lt100List = controlData['lt100List'].sort();
    var lt250List = controlData['lt250List'].sort();
    var gt250List = controlData['gt250List'].sort();
    
	var control_template = Handlebars.compile(document.getElementById("control-plan-cert-validation-template").innerHTML); 
	
	$('#control-plan-modal #modal-control-name').html(controlData['name']);
	
	$('#control-plan-modal .modal-body').empty();
	
    var settings = {
        "unknownList" : unknownList,
        "lt100List" : lt100List,
        "lt250List" : lt250List,
        "gt250List" : gt250List,
    }
    
	$('#control-plan-modal .modal-body').append($(control_template(settings)));
	
    $.each(settings, function(id, val){
        $('#certs-' + id).dataTable().fnDestroy();
        $('#certs-' + id).DataTable({
            //"order": [[ 4, "asc" ]],
            dom: 'r<"col-sm-6"l><"col-xs-6 col-sm-3"f><"col-sm-6 col-sm-3 text-right"B><"clearfix">tip',
            pageLength: 5,
            buttons: [ {
                extend: 'excel', 
                title:  id, 
                text: 'Excel',
                titleAttr: 'Excel', 
                exportOptions : {
                    format : {
                        body: function ( data, row, column, node ) {
                            return data;
                        }
                    }
                }
            }]
        });
        
    });
    
}