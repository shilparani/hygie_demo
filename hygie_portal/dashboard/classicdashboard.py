# -*- coding: utf-8 -*-
"""Helper utilities and decorators."""

from hygie.settings import logger
from datetime import timedelta
from dateutil import tz
from math import floor
from collections import defaultdict,OrderedDict
from hygie_portal.models import classicDashboardState
import datetime
import pytz
import hygie_portal.tickets.common as f
import hygie_portal.querymanager.querymanager as qm
import operator
import json
import ast



def save_dashboard_state(request, new_dashboard_state):
    logger.info("save_dashboard_state :: "%new_dashboard_state)
    
    current_dashboard_state = classicDashboardState.objects.filter(username = request.user.username).first()
    logger.info(current_dashboard_state)
    if current_dashboard_state:
        #update existing settings
        classicDashboardState.objects.filter(username = request.user.username).update(dashboard_state = json.dumps(new_dashboard_state))
        
    else:
        #create new settings
        logger.info("new dashboard state")
        new_dashboard_state = classicDashboardState(username = request.user.username, dashboard_state = json.dumps(new_dashboard_state))
        new_dashboard_state.save()
    
    return True
    
def get_user_dashboard_filters(request, current_user):

    current_dashboard_state = classicDashboardState.objects.filter(username = current_user).first()
    
    default_all_dashboard_state = OrderedDict({
        'ams' : {
            'name' : 'ams',
            'state' : 0,
            'permission_group' : ['ams_user']
        },
        'itl4' : {
            'name' : 'itl4',
            'state' : 0,
            'permission_group' : ['itl4_user']
        },
        'network' : {
            'name' : 'network',
            'state' : 0,
            'permission_group' : ['network_user']
        },
        'proximity' : {
            'name' : 'proximity',
            'state' : 0,
            'permission_group' : ['proxi_']
        },
        'proxi_america' : {
            'name' : 'proxi_america',
            'state' : 0,
            'permission_group' : ['proxi_us','proxi_mng']
        },
        'proxi_berluti' : {
            'name' : 'proxi_berluti',
            'state' : 0,
            'permission_group' : ['proxi_blt','proxi_mng']
        },
        'proxi_emea' : {
            'name' : 'proxi_emea',
            'state' : 0,
            'permission_group' : ['proxi_emea','proxi_mng']
        },
        'proxi_fondation' : {
            'name' : 'proxi_fondation',
            'state' : 0,
            'permission_group' : ['proxi_fdt','proxi_mng']
        },
        'proxi_global_asie' : {
            'name' : 'proxi_global_asie',
            'state' : 0,
            'permission_group' : ['proxi_gba','proxi_mng']
        },
        'service_desk' : {
            'name' : 'service_desk',
            'state' : 0,
            'permission_group' : ['fo_user']
        },
        'tsc' : {
            'name' : 'tsc',
            'state' : 0,
            'permission_group' : ['tsc_user']
        }
    })
    
    curr_dashboard_state_list = []
    curr_dashboard_state = OrderedDict()
    default_user_dashboard_state = OrderedDict()
    indexCounter = 0
    
    if current_dashboard_state:
        d = current_dashboard_state.__dict__
        del d['_state']
        curr_dashboard_state_list = json.loads(d['dashboard_state'])
    
    
    for group, state in default_all_dashboard_state.items():
        logger.info("========== USER DASHBOARD STATE :: state :: ",state)
        for perm in state['permission_group']:
            if perm in request.user.role:
                default_user_dashboard_state[group] = state
                break
    
    for i in range(0, len(curr_dashboard_state_list) ):
        curr_dashboard_state[i] = curr_dashboard_state_list[i]
            
    for index, curr_state in curr_dashboard_state.items():
        if curr_state['name'] not in default_user_dashboard_state.keys():
            del curr_dashboard_state[index]
        else:
            del default_user_dashboard_state[curr_state['name']]
        
    curr_dashboard_state.update(default_user_dashboard_state)
    
    curr_open_states = [data['name'] for i, data in curr_dashboard_state.items() if data['state']]
                        
              
    logger.info("========== USER DASHBOARD STATE :: curr_dashboard_state :: ",curr_dashboard_state)
    #import pdb; pdb.set_trace()  
    return {'curr_dashboard_state' : curr_dashboard_state, 'curr_open_states' : curr_open_states}
