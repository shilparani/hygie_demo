# -*- coding: utf-8 -*-
"""Helper utilities and decorators."""

from datetime import timedelta
from dateutil import tz
from math import floor
from collections import defaultdict,OrderedDict
from hygie_portal.models import customDashboard
import datetime
import pytz
import hygie_portal.tickets.common as f
import hygie_portal.querymanager.querymanager as qm
from hygie.settings import logger
from django.conf import settings
import operator
import json
import ast

# Constants
WHITE = "#ffffff"
BLACK = "#000000"

def get_sla_summary_by_team(request, parent_team=None, team_name=None, local_tz=None, ticket_type=None, source='deis'):
    
    logger.info('get_sla_summary_by_team')
    db_conn = f._get_connection(request) if source=='deis' else f._get_connection_itsm(request)
    cursor = db_conn.cursor()
    
    teams_dict = settings.ALL_TEAMS_MAPPING
    TEAMS = settings.TEAMS
    GRP_TEAMS =[]
   
    sla_grp="ITL4"
    if "[" in team_name:
        team_name = eval(team_name)
    
    #individual team list calls like [WW_TM_L3_TSC_AS400], [WW_TM_L3_TSC_BI_DATABASE] etc..
    if isinstance(team_name, list):
        GRP_TEAMS = team_name
    #all_proximity_ds teams                        
    elif team_name.lower() == 'all_proximity_ds':
        keys = ['PROXI_EMEA', 'PROXI_AMERICA', 'PROXI_GLOBAL_ASIE', 'PROXI_BERLUTI', 'PROXI_FONDATION']
        for k in keys:
            for data in TEAMS[k]:
                if len(data['team']) > 1:
                    for t1 in data['team']:
                        GRP_TEAMS.extend(t1)
                else:
                    GRP_TEAMS.extend(data['team'])
    #all_teams like all_tsc, all_itl4, all_service_desk, all_ams etc
    elif team_name.lower() in teams_dict.keys():    
        team_data =settings.teams_dict[team_name.lower()] if (team_name.lower()== 'all_ams_cgi' or team_name.lower()=='all_ams_valtech') else TEAMS[teams_dict[team_name.lower()]]
        for data in team_data:
            for team in data['team']:
                GRP_TEAMS.append(team)
            
    ticket_type = ticket_type if ticket_type else 'ALL'
    TICKET_STATUS = ",".join("'{0}'".format(w) for w in settings.TICKET_STATUS)
    TICKET_TYPES = ",".join("'{0}'".format(w) for w in settings.TICKET_TYPES[ticket_type])
    
    if source == 'itsm':
        #view = '_TELDETAI_HYGIE_4MONTH'
        db_view_query = qm.get_query_by_title('_TELMASTE_HYGIE_Open')
        view = '({0})'.format(db_view_query)

    conn_str = "select [Open Date & Time], [Urgency ID:], [Due Date & Time:], [Ticket Type], [Group Name], NULL as third_party from _SMDBA_.Incident where [Close Date & Time] is NULL and upper([Status Description:]) in ({0}) and upper([Ticket Type]) in ({1}) and [InActive:] =0 and [Group Name] in ({2})" if source=='deis' else "select [DATE OPEN], urgency_desc as Urgency, [DUE_DATE:] , TTYPE, group_desc  as GroupName, status_desc as third_party from {3} as details where [CLOSED ON] is null and ('OPEN') in ({0}) and group_desc in ({2}) and upper(ttype) in ({1})"
 
    GRP = ",".join("'{0}'".format(w) for w in GRP_TEAMS)
    conn_str = conn_str.format(TICKET_STATUS, TICKET_TYPES, GRP) if source == 'deis' else conn_str.format(TICKET_STATUS, TICKET_TYPES, GRP, view)

    logger.info('conn_str:%s'%conn_str)
    cursor.execute(conn_str)
    data=cursor.fetchall()
    f._close_connection(request, db_conn)
    
    total_tickets, oosla_count, soosla_count, third_party_count = 0, 0, 0, 0
    now_date = datetime.datetime.now(pytz.timezone(settings.TIMEZONE))
    TZ = pytz.timezone(settings.TIMEZONE)

    for t in data:
        open_date, criticality, due_date, comp_due_date, is_oosla, is_planned, is_soosla, is_third_party, ttype, grp_name, third_party = t[0], t[1], t[2], None, 0, 0, 0, 0, t[3], t[4], t[5]

        #Make PROXIMITY teams to 'EMEA' to select SLA_HOURS
        if grp_name in settings.SLA_GRP_AMS_CGI:
            sla_grp = 'AMS_CGI'
        elif grp_name in settings.SLA_GRP_AMS_VALTECH:
            sla_grp = 'AMS_VALTECH'
        else:
            for key, value in TEAMS.items():
                for v in value:
                    if grp_name in v['team']:
                        if key in settings.SLA_GRP_PROXIMITY:
                            sla_grp = 'EMEA'
                        else:
                            sla_grp = key

        if criticality in ['PLANNED','P_PLANNED']:
            is_planned = 1
            if now_date > TZ.localize(due_date):
                is_oosla = 1
                oosla_count += 1
            if not is_oosla and due_date and (now_date > TZ.localize(due_date - timedelta(hours=4))):
                is_soosla = 1
                soosla_count += 1
        else:
            comp_due_date = TZ.localize(f._comp_duedate_95(criticality, open_date, sla_grp, ttype))

        # check if Out of SLA
        if not is_planned and comp_due_date and now_date > comp_due_date and third_party!='With 3rd Party':
            is_oosla = 1
            oosla_count += 1
        
        # check if Soon OoSLA
        if not is_planned and not is_oosla and comp_due_date and (now_date > f._comp_soosla_date(criticality, comp_due_date, sla_grp, ttype)) and third_party!='With 3rd Party':
            is_soosla = 1
            soosla_count += 1
            
        # check if with 3party
        if third_party == 'With 3rd Party':
            is_third_party = 1
            third_party_count += 1

        # check if ordirary ticket
        if not is_oosla and not is_soosla and not is_third_party:
            total_tickets += 1

    # build the dictionary
    teams_data = []
    if total_tickets:
        teams_data.append({'name':'In SLA', 'y':total_tickets, 'color': settings.COLOR_SCHEME['SLA'], 'indexLabelFontColor': BLACK})
    if oosla_count:
        teams_data.append({'name':'Out of SLA', 'y':oosla_count, 'color': settings.COLOR_SCHEME['OSLA'], 'indexLabelFontColor': BLACK})
    if soosla_count:
        teams_data.append({'name':'Soon out of SLA', 'y':soosla_count, 'color': settings.COLOR_SCHEME['SOOSLA'], 'indexLabelFontColor': BLACK})
    if third_party_count:
        teams_data.append({'name':'3rd Party', 'y':third_party_count, 'color': settings.COLOR_SCHEME['THIRDPARTY'], 'indexLabelFontColor': BLACK})

    return teams_data
    
def save_user_settings(request, user_settings_list):
    print("save_user_settings :: %s"%user_settings_list)
    for setting in user_settings_list.values():
        # check if widget id exists from widgetDetails
        widget_details_data = customDashboard.objects.filter(widget=setting['widget']).first()
        print(widget_details_data)
        if widget_details_data:
            if 'delete' in setting:
                #delete widget
                widget_details = customDashboard.objects.filter(widget = setting['widget']).first()
                if widget_details:
                    customDashboard.delete(widget_details)
            else:
                #update existing settings
                customDashboard.objects.filter(widget = setting['widget']).update(dashboard = setting['dashboard'], widget = setting['widget'], title = setting['title'], selected_team = setting['selected_team'], chart_size = setting['chart_size'], widget_pos = setting['widget_pos'])
            
        else:
            #create new settings
            print("new widget details")
            new_widget = customDashboard(username = request.user.username, dashboard = setting['dashboard'], widget = setting['widget'], title = setting['title'], selected_team = setting['selected_team'], chart_size = setting['chart_size'], widget_pos = setting['widget_pos'])
            new_widget.save()        
    
    return True
    
def get_user_dashboard_filters(username):

    dashboard_details = customDashboard.objects.filter(username=username).all()
    filters_list = {}
    
    if dashboard_details:
        for widget in dashboard_details:
            w = widget.__dict__
            del w['_state']
            w['selected_team'] = ast.literal_eval(widget.__dict__['selected_team'])
            w['chart_size'] = ast.literal_eval(widget.__dict__['chart_size'])
            w['widget_pos'] = ast.literal_eval(widget.__dict__['widget_pos'])
            filters_list[w['selected_team']['groupname']] = w
    
    return filters_list
