# -*- coding: utf-8 -*-
#from flask import current_app
from hygie.settings import logger
from datetime import timedelta
from math import floor
from dateutil import tz
import datetime, calendar
import pyodbc
import pytz
import os
import MySQLdb
import collections
from django.conf import settings

def _get_esurvey_connection():
    try:
        #conn = MySQLdb.connect(user=current_app.config.get('REPORTING_SURVEY_MYSQL_USER'), password=current_app.config.get('REPORTING_SURVEY_MYSQL_PASSWORD'), host=current_app.config.get('REPORTING_SURVEY_MYSQL_SERVER'), database=current_app.config.get('REPORTING_SURVEY_MYSQL_DB'), port=current_app.config.get('REPORTING_SURVEY_MYSQL_PORT'))
        conn = MySQLdb.connect(user='root', passwd='Neurones@2018', host='10.236.0.3', db='demo', charset='utf8')
        return conn
    except MySQLdb.Error as err:
        #conn.close()
        logger.info(err)
        return None

def _get_topten_connection():
    try:
        #conn = MySQLdb.connect("user='{0}', passwd='{1}', host='{2}', db='{3}'".format(current_app.config.get('REPORTING_TOPTEN_MYSQL_USER'), current_app.config.get('REPORTING_TOPTEN_MYSQL_PASSWORD'), current_app.config.get('REPORTING_TOPTEN_MYSQL_SERVER'), current_app.config.get('REPORTING_TOPTEN_MYSQL_DB')))
        #conn = MySQLdb.connect(user='esurvey', passwd='esurvey', host='NLVCP2008P', db='top10')        
        conn = MySQLdb.connect(user='root', passwd='Neurones@2018', host='10.236.0.3', db='demo', charset='utf8')
        return conn
    except MySQLdb.Error as err:
        #conn.close()
        logger.info(err)
        return None

def _get_changecalendar_connection():
    try:
        conn = MySQLdb.connect(user='root', passwd='Neurones@2018', host='10.236.0.3', db='Hygie_django', charset='utf8')
        return conn
    except pyodbc.Error as err:
        #conn.close()
        logger.error(err)
        return None
    
def _get_ticketstats_connection():
    try:
        conn = MySQLdb.connect(user='root', passwd='Neurones@2018', host='10.236.0.3', db='Hygie_django', charset='utf8')
        return conn
    except pyodbc.Error as err:
        logger.error(err)
        return None


def _close_connection(conn):
    conn.close()
    logger.info('End query time: %s' % datetime.datetime.now().strftime('%m/%d/%Y %H:%M:%S'))

def flatten(l):
    for el in l:
        if isinstance(el, collections.Iterable) and not isinstance(el, str):
            for sub in flatten(el):
                yield sub
        else:
            yield el

def _convert_to_local_time(from_zone, to_zone, server_time):
    
    #local_time = to_zone.localize(server_time).astimezone(from_zone)
    
    server_time = server_time.replace(tzinfo=from_zone)
    local_time = server_time.astimezone(to_zone)
    return local_time
          