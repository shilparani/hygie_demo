"""Helper utilities and decorators."""

from datetime import timedelta
from dateutil import tz
from math import floor
from collections import defaultdict,OrderedDict
import datetime
import pytz
from hygie_portal.reporting import common as f
import operator
import os
import json

def get_topten_data(zone, month):
    db_conn = f._get_topten_connection()
    cursor = db_conn.cursor()
       
    #conn_str = "select TOP 10 '{0}' as Zone_SD, count (*) as Number_ticket, [Subject_DESC] as Subject, (select count(*) from [dbo].[_TELMASTE_HYGIE_] where ({2}) and Month([DATE OPEN]) = MONTH (convert(datetime, '{1}', 112))) as Total_ticket_SD, count(*) * 100 / (select count(*) from [dbo].[_TELMASTE_HYGIE_] where ({2}) and Month([DATE OPEN]) = MONTH (convert(datetime, '{1}', 112))) as 'ratio(%)' from [dbo].[_TELMASTE_HYGIE_] where Month([DATE OPEN]) = MONTH (convert(datetime, '{1}', 112)) and ({2}) group by [Subject_DESC] ORDER by Number_ticket desc".format(zone, month, sd_groups)

    conn_str = "select category, currentmonthcount, pastmonthcount, reasons, actions, owner, totalmonthcount, id, totalpastmonthcount from bymonth where zone='{0}' and DATE_FORMAT(`month`, '%Y%m') = DATE_FORMAT('{1}', '%Y%m') order by currentmonthcount desc".format(zone, month)

    topten_data = []
    print(conn_str)
    cursor.execute(conn_str)
    for t in cursor.fetchall():
        # convert dbrows to list
        t = [x if x is not None else '' for x in t]
        t[1] = 0 if t[1] is '' else int(t[1])
        t[2] = 0 if t[2] is '' else int(t[2])
#        t[3] = json.dumps(t[3])
 #       t[4] = json.dumps(t[4])
  #      t[5] = json.dumps(t[5])
        t[6] = 0 if t[6] is '' else int(t[6])
        t[7] = 0 if t[7] is '' else int(t[7])
        t[8] = 0 if t[8] is '' else int(t[8])
        topten_data.append(t)
    
    f._close_connection(db_conn)

    return topten_data

def save_topten_data(row_id, reasons, actions, owners):
    db_conn = f._get_topten_connection()
    cursor = db_conn.cursor()
    #conn_str = 'select * from bymonth where id={0}'.format(row_id)
    conn_str = """UPDATE bymonth SET reasons={0}, actions={1}, owner={2} where id={3}""".format(reasons, actions, owners, row_id)
    print(conn_str)
    cursor.execute(conn_str)
    
    db_conn.commit()
    f._close_connection(db_conn)
