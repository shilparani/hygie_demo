"""Helper utilities and decorators."""

from datetime import timedelta
from dateutil import tz
from math import floor
from collections import defaultdict,OrderedDict
from hygie_portal.querymanager import querymanager as qm
from hygie_portal.tickets import common as tickets_common
from hygie_portal.reporting import common as f
from django.conf import settings
from hygie.settings import logger
import datetime
import pytz
import operator
import os
import json

def get_integration_view_data(request, integration_type, local_tz):
    
    TEAMS = settings.TEAMS
    integration_data = []
    conn_str = "select TOP 5000 [CLOSED ON], [DATE OPEN], urgency_desc as Urgency, [DUE_DATE:], num_ticket, [DESCRIPTION], [RESOLUTION], subject_desc as SubjectID, [CLOSED_GROUP:], TTYPE, [OPENED_GROUP:], close_category, Group_DESC, status, [sequence], problem_id, problem_sequence, problem_status, curr_assigned_group, curr_assigned_analyst from {0} as details where subject_desc like 'flow%' and subject_desc not in ('flow monitoring','flow etl flow monitoring','flow lvflowmonitoring')"
    
    view_title = '_TELMASTE_HYGIE_Open' if integration_type == 'backlog' else '_TELMASTE_HYGIE_'
    
    db_view_query = qm.get_query_by_title(view_title)
    view = '({0})'.format(db_view_query)
    conn_str = conn_str.format(view)
    
    #import pdb; pdb.set_trace()
    
    from_zone = pytz.timezone(local_tz)
    to_zone = pytz.timezone(settings.TIMEZONE)
    
    start_yesterday = from_zone.localize(datetime.datetime.strptime((datetime.datetime.now(pytz.timezone(local_tz)) + datetime.timedelta(days=-1)).strftime("%Y-%m-%d 00:00:00"), "%Y-%m-%d %H:%M:%S")).astimezone(to_zone).strftime("%Y-%m-%d %H:%M:%S")
    end_yesterday = from_zone.localize(datetime.datetime.strptime((datetime.datetime.now(pytz.timezone(local_tz)) + datetime.timedelta(days=-1)).strftime("%Y-%m-%d 23:59:59"), "%Y-%m-%d %H:%M:%S")).astimezone(to_zone).strftime("%Y-%m-%d %H:%M:%S")
    
    if integration_type != 'backlog': 
        date_column = '[DATE OPEN]' if integration_type == 'opened_yesterday' else '[CLOSED ON]'
        conn_str = conn_str + " and {0} between '{1}' and '{2}'".format(date_column, start_yesterday, end_yesterday)
        
    logger.info("conn_str INTEGRATION VIEW : %s" %conn_str)
    
    sla_grp = 'ITL4'
    db_conn = tickets_common._get_connection_itsm(request)
    cursor = db_conn.cursor()
    
    cursor.execute(conn_str)
    data_set = cursor.fetchall()
    f._close_connection(db_conn)
    
    for t in data_set:
        ticket_data = {}
        comp_due_date, is_oosla, is_planned, is_insla = None, False, False, False
        
        # convert dbrows to list and replace None in data to ''
        t = ['' if d is None else d for d in t]
        
        default_keys = default_keys = ["close_date","open_date","criticality","due_date","ticket_number","description","resolution","category","closed_group_raw","ticket_type","opened_group","resolution_category","assigned_to","status","sequence", "problem_id", "problem_sequence", "problem_status", "curr_assigned_group", "curr_assigned_analyst"]
        ticket_data = dict(zip(default_keys, t)) #form dict with default_keys and DB data(t)
        
        close_date = ticket_data["close_date"]
        open_date = ticket_data["open_date"]
        due_date = ticket_data["due_date"]
        criticality = ticket_data["criticality"]
        ttype = ticket_data["ticket_type"]
        
        #group_for_sla_calc = "closed_group_raw" if ticket_data["status"] == 'C' else "assigned_to"
        group_for_sla_calc = "curr_assigned_group"
        
        ticket_data["problem_url"] = "https://louisvuittonwa.ivanticloud.com/LouisVuittonWA.WebAccess/wd/Object/Open.rails?class_name=ProblemManagement.Problem&key=" + ticket_data["problem_sequence"] if ticket_data["problem_sequence"] else ''
        
        #change the sla group
        if ticket_data[group_for_sla_calc] in settings.SLA_GRP_AMS_CGI:
            sla_grp = 'AMS_CGI'
        elif ticket_data[group_for_sla_calc] in settings.SLA_GRP_AMS_VALTECH:
            sla_grp = 'AMS_VALTECH'
        else:
            for key, value in TEAMS.items():
                for v in value:
                    if (ticket_data[group_for_sla_calc] in v['team']):
                        ticket_data["curr_assigned_group_name"] = v['name']
                        if key in settings.SLA_GRP_PROXIMITY:
                            sla_grp = 'EMEA'
                        else:
                            sla_grp = key             

        # get the 'grp_name' of closed group from settings.py
        ticket_data["curr_assigned_group_name"] = ticket_data["curr_assigned_group"]
       
        ticket_data["close_date"] = to_zone.localize(ticket_data['close_date']).astimezone(from_zone).strftime("%Y-%m-%d %H:%M:%S") if ticket_data['close_date'] else ticket_data['close_date']
        ticket_data["open_date"] = to_zone.localize(ticket_data['open_date']).astimezone(from_zone).strftime("%Y-%m-%d %H:%M:%S")       
        #ticket_data["close_date"] = TZ.localize(close_date).strftime("%Y-%m-%d %H:%M:%S") if close_date else None
        
        #ticket_data["open_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(open_date)

        #if(local_tz!=current_app.config.get('TIMEZONE')):
        #    ticket_data["close_date"] = f._convert_to_local_time(from_zone, to_zone, ticket_data["close_date"]) if close_date else None
        #    ticket_data["open_date"] = f._convert_to_local_time(from_zone, to_zone, ticket_data["open_date"])
            
        date_for_sla_calc = close_date if close_date else datetime.datetime.now()
        if criticality in ['PLANNED','P_PLANNED']:
            # if planned, due date is planned date
            ticket_data["due_date"] = to_zone.localize(ticket_data['due_date']).astimezone(from_zone).strftime("%Y-%m-%d %H:%M:%S")
            #ticket_data["due_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(due_date) 
            #if(local_tz!=current_app.config.get('TIMEZONE')):
            #    ticket_data["due_date"] = f._convert_to_local_time(from_zone, to_zone, ticket_data["due_date"]) 
            
        else:
            # overwrite due date
            due_date = tickets_common._comp_duedate_95(criticality, open_date, sla_grp, ttype)
            ticket_data["due_date"] = to_zone.localize(due_date).astimezone(from_zone).strftime("%Y-%m-%d %H:%M:%S") 
            #ticket_data["due_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(due_date)
            #if(local_tz!=current_app.config.get('TIMEZONE')):
            #    ticket_data["due_date"] = f._convert_to_local_time(from_zone, to_zone, ticket_data["due_date"]) 
        
        integration_data.append(ticket_data)    
        #endfor
    
    return integration_data

def get_integration_view_stats(request, local_tz):
    
    integration_stats = {}
    
    backlog_conn_str = "select count(*) from {0} as details where subject_desc like '%flow%' and subject_desc not in ('flow monitoring','flow etl flow monitoring','flow lvflowmonitoring')"
    opened_yesterday_conn_str = "select count(*) from {0} as details where subject_desc like '%flow%' and subject_desc not in ('flow monitoring','flow etl flow monitoring','flow lvflowmonitoring') and [DATE OPEN] between '{1}' and '{2}'"
    closed_yesterday_conn_str = "select count(*) from {0} as details where subject_desc like '%flow%' and subject_desc not in ('flow monitoring','flow etl flow monitoring','flow lvflowmonitoring') and [CLOSED ON] between '{1}' and '{2}'"
    
    from_zone = pytz.timezone(local_tz)
    to_zone = pytz.timezone(settings.TIMEZONE)
    
    start_yesterday = from_zone.localize(datetime.datetime.strptime((datetime.datetime.now(pytz.timezone(local_tz)) + datetime.timedelta(days=-1)).strftime("%Y-%m-%d 00:00:00"), "%Y-%m-%d %H:%M:%S")).astimezone(to_zone).strftime("%Y-%m-%d %H:%M:%S")
    end_yesterday = from_zone.localize(datetime.datetime.strptime((datetime.datetime.now(pytz.timezone(local_tz)) + datetime.timedelta(days=-1)).strftime("%Y-%m-%d 23:59:59"), "%Y-%m-%d %H:%M:%S")).astimezone(to_zone).strftime("%Y-%m-%d %H:%M:%S")
    
    telmaste_hygie_open_query = qm.get_query_by_title('_TELMASTE_HYGIE_Open')
    telmaste_hygie_open_view = '({0})'.format(telmaste_hygie_open_query)
    backlog_conn_str = backlog_conn_str.format(telmaste_hygie_open_view)
    
    telmaste_hygie_query = qm.get_query_by_title('_TELMASTE_HYGIE_')
    telmaste_hygie_view = '({0})'.format(telmaste_hygie_query)
    opened_yesterday_conn_str = opened_yesterday_conn_str.format(telmaste_hygie_view, start_yesterday, end_yesterday)
    closed_yesterday_conn_str = closed_yesterday_conn_str.format(telmaste_hygie_view, start_yesterday, end_yesterday)
    
    db_conn = tickets_common._get_connection_itsm(request)
    cursor = db_conn.cursor()
    
    cursor.execute(backlog_conn_str)
    backlog_stat = cursor.fetchone()
    integration_stats['backlog_count'] = list(backlog_stat)[0]
    
    cursor.execute(opened_yesterday_conn_str)
    opened_yesterday_stat = cursor.fetchone()
    integration_stats['opened_yesterday_count'] = list(opened_yesterday_stat)[0]
    
    cursor.execute(closed_yesterday_conn_str)
    closed_yesterday_stat = cursor.fetchone()
    integration_stats['closed_yesterday_count'] = list(closed_yesterday_stat)[0]
    #simport pdb; pdb.set_trace()
    f._close_connection(db_conn)
    
    
    
    return integration_stats
