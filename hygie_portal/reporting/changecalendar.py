"""Helper utilities and decorators."""

from datetime import timedelta
from datetime import date
from dateutil import tz
from math import floor
from collections import defaultdict,OrderedDict
import datetime
import pytz
from hygie_portal.reporting import common as f
import operator
import os
import json

def get_change_calendar_data(team='TSC'):
    
    current_day = date.today().isoweekday()
    start_day = 1 - current_day
    end_day = 7 - current_day + 28
    conn_str = "Select CHG.sequence as 'Change', GROUPS.CODE as 'Group', WO.STARTDATE as 'Start', WO.[CHANGE_DURATION] as 'Duration', CHG.SEQ_CMSTATUS as 'Status', CHG.TSC_VALIDATION as 'TSC', WO.SEQUENCE as 'Workorder ID', CHG.LV_URGJUSTIFY as 'Urgent Justification' from _SMDBA_._CHANGE_ CHG, _SMDBA_._WORKORD_ WO, _SMDBA_._GROUPS_ GROUPS where wo._GROUP_ = groups.sequence AND wo.[SEQ_CHANGE:] = CHG.sequence AND WO.STARTDATE >= cast(DateAdd(day, " + str(start_day) + ", GETDATE()) as date) AND WO.STARTDATE <= cast(DateAdd(day, " + str(end_day) + ", GETDATE()) as date) AND GROUPS.CODE like '%" + team + "%' AND (CHG.SEQ_CMSTATUS='5' OR CHG.SEQ_CMSTATUS='1025' OR CHG.SEQ_CMSTATUS='1026' OR CHG.SEQ_CMSTATUS='1027' OR CHG.SEQ_CMSTATUS='1028')"
    print(conn_str)
    validated_calendar_data = []
    non_validated_calendar_data = []
     
    db_conn = f._get_changecalendar_connection()
    cursor = db_conn.cursor()
    
    if db_conn is None:
        return {"validated" : validated_calendar_data, "non_validated" : non_validated_calendar_data}
    
    cursor.execute(conn_str)
    data = cursor.fetchall()
    f._close_connection(db_conn)
    data = [t if t[7] is not None else (t[0],t[1],t[2],t[3],t[4],t[5],t[6], '') for t in data] #Replace the None with ''
    
    keys = ["changeid", "group","startdate","duration","status","tscvalidation","workorderid","urgentjustification"]
    for t in data:
        # convert dbrows to list
        t = [x for x in t]
        row = dict(zip(keys, t))
        #format startdate
        row['startdate'] = row['startdate'].strftime('%y-%m-%dT%H:%M:%S')
        #separate the data into validated and non validated data
        if row['status'] == 5:
            validated_calendar_data.append(row)
        else:
            non_validated_calendar_data.append(row)
            
    
    return {"validated" : validated_calendar_data, "non_validated" : non_validated_calendar_data}
