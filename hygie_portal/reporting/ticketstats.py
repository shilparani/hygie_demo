"""Helper utilities and decorators."""

from datetime import timedelta
from dateutil import tz
from math import floor,ceil
from collections import defaultdict,OrderedDict
import datetime
import pytz
from hygie_portal.reporting import common as f
import operator
import os
import json
from hygie.settings import logger
from django.conf import settings
from hygie_portal.querymanager import querymanager as qm

def get_stats_data_summary(filters_dic, local_tz=None):
    
    logger.info(filters_dic)
    group_list = [g['team'] for g in settings.ALL_TEAMS if g['id'] in filters_dic['ticket_stats_group']]
    group_list = f.flatten(group_list)
    group_list_str = ",".join("'{0}'".format(w) for w in group_list)
    
    # include transfer tickets or not
    outgoing_action_desc_list = ['HD_FRWD_GROUP','HD_CLOSE'] if filters_dic['transfer_ticket_check'] else ['HD_CLOSE']
    outgoing_action_desc_list = f.flatten(outgoing_action_desc_list)
    outgoing_action_desc_str = ",".join("'{0}'".format(w) for w in outgoing_action_desc_list)
    
    
    # update date based on timezone
    #TZ = pytz.timezone(current_app.config.get('TIMEZONE'))
    #from_zone = tz.gettz(local_tz)
    #to_zone = tz.gettz(current_app.config.get('TIMEZONE'))
    from_zone = pytz.timezone(local_tz)
    to_zone = pytz.timezone(settings.TIMEZONE)
    logger.info("========= FROM TIMEZONE :: %s ", from_zone)
    logger.info("========= TO TIMEZONE :: %s", to_zone)
    
    #start_date = f._convert_to_local_time(from_zone, to_zone, '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(filters_dic['start_date']))
    #end_date = f._convert_to_local_time(from_zone, to_zone, '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(filters_dic['end_date']))
    
    logger.info("========= BEFORE START :: %s", filters_dic['start_date'])
    logger.info("========= BEFORE END  :: %s", filters_dic['end_date'])
    
    start_date = from_zone.localize(filters_dic['start_date']).astimezone(to_zone)
    end_date = from_zone.localize(filters_dic['end_date']).astimezone(to_zone)
    
    #start_date = f._convert_to_local_time(from_zone, to_zone, filters_dic['start_date'])
    #end_date = f._convert_to_local_time(from_zone, to_zone, filters_dic['end_date'])
    
    logger.info("========= AFTER START :: %s", start_date)
    logger.info("========= AFTER END  :: %s", end_date)
    
    logger.info("========= AFTER START :: %s", '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(start_date))
    logger.info("========= AFTER END  :: %s", '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(end_date))
    
    
    #db_view_query = qm.get_query_by_title('_TELDETAI_HYGIE_')
    view = '_teldetai_hygie_' #'({0})'.format(db_view_query)
    
    #get received tickets
    incoming_conn_str = "select lastmodified from {3} as details where `TO_GROUP:` in ({0}) and action_desc = 'HD_FRWD_GROUP' and lastmodified between '{1}' and '{2}'"
    outgoing_conn_str = "select lastmodified from {4} as details where `FROM_GROUP:` in ({0}) and action_desc in ({1}) and lastmodified between '{2}' and '{3}'"

    incoming_conn_str = incoming_conn_str.format(group_list_str, '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d};'.format(start_date), '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(end_date), view)
    outgoing_conn_str = outgoing_conn_str.format(group_list_str, outgoing_action_desc_str, '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(start_date), '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d};'.format(end_date), view)
    
    #incoming_conn_str = incoming_conn_str.format(group_list_str, filters_dic['start_date'], filters_dic['end_date'], view)
    #outgoing_conn_str = outgoing_conn_str.format(group_list_str, outgoing_action_desc_str, filters_dic['start_date'], filters_dic['end_date'], view)
    
    
    
    incoming_stats_day, incoming_stats_day_drilldown, outgoing_stats_day, outgoing_stats_day_drilldown = {}, {}, {}, {}
    incoming_total_count, incoming_day_avg_count, outgoing_total_count, outgoing_day_avg_count = 0, 0, 0, 0
    
    incoming_stats_hour, incoming_stats_hour_drilldown, outgoing_stats_hour, outgoing_stats_hour_drilldown = {}, {}, {}, {}
    incoming_hour_avg_count, outgoing_hour_avg_count = 0, 0
    
    incoming_day_hour_stats, incoming_day_hour_drilldown, outgoing_day_hour_stats, outgoing_day_hour_drilldown = {}, {}, {}, {}
    
    #logger.info(incoming_conn_str)
    logger.info(outgoing_conn_str)
    
    db_conn = f._get_ticketstats_connection()
    cursor = db_conn.cursor()
    
    #incoming data retrieval
    cursor.execute(incoming_conn_str)
    incoming_data = [list(row) for row in cursor.fetchall()]
    
    #outgoing data retrieval
    cursor.execute(outgoing_conn_str)
    outgoing_data = [list(row) for row in cursor.fetchall()]
    
    f._close_connection(db_conn)
    default_keys = ['year','week','day']
    
    isoweekday_mapping = {
        1 : 'Mon',
        2 : 'Tue',
        3 : 'Wed',
        4 : 'Thur',
        5 : 'Fri',
        6 : 'Sat',
        7 : 'Sun'
    }
    
    twohour_period_mapping = {
        0 : '00 - 02',
        1 : '00 - 02',
        2 : '02 - 04',
        3 : '02 - 04',
        4 : '04 - 06',
        5 : '04 - 06',
        6 : '06 - 08',
        7 : '06 - 08',
        8 : '08 - 10',
        9 : '08 - 10',
        10 : '10 - 12',
        11 : '10 - 12',
        12 : '12 - 14',
        13 : '12 - 14',
        14 : '14 - 16',
        15 : '14 - 16',
        16 : '16 - 18',
        17 : '16 - 18',
        18 : '18 - 20',
        19 : '18 - 20',
        20 : '20 - 22',
        21 : '20 - 22',
        22 : '22 - 24',
        23 : '22 - 24'
    }
    
    # stats for incoming 
    for t in incoming_data:
        
        #convert data into local tz before processing
        #t[0] = f._convert_to_local_time(to_zone, from_zone, t[0])
        
        #by day
        day_of_week = t[0].isocalendar()
        date = t[0].strftime('%Y-%m-%d')
        weekday_breakdown = dict(zip(default_keys, day_of_week))
        mapped_name = isoweekday_mapping[weekday_breakdown['day']]
        
        if weekday_breakdown['day'] not in incoming_stats_day:
            incoming_stats_day[weekday_breakdown['day']] = {
                'name' : mapped_name,
                'y' : 0,
                'drilldown' : mapped_name
            }
        incoming_stats_day[weekday_breakdown['day']]['y'] += 1
        incoming_total_count += 1
        
        if mapped_name not in incoming_stats_day_drilldown:
            incoming_stats_day_drilldown[mapped_name] = {
                date : 0
            }
        if date not in incoming_stats_day_drilldown[mapped_name]:
            incoming_stats_day_drilldown[mapped_name][date] = 0
        incoming_stats_day_drilldown[mapped_name][date] += 1
        
        #by 2 hour period
        hour_of_day = t[0].hour
        mapped_hour = twohour_period_mapping[hour_of_day]
        if mapped_hour not in incoming_stats_hour:
            incoming_stats_hour[mapped_hour] = {
                'name' : mapped_hour,
                'y' : 0,
                'drilldown' : mapped_hour
            }
        incoming_stats_hour[mapped_hour]['y'] += 1
        
        if mapped_hour not in incoming_stats_hour_drilldown:
            incoming_stats_hour_drilldown[mapped_hour] = {
                date : 0
            }
        if date not in incoming_stats_hour_drilldown[mapped_hour]:
            incoming_stats_hour_drilldown[mapped_hour][date] = 0
        incoming_stats_hour_drilldown[mapped_hour][date] += 1
        
        #hour breakdown by day
        if mapped_name not in incoming_day_hour_stats:
            incoming_day_hour_stats[mapped_name] = {}
        if mapped_hour not in incoming_day_hour_stats[mapped_name]:
            incoming_day_hour_stats[mapped_name][mapped_hour] = {
                'name' : mapped_hour,
                'y' : 0,
                'drilldown' : mapped_hour
            }  
        incoming_day_hour_stats[mapped_name][mapped_hour]['y'] += 1
        
        if mapped_name not in incoming_day_hour_drilldown:
            incoming_day_hour_drilldown[mapped_name] = {}
        if mapped_hour not in incoming_day_hour_drilldown[mapped_name]:
            incoming_day_hour_drilldown[mapped_name][mapped_hour] = {
                date : 0
            }
        if date not in incoming_day_hour_drilldown[mapped_name][mapped_hour]:
            incoming_day_hour_drilldown[mapped_name][mapped_hour][date] = 0
        incoming_day_hour_drilldown[mapped_name][mapped_hour][date] += 1
    
    if len(incoming_stats_day) > 0:    
        incoming_day_avg_count = ceil(incoming_total_count / len(incoming_stats_day))
    if len(incoming_stats_hour) > 0:
        incoming_hour_avg_count = ceil(incoming_total_count / len(incoming_stats_hour))
    
        
    # stats for outgoing by day
    for t in outgoing_data:
        # convert dbrows to list
        day_of_week = t[0].isocalendar()
        date = t[0].strftime('%Y-%m-%d')
        weekday_breakdown = dict(zip(default_keys, day_of_week))
        mapped_name = isoweekday_mapping[weekday_breakdown['day']]
        
        if weekday_breakdown['day'] not in outgoing_stats_day:
            outgoing_stats_day[weekday_breakdown['day']] = {
                'name' : mapped_name,
                'y' : 0,
                'drilldown' : mapped_name
            }
        outgoing_stats_day[weekday_breakdown['day']]['y'] += 1
        outgoing_total_count += 1
        
        if mapped_name not in outgoing_stats_day_drilldown:
            outgoing_stats_day_drilldown[mapped_name] = {
                date : 0
            }
        if date not in outgoing_stats_day_drilldown[mapped_name]:
            outgoing_stats_day_drilldown[mapped_name][date] = 0
        outgoing_stats_day_drilldown[mapped_name][date] += 1
        
        #by 2 hour period
        hour_of_day = t[0].hour
        mapped_hour = twohour_period_mapping[hour_of_day]
        if mapped_hour not in outgoing_stats_hour:
            outgoing_stats_hour[mapped_hour] = {
                'name' : mapped_hour,
                'y' : 0,
                'drilldown' : mapped_hour
            }
        outgoing_stats_hour[mapped_hour]['y'] += 1
        
        if mapped_hour not in outgoing_stats_hour_drilldown:
            outgoing_stats_hour_drilldown[mapped_hour] = {
                date : 0
            }
        if date not in outgoing_stats_hour_drilldown[mapped_hour]:
            outgoing_stats_hour_drilldown[mapped_hour][date] = 0
        outgoing_stats_hour_drilldown[mapped_hour][date] += 1
        
        #hour breakdown by day
        if mapped_name not in outgoing_day_hour_stats:
            outgoing_day_hour_stats[mapped_name] = {}
        if mapped_hour not in outgoing_day_hour_stats[mapped_name]:
            outgoing_day_hour_stats[mapped_name][mapped_hour] = {
                'name' : mapped_hour,
                'y' : 0,
                'drilldown' : mapped_hour
            } 
        outgoing_day_hour_stats[mapped_name][mapped_hour]['y'] += 1
        
        if mapped_name not in outgoing_day_hour_drilldown:
            outgoing_day_hour_drilldown[mapped_name] = {}
        if mapped_hour not in outgoing_day_hour_drilldown[mapped_name]:
            outgoing_day_hour_drilldown[mapped_name][mapped_hour] = {
                date : 0
            }
        if date not in outgoing_day_hour_drilldown[mapped_name][mapped_hour]:
            outgoing_day_hour_drilldown[mapped_name][mapped_hour][date] = 0
        outgoing_day_hour_drilldown[mapped_name][mapped_hour][date] += 1
    
    if len(outgoing_stats_day) > 0:
        outgoing_day_avg_count = ceil(outgoing_total_count / len(outgoing_stats_day))
    if len(outgoing_stats_hour) > 0:
        outgoing_hour_avg_count = ceil(outgoing_total_count / len(outgoing_stats_hour))
        
   
    stats_result = {
        'incoming_total_count' : incoming_total_count,
        'outgoing_total_count' : outgoing_total_count,
        'incoming_stats_day' : incoming_stats_day, 
        'incoming_stats_day_drilldown' : incoming_stats_day_drilldown,
        'incoming_day_avg_count' : incoming_day_avg_count, 
        'outgoing_stats_day' : outgoing_stats_day, 
        'outgoing_stats_day_drilldown' : outgoing_stats_day_drilldown, 
        'outgoing_day_avg_count' : outgoing_day_avg_count,
        'incoming_stats_hour' : incoming_stats_hour, 
        'incoming_stats_hour_drilldown' : incoming_stats_hour_drilldown,
        'incoming_hour_avg_count' : incoming_hour_avg_count, 
        'outgoing_stats_hour' : outgoing_stats_hour, 
        'outgoing_stats_hour_drilldown' : outgoing_stats_hour_drilldown, 
        'outgoing_hour_avg_count' : outgoing_hour_avg_count,
        'incoming_day_hour_stats' : incoming_day_hour_stats,
        'outgoing_day_hour_stats' : outgoing_day_hour_stats,
        'incoming_day_hour_drilldown' : incoming_day_hour_drilldown,
        'outgoing_day_hour_drilldown' : outgoing_day_hour_drilldown
    }
    
    return stats_result
