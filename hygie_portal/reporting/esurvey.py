"""Helper utilities and decorators."""

from datetime import timedelta
from dateutil import tz
from math import floor
from collections import defaultdict,OrderedDict
import datetime
import pytz
from hygie_portal.reporting import common as f
from hygie.settings import logger
import operator
import os
import json

def get_esurvey_data_summary(satisfaction_level, start_date, end_date, group_by_param):
    db_conn = f._get_esurvey_connection()
    cursor = db_conn.cursor()
   
    if group_by_param == 'country': 
        conn_str = "select count(*),zone,country from esurvey where question1 like '{0}' and date between '{1}' and '{2}' group by {3}".format(satisfaction_level, start_date, end_date, group_by_param)
    elif group_by_param == 'zone':
        conn_str = "select count(*),zone from esurvey where open_group like '%SERVICE_DESK%' and question1 like '{0}' and date between '{1}' and '{2}' group by {3}".format(satisfaction_level, start_date, end_date, group_by_param)
    else:
        conn_str = "select count(*),close_group from esurvey where question1 like '{0}' and date between '{1}' and '{2}' group by close_group".format(satisfaction_level, start_date, end_date)

    esurvey_data = []
    print("===== ESURVEY SUMMARY :: %s"%conn_str)
    cursor.execute(conn_str)
    for t in cursor.fetchall():
        # convert dbrows to list
        t = [x for x in t]
        t[0] = int(t[0])
        esurvey_data.append(t)
    
    f._close_connection(db_conn)

    return esurvey_data

def get_esurvey_data_details(mode, mode_param, satisfaction_level, start_date, end_date):
    db_conn = f._get_esurvey_connection()
    cursor = db_conn.cursor()

    if mode == "country":
        if satisfaction_level == 'Satisfied':
            satisfaction_level_tuple = ('Satisfied','Very satisfied')
            conn_str = "select ticket,DATE_FORMAT(date,'%d %M %Y'),user,zone,country,site,description,open_group,close_group,question1,question2,question3,question4,question5,verbatim from esurvey where country like '{0}' and question1 in {1} and date between '{2}' and '{3}'".format(mode_param, satisfaction_level_tuple, start_date, end_date)
        else:
            conn_str = "select ticket,DATE_FORMAT(date,'%d %M %Y'),user,zone,country,site,description,open_group,close_group,question1,question2,question3,question4,question5,verbatim from esurvey where country like '{0}' and question1 in ('{1}') and date between '{2}' and '{3}'".format(mode_param, satisfaction_level, start_date, end_date)
    elif mode == "zone":
        if mode_param == 'AMERICAS':
            mode_param = "NA"
        
        if satisfaction_level == 'Satisfied':
            satisfaction_level_tuple = ('Satisfied','Very satisfied')
            conn_str = "select ticket,DATE_FORMAT(date,'%d %M %Y'),user,zone,country,site,description,open_group,close_group,question1,question2,question3,question4,question5,verbatim from esurvey where open_group like '%SERVICE_DESK%' and question1 in {0} and date between '{1}' and '{2}'".format(satisfaction_level_tuple, start_date, end_date)
        else:
            conn_str = "select ticket,DATE_FORMAT(date,'%d %M %Y'),user,zone,country,site,description,open_group,close_group,question1,question2,question3,question4,question5,verbatim from esurvey where open_group like '%SERVICE_DESK%' and question1 in ('{0}') and date between '{1}' and '{2}'".format(satisfaction_level, start_date, end_date)
        
        if mode_param == "WORLDWIDE":
            conn_str = conn_str + " and zone in ('NA', 'EMEA', 'APAC')"
        else:
            conn_str = conn_str + " and zone like '" + mode_param + "'" 
    else:
        if satisfaction_level == 'Satisfied':
            satisfaction_level_tuple = ('Satisfied','Very satisfied')
            conn_str = "select ticket,DATE_FORMAT(date,'%d %M %Y'),user,zone,country,site,description,open_group,close_group,question1,question2,question3,question4,question5,verbatim from esurvey where close_group like '{0}' and question1 in {1} and date between '{2}' and '{3}'".format(mode_param, satisfaction_level_tuple, start_date, end_date)
        else:
            conn_str = "select ticket,DATE_FORMAT(date,'%d %M %Y'),user,zone,country,site,description,open_group,close_group,question1,question2,question3,question4,question5,verbatim from esurvey where close_group like '{0}' and question1 in ('{1}') and date between '{2}' and '{3}'".format(mode_param, satisfaction_level, start_date, end_date)        
    logger.info("======== ESURVEY CONN STR :: %s"%conn_str)
    esurvey_data = []

    cursor.execute(conn_str)
    for t in cursor.fetchall():
        # convert dbrows to list
        t = [x for x in t]
        print(t)
        t[2] = t[2]
        t[2] = t[2]
        t[5] = t[5]
        t[5] = t[5]
        t[6] = t[6]
        t[6] = t[6]
        t[14] = t[14]
        t[14] = t[14]
        esurvey_data.append(t)

    f._close_connection(db_conn)
    return esurvey_data

def get_stats_data_by_zone(start_date, end_date):
    db_conn = f._get_esurvey_connection()
    cursor = db_conn.cursor()
    
    stats_data = {}

    emea_conn_str = "select question1, count(*) from esurvey where zone like '{0}' and date between '{1}' and '{2}' group by question1".format("emea", start_date, end_date)
  
    cursor.execute(emea_conn_str)
    for t in cursor.fetchall():
        if 'emea' not in stats_data:
            stats_data['emea'] = []
        # convert dbrows to list
        t = [x for x in t]
        t[1] = int(t[1])
        stats_data['emea'].append(t)

    america_conn_str = "select question1, count(*) from esurvey where zone like '{0}' and date between '{1}' and '{2}' group by question1".format("na", start_date, end_date)

    cursor.execute(america_conn_str)
    for t in cursor.fetchall():
        if 'america' not in stats_data:
            stats_data['america'] = []
        # convert dbrows to list
        t = [x for x in t]
        t[1] = int(t[1])
        stats_data['america'].append(t)

    apac_conn_str = "select question1, count(*) from esurvey where zone like '{0}' and date between '{1}' and '{2}' group by question1".format("apac", start_date, end_date)

    cursor.execute(apac_conn_str)
    for t in cursor.fetchall():
        if 'apac' not in stats_data:
            stats_data['apac'] = []
        # convert dbrows to list
        t = [x for x in t]
        t[1] = int(t[1])
        stats_data['apac'].append(t)

    f._close_connection(db_conn)
    print("=============== ESURVERY STATS DATA : %s"%stats_data)
    return stats_data   

def get_total_closed_tickets_count(start_date, end_date):
    db_conn = f._get_esurvey_connection()
    cursor = db_conn.cursor()

    total_tickets_count = []

    conn_str = "select sum(AM) as 'NA', sum(AP) as 'APAC', sum(EMEA) as 'EMEA', sum(Global) as 'WORLDWIDE' from esurvey_total_tickets where date between '{0}' and '{1}'".format(start_date, end_date)

    cursor.execute(conn_str)
    for t in cursor.fetchall():
        # convert dbrows to list
        t = [x for x in t]
        total_tickets_count.append(t) 

    total_tickets_count = [int(x) if x is not None else 0 for x in total_tickets_count[0]]
    f._close_connection(db_conn)
    return total_tickets_count
