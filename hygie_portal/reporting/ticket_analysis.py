"""Helper utilities and decorators."""

from datetime import timedelta
from dateutil import tz
from math import floor
from dateutil import tz
from os import path
from collections import defaultdict,OrderedDict
from hygie_portal.tickets import common as tickets_common
from hygie_portal.querymanager import querymanager as qm
from hygie.settings import logger
import datetime
import pytz
from hygie_portal.reporting import common as f
from django.conf import settings
import operator
import os
import json
import functools

#each transfer is considered as escalation regardless of the number of times it's transferred between 2 groups
def escalation(request, source_grp=None, target_grp=None,start_date=None,end_date=None,local_tz=None):
    total_tickets_count = 0
    escalation_total_count = [0]
    teams_data = []
    data_result = {}
    teams_breakdown_temp = {}
    teams_ticket_seq = []
    tickets_data_result =[]
    teams_data_result = []
    ticket_num_keys = {} 
    display_results = []
    data_result['chart_breakdown'] = {}
    ALL_TEAMS = settings.ALL_TEAMS
    
    #db_view_query = qm.get_query_by_title('_TELDETAI_HYGIE_4MONTH')
    view = '_teldetai_' #'({0})'.format(db_view_query)
    
    conn_str = "select `FROM_GROUP:`,`TO_GROUP:`,`SEQUENCE`, count(*) as counter from {2} as details where ACTION_DESC='HD_FRWD_GROUP' and LASTMODIFIED between '{0}' and '{1}'".format(start_date,end_date,view)

    escalation_count_conn_str = "SELECT COUNT(DISTINCT `SEQUENCE`) FROM {2} as details where LASTMODIFIED between '{0}' and '{1}' and ACTION_DESC='HD_FRWD_GROUP'".format(start_date,end_date,view)
    
    if source_grp:
        #get the corresponding 'team' from 'id'
        source_team = [grps['team'] for grps in ALL_TEAMS for grp_id in source_grp if grps['id'] == grp_id]
        SRC_GRP_TEAM = ",".join("'{0}'".format(w) for w in functools.reduce(lambda x,y: x+y,source_team)) if source_team else "''"  #lamba used to flatten the list [[],[]]
        conn_str += " and `FROM_GROUP:` IN ({0}) and `TO_GROUP:` not in ({0})".format(SRC_GRP_TEAM)
        escalation_count_conn_str += "and `FROM_GROUP:` IN ({0}) and `TO_GROUP:` not in ({0});".format(SRC_GRP_TEAM)
        
    if target_grp:    
        #get the corresponding 'team' from 'id'
        target_team = [grps['team'] for grps in ALL_TEAMS for grp_id in target_grp if grps['id'] == grp_id]
        TGT_GRP_TEAM = ",".join("'{0}'".format(w) for w in functools.reduce(lambda x,y: x+y,target_team)) if target_team else "''"  #lamba used to flatten the list [[],[]]
        conn_str += " and `TO_GROUP:` IN ({0}) and `FROM_GROUP:` not in ({0})".format(TGT_GRP_TEAM)     
    
    conn_str += " group by `FROM_GROUP:`,`TO_GROUP:`,`SEQUENCE`;"
    
    logger.info('conn_str***************:%s'%conn_str)
    db_conn = tickets_common._get_connection_itsm(request)
    cursor = db_conn.cursor()

    # check DB connection, if none return
    if db_conn is None:
        return data_result
     
    #import pdb;pdb.set_trace()
    cursor.execute(conn_str)
    escalation_data = cursor.fetchall()
    
    #import pdb;pdb.set_trace()
    #to calculate the escalation percentage
    if source_grp and target_grp:
        cursor.execute(escalation_count_conn_str)
        escalation_total_count = cursor.fetchone() #Total tickets open/created by the source group in that date range
        
    f._close_connection(db_conn)

    #Map keys to DB data and extract sequence
    for t in escalation_data:
        default_keys = ["from_group","to_group","sequence","counter"]
        
        #zip the keys with values
        tickets_data = dict(zip(default_keys, t))
  
        teams_ticket_seq.append(tickets_data["sequence"])
        tickets_data_result.append(tickets_data)
    
    ticket_sequence = ",".join("'{0}'".format(w) for w in teams_ticket_seq) if teams_ticket_seq else "''"           
    #tickets_data_conn_str = "select TOP 5000 [DATE OPEN] , Urgency_desc, [DUE_DATE:], NUM_TICKET , DESCRIPTION, [CLOSED_GROUP:], group_desc, assign_desc , Subject_desc, Client_desc, TTYPE as TicketType, lastmodified as LastModified, lastmodified as LastDate, status_desc as third_party, [SEQUENCE], [SEQUENCE_Parent], title, type as type, Client_desc as Client, User_Location as Location from _TELMASTE_HYGIE_ where NUM_TICKET IN  ({0})".format(ticket_numbers
    
    teams_data = ticket_analysis_query(request, ticket_sequence,local_tz) if ticket_sequence != "''" else []
    
    result = []
    #logic to merge teldati and telmaste DB data from the sequence
    for td in teams_data:
        for tdr in tickets_data_result:
            if td["itsm_url"] == tdr["sequence"]:
                tdr.update(td)
                result.append(tdr)
               
    #Remove duplicates from the display results - TELMASTE
    for tdr in result:
        if tdr["ticket_number"] not in ticket_num_keys.keys():
            ticket_num_keys[tdr["ticket_number"]] = 1
            display_results.append(tdr)
       
    for dr in display_results:   
        if source_grp and target_grp:
            total_tickets_count += dr['counter'] #Number of tickets transferred
        elif source_grp and not target_grp:
            if dr['to_group'] not in teams_breakdown_temp.keys(): #if team does not exist in keys, add and initialize to 0
                teams_breakdown_temp[dr['to_group']] = 0
            teams_breakdown_temp[dr['to_group']] += 1
        elif target_grp and not source_grp:
            if dr['from_group'] not in teams_breakdown_temp.keys(): #if team does not exist in keys, add and initialize to 0
                teams_breakdown_temp[dr['from_group']] = 0
            teams_breakdown_temp[dr['from_group']] += 1
    
    for t in ALL_TEAMS:
        for key, value in teams_breakdown_temp.items(): 
            if key in t['team']:
                data_result['chart_breakdown'][t['name']] = value if t['name'] not in data_result['chart_breakdown'].keys() else data_result['chart_breakdown'][t['name']] + value
    
    data_result['escalation_count'] = "{0:0.1f}".format((float(total_tickets_count)/escalation_total_count[0])*100) if escalation_total_count[0] else 0
    data_result['teams_data'] = display_results 
    logger.info("Escalation result:%s"%data_result)
    
    return data_result

def reopen(request, reopen_grp=None, closed_grp=None,start_date=None,end_date=None,local_tz=None):
    data_result = {}
    data_result['teams_data'] = {}
    ticket_num_dict ={}
    ticket_data_without_duplicate_temp = []
    reopened_once = []
    reopened_thrice = []
    reopened_five = []
    teams_ticket_num = []
    ticket_num_keys = {}
    display_results = []
    result_sequence = []
    next_count = 1
    db_conn = tickets_common._get_connection_itsm(request)
    cursor = db_conn.cursor()
    
    # check DB connection, if none return
    if db_conn is None:
        return data_result
    
    ALL_TEAMS = settings.ALL_TEAMS
    
    #db_view_query = qm.get_query_by_title('_TELDETAI_HYGIE_4MONTH')
    view = '_teldetai_' #'({0})'.format(db_view_query)
    
    reopen_grp_selected = "select distinct `sequence`, count(ACTION_DESC) as reopen_count from {2} as details where ACTION_DESC='HD_REOPEN' and LASTMODIFIED between '{0}' and '{1}'".format(start_date,end_date,view)
    
    sub_query = "select distinct `sequence` from {2} as details where ACTION_DESC = 'HD_REOPEN' and LASTMODIFIED between '{0}' and '{1}'".format(start_date,end_date,view)
    
    if (reopen_grp and closed_grp) or (closed_grp and not reopen_grp):
        reopen_team = [grps['team'] for grps in ALL_TEAMS for grp_id in reopen_grp if grps['id'] == grp_id] if reopen_grp else []
        reopen_team_list = [w for w in functools.reduce(lambda x,y: x+y,reopen_team)] if reopen_grp else []
        REOPEN_GRP_TEAM = ",".join("'{0}'".format(w) for w in functools.reduce(lambda x,y: x+y,reopen_team)) if reopen_team else "''" #lamba used to flatten the list [[],[]]
        
        closed_team = [grps['team'] for grps in ALL_TEAMS for grp_id in closed_grp if grps['id'] == grp_id]
        CLOSED_GRP_TEAM = ",".join("'{0}'".format(w) for w in functools.reduce(lambda x,y: x+y,closed_team)) if closed_team else "''" #lamba used to flatten the list [[],[]]
        
        if reopen_grp:
            sub_query += " and `FROM_GROUP:` IN ({0}) GROUP BY `sequence`;".format(REOPEN_GRP_TEAM) 
        else:
            sub_query += " GROUP BY `sequence`;"
        
        cursor.execute(sub_query)
        ticket_sequence_list_temp = cursor.fetchall()
        ticket_seq_list = [ticket_seq[0] for ticket_seq in ticket_sequence_list_temp if ticket_sequence_list_temp]
        ticket_sequence = ",".join("'{0}'".format(w) for w in ticket_seq_list) if ticket_seq_list else "''" 
        
        if ticket_sequence != "''":
            
            conn_str = "SELECT `SEQUENCE`,LASTMODIFIED,`FROM_GROUP:`,ACTION_DESC FROM {4} as details where ((ACTION_DESC = 'HD_CLOSE' and `FROM_GROUP:` in ({0})) or (ACTION_DESC = 'HD_REOPEN' and LASTMODIFIED between '{1}' and '{2}')) and  `sequence` in ({3}) ORDER BY `sequence`, lastmodified desc;".format(CLOSED_GRP_TEAM,start_date,end_date,ticket_sequence,view)
            
            logger.info('conn_str***************:%s'%conn_str)
            cursor.execute(conn_str)
            reopen_data_db = cursor.fetchall()
            
            next_count = 1
            
            #logic to get the closed ticket for respective reopen in the date range
            for t in reopen_data_db:
                default_keys = ["sequence","last_modified","from_group","action"]
                tickets_data = dict(zip(default_keys, t))
                #Check for the last element in the list
                if t == reopen_data_db[-1]: 
                    if reopen_grp and tickets_data["action"] == 'HD_REOPEN' and tickets_data["from_group"] in reopen_team_list and tickets_data["sequence"] not in ticket_num_dict:
                        ticket_num_dict[tickets_data["sequence"]] = 0
                    if reopen_grp and tickets_data["action"] == 'HD_REOPEN' and tickets_data["from_group"] in reopen_team_list:
                        ticket_num_dict[tickets_data["sequence"]] += 1
                else:
                    if tickets_data["sequence"] == reopen_data_db[next_count][0]:
                        #reopen and closed filter selected
                        if reopen_grp:
                            if tickets_data["action"] == 'HD_REOPEN' and reopen_data_db[next_count][3] == 'HD_CLOSE' and tickets_data["from_group"] in reopen_team_list and tickets_data["sequence"] not in ticket_num_dict:
                                ticket_num_dict[tickets_data["sequence"]] = 0
                                result_sequence.append(tickets_data["sequence"])
                            if tickets_data["action"] == 'HD_REOPEN' and reopen_data_db[next_count][3] == 'HD_CLOSE' and tickets_data["from_group"] in reopen_team_list:
                                ticket_num_dict[tickets_data["sequence"]] += 1
                        #Only closed filter selected 
                        else:
                            if tickets_data["action"] == 'HD_REOPEN' and reopen_data_db[next_count][3] == 'HD_CLOSE' and tickets_data["sequence"] not in ticket_num_dict:
                                ticket_num_dict[tickets_data["sequence"]] = 0
                                result_sequence.append(tickets_data["sequence"])
                            if tickets_data["action"] == 'HD_REOPEN' and reopen_data_db[next_count][3] == 'HD_CLOSE':
                                ticket_num_dict[tickets_data["sequence"]] += 1
                            
                    next_count+=1 #increase the counter to get the next element in the list
    
        
    elif reopen_grp and not closed_grp:    
        #get the corresponding 'team' from 'id'
        reopen_team = [grps['team'] for grps in ALL_TEAMS for grp_id in reopen_grp if grps['id'] == grp_id]
        REOPEN_GRP_TEAM = ",".join("'{0}'".format(w) for w in functools.reduce(lambda x,y: x+y,reopen_team)) if reopen_team else "''"  #lamba used to flatten the list [[],[]]
        reopen_grp_selected += " and `FROM_GROUP:` IN ({0}) GROUP BY `sequence`;".format(REOPEN_GRP_TEAM) 
        
        logger.info('Reopen conn_str***************:%s'%reopen_grp_selected)
        cursor.execute(reopen_grp_selected)
        reopen_data_db = cursor.fetchall()

        for t in reopen_data_db:
            default_keys = ["sequence", "reopen_count"]
            
            #zip the keys with values
            tickets_data = dict(zip(default_keys, t))
            ticket_num_dict[tickets_data["sequence"]] = tickets_data["reopen_count"]
            result_sequence.append(tickets_data["sequence"]) 
   
    f._close_connection(db_conn)       
    
    for key,value in ticket_num_dict.items():
        if 1 <= value < 3:
            reopened_once.append(key)
        if 3 <= value < 5:
            reopened_thrice.append(key)
        if value >= 5:
            reopened_five.append(key)
    
    result_ticket_seq = ",".join("'{0}'".format(w) for w in result_sequence) if result_sequence else "''"
        
    reopen_tickets_data_telmaste = ticket_analysis_query(request, result_ticket_seq,local_tz) if result_ticket_seq != "''" else []
    
    #Remove duplicates from the display results - TELMASTE
    for tdr in reopen_tickets_data_telmaste:
        if tdr["ticket_number"] not in ticket_num_keys.keys():
            ticket_num_keys[tdr["ticket_number"]] = 1
            if tdr["itsm_url"] in ticket_num_dict.keys():
                tdr["counter"] = ticket_num_dict[tdr["itsm_url"]]
                display_results.append(tdr)
    
    data_result['teams_data']['reopened_once'] = []    
    data_result['teams_data']['reopened_thrice'] = []  
    data_result['teams_data']['reopened_five'] = []
    
    #Map the results from TELMASTE to their respective reopened_once or reopened_thrice or reopened_five groups
    for reopened_tickets in display_results:
        if reopened_tickets["itsm_url"] in reopened_once:
            data_result['teams_data']['reopened_once'].append(reopened_tickets)
        elif reopened_tickets["itsm_url"] in reopened_thrice:
            data_result['teams_data']['reopened_thrice'].append(reopened_tickets)
        elif reopened_tickets["itsm_url"] in reopened_five:
            data_result['teams_data']['reopened_five'].append(reopened_tickets)
    
    data_result["total_tickets"] = len(data_result['teams_data']['reopened_once']) + len(data_result['teams_data']['reopened_thrice']) + len(data_result['teams_data']['reopened_five'])

    logger.info("Reopen once:%s"%len(reopened_once))
    logger.info("Reopen thrice:%s"%len(reopened_thrice))
    logger.info("Reopen five or more:%s"%len(reopened_five))
 
    return data_result

def bouncing(request, bouncing_grp=None,start_date=None,end_date=None,local_tz=None):
    ticket_num_dict = {}
    data_result = {}
    data_result['ticket_breakdown'] = {}
    ticket_data_without_duplicate_temp = []
    teams_data = []
    ticket_num_keys = {}
    display_results = []
    ALL_TEAMS = settings.ALL_TEAMS

    #conn_str = "select [SEQUENCE],LASTMODIFIED,[FROM_GROUP:],ACTION_DESC from _TELDETAI_HYGIE_4MONTH where LASTMODIFIED between '{0}' and '{1}'".format(start_date,end_date)

    if bouncing_grp:
        #get the corresponding 'team' from 'id'
        bouncing_team = [grps['team'] for grps in ALL_TEAMS for grp_id in bouncing_grp if grps['id'] == grp_id]
        #BOUNCING_GRP_TEAM = ",".join("'{0}'".format(w) for w in reduce(lambda x,y: x+y,bouncing_team)) #lamba used to flatten the list [[],[]]
        BOUNCING_GRP_TEAM = ",".join("'{0}'".format(w) for w in functools.reduce(lambda x,y: x+y,bouncing_team)) #lamba used to flatten the list [[],[]]
        #conn_str += " and ((ACTION_DESC = 'HD_FRWD_GROUP' AND [TO_GROUP:] IN ({0}) AND ([FROM_GROUP:] NOT IN ({0}) AND [FROM_GROUP:] IS NOT NULL)) OR ACTION
        #conn_str += " and ((ACTION_DESC = 'HD_FRWD_GROUP' AND [TO_GROUP:] IN ({0}) AND ([FROM_GROUP:] NOT IN ({0}) AND [FROM_GROUP:] IS NOT NULL)) OR ACTION_DESC = 'HD_REOPEN') order by [sequence], lastmodified desc ".format(BOUNCING_GRP_TEAM)
        
        query_path = path.dirname( path.abspath(__file__) )
        
        '''bouncing_file = os.path.join(query_path,"bouncing_query.sql")
        with open(bouncing_file,'r') as bouncing_file:
            bouncing_query = bouncing_file.read()
            bouncing_query = bouncing_query.format(start_date,end_date,BOUNCING_GRP_TEAM)'''
            
    sub_query = "select `sequence` from _teldetai_ where ACTION_DESC = 'HD_REOPEN' and LASTMODIFIED between '{0}' and '{1}';".format(start_date,end_date)
    
    db_conn = tickets_common._get_connection_itsm(request)
    cursor = db_conn.cursor()
    # check DB connection, if none return
    if db_conn is None:
        return data_result
        
    cursor.execute(sub_query)
    subquery_sequence_temp = cursor.fetchall()    
    ticket_seq_list = [ticket_seq[0] for ticket_seq in subquery_sequence_temp if subquery_sequence_temp]
    ticket_sequence = ",".join("'{0}'".format(w) for w in ticket_seq_list) if ticket_seq_list else "''" 
    
    bouncing_query = "select num_ticket,`sequence`,LASTMODIFIED,LASTUSER,`FROM_GROUP:`,`TO_GROUP:`,DESCRIPTION,TO_STAFF,ACTION_DESC,GROUP_DESC from _teldetai_ where LASTMODIFIED between '{0}' and '{1}' and ACTION_DESC='HD_FRWD_GROUP'  and `TO_GROUP:` IN ({2}) and `FROM_GROUP:` <> `TO_GROUP:` and `sequence` not in ({3});".format(start_date,end_date,BOUNCING_GRP_TEAM,ticket_sequence)
    
    print("bouncing_query : ",bouncing_query )
    
    if ticket_sequence != "''":
    
        cursor.execute(bouncing_query)
        bouncing_data = cursor.fetchall()
    else:
        bouncing_data = ()
        
    f._close_connection(db_conn)
    
    result = []
    next_count = 1
    #Form a dict with key as ticket number to get the count(transferred)
    for t in bouncing_data:
        default_keys = ["ticket_number","sequence","last_modified","from_group","to_group","action"]
        
        #zip the keys with values
        tickets_data = dict(zip(default_keys, t))
        
        if t == bouncing_data[-1]:
            ticket_num_dict[tickets_data["sequence"]] = 2
            if (bouncing_data[-2][5] == 'HD_REOPEN' and tickets_data["action"] =='HD_FRWD_GROUP') or bouncing_data[-2][5] == 'HD_FRWD_GROUP' and tickets_data["action"] =='HD_REOPEN':
                result.append( bouncing_data[-2])
            else:
                if bouncing_data[-2][5] == 'HD_FRWD_GROUP' and tickets_data["action"] =='HD_FRWD_GROUP' and bouncing_data[-2][1] not in ticket_num_dict:
                    ticket_num_dict[bouncing_data[-2][1]] = 1
                if bouncing_data[-2][5] == 'HD_FRWD_GROUP' and tickets_data["action"] =='HD_FRWD_GROUP' and bouncing_data[-2][1] in ticket_num_dict:
                    ticket_num_dict[bouncing_data[-2][1]] += 1 
        else:
            ticket_num_dict[tickets_data["sequence"]] = 1
            if tickets_data["sequence"] == bouncing_data[next_count][1]:
                if (bouncing_data[next_count][5] == 'HD_REOPEN' and tickets_data["action"] =='HD_FRWD_GROUP') or bouncing_data[next_count][5] == 'HD_FRWD_GROUP' and tickets_data["action"] =='HD_REOPEN':
                    result.append( bouncing_data[next_count])
                else:
                    if bouncing_data[next_count][5] == 'HD_FRWD_GROUP' and tickets_data["action"] =='HD_FRWD_GROUP' and bouncing_data[next_count][1] in ticket_num_dict:
                        ticket_num_dict[bouncing_data[next_count][1]] += 1
                    if bouncing_data[next_count][5] == 'HD_FRWD_GROUP' and tickets_data["action"] =='HD_FRWD_GROUP' and bouncing_data[next_count][1] not in ticket_num_dict:
                        ticket_num_dict[bouncing_data[next_count][1]] = 1 
                next_count+=1
    
    #form a dict with tickets bounced more than or equal to 2 --  {'I261430': 2, 'I258980': 3}       
    ticket_num_count = {}
    for key,value in ticket_num_dict.items():
        if value > 1:
            ticket_num_count[key] = value
 
    chart_data = {} 
    
    #Prepare the donut chart data
    for value in ticket_num_count.values():
        if value == 2 and "Total_Tickets_Bounced_Two_Times" not in chart_data.keys():
            chart_data["Total_Tickets_Bounced_Two_Times"] = 1
        elif value == 2:
            chart_data["Total_Tickets_Bounced_Two_Times"] += 1
            
        if value == 3 and "Total_Tickets_Bounced_Three_Times" not in chart_data.keys():
            chart_data["Total_Tickets_Bounced_Three_Times"] = 1
        elif value == 3:
            chart_data["Total_Tickets_Bounced_Three_Times"] += 1
            
        if value == 4 and "Total_Tickets_Bounced_Four_Times" not in chart_data.keys():
            chart_data["Total_Tickets_Bounced_Four_Times"] = 1        
        elif value == 4:
            chart_data["Total_Tickets_Bounced_Four_Times"] += 1
    
        if value >=5 and "Total_Tickets_Bounced_>=5_Times" not in chart_data.keys():
            chart_data["Total_Tickets_Bounced_>=5_Times"] = 1        
        elif value >= 5:
            chart_data["Total_Tickets_Bounced_>=5_Times"] += 1
            
    ticket_sequence = ",".join("'{0}'".format(w) for w in ticket_num_count.keys()) if ticket_num_count.keys() else "''"
    
    teams_data = ticket_analysis_query(request, ticket_sequence,local_tz) if ticket_sequence != "''" else []
    
    #Remove duplicates from the display results - TELMASTE
    for tdr in teams_data:
        if tdr["ticket_number"] not in ticket_num_keys.keys():
            ticket_num_keys[tdr["ticket_number"]] = 1
            display_results.append(tdr)

    logger.info("ticket_num_count:%s"%ticket_num_count)
    logger.info("chart data:%s"%chart_data)
    data_result["teams_data"] = display_results
    data_result["ticket_breakdown"] = chart_data
    logger.info("Bouncing ticket_breakdown result:%s"%len(data_result["teams_data"]))
    #import pdb; pdb.set_trace()
    return data_result

def bouncing_old(request, bouncing_grp=None,start_date=None,end_date=None,local_tz=None):
    ticket_num_dict = {}
    data_result = {}
    data_result['ticket_breakdown'] = {}
    ticket_data_without_duplicate_temp = []
    teams_data = []
    ticket_num_keys = {}
    display_results = []
    ALL_TEAMS = settings.ALL_TEAMS

    #conn_str = "select [SEQUENCE],LASTMODIFIED,[FROM_GROUP:],ACTION_DESC from _TELDETAI_HYGIE_4MONTH where LASTMODIFIED between '{0}' and '{1}'".format(start_date,end_date)

    #db_view_query = qm.get_query_by_title('_TELDETAI_HYGIE_4MONTH')
    view = '_teldetai_' #'({0})'.format(db_view_query)
    
    if bouncing_grp:
        #get the corresponding 'team' from 'id'
        bouncing_team = [grps['team'] for grps in ALL_TEAMS for grp_id in bouncing_grp if grps['id'] == grp_id]
        BOUNCING_GRP_TEAM = ",".join("'{0}'".format(w) for w in functools.reduce(lambda x,y: x+y,bouncing_team)) #lamba used to flatten the list [[],[]]
        #conn_str += " and ((ACTION_DESC = 'HD_FRWD_GROUP' AND [TO_GROUP:] IN ({0}) AND ([FROM_GROUP:] NOT IN ({0}) AND [FROM_GROUP:] IS NOT NULL)) OR ACTION_DESC = 'HD_REOPEN') order by [sequence], lastmodified desc ".format(BOUNCING_GRP_TEAM)
        
        query_path = path.dirname( path.abspath(__file__) )
        
        bouncing_file = os.path.join(query_path,"bouncing_query.sql")
        with open(bouncing_file,'r') as bouncing_file:
            bouncing_query = bouncing_file.read()
            bouncing_query = bouncing_query.format(start_date,end_date,BOUNCING_GRP_TEAM, view)
            
    logger.info('conn_str***************:%s'%bouncing_query)
    
    db_conn = tickets_common._get_connection_itsm(request)
    cursor = db_conn.cursor()
    # check DB connection, if none return
    if db_conn is None:
        return data_result
           
    cursor.execute(bouncing_query)
    bouncing_data = cursor.fetchall()
        
    f._close_connection(db_conn)
    
    result = []
    next_count = 1
    #Form a dict with key as ticket number to get the count(transferred)
    for t in bouncing_data:
        default_keys = ["ticket_number","sequence","last_modified","from_group","to_group","action"]
        
        #zip the keys with values
        tickets_data = dict(zip(default_keys, t))
        
        if t == bouncing_data[-1]:
            if (bouncing_data[-2][5] == 'HD_REOPEN' and tickets_data["action"] =='HD_FRWD_GROUP') or bouncing_data[-2][5] == 'HD_FRWD_GROUP' and tickets_data["action"] =='HD_REOPEN':
                result.append( bouncing_data[-2])
            else:
                if bouncing_data[-2][5] == 'HD_FRWD_GROUP' and tickets_data["action"] =='HD_FRWD_GROUP' and bouncing_data[-2][1] not in ticket_num_dict:
                    ticket_num_dict[bouncing_data[-2][1]] = 1
                if bouncing_data[-2][5] == 'HD_FRWD_GROUP' and tickets_data["action"] =='HD_FRWD_GROUP' and bouncing_data[-2][1] in ticket_num_dict:
                    ticket_num_dict[bouncing_data[-2][1]] += 1 
        else:
            if tickets_data["sequence"] == bouncing_data[next_count][1]:
                if (bouncing_data[next_count][5] == 'HD_REOPEN' and tickets_data["action"] =='HD_FRWD_GROUP') or bouncing_data[next_count][5] == 'HD_FRWD_GROUP' and tickets_data["action"] =='HD_REOPEN':
                    result.append( bouncing_data[next_count])
                else:
                    if bouncing_data[next_count][5] == 'HD_FRWD_GROUP' and tickets_data["action"] =='HD_FRWD_GROUP' and bouncing_data[next_count][1] in ticket_num_dict:
                        ticket_num_dict[bouncing_data[next_count][1]] += 1
                    if bouncing_data[next_count][5] == 'HD_FRWD_GROUP' and tickets_data["action"] =='HD_FRWD_GROUP' and bouncing_data[next_count][1] not in ticket_num_dict:
                        ticket_num_dict[bouncing_data[next_count][1]] = 1 
                next_count+=1
    
    #form a dict with tickets bounced more than or equal to 2 --  {'I261430': 2, 'I258980': 3}       
    ticket_num_count = {}
    for key,value in ticket_num_dict.items():
        if value > 1:
            ticket_num_count[key] = value
 
    chart_data = {} 
    
    #Prepare the donut chart data
    for value in ticket_num_count.values():
        if value == 2 and "Total_Tickets_Bounced_Two_Times" not in chart_data.keys():
            chart_data["Total_Tickets_Bounced_Two_Times"] = 1
        elif value == 2:
            chart_data["Total_Tickets_Bounced_Two_Times"] += 1
            
        if value == 3 and "Total_Tickets_Bounced_Three_Times" not in chart_data.keys():
            chart_data["Total_Tickets_Bounced_Three_Times"] = 1
        elif value == 3:
            chart_data["Total_Tickets_Bounced_Three_Times"] += 1
            
        if value == 4 and "Total_Tickets_Bounced_Four_Times" not in chart_data.keys():
            chart_data["Total_Tickets_Bounced_Four_Times"] = 1        
        elif value == 4:
            chart_data["Total_Tickets_Bounced_Four_Times"] += 1
    
        if value >=5 and "Total_Tickets_Bounced_>=5_Times" not in chart_data.keys():
            chart_data["Total_Tickets_Bounced_>=5_Times"] = 1        
        elif value >= 5:
            chart_data["Total_Tickets_Bounced_>=5_Times"] += 1
            
    ticket_sequence = ",".join("'{0}'".format(w) for w in ticket_num_count.keys()) if ticket_num_count.keys() else "''"
    
    teams_data = ticket_analysis_query(request, ticket_sequence,local_tz) if ticket_sequence != "''" else []
    
    #Remove duplicates from the display results - TELMASTE
    for tdr in teams_data:
        if tdr["ticket_number"] not in ticket_num_keys.keys():
            ticket_num_keys[tdr["ticket_number"]] = 1
            display_results.append(tdr)

    logger.info("ticket_num_count:%s"%ticket_num_count)
    logger.info("chart data:%s"%chart_data)
    data_result["teams_data"] = display_results
    data_result["chart_breakdown"] = chart_data
    logger.info("Bouncing ticket_breakdown result:%s"%len(data_result["teams_data"]))
    return data_result

def callback(request, callback_grp = None, assigned_grp = None, start_date = None, end_date = None, local_tz = None):
    '''data_result = {}
    data_result['teams_data'] = {}
    ticket_num_dict ={}
    ticket_data_without_duplicate_temp = []
    callback_once = []
    callback_thrice = []
    callback_five = []
    teams_ticket_num = []
    ticket_num_keys = {}
    display_results = []
    result_sequence = []
    next_count = 1
    closed_count = 0
    db_conn = tickets_common._get_connection_itsm(request)
    cursor = db_conn.cursor()
    
    # check DB connection, if none return
    if db_conn is None:
        return data_result
    
    logger.info('callback local_tz***************:%s'%local_tz)
    from_zone = pytz.timezone(local_tz)
    to_zone = pytz.timezone(settings.TIMEZONE)
    start_date = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(from_zone.localize(start_date).astimezone(to_zone))
    end_date = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(from_zone.localize(end_date).astimezone(to_zone))
    
    ALL_TEAMS = settings.ALL_TEAMS
    
    teldetai_view_query = qm.get_query_by_title('_TELDETAI_HYGIE_4MONTH')
    teldetai_view = '({0})'.format(teldetai_view_query)
    
    reminder_view_query = """
        SELECT
        [dbo].[im_reminder].im_incident_guid as sequence,
        CONVERT(varchar, DATEADD (hour , 1 , [dbo].[im_reminder].im_send_date), 120) AS lastmodified,
        [dbo].[im_reminder].im_subject as subject,
        A.tps_title as [FROM_GROUP:],
        B.tps_title as [TO_GROUP:]
        from [dbo].[im_reminder]
        join [dbo].[tps_group] as A on (A.tps_guid = [dbo].[im_reminder].usr_group1)
        join [dbo].[tps_group] as B on (B.tps_guid = [dbo].[im_reminder].im_group_guid)
    """
    reminder_view = '({0})'.format(reminder_view_query)
    
    callback_team = [grps['team'] for grps in ALL_TEAMS for grp_id in callback_grp if grps['id'] == grp_id] if callback_grp else []
    callback_team_list = [w for w in functools.reduce(lambda x,y: x+y,callback_team)] if callback_grp else []
    callback_grp_str = ",".join("'{0}'".format(w) for w in callback_team_list) if callback_grp else "''"
    
        
    assigned_team = [grps['team'] for grps in ALL_TEAMS for grp_id in assigned_grp if grps['id'] == grp_id] if assigned_grp else []
    assigned_team_list = [w for w in functools.reduce(lambda x,y: x+y,assigned_team)] if assigned_grp else []
    assigned_grp_str = ",".join("'{0}'".format(w) for w in assigned_team_list) if assigned_grp else "''"
    
    
    conn_str = "select [sequence], count([sequence]) from {2} as reminder where (subject like '%call back%' or subject like '%callback%') and lastmodified between '{0}' and '{1}' and {3} group by [sequence]"
    
    filters_list = []
    if callback_grp:    
        filters_list.append("[FROM_GROUP:] in ({0})".format(callback_grp_str))
        
    if assigned_grp:    
        filters_list.append("[TO_GROUP:] in ({0})".format(assigned_grp_str))
        #get number of tickets closed by the assigned groups
        get_closed_count_query = "select count([sequence]) from {3} as teldetai where action_desc = 'HD_CLOSE' and lastmodified between '{0}' and '{1}' and [FROM_GROUP:] in ({2})".format(start_date, end_date, assigned_grp_str, teldetai_view)
        cursor.execute(get_closed_count_query)
        closed_count = cursor.fetchone()[0]    
        
    filters_str = " and ".join(filters_list)
    conn_str = conn_str.format(start_date, end_date, reminder_view, filters_str)
    
    logger.info('callback conn_str***************:%s'%conn_str)
    cursor.execute(conn_str)
    callback_data_db = cursor.fetchall()

    for t in callback_data_db:
        default_keys = ["sequence", "callback_count"]
        
        #zip the keys with values
        tickets_data = dict(zip(default_keys, t))
        ticket_num_dict[tickets_data["sequence"]] = tickets_data["callback_count"]
        result_sequence.append(tickets_data["sequence"])
        
      
    
    f._close_connection(db_conn)       
    
    for key,value in ticket_num_dict.items():
        if 1 <= value < 3:
            callback_once.append(key)
        if 3 <= value < 5:
            callback_thrice.append(key)
        if value >= 5:
            callback_five.append(key)
    
    result_ticket_seq = ",".join("'{0}'".format(w) for w in result_sequence) if result_sequence else "''"
        
    callback_tickets_data_telmaste = ticket_analysis_query(request, result_ticket_seq,local_tz) if result_ticket_seq != "''" else []
    
    #Remove duplicates from the display results - TELMASTE
    for tdr in callback_tickets_data_telmaste:
        if tdr["ticket_number"] not in ticket_num_keys.keys():
            ticket_num_keys[tdr["ticket_number"]] = 1
            if tdr["itsm_url"] in ticket_num_dict.keys():
                tdr["counter"] = ticket_num_dict[tdr["itsm_url"]]
                display_results.append(tdr)
    
    data_result['teams_data']['callback_once'] = []    
    data_result['teams_data']['callback_thrice'] = []  
    data_result['teams_data']['callback_five'] = []
    
    #Map the results from TELMASTE to their respective callback_once or callback_thrice or callback_five groups
    for callback_tickets in display_results:
        if callback_tickets["itsm_url"] in callback_once:
            data_result['teams_data']['callback_once'].append(callback_tickets)
        elif callback_tickets["itsm_url"] in callback_thrice:
            data_result['teams_data']['callback_thrice'].append(callback_tickets)
        elif callback_tickets["itsm_url"] in callback_five:
            data_result['teams_data']['callback_five'].append(callback_tickets)

    logger.info("callback once:%s"%len(callback_once))
    logger.info("callback thrice:%s"%len(callback_thrice))
    logger.info("callback five or more:%s"%len(callback_five))
    
    data_result['callback_ratio'] = "%.1f"%(float(len(data_result['teams_data']['callback_five']) + len(data_result['teams_data']['callback_thrice']) + len(data_result['teams_data']['callback_once'])) / float(closed_count) * 100) if closed_count else 0'''
 
    data_result = {'teams_data': {'callback_once': [{'open_date': '2019-08-06 15:44:28', 'criticality': 'MEDIUM', 'due_date': '2019-08-07  1:00:00', 'ticket_number': 'I460398', 'description': '[DESCRIPTION]: the user received the packaging list 4351552 but\xa0 can\xa0 \xa0not integrate in Rms, not present in the software\xa0\n\n', 'closed_group': '', 'raw_group_name': 'WW_AM_L2_RETAIL_WEB_APPLIS', 'taken_by': '', 'category': 'RMS', 'open_by': 'Ayoub', 'ticket_type': 'Incident', 'ticket_status': 'On Hold', 'itsm_url': 'EE6C2B64-F7E1-494E-911D-08A4DFB07583', 'itsm_parent_url': '', 'title': 'RMS integration issue', 'task_type': 'User Service Level', 'client': 'Farid AOUNE', 'location': 'EMEA_FR_LV Paris Galeries Lafayette', 'sequence': 'EE6C2B64-F7E1-494E-911D-08A4DFB07583', 'counter': 1}, {'open_date': '2019-08-02 17:21:45', 'criticality': 'MEDIUM', 'due_date': '2019-08-05 16:21:45', 'ticket_number': 'I458604', 'description': 'De\xa0: XXXX \r\n Envoyé\xa0: vendredi 2 août 2019 09:13À\xa0: ContactIS <contactis@nita.com>Objet\xa0: urgent CN\n\nDear\r\nColleagues, \n\nCould you\r\nplease help me, I created a CN for invoice 401700xxxx with credit memo number\r\n10000XXX, but I can’t unload a CN through\xa0\n\n.Is it a mistake? \n\nBest regards,\nPolina\n\n\nclient | Logistics Assistant ', 'closed_group': '', 'raw_group_name': 'WW_AM_L3_SAP_FINANCE', 'taken_by': 'Rajashekhar', 'category': 'SAP', 'open_by': 'Shilpa', 'ticket_type': 'Incident', 'ticket_status': 'On Hold', 'itsm_url': '306CAD36-0C3D-4F77-998D-7239DE4EE3F6', 'itsm_parent_url': '', 'title': "SAP - CN can't unload", 'task_type': 'User Service Level', 'client': 'Polina KLOBERDANTS', 'location': 'EMEA_RU_Louis Vuitton Russia Ho', 'sequence': '306CAD36-0C3D-4F77-998D-7239DE4EE3F6', 'counter': 1}, {'open_date': '2019-08-07 14:44:42', 'criticality': 'MEDIUM', 'due_date': '2019-08-07 22:44:42', 'ticket_number': 'I461056', 'description': 'hang up calls', 'closed_group': 'WW_TM_L1_NEST_MONITORING', 'raw_group_name': 'WW_TM_L1_NEST_MONITORING', 'taken_by': 'Sebastien', 'category': 'WAN LV', 'open_by': 'Autobot', 'ticket_type': 'Incident', 'ticket_status': 'Resolved', 'itsm_url': '726C7C69-816B-4D13-B47F-9C737D16FB0E', 'itsm_parent_url': '', 'title': '[AMER] WAN connection wrongly connected in Core switch : LV BR CAMPINAS IGUATEMI POP_UIR', 'task_type': 'User Service Level', 'client': 'LV CAMPINA IGUATEMI TEMP STORE_UIR', 'location': 'Americas_BR_LV Campinas Iguatemi Temp', 'sequence': '726C7C69-816B-4D13-B47F-9C737D16FB0E', 'counter': 1}, {'open_date': '2019-07-30  0:25:34', 'criticality': 'LOW', 'due_date': '2019-08-01 21:25:34', 'ticket_number': 'I455631', 'description': 'User placed a call because store register was not working properly and was freezing.The store was stopped and reestartedThe markers was removed and the speedtest was done as well.IP: 10.32.xxx.xxComputer name: NITA Code: XXX/3805', 'closed_group': '', 'raw_group_name': 'WW_AM_L3_XSTORE', 'taken_by': '', 'category': 'NITA', 'open_by': 'Anu', 'ticket_type': 'Incident', 'ticket_status': 'In Progress', 'itsm_url': '9E3CC452-D0C3-4F6A-8ADE-6378D9963E19', 'itsm_parent_url': '', 'title': 'store register issue', 'task_type': 'User Service Level', 'client': 'LV MONTERREY PALACIO DE HIERRO STORE_UMY', 'location': 'Americas_MX_LV Monterrey Palacio De Hierro', 'sequence': '9E3CC452-D0C3-4F6A-8ADE-6378D9963E19', 'counter': 2}, {'open_date': '2019-07-30 17:21:50', 'criticality': 'MEDIUM', 'due_date': '2019-07-31  1:21:50', 'ticket_number': 'I456110', 'description': '[DESCRIPTION]:\xa0The user has questions about BW, she does not see the correct access granted to her\n[HOSTNAME / IP] : NITAXXXX\n[REMOTE CONTROL] : OK\n[ACTIONS PERFORMED] : \nPRD-PW (IBM) - entity 53\n\nShe cannot find the access to the entity 53, she should be able to find it in the list above\n\n', 'closed_group': '', 'raw_group_name': 'CP_IT_L3_WINDOWS_STATION', 'taken_by': '', 'category': 'SAP', 'open_by': 'Anu', 'ticket_type': 'Incident', 'ticket_status': 'In Progress', 'itsm_url': 'C5D97603-582B-4FBA-ADC4-752C7EB82310', 'itsm_parent_url': '', 'title': 'SAP - Incorrect access', 'task_type': 'User Service Level', 'client': 'Adelie CONAN NGUYEN', 'location': 'Corporate_FR_LVM - Bassano', 'sequence': 'C5D97603-582B-4FBA-ADC4-752C7EB82310', 'counter': 1}, {'open_date': '2019-08-06 19:03:22', 'criticality': 'MEDIUM', 'due_date': '2019-08-07 18:03:22', 'ticket_number': 'I460514', 'description': 'Dear Team,\n\nUser reports that suppliers list on MYPO is empty.\nLinked to global incident\xa0currently in progress, and handled by the SAP team.\n\nINC 459962\nThanks in advance,\nBest regards.\n', 'closed_group': 'WW_AM_L2_SAP_EMEA', 'raw_group_name': 'WW_AM_L3_SAP_AUTHOR', 'taken_by': 'Zaheer', 'category': 'MYPO', 'open_by': 'Guillaume', 'ticket_type': 'Incident', 'ticket_status': 'Resolved', 'itsm_url': 'B363F12F-BD51-4DEC-91B6-9F62DCDDF482', 'itsm_parent_url': '', 'title': 'MYPO - Suppliers list empty', 'task_type': 'User Service Level', 'client': 'Nathalie MOORTY', 'location': 'Corporate_FR_LVM Siege Central Pont Neuf', 'sequence': 'B363F12F-BD51-4DEC-91B6-9F62DCDDF482', 'counter': 1}, {'open_date': '2019-07-31  1:42:48', 'criticality': 'LOW', 'due_date': '2019-08-02  1:42:48', 'ticket_number': 'I456480', 'description': "Preferred method of contact (Skype, phone, email): PhonePhone#:\xa0Your schedule:\xa0Location: (floor, station, store):Computer IP: 10.35.xxx.xxxComputer Name:\xa0Resolution:XXXX called today because they are getting an error message in a chart about not able to open a chart because it wasn't the same version of excel.-Attempted to remote in-Saved the document locally-Verified both users were using the same excel version-can you please advise\xa0", 'closed_group': 'AM_SD_L1_SERVICE_DESK', 'raw_group_name': 'AM_SD_L1_SERVICE_DESK', 'taken_by': 'Hugo', 'category': 'EXCEL', 'open_by': 'Shilpa', 'ticket_type': 'Incident', 'ticket_status': 'Resolved', 'itsm_url': 'F1A0FF59-1AC0-42BA-BD62-E10AC2386D69', 'itsm_parent_url': '', 'title': 'Error Message When Pulling Up Chart in Excel', 'task_type': 'User Service Level', 'client': 'Anonymous', 'location': 'Americas_US_LV USA Office North', 'sequence': 'F1A0FF59-1AC0-42BA-BD62-E10AC2386D69', 'counter': 1}, {'open_date': '2019-07-19 20:43:46', 'criticality': 'LOW', 'due_date': '2019-07-22 17:43:46', 'ticket_number': 'I447974', 'description': 'Hi all \n\nWe identified an issue on “Total tax” in Daily Sales and\r\nCash reports for 2 stores. Please advise.\nSee example below:\n\nX-Admin Daily Sales\r\n     & Cash ReportX-Admin End of Month\r\n     Sale\n', 'closed_group': '', 'raw_group_name': 'WW_AM_L3_XSTORE', 'taken_by': '', 'category': 'OFFICE', 'open_by': 'adrien', 'ticket_type': 'Incident', 'ticket_status': 'In Progress', 'itsm_url': '528EFD21-1C47-49B6-BAC0-C43A748A10FB', 'itsm_parent_url': '', 'title': 'FW: Daily Sales & Cash Report Discrepancies', 'task_type': 'User Service Level', 'client': 'Fernando GARCIA', 'location': 'Americas_MX_Mexico Office', 'sequence': '528EFD21-1C47-49B6-BAC0-C43A748A10FB', 'counter': 1}, {'open_date': '2019-08-05 22:37:47', 'criticality': 'MEDIUM', 'due_date': '2019-08-06 18:37:47', 'ticket_number': 'I459991', 'description': '\nDe\xa0: xxxx\r\nxxxxx Envoyé\xa0: lundi 5 août 2019 16:19À\xa0: ContactIS <contactis@nita.com>Objet\xa0: Mise à jour compteuse billets\n\n\r\nHello,\nWe cannot update our 2 Safescan counters for new 100 euros bills. \nHerre is error message : \nThanks for your feedback and action. \n\nBest regardS. \n\nAnonymous\nManager\r\nXXXX\n\nNITA\n22, avenue Montaigne\nPhone : +33 1 53 xx xx xx\nMobile : +33 7 62 xx xx xx\n\n', 'closed_group': '', 'raw_group_name': 'EU_OS_L2_FRANCE', 'taken_by': '', 'category': 'OFF THE SHELF', 'open_by': 'Anu', 'ticket_type': 'Incident', 'ticket_status': 'In Progress', 'itsm_url': '3AEEC3CF-2690-4075-BF9B-5155C2918A56', 'itsm_parent_url': '', 'title': 'OFF THE SHELF - users cannot update bills counters > blocked by GPO', 'task_type': 'User Service Level', 'client': 'Emmanuel MESRINE', 'location': 'EMEA_FR_LV Paris Montaigne', 'sequence': '3AEEC3CF-2690-4075-BF9B-5155C2918A56', 'counter': 1}], 'callback_thrice': [{'open_date': '2019-08-06 14:11:14', 'criticality': 'HIGH', 'due_date': '2019-08-06 19:00:00', 'ticket_number': 'I460297', 'description': 'xxxx encounter an error message when using Cisco app. Try to take RC on her computer, refused on both Landesk and web.\n\nCould you please intervene ?\n\n\n-------------------\n07/08/2019 08:20\nCall back from user\nShe explains that when she connects on her CISCO application, it doesn\'t show the contacts directory correctly\nPC already rebooted\n\nResolved by changing repertory filtering, by putting "Last name" (Nom)\xa0before "First name" XXXX\n\n\n XXXX is OK with this view and will call us back today if the organization of the colums changes back again\nIf no call back, ticket can be closed\n\n', 'closed_group': 'CP_SD_L1_SERVICE_DESK', 'raw_group_name': 'CP_SD_L1_SERVICE_DESK', 'taken_by': '', 'category': 'LAPTOP', 'open_by': 'Sebastien', 'ticket_type': 'Incident', 'ticket_status': 'Resolved', 'itsm_url': 'BE7C10EC-6A41-459F-9914-0DA538654B5D', 'itsm_parent_url': '', 'title': 'Laptop - no remote control possible', 'task_type': 'User Service Level', 'client': 'XXXX', 'location': 'Corporate_FR_LVM Siege Central Pont Neuf', 'sequence': 'BE7C10EC-6A41-459F-9914-0DA538654B5D', 'counter': 3}, {'open_date': '2019-07-17 20:50:50', 'criticality': 'LOW', 'due_date': '2019-07-19 20:50:50', 'ticket_number': 'I446350', 'description': 'XXXX Preferred method of contact ( Skype, cell, email): emailPhone # 12234444 When are you available (Schedule):\xa0 \xa0off on friday and sund\xa0Verify Location (store, floor, station..)\xa0 MONTREAL STORE_UMJNo Markers store does not launch. when launching the screen below appears.Rebooted PCissue continues', 'closed_group': '', 'raw_group_name': 'AM_OS_L2_CANADA', 'taken_by': 'Shilpa', 'category': 'NITA', 'open_by': 'Autobot', 'ticket_type': 'Incident', 'ticket_status': 'In Progress', 'itsm_url': '85BACFD2-86D0-4D45-937B-D58BDD5B7C16', 'itsm_parent_url': '', 'title': 'store does not launch', 'task_type': 'User Service Level', 'client': 'LV HOLT RENFREW MONTREAL STORE_UMJ', 'location': 'Americas_CA_LV Montreal HR Ogilvy Temp', 'sequence': '85BACFD2-86D0-4D45-937B-D58BDD5B7C16', 'counter': 3}, {'open_date': '2019-08-01 15:54:25', 'criticality': 'MEDIUM', 'due_date': '2019-08-01 23:54:25', 'ticket_number': 'I457743', 'description': '[DESCRIPTION]: The user reports that her 2nd monitor does not seem to work and she lost access to her network\n[HOSTNAME / IP] :\xa0NITA53C0\n[REMOTE CONTROL] : OK\n[ACTIONS PERFORMED] : \nShe does not have a docking station, only a cable she plugs\nUnpluging and repluging the cable - NOK\nReboot - network OK\n2nd monitor still KO - in the settings: Detection of other monitor: Nothing available\nMonitor is pluged, power supply OK\n"No signal from USB" appears on the monitor\n\nCan you please check on this?Kind regards\n', 'closed_group': 'CP_OS_L2_HQ_P9_RIVOLI', 'raw_group_name': 'CP_OS_L2_HQ_P9_RIVOLI', 'taken_by': 'Philippe', 'category': 'MONITOR', 'open_by': 'Victoria', 'ticket_type': 'Incident', 'ticket_status': 'Resolved', 'itsm_url': '4E33D68C-8033-400B-BC76-8D97A408CEE6', 'itsm_parent_url': '', 'title': 'Monitor - Unable to connect', 'task_type': 'User Service Level', 'client': 'Victoria RIQUELME MONTIEL', 'location': 'Corporate_FR_LVM Siege Central Pont Neuf', 'sequence': '4E33D68C-8033-400B-BC76-8D97A408CEE6', 'counter': 3}], 'callback_five': [{'open_date': '2019-08-07 13:03:21', 'criticality': 'MEDIUM', 'due_date': '2019-08-07 23:00:00', 'ticket_number': 'I460982', 'description': 'hang up', 'closed_group': '', 'raw_group_name': 'CP_SD_L1_SERVICE_DESK', 'taken_by': 'David', 'category': 'HANG UP CALLS', 'open_by': 'Matthieu', 'ticket_type': 'Incident', 'ticket_status': 'In Progress', 'itsm_url': 'A4EBCADB-4344-4888-AED8-2207FC059CC5', 'itsm_parent_url': '', 'title': 'hang up management 07_08_2019', 'task_type': 'User Service Level', 'client': 'Matthieu CHALANDRE', 'location': 'Corporate_FR_LVM Siege Bailleul', 'sequence': 'A4EBCADB-4344-4888-AED8-2207FC059CC5', 'counter': 14}, {'open_date': '2019-08-01 18:14:19', 'criticality': 'LOW', 'due_date': '2019-08-06  0:14:19', 'ticket_number': 'I457837', 'description': "This ticket's purpose as to record all reminders and follow up on request regarding EMEA end user solicitation to Service Desk for the present month (August 2019).", 'closed_group': '', 'raw_group_name': 'WW_TM_L2_REQUEST_FOLLOW_UP', 'taken_by': 'Philippe', 'category': 'OUT OF PRODUCTION', 'open_by': 'Sebastien', 'ticket_type': 'Incident', 'ticket_status': 'In Progress', 'itsm_url': 'FCC9D0C7-08FB-4B2F-96A7-C2490D7368C6', 'itsm_parent_url': '', 'title': 'REQUEST REMINDER : Call counter', 'task_type': 'User Service Level', 'client': 'Damien LEHAY', 'location': 'Corporate_FR_LVM Siege Bailleul', 'sequence': 'FCC9D0C7-08FB-4B2F-96A7-C2490D7368C6', 'counter': 36}, {'open_date': '2019-08-06 13:06:21', 'criticality': 'LOW', 'due_date': '2019-08-08  3:00:00', 'ticket_number': 'I460265', 'description': 'Hang-up management ticket - 06/08/2019', 'closed_group': 'CP_SD_L1_SERVICE_DESK', 'raw_group_name': 'CP_SD_L1_SERVICE_DESK', 'taken_by': 'Matthieu', 'category': 'HANG UP CALLS', 'open_by': 'Mickael', 'ticket_type': 'Incident', 'ticket_status': 'Resolved', 'itsm_url': 'CBB51EEE-D49E-461E-8EDF-A326BA9AB4A2', 'itsm_parent_url': '', 'title': 'Hang-up management ticket - 06/08/2019', 'task_type': 'User Service Level', 'client': 'Mickael GUEGUEN', 'location': 'Corporate_FR_LVM Siege Central Pont Neuf', 'sequence': 'CBB51EEE-D49E-461E-8EDF-A326BA9AB4A2', 'counter': 9}, {'open_date': '2019-08-05 12:59:46', 'criticality': 'LOW', 'due_date': '2019-08-07  3:00:00', 'ticket_number': 'I459540', 'description': 'Hang-up management ticket - 05/08/2019', 'closed_group': 'CP_SD_L1_SERVICE_DESK', 'raw_group_name': 'CP_SD_L1_SERVICE_DESK', 'taken_by': 'Mickael', 'category': 'HANG UP CALLS', 'open_by': 'Sebastien', 'ticket_type': 'Incident', 'ticket_status': 'Resolved', 'itsm_url': '6DE55441-A3CF-4677-BD2E-0FC84C61D6B7', 'itsm_parent_url': '', 'title': 'Hang-up management ticket - 05/08/2019', 'task_type': 'User Service Level', 'client': 'Mickael GUEGUEN', 'location': 'Corporate_FR_LVM Siege Central Pont Neuf', 'sequence': '6DE55441-A3CF-4677-BD2E-0FC84C61D6B7', 'counter': 8}, {'open_date': '2019-08-04 14:05:33', 'criticality': 'LOW', 'due_date': '2019-08-06 17:00:00', 'ticket_number': 'I459240', 'description': 'Hang-up management ticket - 04/08', 'closed_group': 'CP_SD_L1_SERVICE_DESK', 'raw_group_name': 'CP_SD_L1_SERVICE_DESK', 'taken_by': 'Mickael', 'category': 'HANG UP CALLS', 'open_by': 'Billy', 'ticket_type': 'Incident', 'ticket_status': 'Resolved', 'itsm_url': '64E1F08F-0FBB-43BA-AAD3-765EB0CC0C75', 'itsm_parent_url': '', 'title': 'HANGUP - Hang-up management ticket - 04/08', 'task_type': 'User Service Level', 'client': 'Mickael GUEGUEN', 'location': 'Corporate_FR_LVM Siege Central Pont Neuf', 'sequence': '64E1F08F-0FBB-43BA-AAD3-765EB0CC0C75', 'counter': 5}, {'open_date': '2019-08-02 12:59:55', 'criticality': 'MEDIUM', 'due_date': '2019-08-02 23:00:00', 'ticket_number': 'I458374', 'description': 'hang up', 'closed_group': 'CP_SD_L1_SERVICE_DESK', 'raw_group_name': 'CP_SD_L1_SERVICE_DESK', 'taken_by': 'Anu', 'category': 'HANG UP CALLS', 'open_by': 'Matthieu', 'ticket_type': 'Incident', 'ticket_status': 'Resolved', 'itsm_url': '68C9C9BF-592E-46C0-A979-4287CFB896DE', 'itsm_parent_url': '', 'title': 'hang up management 02_08_2019', 'task_type': 'User Service Level', 'client': 'Matthieu CHALANDRE', 'location': 'Corporate_FR_LVM Siege Bailleul', 'sequence': '68C9C9BF-592E-46C0-A979-4287CFB896DE', 'counter': 9}, {'open_date': '2019-08-01 13:15:36', 'criticality': 'MEDIUM', 'due_date': '2019-08-01 23:00:00', 'ticket_number': 'I457576', 'description': 'hang up', 'closed_group': 'CP_SD_L1_SERVICE_DESK', 'raw_group_name': 'CP_SD_L1_SERVICE_DESK', 'taken_by': 'Matthieu', 'category': 'HANG UP CALLS', 'open_by': 'Matthieu', 'ticket_type': 'Incident', 'ticket_status': 'Closed', 'itsm_url': 'C8FBCE96-E9EA-4B5F-97DF-D6560000F679', 'itsm_parent_url': '', 'title': 'hang up management 01_08_2019', 'task_type': 'User Service Level', 'client': 'Matthieu CHALANDRE', 'location': 'Corporate_FR_LVM Siege Bailleul', 'sequence': 'C8FBCE96-E9EA-4B5F-97DF-D6560000F679', 'counter': 9}]}, 'callback_ratio': 0, 'total_tickets': 19}
 
    return data_result

def ticket_analysis_query(request, ticket_sequence,local_tz):
    teams_data = []
    sla_grp='ITL4'
    
    #db_view_query = qm.get_query_by_title('_TELMASTE_HYGIE_')
    view = '_telmaste_hygie_' #'({0})'.format(db_view_query)
    tickets_data_conn_str = "select DISTINCT `DATE OPEN` , Urgency_desc, null as due_date, NUM_TICKET , DESCRIPTION, `CLOSED_GROUP:`, group_desc, assign_desc , Subject_desc, Client_desc, TTYPE as TicketType, lastmodified as LastModified, lastmodified as LastDate, status_desc as third_party, `SEQUENCE`, `SEQUENCE_Parent`, title, type as type, Client_desc as Client, User_Location as Location from {1} as details where `SEQUENCE` IN ({0}) order by lastmodified desc".format(ticket_sequence, view)
    
    logger.info('tickets_data_conn_str***************:%s'%tickets_data_conn_str)
    db_conn = tickets_common._get_connection_itsm(request)
    cursor = db_conn.cursor()
    
    # check DB connection, if none return
    if db_conn is None:
        return data_result
    
    if ticket_sequence is '':
        return teams_data
    
    cursor.execute(tickets_data_conn_str)
    tickets_data_db = cursor.fetchall()
    
    f._close_connection(db_conn)
    
    TEAMS = settings.TEAMS
    now_date = datetime.datetime.now(pytz.timezone(settings.TIMEZONE))
    TZ = pytz.timezone(settings.TIMEZONE)
    to_zone = tz.gettz(local_tz)
    from_zone_ivanti_sr = tz.gettz(settings.TIMEZONE_IVANTI_DB_SR)
    from_zone_ivanti_inc = tz.gettz(settings.TIMEZONE_IVANTI_DB_INC)
    
    for t in tickets_data_db:
        # convert dbrows to list and replace None in data to ''
        t = ['' if d is None else d for d in t]
        default_keys = ["open_date","criticality","due_date","ticket_number","description","closed_group","raw_group_name","taken_by","category","open_by","ticket_type","is_sleeping_ticket",
                        "duration_at_group","ticket_status","itsm_url","itsm_parent_url","title","task_type","client","location"]    
        tickets_data = dict(zip(default_keys, t)) #form dict with default_keys and DB data(t)
        del tickets_data['is_sleeping_ticket'] #removing since throwing error on frontend for datetime format
        del tickets_data['duration_at_group'] #removing since throwing error on frontend for datetime format
        
        tickets_data['sequence'] = tickets_data['itsm_url']
        #change the sla group
        if tickets_data["raw_group_name"] in settings.SLA_GRP_AMS_CGI:
            sla_grp = 'AMS_CGI'
        elif tickets_data["raw_group_name"] in settings.SLA_GRP_AMS_VALTECH:
            sla_grp = 'AMS_VALTECH'
        else:
            for key, value in TEAMS.items():
                for v in value:
                    if tickets_data["raw_group_name"] in v['team']:
                        if key in settings.SLA_GRP_PROXIMITY:
                            sla_grp = 'EMEA'
                        else:
                            sla_grp = key
        
        # temporary "due_date" assignment for specific format
        due_date_temp = tickets_data["due_date"]
        open_date_temp = tickets_data["open_date"]
        
        if tickets_data["criticality"] in ['PLANNED','P_PLANNED']:
            is_planned = True
            tickets_data["due_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(due_date_temp)
            tickets_data["open_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(open_date_temp)
            if "I" in tickets_data["ticket_number"]:
                tickets_data["due_date"] = tickets_common._convert_to_local_time(from_zone_ivanti_inc, to_zone, tickets_data["due_date"]) 
                _tickets_data["open_date"] = tickets_common._convert_to_local_time(from_zone_ivanti_inc, to_zone, tickets_data["open_date"])
            if "S" in tickets_data["ticket_number"]:
                tickets_data["due_date"] = tickets_common._convert_to_local_time(from_zone_ivanti_sr, to_zone, tickets_data["due_date"]) 
                tickets_data["open_date"] = tickets_common._convert_to_local_time(from_zone_ivanti_sr, to_zone, tickets_data["open_date"])
        else:
            tickets_data["open_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(open_date_temp)
            comp_due_date = TZ.localize(tickets_common._comp_duedate_95(tickets_data["criticality"], open_date_temp, sla_grp, tickets_data["ticket_type"]))
            due_date_temp = tickets_common._comp_duedate_95(tickets_data["criticality"], open_date_temp, sla_grp, tickets_data["ticket_type"])
            tickets_data["due_date"] = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:2d}:{0.minute:02d}:{0.second:02d}'.format(due_date_temp)
            if "I" in tickets_data["ticket_number"]:
                tickets_data["due_date"] = tickets_common._convert_to_local_time(from_zone_ivanti_inc, to_zone, tickets_data["due_date"]) 
                tickets_data["open_date"] = tickets_common._convert_to_local_time(from_zone_ivanti_inc, to_zone, tickets_data["open_date"])
            if "S" in tickets_data["ticket_number"]:
                tickets_data["due_date"] = tickets_common._convert_to_local_time(from_zone_ivanti_sr, to_zone, tickets_data["due_date"]) 
                tickets_data["open_date"] = tickets_common._convert_to_local_time(from_zone_ivanti_sr, to_zone, tickets_data["open_date"])
        
        
        teams_data.append(tickets_data)
        
    return teams_data


    
    