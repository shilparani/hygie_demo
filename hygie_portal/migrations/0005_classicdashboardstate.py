# Generated by Django 2.1.7 on 2019-07-23 03:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hygie_portal', '0004_discrepancyserver_discrepancyworkstation'),
    ]

    operations = [
        migrations.CreateModel(
            name='classicDashboardState',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(blank=True, max_length=80, null=True)),
                ('dashboard_state', models.TextField(blank=True, null=True)),
            ],
            options={
                'db_table': 'classicdashboardstate',
                'managed': False,
            },
        ),
    ]
