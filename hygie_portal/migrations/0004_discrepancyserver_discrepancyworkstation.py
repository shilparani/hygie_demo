# Generated by Django 2.1.7 on 2019-05-15 06:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hygie_portal', '0003_applications_contentmanager_holidays_querymanager_rca_srvdiscre_trendtickets_wksdiscre'),
    ]

    operations = [
        migrations.CreateModel(
            name='DiscrepancyServer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('devicename', models.CharField(max_length=80, unique=True)),
                ('ipaddress', models.CharField(max_length=80)),
                ('adindicator', models.CharField(max_length=80)),
                ('ldindicator', models.CharField(max_length=80)),
                ('kasindicator', models.CharField(max_length=80)),
                ('deisindicator', models.CharField(blank=True, max_length=80, null=True)),
                ('adcheck', models.IntegerField(blank=True, null=True)),
                ('ldcheck', models.IntegerField(blank=True, null=True)),
                ('kascheck', models.IntegerField(blank=True, null=True)),
                ('deischeck', models.IntegerField(blank=True, null=True)),
                ('comments', models.TextField()),
                ('datehistory', models.TextField()),
                ('commenthistory', models.TextField()),
                ('lockby', models.CharField(max_length=80)),
                ('last_update', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'srvdiscre',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='DiscrepancyWorkstation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('devicename', models.CharField(max_length=80, unique=True)),
                ('ipaddress', models.CharField(max_length=80)),
                ('adindicator', models.CharField(max_length=80)),
                ('ldindicator', models.CharField(max_length=80)),
                ('kasindicator', models.CharField(max_length=80)),
                ('deisindicator', models.CharField(blank=True, max_length=80, null=True)),
                ('adcheck', models.IntegerField(blank=True, null=True)),
                ('ldcheck', models.IntegerField(blank=True, null=True)),
                ('kascheck', models.IntegerField(blank=True, null=True)),
                ('deischeck', models.IntegerField(blank=True, null=True)),
                ('comments', models.TextField()),
                ('datehistory', models.TextField()),
                ('commenthistory', models.TextField()),
                ('lockby', models.CharField(max_length=80)),
                ('last_update', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'wksdiscre',
                'managed': False,
            },
        ),
    ]
